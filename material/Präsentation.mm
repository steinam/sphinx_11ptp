<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Pr&#xe4;sentation" LOCALIZED_STYLE_REF="default" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1415215115645"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="2"/>
<node TEXT="Thema" POSITION="right" ID="ID_1982614393" CREATED="1415215253889" MODIFIED="1415215256950">
<edge COLOR="#7c007c"/>
<node TEXT="Thema festlegen/finden/eingrenzen" ID="ID_1755085999" CREATED="1415215073894" MODIFIED="1415215261114"/>
<node TEXT="Ziel der Pr&#xe4;sentation festlegen (&#xfc;berpr&#xfc;fbares Ziel)" ID="ID_1522088007" CREATED="1415215073905" MODIFIED="1415215261141"/>
<node TEXT="&#xf0b7;  Zielgruppe beschreiben und Pr&#xe4;sentation darauf ausrichten" ID="ID_1428033646" CREATED="1415215073951" MODIFIED="1415215261171"/>
<node TEXT="&#xf0b7;  Anlass und Vorgaben (Raum, Zeit, Inhalt, Ablauf) ber&#xfc;cksichtigen" ID="ID_1390813535" CREATED="1415215073943" MODIFIED="1415215261200"/>
</node>
<node TEXT="Inhalt" POSITION="right" ID="ID_1216303541" CREATED="1415215281942" MODIFIED="1415215284374">
<edge COLOR="#007c7c"/>
<node TEXT="&#xf0b7;  Brainstorming, Gliederung, Strukturierung" ID="ID_555694370" CREATED="1415215073963" MODIFIED="1415215990770"/>
<node TEXT="&#xf0b7;  Stoffsammlung / Materialsammlung, Recherche mit Quellen(-beurteilung)" ID="ID_505313982" CREATED="1415215073972" MODIFIED="1415215994719"/>
<node TEXT="&#xf0b7;  Zitieren und Bildrechte kennen" ID="ID_1137757326" CREATED="1415215073984" MODIFIED="1415215999221"/>
<node TEXT="&#xf0b7;  Argumentationsaufbau entspr. Informations- oder &#xdc;berzeugungspr&#xe4;sentation" ID="ID_680305073" CREATED="1415215073995" MODIFIED="1415216057064"/>
</node>
<node TEXT="Presentation" POSITION="right" ID="ID_1497145331" CREATED="1415216033838" MODIFIED="1415216042238">
<edge COLOR="#7c7c00"/>
<node TEXT="&#xf0b7;  Kenntnis verschiedener Pr&#xe4;sentationsmethoden" ID="ID_1060691711" CREATED="1415215074000" MODIFIED="1415216016806"/>
<node TEXT="&#xf0b7;  Auswahl einer angemessenen Pr&#xe4;sentationsmethode" ID="ID_1217075594" CREATED="1415215074007" MODIFIED="1415216053114"/>
<node TEXT="&#xf0b7;  Visualisierung der wichtigsten Aussagen mithilfe von Schaubildern, Grafiken, Fotos, Diagrammen" ID="ID_814242995" CREATED="1415215074014" MODIFIED="1415216095927"/>
</node>
<node TEXT="Dramaturgie" POSITION="left" ID="ID_799171569" CREATED="1415216255767" MODIFIED="1415216262410">
<edge COLOR="#0000ff"/>
<node TEXT="&#xf0b7;  Inszenierung des Vortrags (Rhetorik, Koordination etc.)" ID="ID_1144482373" CREATED="1415215074031" MODIFIED="1415216266171"/>
<node TEXT="&#xf0a7;  Koordinierung Vortrag &#x2013; Technik" ID="ID_1564001492" CREATED="1415215074060" MODIFIED="1415216272042"/>
</node>
<node TEXT="Techniken" POSITION="left" ID="ID_832558426" CREATED="1415216181935" MODIFIED="1415216284130">
<edge COLOR="#ff0000"/>
<node TEXT="&#xf0a7;  Beherrschen des Programms" ID="ID_1816542484" CREATED="1415215074042" MODIFIED="1415216189935"/>
<node TEXT="&#xf0a7;  Beachten der Regeln f&#xfc;r eine gute Pr&#xe4;sentation" ID="ID_1540744921" CREATED="1415215074048" MODIFIED="1415216284128"/>
<node TEXT="&#xf0a7;  Gegebenenfalls Kenntnisse Bildbearbeitung" ID="ID_1459913002" CREATED="1415215074054" MODIFIED="1415216204667"/>
</node>
</node>
</map>
