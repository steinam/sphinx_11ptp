
var spnetENV_DEFAULT_DOMAIN = 'www.manager-magazin.de';

		spnetENV_SERVER 	= window.location.protocol + '//www.manager-magazin.de';
		spnetENV_SERVER_SSL = 'https:/' + '/www.manager-magazin.de';
	

var spnetENV_CSS_VERSION='8-11';
var spnetENV_JS_VERSION='8-11';

var spnetENV_JWPLAYER_KEY = '6vGg1pMcuxNYA9KEtHWcw1kNq5bhLmvm7XUGzA==';
var spnetENV_JWPLAYER_ADMESSAGE = 'Anzeige xx s';
var spMobilePlusUrl;
var spOldMobileUrl;

var spMobilePlusHost="http:/"+"/m.manager-magazin.de";
var spOldMobileHost="http:/"+"/ml.manager-magazin.de";

var spDeviceConfig ={
	"devices": [
	           {"name": "iPad", "target":"ignore",
	        	"patterngroups": [
	        	                  {"white": ["ipad"]}
	        	                  ]
	           }, 
	           {"name": "iPhone/iPod", "target":"mobileplus",
	        	   "patterngroups": [
	        	                     {"white": ["iphone"]},
	        	                     {"white": ["ipod"]}
	        	                     ]
	           }, 
	           {"name": "OperaMini", "target":"ignore",
	        	   "patterngroups": [
	        	                     {"white": ["opera","mobile"]},
	        	                     {"white": ["opera","mini"]},
	        	                     {"white": ["opera","htc"]}
	        	                     ]
	           },
	           {"name": "AndroidMobile", "target":"mobileplus",
	        	   "patterngroups": [
	        	                     {"white": ["android"]},
	        	                     {"white": ["mobile"]}      	                     
	        	                     ]
	           },
	           {"name": "Android", "target":"ignore",
	        	   "patterngroups": [
	        	                     {"white": ["android"]}
	        	                     ]
	           },
	           {"name": "ChromeOS", "target":"www",
	        	   "patterngroups": [
	        	                     {"white": ["CrOS"]},
	        	                     {"black": ["Microsoft"]}
	        	                     ]
	           },
	           {"name": "FirefoxOS", "target":"mobileplus",
	        	   "patterngroups": [
	        	                     {"white": ["mobile","firefox/"]},
	        	                     {"white": ["tablet","firefox/"]}
	        	                     ]
	           },
	           {"name": "Windows Phone", "target":"mobileplus",
	        	   "patterngroups": [
	        	                     	{"white": ["Windows Phone", "IEMobile/7"]},
	        	                     	{"white": ["Windows Phone", "IEMobile/8"]},
	        	                     	{"white": ["Windows Phone", "IEMobile/9"]},
	        	                     	{"white": ["Windows Phone", "IEMobile/10"]},
	        	                     	{"white": ["Windows Phone", "IEMobile/11"]}
	        	                     ]
	           },
	           {"name": "Windows Tablet ARM", "target":"www",
	        	   "patterngroups": [
	        	                     	{"white": ["MSIE", " arm"]}
	        	                     ]
	           },
	           {"name": "Blackberry", "target":"mobileplus",
	        	   "patterngroups": [
	        	                     	{"white": ["blackberry","Version/6"]},
	        	                     	{"white": ["blackberry","Version/7"]},
	        	                     	{"white": ["blackberry","Version/8"]},
	        	                     	{"white": ["bb10","Version/10"]}
	        	                     ]
	           },
	           {"name": "OldMobileDevice","target":"mobile",
	        	   "patterngroups": [
									{"white": ["blackberry"]},
									{"white": ["midp"]},
									{"white": ["240x320"]},
									{"white": ["480x640"]},
									{"white": ["netfront"]},
									{"white": ["nokia"]},
									{"white": ["panasonic"]},
									{"white": ["portalmmm"]},
									{"white": ["sharp"]},
									{"white": ["sie-"]},
									{"white": ["sonyericsson"]},
									{"white": ["symbian"]},
									{"white": ["windows ce"]},
									{"white": ["benq"]},
									{"white": ["mda"]},
									{"white": ["mot-"]},
									{"white": ["opera mini"]},
									{"white": ["philips"]},
									{"white": ["pocket pc"]},
									{"white": ["sagem"]},
									{"white": ["samsung"]},
									{"white": ["sda"]},
									{"white": ["sgh-"]},
									{"white": ["vodafone"]},
									{"white": ["xda"]},
									{"white": ["htc"]},
									{"white": ["palm"]},
									{"white": ["webos"]},
									{"white": ["mobile"]},
									{"white": ["mobi"]},
									{"white": ["mini"]},
									{"white": ["XV6850"]},
									{"white": ["plucker"]},
									{"white": ["Phone"]},
									{"white": ["Novarra-Vision"]},	        	                     
	        	                    {"white": ["windows", "mobi"]},
	        	                    {"white": ["opera", "mobi"]},
	        	                    {"white": ["nintendo", "dsi"]}
	        	                     ]
		       },
	           {"name": "Desktop", "target":"www",
	        	   "patterngroups": [
	        	                     {"white": ["Windows"]},
	        	                     {"white": ["Linux"]},
	        	                     {"white": ["konqueror"]},
	        	                     {"white": ["Macintosh"]},
	        	                     {"white": ["mac_powerpc"]}
	        	                     ]
			   }
	           ]
};

if (spMobilePlusUrl && spOldMobileUrl) {
	var skipDelegation = SPONgetCookie('spSkipDelegation');
	
	var spMobileHpCompact = SPONgetCookie('spMobileHpCompactView');
	if (spMobileHpCompact != null && spMobileHpCompact == 'true') {
		if (spMobilePlusUrl == '/index.html')
			spMobilePlusUrl='/index-compact.html';
	}	

	if (skipDelegation == null || skipDelegation != 'true') {
		var redirectHash="spRedirectedFrom=www";
		var ua=navigator.userAgent;
		
		var device=spFindDevice(spDeviceConfig, ua);
		if (device != null) {
			if (device.target == 'mobileplus') {
				if(window.location.hash) {
					redirectHash = redirectHash + '&' + window.location.hash.substr(1);
				}
				document.location.href=spMobilePlusHost+spMobilePlusUrl+'#'+redirectHash;
			}
			else if (device.target == 'mobile') {
				document.location.href=spOldMobileHost+spOldMobileUrl;
			}
		}
	}
}



function spFindDevice(spDeviceConfig, ua) {
	ua=ua.toLowerCase();
	var devices=spDeviceConfig.devices;
	for (var key in devices) {
		var device=devices[key];

		var patterngroups=device.patterngroups
		for (var p in patterngroups) {

			var  found = true;
			var patternList=patterngroups[p].white
			for (var k in patternList) {
				var pattern=patternList[k].toLowerCase();
				if (ua.indexOf(pattern) == -1) {
					found=false;
					break;
				}
			}
			if (found) {
				var patternList=patterngroups[p].black
				for (var k in patternList) {
					var pattern=patternList[k].toLowerCase();
					if (ua.indexOf(pattern) != -1) {
						found=false;
						break;
					}
				}
			}
			if (found)
				return device;			
		}
	}
	return null;
}/*! jQuery v1.11.1 | (c) 2005, 2014 jQuery Foundation, Inc. | jquery.org/license */
!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=c.slice,e=c.concat,f=c.push,g=c.indexOf,h={},i=h.toString,j=h.hasOwnProperty,k={},l="1.11.1",m=function(a,b){return new m.fn.init(a,b)},n=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,o=/^-ms-/,p=/-([\da-z])/gi,q=function(a,b){return b.toUpperCase()};m.fn=m.prototype={jquery:l,constructor:m,selector:"",length:0,toArray:function(){return d.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:d.call(this)},pushStack:function(a){var b=m.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a,b){return m.each(this,a,b)},map:function(a){return this.pushStack(m.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(d.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor(null)},push:f,sort:c.sort,splice:c.splice},m.extend=m.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||m.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(e=arguments[h]))for(d in e)a=g[d],c=e[d],g!==c&&(j&&c&&(m.isPlainObject(c)||(b=m.isArray(c)))?(b?(b=!1,f=a&&m.isArray(a)?a:[]):f=a&&m.isPlainObject(a)?a:{},g[d]=m.extend(j,f,c)):void 0!==c&&(g[d]=c));return g},m.extend({expando:"jQuery"+(l+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===m.type(a)},isArray:Array.isArray||function(a){return"array"===m.type(a)},isWindow:function(a){return null!=a&&a==a.window},isNumeric:function(a){return!m.isArray(a)&&a-parseFloat(a)>=0},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},isPlainObject:function(a){var b;if(!a||"object"!==m.type(a)||a.nodeType||m.isWindow(a))return!1;try{if(a.constructor&&!j.call(a,"constructor")&&!j.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}if(k.ownLast)for(b in a)return j.call(a,b);for(b in a);return void 0===b||j.call(a,b)},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?h[i.call(a)]||"object":typeof a},globalEval:function(b){b&&m.trim(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(o,"ms-").replace(p,q)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b,c){var d,e=0,f=a.length,g=r(a);if(c){if(g){for(;f>e;e++)if(d=b.apply(a[e],c),d===!1)break}else for(e in a)if(d=b.apply(a[e],c),d===!1)break}else if(g){for(;f>e;e++)if(d=b.call(a[e],e,a[e]),d===!1)break}else for(e in a)if(d=b.call(a[e],e,a[e]),d===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(n,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(r(Object(a))?m.merge(c,"string"==typeof a?[a]:a):f.call(c,a)),c},inArray:function(a,b,c){var d;if(b){if(g)return g.call(b,a,c);for(d=b.length,c=c?0>c?Math.max(0,d+c):c:0;d>c;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,b){var c=+b.length,d=0,e=a.length;while(c>d)a[e++]=b[d++];if(c!==c)while(void 0!==b[d])a[e++]=b[d++];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,f=0,g=a.length,h=r(a),i=[];if(h)for(;g>f;f++)d=b(a[f],f,c),null!=d&&i.push(d);else for(f in a)d=b(a[f],f,c),null!=d&&i.push(d);return e.apply([],i)},guid:1,proxy:function(a,b){var c,e,f;return"string"==typeof b&&(f=a[b],b=a,a=f),m.isFunction(a)?(c=d.call(arguments,2),e=function(){return a.apply(b||this,c.concat(d.call(arguments)))},e.guid=a.guid=a.guid||m.guid++,e):void 0},now:function(){return+new Date},support:k}),m.each("Boolean Number String Function Array Date RegExp Object Error".split(" "),function(a,b){h["[object "+b+"]"]=b.toLowerCase()});function r(a){var b=a.length,c=m.type(a);return"function"===c||m.isWindow(a)?!1:1===a.nodeType&&b?!0:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var s=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+-new Date,v=a.document,w=0,x=0,y=gb(),z=gb(),A=gb(),B=function(a,b){return a===b&&(l=!0),0},C="undefined",D=1<<31,E={}.hasOwnProperty,F=[],G=F.pop,H=F.push,I=F.push,J=F.slice,K=F.indexOf||function(a){for(var b=0,c=this.length;c>b;b++)if(this[b]===a)return b;return-1},L="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",M="[\\x20\\t\\r\\n\\f]",N="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",O=N.replace("w","w#"),P="\\["+M+"*("+N+")(?:"+M+"*([*^$|!~]?=)"+M+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+O+"))|)"+M+"*\\]",Q=":("+N+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+P+")*)|.*)\\)|)",R=new RegExp("^"+M+"+|((?:^|[^\\\\])(?:\\\\.)*)"+M+"+$","g"),S=new RegExp("^"+M+"*,"+M+"*"),T=new RegExp("^"+M+"*([>+~]|"+M+")"+M+"*"),U=new RegExp("="+M+"*([^\\]'\"]*?)"+M+"*\\]","g"),V=new RegExp(Q),W=new RegExp("^"+O+"$"),X={ID:new RegExp("^#("+N+")"),CLASS:new RegExp("^\\.("+N+")"),TAG:new RegExp("^("+N.replace("w","w*")+")"),ATTR:new RegExp("^"+P),PSEUDO:new RegExp("^"+Q),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+M+"*(even|odd|(([+-]|)(\\d*)n|)"+M+"*(?:([+-]|)"+M+"*(\\d+)|))"+M+"*\\)|)","i"),bool:new RegExp("^(?:"+L+")$","i"),needsContext:new RegExp("^"+M+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+M+"*((?:-\\d)?\\d*)"+M+"*\\)|)(?=[^-]|$)","i")},Y=/^(?:input|select|textarea|button)$/i,Z=/^h\d$/i,$=/^[^{]+\{\s*\[native \w/,_=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,ab=/[+~]/,bb=/'|\\/g,cb=new RegExp("\\\\([\\da-f]{1,6}"+M+"?|("+M+")|.)","ig"),db=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)};try{I.apply(F=J.call(v.childNodes),v.childNodes),F[v.childNodes.length].nodeType}catch(eb){I={apply:F.length?function(a,b){H.apply(a,J.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function fb(a,b,d,e){var f,h,j,k,l,o,r,s,w,x;if((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,d=d||[],!a||"string"!=typeof a)return d;if(1!==(k=b.nodeType)&&9!==k)return[];if(p&&!e){if(f=_.exec(a))if(j=f[1]){if(9===k){if(h=b.getElementById(j),!h||!h.parentNode)return d;if(h.id===j)return d.push(h),d}else if(b.ownerDocument&&(h=b.ownerDocument.getElementById(j))&&t(b,h)&&h.id===j)return d.push(h),d}else{if(f[2])return I.apply(d,b.getElementsByTagName(a)),d;if((j=f[3])&&c.getElementsByClassName&&b.getElementsByClassName)return I.apply(d,b.getElementsByClassName(j)),d}if(c.qsa&&(!q||!q.test(a))){if(s=r=u,w=b,x=9===k&&a,1===k&&"object"!==b.nodeName.toLowerCase()){o=g(a),(r=b.getAttribute("id"))?s=r.replace(bb,"\\$&"):b.setAttribute("id",s),s="[id='"+s+"'] ",l=o.length;while(l--)o[l]=s+qb(o[l]);w=ab.test(a)&&ob(b.parentNode)||b,x=o.join(",")}if(x)try{return I.apply(d,w.querySelectorAll(x)),d}catch(y){}finally{r||b.removeAttribute("id")}}}return i(a.replace(R,"$1"),b,d,e)}function gb(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function hb(a){return a[u]=!0,a}function ib(a){var b=n.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function jb(a,b){var c=a.split("|"),e=a.length;while(e--)d.attrHandle[c[e]]=b}function kb(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||D)-(~a.sourceIndex||D);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function lb(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function mb(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function nb(a){return hb(function(b){return b=+b,hb(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function ob(a){return a&&typeof a.getElementsByTagName!==C&&a}c=fb.support={},f=fb.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=fb.setDocument=function(a){var b,e=a?a.ownerDocument||a:v,g=e.defaultView;return e!==n&&9===e.nodeType&&e.documentElement?(n=e,o=e.documentElement,p=!f(e),g&&g!==g.top&&(g.addEventListener?g.addEventListener("unload",function(){m()},!1):g.attachEvent&&g.attachEvent("onunload",function(){m()})),c.attributes=ib(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ib(function(a){return a.appendChild(e.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=$.test(e.getElementsByClassName)&&ib(function(a){return a.innerHTML="<div class='a'></div><div class='a i'></div>",a.firstChild.className="i",2===a.getElementsByClassName("i").length}),c.getById=ib(function(a){return o.appendChild(a).id=u,!e.getElementsByName||!e.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if(typeof b.getElementById!==C&&p){var c=b.getElementById(a);return c&&c.parentNode?[c]:[]}},d.filter.ID=function(a){var b=a.replace(cb,db);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(cb,db);return function(a){var c=typeof a.getAttributeNode!==C&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return typeof b.getElementsByTagName!==C?b.getElementsByTagName(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return typeof b.getElementsByClassName!==C&&p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=$.test(e.querySelectorAll))&&(ib(function(a){a.innerHTML="<select msallowclip=''><option selected=''></option></select>",a.querySelectorAll("[msallowclip^='']").length&&q.push("[*^$]="+M+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+M+"*(?:value|"+L+")"),a.querySelectorAll(":checked").length||q.push(":checked")}),ib(function(a){var b=e.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+M+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=$.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ib(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",Q)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=$.test(o.compareDocumentPosition),t=b||$.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===e||a.ownerDocument===v&&t(v,a)?-1:b===e||b.ownerDocument===v&&t(v,b)?1:k?K.call(k,a)-K.call(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,f=a.parentNode,g=b.parentNode,h=[a],i=[b];if(!f||!g)return a===e?-1:b===e?1:f?-1:g?1:k?K.call(k,a)-K.call(k,b):0;if(f===g)return kb(a,b);c=a;while(c=c.parentNode)h.unshift(c);c=b;while(c=c.parentNode)i.unshift(c);while(h[d]===i[d])d++;return d?kb(h[d],i[d]):h[d]===v?-1:i[d]===v?1:0},e):n},fb.matches=function(a,b){return fb(a,null,null,b)},fb.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(U,"='$1']"),!(!c.matchesSelector||!p||r&&r.test(b)||q&&q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return fb(b,n,null,[a]).length>0},fb.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},fb.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&E.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},fb.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},fb.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=fb.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=fb.selectors={cacheLength:50,createPseudo:hb,match:X,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(cb,db),a[3]=(a[3]||a[4]||a[5]||"").replace(cb,db),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||fb.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&fb.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return X.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&V.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(cb,db).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+M+")"+a+"("+M+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||typeof a.getAttribute!==C&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=fb.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h;if(q){if(f){while(p){l=b;while(l=l[p])if(h?l.nodeName.toLowerCase()===r:1===l.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){k=q[u]||(q[u]={}),j=k[a]||[],n=j[0]===w&&j[1],m=j[0]===w&&j[2],l=n&&q.childNodes[n];while(l=++n&&l&&l[p]||(m=n=0)||o.pop())if(1===l.nodeType&&++m&&l===b){k[a]=[w,n,m];break}}else if(s&&(j=(b[u]||(b[u]={}))[a])&&j[0]===w)m=j[1];else while(l=++n&&l&&l[p]||(m=n=0)||o.pop())if((h?l.nodeName.toLowerCase()===r:1===l.nodeType)&&++m&&(s&&((l[u]||(l[u]={}))[a]=[w,m]),l===b))break;return m-=e,m===d||m%d===0&&m/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||fb.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?hb(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=K.call(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:hb(function(a){var b=[],c=[],d=h(a.replace(R,"$1"));return d[u]?hb(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),!c.pop()}}),has:hb(function(a){return function(b){return fb(a,b).length>0}}),contains:hb(function(a){return function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:hb(function(a){return W.test(a||"")||fb.error("unsupported lang: "+a),a=a.replace(cb,db).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return Z.test(a.nodeName)},input:function(a){return Y.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:nb(function(){return[0]}),last:nb(function(a,b){return[b-1]}),eq:nb(function(a,b,c){return[0>c?c+b:c]}),even:nb(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:nb(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:nb(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:nb(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=lb(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=mb(b);function pb(){}pb.prototype=d.filters=d.pseudos,d.setFilters=new pb,g=fb.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){(!c||(e=S.exec(h)))&&(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=T.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(R," ")}),h=h.slice(c.length));for(g in d.filter)!(e=X[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?fb.error(a):z(a,i).slice(0)};function qb(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function rb(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j=[w,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(i=b[u]||(b[u]={}),(h=i[d])&&h[0]===w&&h[1]===f)return j[2]=h[2];if(i[d]=j,j[2]=a(b,c,g))return!0}}}function sb(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function tb(a,b,c){for(var d=0,e=b.length;e>d;d++)fb(a,b[d],c);return c}function ub(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(!c||c(f,d,e))&&(g.push(f),j&&b.push(h));return g}function vb(a,b,c,d,e,f){return d&&!d[u]&&(d=vb(d)),e&&!e[u]&&(e=vb(e,f)),hb(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||tb(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:ub(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=ub(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?K.call(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=ub(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):I.apply(g,r)})}function wb(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=rb(function(a){return a===b},h,!0),l=rb(function(a){return K.call(b,a)>-1},h,!0),m=[function(a,c,d){return!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d))}];f>i;i++)if(c=d.relative[a[i].type])m=[rb(sb(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return vb(i>1&&sb(m),i>1&&qb(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(R,"$1"),c,e>i&&wb(a.slice(i,e)),f>e&&wb(a=a.slice(e)),f>e&&qb(a))}m.push(c)}return sb(m)}function xb(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,m,o,p=0,q="0",r=f&&[],s=[],t=j,u=f||e&&d.find.TAG("*",k),v=w+=null==t?1:Math.random()||.1,x=u.length;for(k&&(j=g!==n&&g);q!==x&&null!=(l=u[q]);q++){if(e&&l){m=0;while(o=a[m++])if(o(l,g,h)){i.push(l);break}k&&(w=v)}c&&((l=!o&&l)&&p--,f&&r.push(l))}if(p+=q,c&&q!==p){m=0;while(o=b[m++])o(r,s,g,h);if(f){if(p>0)while(q--)r[q]||s[q]||(s[q]=G.call(i));s=ub(s)}I.apply(i,s),k&&!f&&s.length>0&&p+b.length>1&&fb.uniqueSort(i)}return k&&(w=v,j=t),r};return c?hb(f):f}return h=fb.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=wb(b[c]),f[u]?d.push(f):e.push(f);f=A(a,xb(e,d)),f.selector=a}return f},i=fb.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(cb,db),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=X.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(cb,db),ab.test(j[0].type)&&ob(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&qb(j),!a)return I.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,ab.test(a)&&ob(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ib(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))}),ib(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||jb("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ib(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||jb("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ib(function(a){return null==a.getAttribute("disabled")})||jb(L,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),fb}(a);m.find=s,m.expr=s.selectors,m.expr[":"]=m.expr.pseudos,m.unique=s.uniqueSort,m.text=s.getText,m.isXMLDoc=s.isXML,m.contains=s.contains;var t=m.expr.match.needsContext,u=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,v=/^.[^:#\[\.,]*$/;function w(a,b,c){if(m.isFunction(b))return m.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return m.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(v.test(b))return m.filter(b,a,c);b=m.filter(b,a)}return m.grep(a,function(a){return m.inArray(a,b)>=0!==c})}m.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?m.find.matchesSelector(d,a)?[d]:[]:m.find.matches(a,m.grep(b,function(a){return 1===a.nodeType}))},m.fn.extend({find:function(a){var b,c=[],d=this,e=d.length;if("string"!=typeof a)return this.pushStack(m(a).filter(function(){for(b=0;e>b;b++)if(m.contains(d[b],this))return!0}));for(b=0;e>b;b++)m.find(a,d[b],c);return c=this.pushStack(e>1?m.unique(c):c),c.selector=this.selector?this.selector+" "+a:a,c},filter:function(a){return this.pushStack(w(this,a||[],!1))},not:function(a){return this.pushStack(w(this,a||[],!0))},is:function(a){return!!w(this,"string"==typeof a&&t.test(a)?m(a):a||[],!1).length}});var x,y=a.document,z=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,A=m.fn.init=function(a,b){var c,d;if(!a)return this;if("string"==typeof a){if(c="<"===a.charAt(0)&&">"===a.charAt(a.length-1)&&a.length>=3?[null,a,null]:z.exec(a),!c||!c[1]&&b)return!b||b.jquery?(b||x).find(a):this.constructor(b).find(a);if(c[1]){if(b=b instanceof m?b[0]:b,m.merge(this,m.parseHTML(c[1],b&&b.nodeType?b.ownerDocument||b:y,!0)),u.test(c[1])&&m.isPlainObject(b))for(c in b)m.isFunction(this[c])?this[c](b[c]):this.attr(c,b[c]);return this}if(d=y.getElementById(c[2]),d&&d.parentNode){if(d.id!==c[2])return x.find(a);this.length=1,this[0]=d}return this.context=y,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):m.isFunction(a)?"undefined"!=typeof x.ready?x.ready(a):a(m):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),m.makeArray(a,this))};A.prototype=m.fn,x=m(y);var B=/^(?:parents|prev(?:Until|All))/,C={children:!0,contents:!0,next:!0,prev:!0};m.extend({dir:function(a,b,c){var d=[],e=a[b];while(e&&9!==e.nodeType&&(void 0===c||1!==e.nodeType||!m(e).is(c)))1===e.nodeType&&d.push(e),e=e[b];return d},sibling:function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c}}),m.fn.extend({has:function(a){var b,c=m(a,this),d=c.length;return this.filter(function(){for(b=0;d>b;b++)if(m.contains(this,c[b]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=t.test(a)||"string"!=typeof a?m(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&m.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?m.unique(f):f)},index:function(a){return a?"string"==typeof a?m.inArray(this[0],m(a)):m.inArray(a.jquery?a[0]:a,this):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(m.unique(m.merge(this.get(),m(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function D(a,b){do a=a[b];while(a&&1!==a.nodeType);return a}m.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return m.dir(a,"parentNode")},parentsUntil:function(a,b,c){return m.dir(a,"parentNode",c)},next:function(a){return D(a,"nextSibling")},prev:function(a){return D(a,"previousSibling")},nextAll:function(a){return m.dir(a,"nextSibling")},prevAll:function(a){return m.dir(a,"previousSibling")},nextUntil:function(a,b,c){return m.dir(a,"nextSibling",c)},prevUntil:function(a,b,c){return m.dir(a,"previousSibling",c)},siblings:function(a){return m.sibling((a.parentNode||{}).firstChild,a)},children:function(a){return m.sibling(a.firstChild)},contents:function(a){return m.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:m.merge([],a.childNodes)}},function(a,b){m.fn[a]=function(c,d){var e=m.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=m.filter(d,e)),this.length>1&&(C[a]||(e=m.unique(e)),B.test(a)&&(e=e.reverse())),this.pushStack(e)}});var E=/\S+/g,F={};function G(a){var b=F[a]={};return m.each(a.match(E)||[],function(a,c){b[c]=!0}),b}m.Callbacks=function(a){a="string"==typeof a?F[a]||G(a):m.extend({},a);var b,c,d,e,f,g,h=[],i=!a.once&&[],j=function(l){for(c=a.memory&&l,d=!0,f=g||0,g=0,e=h.length,b=!0;h&&e>f;f++)if(h[f].apply(l[0],l[1])===!1&&a.stopOnFalse){c=!1;break}b=!1,h&&(i?i.length&&j(i.shift()):c?h=[]:k.disable())},k={add:function(){if(h){var d=h.length;!function f(b){m.each(b,function(b,c){var d=m.type(c);"function"===d?a.unique&&k.has(c)||h.push(c):c&&c.length&&"string"!==d&&f(c)})}(arguments),b?e=h.length:c&&(g=d,j(c))}return this},remove:function(){return h&&m.each(arguments,function(a,c){var d;while((d=m.inArray(c,h,d))>-1)h.splice(d,1),b&&(e>=d&&e--,f>=d&&f--)}),this},has:function(a){return a?m.inArray(a,h)>-1:!(!h||!h.length)},empty:function(){return h=[],e=0,this},disable:function(){return h=i=c=void 0,this},disabled:function(){return!h},lock:function(){return i=void 0,c||k.disable(),this},locked:function(){return!i},fireWith:function(a,c){return!h||d&&!i||(c=c||[],c=[a,c.slice?c.slice():c],b?i.push(c):j(c)),this},fire:function(){return k.fireWith(this,arguments),this},fired:function(){return!!d}};return k},m.extend({Deferred:function(a){var b=[["resolve","done",m.Callbacks("once memory"),"resolved"],["reject","fail",m.Callbacks("once memory"),"rejected"],["notify","progress",m.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return m.Deferred(function(c){m.each(b,function(b,f){var g=m.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&m.isFunction(a.promise)?a.promise().done(c.resolve).fail(c.reject).progress(c.notify):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?m.extend(a,d):d}},e={};return d.pipe=d.then,m.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=d.call(arguments),e=c.length,f=1!==e||a&&m.isFunction(a.promise)?e:0,g=1===f?a:m.Deferred(),h=function(a,b,c){return function(e){b[a]=this,c[a]=arguments.length>1?d.call(arguments):e,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(e>1)for(i=new Array(e),j=new Array(e),k=new Array(e);e>b;b++)c[b]&&m.isFunction(c[b].promise)?c[b].promise().done(h(b,k,c)).fail(g.reject).progress(h(b,j,i)):--f;return f||g.resolveWith(k,c),g.promise()}});var H;m.fn.ready=function(a){return m.ready.promise().done(a),this},m.extend({isReady:!1,readyWait:1,holdReady:function(a){a?m.readyWait++:m.ready(!0)},ready:function(a){if(a===!0?!--m.readyWait:!m.isReady){if(!y.body)return setTimeout(m.ready);m.isReady=!0,a!==!0&&--m.readyWait>0||(H.resolveWith(y,[m]),m.fn.triggerHandler&&(m(y).triggerHandler("ready"),m(y).off("ready")))}}});function I(){y.addEventListener?(y.removeEventListener("DOMContentLoaded",J,!1),a.removeEventListener("load",J,!1)):(y.detachEvent("onreadystatechange",J),a.detachEvent("onload",J))}function J(){(y.addEventListener||"load"===event.type||"complete"===y.readyState)&&(I(),m.ready())}m.ready.promise=function(b){if(!H)if(H=m.Deferred(),"complete"===y.readyState)setTimeout(m.ready);else if(y.addEventListener)y.addEventListener("DOMContentLoaded",J,!1),a.addEventListener("load",J,!1);else{y.attachEvent("onreadystatechange",J),a.attachEvent("onload",J);var c=!1;try{c=null==a.frameElement&&y.documentElement}catch(d){}c&&c.doScroll&&!function e(){if(!m.isReady){try{c.doScroll("left")}catch(a){return setTimeout(e,50)}I(),m.ready()}}()}return H.promise(b)};var K="undefined",L;for(L in m(k))break;k.ownLast="0"!==L,k.inlineBlockNeedsLayout=!1,m(function(){var a,b,c,d;c=y.getElementsByTagName("body")[0],c&&c.style&&(b=y.createElement("div"),d=y.createElement("div"),d.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(d).appendChild(b),typeof b.style.zoom!==K&&(b.style.cssText="display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1",k.inlineBlockNeedsLayout=a=3===b.offsetWidth,a&&(c.style.zoom=1)),c.removeChild(d))}),function(){var a=y.createElement("div");if(null==k.deleteExpando){k.deleteExpando=!0;try{delete a.test}catch(b){k.deleteExpando=!1}}a=null}(),m.acceptData=function(a){var b=m.noData[(a.nodeName+" ").toLowerCase()],c=+a.nodeType||1;return 1!==c&&9!==c?!1:!b||b!==!0&&a.getAttribute("classid")===b};var M=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,N=/([A-Z])/g;function O(a,b,c){if(void 0===c&&1===a.nodeType){var d="data-"+b.replace(N,"-$1").toLowerCase();if(c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:M.test(c)?m.parseJSON(c):c}catch(e){}m.data(a,b,c)}else c=void 0}return c}function P(a){var b;for(b in a)if(("data"!==b||!m.isEmptyObject(a[b]))&&"toJSON"!==b)return!1;return!0}function Q(a,b,d,e){if(m.acceptData(a)){var f,g,h=m.expando,i=a.nodeType,j=i?m.cache:a,k=i?a[h]:a[h]&&h;
if(k&&j[k]&&(e||j[k].data)||void 0!==d||"string"!=typeof b)return k||(k=i?a[h]=c.pop()||m.guid++:h),j[k]||(j[k]=i?{}:{toJSON:m.noop}),("object"==typeof b||"function"==typeof b)&&(e?j[k]=m.extend(j[k],b):j[k].data=m.extend(j[k].data,b)),g=j[k],e||(g.data||(g.data={}),g=g.data),void 0!==d&&(g[m.camelCase(b)]=d),"string"==typeof b?(f=g[b],null==f&&(f=g[m.camelCase(b)])):f=g,f}}function R(a,b,c){if(m.acceptData(a)){var d,e,f=a.nodeType,g=f?m.cache:a,h=f?a[m.expando]:m.expando;if(g[h]){if(b&&(d=c?g[h]:g[h].data)){m.isArray(b)?b=b.concat(m.map(b,m.camelCase)):b in d?b=[b]:(b=m.camelCase(b),b=b in d?[b]:b.split(" ")),e=b.length;while(e--)delete d[b[e]];if(c?!P(d):!m.isEmptyObject(d))return}(c||(delete g[h].data,P(g[h])))&&(f?m.cleanData([a],!0):k.deleteExpando||g!=g.window?delete g[h]:g[h]=null)}}}m.extend({cache:{},noData:{"applet ":!0,"embed ":!0,"object ":"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},hasData:function(a){return a=a.nodeType?m.cache[a[m.expando]]:a[m.expando],!!a&&!P(a)},data:function(a,b,c){return Q(a,b,c)},removeData:function(a,b){return R(a,b)},_data:function(a,b,c){return Q(a,b,c,!0)},_removeData:function(a,b){return R(a,b,!0)}}),m.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=m.data(f),1===f.nodeType&&!m._data(f,"parsedAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=m.camelCase(d.slice(5)),O(f,d,e[d])));m._data(f,"parsedAttrs",!0)}return e}return"object"==typeof a?this.each(function(){m.data(this,a)}):arguments.length>1?this.each(function(){m.data(this,a,b)}):f?O(f,a,m.data(f,a)):void 0},removeData:function(a){return this.each(function(){m.removeData(this,a)})}}),m.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=m._data(a,b),c&&(!d||m.isArray(c)?d=m._data(a,b,m.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=m.queue(a,b),d=c.length,e=c.shift(),f=m._queueHooks(a,b),g=function(){m.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return m._data(a,c)||m._data(a,c,{empty:m.Callbacks("once memory").add(function(){m._removeData(a,b+"queue"),m._removeData(a,c)})})}}),m.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?m.queue(this[0],a):void 0===b?this:this.each(function(){var c=m.queue(this,a,b);m._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&m.dequeue(this,a)})},dequeue:function(a){return this.each(function(){m.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=m.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=m._data(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}});var S=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,T=["Top","Right","Bottom","Left"],U=function(a,b){return a=b||a,"none"===m.css(a,"display")||!m.contains(a.ownerDocument,a)},V=m.access=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===m.type(c)){e=!0;for(h in c)m.access(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,m.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(m(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},W=/^(?:checkbox|radio)$/i;!function(){var a=y.createElement("input"),b=y.createElement("div"),c=y.createDocumentFragment();if(b.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",k.leadingWhitespace=3===b.firstChild.nodeType,k.tbody=!b.getElementsByTagName("tbody").length,k.htmlSerialize=!!b.getElementsByTagName("link").length,k.html5Clone="<:nav></:nav>"!==y.createElement("nav").cloneNode(!0).outerHTML,a.type="checkbox",a.checked=!0,c.appendChild(a),k.appendChecked=a.checked,b.innerHTML="<textarea>x</textarea>",k.noCloneChecked=!!b.cloneNode(!0).lastChild.defaultValue,c.appendChild(b),b.innerHTML="<input type='radio' checked='checked' name='t'/>",k.checkClone=b.cloneNode(!0).cloneNode(!0).lastChild.checked,k.noCloneEvent=!0,b.attachEvent&&(b.attachEvent("onclick",function(){k.noCloneEvent=!1}),b.cloneNode(!0).click()),null==k.deleteExpando){k.deleteExpando=!0;try{delete b.test}catch(d){k.deleteExpando=!1}}}(),function(){var b,c,d=y.createElement("div");for(b in{submit:!0,change:!0,focusin:!0})c="on"+b,(k[b+"Bubbles"]=c in a)||(d.setAttribute(c,"t"),k[b+"Bubbles"]=d.attributes[c].expando===!1);d=null}();var X=/^(?:input|select|textarea)$/i,Y=/^key/,Z=/^(?:mouse|pointer|contextmenu)|click/,$=/^(?:focusinfocus|focusoutblur)$/,_=/^([^.]*)(?:\.(.+)|)$/;function ab(){return!0}function bb(){return!1}function cb(){try{return y.activeElement}catch(a){}}m.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,n,o,p,q,r=m._data(a);if(r){c.handler&&(i=c,c=i.handler,e=i.selector),c.guid||(c.guid=m.guid++),(g=r.events)||(g=r.events={}),(k=r.handle)||(k=r.handle=function(a){return typeof m===K||a&&m.event.triggered===a.type?void 0:m.event.dispatch.apply(k.elem,arguments)},k.elem=a),b=(b||"").match(E)||[""],h=b.length;while(h--)f=_.exec(b[h])||[],o=q=f[1],p=(f[2]||"").split(".").sort(),o&&(j=m.event.special[o]||{},o=(e?j.delegateType:j.bindType)||o,j=m.event.special[o]||{},l=m.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&m.expr.match.needsContext.test(e),namespace:p.join(".")},i),(n=g[o])||(n=g[o]=[],n.delegateCount=0,j.setup&&j.setup.call(a,d,p,k)!==!1||(a.addEventListener?a.addEventListener(o,k,!1):a.attachEvent&&a.attachEvent("on"+o,k))),j.add&&(j.add.call(a,l),l.handler.guid||(l.handler.guid=c.guid)),e?n.splice(n.delegateCount++,0,l):n.push(l),m.event.global[o]=!0);a=null}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,n,o,p,q,r=m.hasData(a)&&m._data(a);if(r&&(k=r.events)){b=(b||"").match(E)||[""],j=b.length;while(j--)if(h=_.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=m.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,n=k[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),i=f=n.length;while(f--)g=n[f],!e&&q!==g.origType||c&&c.guid!==g.guid||h&&!h.test(g.namespace)||d&&d!==g.selector&&("**"!==d||!g.selector)||(n.splice(f,1),g.selector&&n.delegateCount--,l.remove&&l.remove.call(a,g));i&&!n.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||m.removeEvent(a,o,r.handle),delete k[o])}else for(o in k)m.event.remove(a,o+b[j],c,d,!0);m.isEmptyObject(k)&&(delete r.handle,m._removeData(a,"events"))}},trigger:function(b,c,d,e){var f,g,h,i,k,l,n,o=[d||y],p=j.call(b,"type")?b.type:b,q=j.call(b,"namespace")?b.namespace.split("."):[];if(h=l=d=d||y,3!==d.nodeType&&8!==d.nodeType&&!$.test(p+m.event.triggered)&&(p.indexOf(".")>=0&&(q=p.split("."),p=q.shift(),q.sort()),g=p.indexOf(":")<0&&"on"+p,b=b[m.expando]?b:new m.Event(p,"object"==typeof b&&b),b.isTrigger=e?2:3,b.namespace=q.join("."),b.namespace_re=b.namespace?new RegExp("(^|\\.)"+q.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=d),c=null==c?[b]:m.makeArray(c,[b]),k=m.event.special[p]||{},e||!k.trigger||k.trigger.apply(d,c)!==!1)){if(!e&&!k.noBubble&&!m.isWindow(d)){for(i=k.delegateType||p,$.test(i+p)||(h=h.parentNode);h;h=h.parentNode)o.push(h),l=h;l===(d.ownerDocument||y)&&o.push(l.defaultView||l.parentWindow||a)}n=0;while((h=o[n++])&&!b.isPropagationStopped())b.type=n>1?i:k.bindType||p,f=(m._data(h,"events")||{})[b.type]&&m._data(h,"handle"),f&&f.apply(h,c),f=g&&h[g],f&&f.apply&&m.acceptData(h)&&(b.result=f.apply(h,c),b.result===!1&&b.preventDefault());if(b.type=p,!e&&!b.isDefaultPrevented()&&(!k._default||k._default.apply(o.pop(),c)===!1)&&m.acceptData(d)&&g&&d[p]&&!m.isWindow(d)){l=d[g],l&&(d[g]=null),m.event.triggered=p;try{d[p]()}catch(r){}m.event.triggered=void 0,l&&(d[g]=l)}return b.result}},dispatch:function(a){a=m.event.fix(a);var b,c,e,f,g,h=[],i=d.call(arguments),j=(m._data(this,"events")||{})[a.type]||[],k=m.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=m.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,g=0;while((e=f.handlers[g++])&&!a.isImmediatePropagationStopped())(!a.namespace_re||a.namespace_re.test(e.namespace))&&(a.handleObj=e,a.data=e.data,c=((m.event.special[e.origType]||{}).handle||e.handler).apply(f.elem,i),void 0!==c&&(a.result=c)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&(!a.button||"click"!==a.type))for(;i!=this;i=i.parentNode||this)if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(e=[],f=0;h>f;f++)d=b[f],c=d.selector+" ",void 0===e[c]&&(e[c]=d.needsContext?m(c,this).index(i)>=0:m.find(c,this,null,[i]).length),e[c]&&e.push(d);e.length&&g.push({elem:i,handlers:e})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},fix:function(a){if(a[m.expando])return a;var b,c,d,e=a.type,f=a,g=this.fixHooks[e];g||(this.fixHooks[e]=g=Z.test(e)?this.mouseHooks:Y.test(e)?this.keyHooks:{}),d=g.props?this.props.concat(g.props):this.props,a=new m.Event(f),b=d.length;while(b--)c=d[b],a[c]=f[c];return a.target||(a.target=f.srcElement||y),3===a.target.nodeType&&(a.target=a.target.parentNode),a.metaKey=!!a.metaKey,g.filter?g.filter(a,f):a},props:"altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,d,e,f=b.button,g=b.fromElement;return null==a.pageX&&null!=b.clientX&&(d=a.target.ownerDocument||y,e=d.documentElement,c=d.body,a.pageX=b.clientX+(e&&e.scrollLeft||c&&c.scrollLeft||0)-(e&&e.clientLeft||c&&c.clientLeft||0),a.pageY=b.clientY+(e&&e.scrollTop||c&&c.scrollTop||0)-(e&&e.clientTop||c&&c.clientTop||0)),!a.relatedTarget&&g&&(a.relatedTarget=g===a.target?b.toElement:g),a.which||void 0===f||(a.which=1&f?1:2&f?3:4&f?2:0),a}},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==cb()&&this.focus)try{return this.focus(),!1}catch(a){}},delegateType:"focusin"},blur:{trigger:function(){return this===cb()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return m.nodeName(this,"input")&&"checkbox"===this.type&&this.click?(this.click(),!1):void 0},_default:function(a){return m.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}},simulate:function(a,b,c,d){var e=m.extend(new m.Event,c,{type:a,isSimulated:!0,originalEvent:{}});d?m.event.trigger(e,null,b):m.event.dispatch.call(b,e),e.isDefaultPrevented()&&c.preventDefault()}},m.removeEvent=y.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c,!1)}:function(a,b,c){var d="on"+b;a.detachEvent&&(typeof a[d]===K&&(a[d]=null),a.detachEvent(d,c))},m.Event=function(a,b){return this instanceof m.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?ab:bb):this.type=a,b&&m.extend(this,b),this.timeStamp=a&&a.timeStamp||m.now(),void(this[m.expando]=!0)):new m.Event(a,b)},m.Event.prototype={isDefaultPrevented:bb,isPropagationStopped:bb,isImmediatePropagationStopped:bb,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=ab,a&&(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=ab,a&&(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=ab,a&&a.stopImmediatePropagation&&a.stopImmediatePropagation(),this.stopPropagation()}},m.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){m.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return(!e||e!==d&&!m.contains(d,e))&&(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),k.submitBubbles||(m.event.special.submit={setup:function(){return m.nodeName(this,"form")?!1:void m.event.add(this,"click._submit keypress._submit",function(a){var b=a.target,c=m.nodeName(b,"input")||m.nodeName(b,"button")?b.form:void 0;c&&!m._data(c,"submitBubbles")&&(m.event.add(c,"submit._submit",function(a){a._submit_bubble=!0}),m._data(c,"submitBubbles",!0))})},postDispatch:function(a){a._submit_bubble&&(delete a._submit_bubble,this.parentNode&&!a.isTrigger&&m.event.simulate("submit",this.parentNode,a,!0))},teardown:function(){return m.nodeName(this,"form")?!1:void m.event.remove(this,"._submit")}}),k.changeBubbles||(m.event.special.change={setup:function(){return X.test(this.nodeName)?(("checkbox"===this.type||"radio"===this.type)&&(m.event.add(this,"propertychange._change",function(a){"checked"===a.originalEvent.propertyName&&(this._just_changed=!0)}),m.event.add(this,"click._change",function(a){this._just_changed&&!a.isTrigger&&(this._just_changed=!1),m.event.simulate("change",this,a,!0)})),!1):void m.event.add(this,"beforeactivate._change",function(a){var b=a.target;X.test(b.nodeName)&&!m._data(b,"changeBubbles")&&(m.event.add(b,"change._change",function(a){!this.parentNode||a.isSimulated||a.isTrigger||m.event.simulate("change",this.parentNode,a,!0)}),m._data(b,"changeBubbles",!0))})},handle:function(a){var b=a.target;return this!==b||a.isSimulated||a.isTrigger||"radio"!==b.type&&"checkbox"!==b.type?a.handleObj.handler.apply(this,arguments):void 0},teardown:function(){return m.event.remove(this,"._change"),!X.test(this.nodeName)}}),k.focusinBubbles||m.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){m.event.simulate(b,a.target,m.event.fix(a),!0)};m.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=m._data(d,b);e||d.addEventListener(a,c,!0),m._data(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=m._data(d,b)-1;e?m._data(d,b,e):(d.removeEventListener(a,c,!0),m._removeData(d,b))}}}),m.fn.extend({on:function(a,b,c,d,e){var f,g;if("object"==typeof a){"string"!=typeof b&&(c=c||b,b=void 0);for(f in a)this.on(f,b,c,a[f],e);return this}if(null==c&&null==d?(d=b,c=b=void 0):null==d&&("string"==typeof b?(d=c,c=void 0):(d=c,c=b,b=void 0)),d===!1)d=bb;else if(!d)return this;return 1===e&&(g=d,d=function(a){return m().off(a),g.apply(this,arguments)},d.guid=g.guid||(g.guid=m.guid++)),this.each(function(){m.event.add(this,a,d,c,b)})},one:function(a,b,c,d){return this.on(a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,m(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return(b===!1||"function"==typeof b)&&(c=b,b=void 0),c===!1&&(c=bb),this.each(function(){m.event.remove(this,a,c,b)})},trigger:function(a,b){return this.each(function(){m.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?m.event.trigger(a,b,c,!0):void 0}});function db(a){var b=eb.split("|"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}var eb="abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",fb=/ jQuery\d+="(?:null|\d+)"/g,gb=new RegExp("<(?:"+eb+")[\\s/>]","i"),hb=/^\s+/,ib=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,jb=/<([\w:]+)/,kb=/<tbody/i,lb=/<|&#?\w+;/,mb=/<(?:script|style|link)/i,nb=/checked\s*(?:[^=]|=\s*.checked.)/i,ob=/^$|\/(?:java|ecma)script/i,pb=/^true\/(.*)/,qb=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,rb={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],area:[1,"<map>","</map>"],param:[1,"<object>","</object>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:k.htmlSerialize?[0,"",""]:[1,"X<div>","</div>"]},sb=db(y),tb=sb.appendChild(y.createElement("div"));rb.optgroup=rb.option,rb.tbody=rb.tfoot=rb.colgroup=rb.caption=rb.thead,rb.th=rb.td;function ub(a,b){var c,d,e=0,f=typeof a.getElementsByTagName!==K?a.getElementsByTagName(b||"*"):typeof a.querySelectorAll!==K?a.querySelectorAll(b||"*"):void 0;if(!f)for(f=[],c=a.childNodes||a;null!=(d=c[e]);e++)!b||m.nodeName(d,b)?f.push(d):m.merge(f,ub(d,b));return void 0===b||b&&m.nodeName(a,b)?m.merge([a],f):f}function vb(a){W.test(a.type)&&(a.defaultChecked=a.checked)}function wb(a,b){return m.nodeName(a,"table")&&m.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function xb(a){return a.type=(null!==m.find.attr(a,"type"))+"/"+a.type,a}function yb(a){var b=pb.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function zb(a,b){for(var c,d=0;null!=(c=a[d]);d++)m._data(c,"globalEval",!b||m._data(b[d],"globalEval"))}function Ab(a,b){if(1===b.nodeType&&m.hasData(a)){var c,d,e,f=m._data(a),g=m._data(b,f),h=f.events;if(h){delete g.handle,g.events={};for(c in h)for(d=0,e=h[c].length;e>d;d++)m.event.add(b,c,h[c][d])}g.data&&(g.data=m.extend({},g.data))}}function Bb(a,b){var c,d,e;if(1===b.nodeType){if(c=b.nodeName.toLowerCase(),!k.noCloneEvent&&b[m.expando]){e=m._data(b);for(d in e.events)m.removeEvent(b,d,e.handle);b.removeAttribute(m.expando)}"script"===c&&b.text!==a.text?(xb(b).text=a.text,yb(b)):"object"===c?(b.parentNode&&(b.outerHTML=a.outerHTML),k.html5Clone&&a.innerHTML&&!m.trim(b.innerHTML)&&(b.innerHTML=a.innerHTML)):"input"===c&&W.test(a.type)?(b.defaultChecked=b.checked=a.checked,b.value!==a.value&&(b.value=a.value)):"option"===c?b.defaultSelected=b.selected=a.defaultSelected:("input"===c||"textarea"===c)&&(b.defaultValue=a.defaultValue)}}m.extend({clone:function(a,b,c){var d,e,f,g,h,i=m.contains(a.ownerDocument,a);if(k.html5Clone||m.isXMLDoc(a)||!gb.test("<"+a.nodeName+">")?f=a.cloneNode(!0):(tb.innerHTML=a.outerHTML,tb.removeChild(f=tb.firstChild)),!(k.noCloneEvent&&k.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||m.isXMLDoc(a)))for(d=ub(f),h=ub(a),g=0;null!=(e=h[g]);++g)d[g]&&Bb(e,d[g]);if(b)if(c)for(h=h||ub(a),d=d||ub(f),g=0;null!=(e=h[g]);g++)Ab(e,d[g]);else Ab(a,f);return d=ub(f,"script"),d.length>0&&zb(d,!i&&ub(a,"script")),d=h=e=null,f},buildFragment:function(a,b,c,d){for(var e,f,g,h,i,j,l,n=a.length,o=db(b),p=[],q=0;n>q;q++)if(f=a[q],f||0===f)if("object"===m.type(f))m.merge(p,f.nodeType?[f]:f);else if(lb.test(f)){h=h||o.appendChild(b.createElement("div")),i=(jb.exec(f)||["",""])[1].toLowerCase(),l=rb[i]||rb._default,h.innerHTML=l[1]+f.replace(ib,"<$1></$2>")+l[2],e=l[0];while(e--)h=h.lastChild;if(!k.leadingWhitespace&&hb.test(f)&&p.push(b.createTextNode(hb.exec(f)[0])),!k.tbody){f="table"!==i||kb.test(f)?"<table>"!==l[1]||kb.test(f)?0:h:h.firstChild,e=f&&f.childNodes.length;while(e--)m.nodeName(j=f.childNodes[e],"tbody")&&!j.childNodes.length&&f.removeChild(j)}m.merge(p,h.childNodes),h.textContent="";while(h.firstChild)h.removeChild(h.firstChild);h=o.lastChild}else p.push(b.createTextNode(f));h&&o.removeChild(h),k.appendChecked||m.grep(ub(p,"input"),vb),q=0;while(f=p[q++])if((!d||-1===m.inArray(f,d))&&(g=m.contains(f.ownerDocument,f),h=ub(o.appendChild(f),"script"),g&&zb(h),c)){e=0;while(f=h[e++])ob.test(f.type||"")&&c.push(f)}return h=null,o},cleanData:function(a,b){for(var d,e,f,g,h=0,i=m.expando,j=m.cache,l=k.deleteExpando,n=m.event.special;null!=(d=a[h]);h++)if((b||m.acceptData(d))&&(f=d[i],g=f&&j[f])){if(g.events)for(e in g.events)n[e]?m.event.remove(d,e):m.removeEvent(d,e,g.handle);j[f]&&(delete j[f],l?delete d[i]:typeof d.removeAttribute!==K?d.removeAttribute(i):d[i]=null,c.push(f))}}}),m.fn.extend({text:function(a){return V(this,function(a){return void 0===a?m.text(this):this.empty().append((this[0]&&this[0].ownerDocument||y).createTextNode(a))},null,a,arguments.length)},append:function(){return this.domManip(arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=wb(this,a);b.appendChild(a)}})},prepend:function(){return this.domManip(arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=wb(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},remove:function(a,b){for(var c,d=a?m.filter(a,this):this,e=0;null!=(c=d[e]);e++)b||1!==c.nodeType||m.cleanData(ub(c)),c.parentNode&&(b&&m.contains(c.ownerDocument,c)&&zb(ub(c,"script")),c.parentNode.removeChild(c));return this},empty:function(){for(var a,b=0;null!=(a=this[b]);b++){1===a.nodeType&&m.cleanData(ub(a,!1));while(a.firstChild)a.removeChild(a.firstChild);a.options&&m.nodeName(a,"select")&&(a.options.length=0)}return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return m.clone(this,a,b)})},html:function(a){return V(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a)return 1===b.nodeType?b.innerHTML.replace(fb,""):void 0;if(!("string"!=typeof a||mb.test(a)||!k.htmlSerialize&&gb.test(a)||!k.leadingWhitespace&&hb.test(a)||rb[(jb.exec(a)||["",""])[1].toLowerCase()])){a=a.replace(ib,"<$1></$2>");try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(m.cleanData(ub(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=arguments[0];return this.domManip(arguments,function(b){a=this.parentNode,m.cleanData(ub(this)),a&&a.replaceChild(b,this)}),a&&(a.length||a.nodeType)?this:this.remove()},detach:function(a){return this.remove(a,!0)},domManip:function(a,b){a=e.apply([],a);var c,d,f,g,h,i,j=0,l=this.length,n=this,o=l-1,p=a[0],q=m.isFunction(p);if(q||l>1&&"string"==typeof p&&!k.checkClone&&nb.test(p))return this.each(function(c){var d=n.eq(c);q&&(a[0]=p.call(this,c,d.html())),d.domManip(a,b)});if(l&&(i=m.buildFragment(a,this[0].ownerDocument,!1,this),c=i.firstChild,1===i.childNodes.length&&(i=c),c)){for(g=m.map(ub(i,"script"),xb),f=g.length;l>j;j++)d=i,j!==o&&(d=m.clone(d,!0,!0),f&&m.merge(g,ub(d,"script"))),b.call(this[j],d,j);if(f)for(h=g[g.length-1].ownerDocument,m.map(g,yb),j=0;f>j;j++)d=g[j],ob.test(d.type||"")&&!m._data(d,"globalEval")&&m.contains(h,d)&&(d.src?m._evalUrl&&m._evalUrl(d.src):m.globalEval((d.text||d.textContent||d.innerHTML||"").replace(qb,"")));i=c=null}return this}}),m.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){m.fn[a]=function(a){for(var c,d=0,e=[],g=m(a),h=g.length-1;h>=d;d++)c=d===h?this:this.clone(!0),m(g[d])[b](c),f.apply(e,c.get());return this.pushStack(e)}});var Cb,Db={};function Eb(b,c){var d,e=m(c.createElement(b)).appendTo(c.body),f=a.getDefaultComputedStyle&&(d=a.getDefaultComputedStyle(e[0]))?d.display:m.css(e[0],"display");return e.detach(),f}function Fb(a){var b=y,c=Db[a];return c||(c=Eb(a,b),"none"!==c&&c||(Cb=(Cb||m("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=(Cb[0].contentWindow||Cb[0].contentDocument).document,b.write(),b.close(),c=Eb(a,b),Cb.detach()),Db[a]=c),c}!function(){var a;k.shrinkWrapBlocks=function(){if(null!=a)return a;a=!1;var b,c,d;return c=y.getElementsByTagName("body")[0],c&&c.style?(b=y.createElement("div"),d=y.createElement("div"),d.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(d).appendChild(b),typeof b.style.zoom!==K&&(b.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1",b.appendChild(y.createElement("div")).style.width="5px",a=3!==b.offsetWidth),c.removeChild(d),a):void 0}}();var Gb=/^margin/,Hb=new RegExp("^("+S+")(?!px)[a-z%]+$","i"),Ib,Jb,Kb=/^(top|right|bottom|left)$/;a.getComputedStyle?(Ib=function(a){return a.ownerDocument.defaultView.getComputedStyle(a,null)},Jb=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ib(a),g=c?c.getPropertyValue(b)||c[b]:void 0,c&&(""!==g||m.contains(a.ownerDocument,a)||(g=m.style(a,b)),Hb.test(g)&&Gb.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f)),void 0===g?g:g+""}):y.documentElement.currentStyle&&(Ib=function(a){return a.currentStyle},Jb=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ib(a),g=c?c[b]:void 0,null==g&&h&&h[b]&&(g=h[b]),Hb.test(g)&&!Kb.test(b)&&(d=h.left,e=a.runtimeStyle,f=e&&e.left,f&&(e.left=a.currentStyle.left),h.left="fontSize"===b?"1em":g,g=h.pixelLeft+"px",h.left=d,f&&(e.left=f)),void 0===g?g:g+""||"auto"});function Lb(a,b){return{get:function(){var c=a();if(null!=c)return c?void delete this.get:(this.get=b).apply(this,arguments)}}}!function(){var b,c,d,e,f,g,h;if(b=y.createElement("div"),b.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",d=b.getElementsByTagName("a")[0],c=d&&d.style){c.cssText="float:left;opacity:.5",k.opacity="0.5"===c.opacity,k.cssFloat=!!c.cssFloat,b.style.backgroundClip="content-box",b.cloneNode(!0).style.backgroundClip="",k.clearCloneStyle="content-box"===b.style.backgroundClip,k.boxSizing=""===c.boxSizing||""===c.MozBoxSizing||""===c.WebkitBoxSizing,m.extend(k,{reliableHiddenOffsets:function(){return null==g&&i(),g},boxSizingReliable:function(){return null==f&&i(),f},pixelPosition:function(){return null==e&&i(),e},reliableMarginRight:function(){return null==h&&i(),h}});function i(){var b,c,d,i;c=y.getElementsByTagName("body")[0],c&&c.style&&(b=y.createElement("div"),d=y.createElement("div"),d.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(d).appendChild(b),b.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute",e=f=!1,h=!0,a.getComputedStyle&&(e="1%"!==(a.getComputedStyle(b,null)||{}).top,f="4px"===(a.getComputedStyle(b,null)||{width:"4px"}).width,i=b.appendChild(y.createElement("div")),i.style.cssText=b.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",i.style.marginRight=i.style.width="0",b.style.width="1px",h=!parseFloat((a.getComputedStyle(i,null)||{}).marginRight)),b.innerHTML="<table><tr><td></td><td>t</td></tr></table>",i=b.getElementsByTagName("td"),i[0].style.cssText="margin:0;border:0;padding:0;display:none",g=0===i[0].offsetHeight,g&&(i[0].style.display="",i[1].style.display="none",g=0===i[0].offsetHeight),c.removeChild(d))}}}(),m.swap=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e};var Mb=/alpha\([^)]*\)/i,Nb=/opacity\s*=\s*([^)]*)/,Ob=/^(none|table(?!-c[ea]).+)/,Pb=new RegExp("^("+S+")(.*)$","i"),Qb=new RegExp("^([+-])=("+S+")","i"),Rb={position:"absolute",visibility:"hidden",display:"block"},Sb={letterSpacing:"0",fontWeight:"400"},Tb=["Webkit","O","Moz","ms"];function Ub(a,b){if(b in a)return b;var c=b.charAt(0).toUpperCase()+b.slice(1),d=b,e=Tb.length;while(e--)if(b=Tb[e]+c,b in a)return b;return d}function Vb(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=m._data(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&U(d)&&(f[g]=m._data(d,"olddisplay",Fb(d.nodeName)))):(e=U(d),(c&&"none"!==c||!e)&&m._data(d,"olddisplay",e?c:m.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}function Wb(a,b,c){var d=Pb.exec(b);return d?Math.max(0,d[1]-(c||0))+(d[2]||"px"):b}function Xb(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=m.css(a,c+T[f],!0,e)),d?("content"===c&&(g-=m.css(a,"padding"+T[f],!0,e)),"margin"!==c&&(g-=m.css(a,"border"+T[f]+"Width",!0,e))):(g+=m.css(a,"padding"+T[f],!0,e),"padding"!==c&&(g+=m.css(a,"border"+T[f]+"Width",!0,e)));return g}function Yb(a,b,c){var d=!0,e="width"===b?a.offsetWidth:a.offsetHeight,f=Ib(a),g=k.boxSizing&&"border-box"===m.css(a,"boxSizing",!1,f);if(0>=e||null==e){if(e=Jb(a,b,f),(0>e||null==e)&&(e=a.style[b]),Hb.test(e))return e;d=g&&(k.boxSizingReliable()||e===a.style[b]),e=parseFloat(e)||0}return e+Xb(a,b,c||(g?"border":"content"),d,f)+"px"}m.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Jb(a,"opacity");return""===c?"1":c}}}},cssNumber:{columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":k.cssFloat?"cssFloat":"styleFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=m.camelCase(b),i=a.style;if(b=m.cssProps[h]||(m.cssProps[h]=Ub(i,h)),g=m.cssHooks[b]||m.cssHooks[h],void 0===c)return g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b];if(f=typeof c,"string"===f&&(e=Qb.exec(c))&&(c=(e[1]+1)*e[2]+parseFloat(m.css(a,b)),f="number"),null!=c&&c===c&&("number"!==f||m.cssNumber[h]||(c+="px"),k.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),!(g&&"set"in g&&void 0===(c=g.set(a,c,d)))))try{i[b]=c}catch(j){}}},css:function(a,b,c,d){var e,f,g,h=m.camelCase(b);return b=m.cssProps[h]||(m.cssProps[h]=Ub(a.style,h)),g=m.cssHooks[b]||m.cssHooks[h],g&&"get"in g&&(f=g.get(a,!0,c)),void 0===f&&(f=Jb(a,b,d)),"normal"===f&&b in Sb&&(f=Sb[b]),""===c||c?(e=parseFloat(f),c===!0||m.isNumeric(e)?e||0:f):f}}),m.each(["height","width"],function(a,b){m.cssHooks[b]={get:function(a,c,d){return c?Ob.test(m.css(a,"display"))&&0===a.offsetWidth?m.swap(a,Rb,function(){return Yb(a,b,d)}):Yb(a,b,d):void 0},set:function(a,c,d){var e=d&&Ib(a);return Wb(a,c,d?Xb(a,b,d,k.boxSizing&&"border-box"===m.css(a,"boxSizing",!1,e),e):0)}}}),k.opacity||(m.cssHooks.opacity={get:function(a,b){return Nb.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?.01*parseFloat(RegExp.$1)+"":b?"1":""},set:function(a,b){var c=a.style,d=a.currentStyle,e=m.isNumeric(b)?"alpha(opacity="+100*b+")":"",f=d&&d.filter||c.filter||"";c.zoom=1,(b>=1||""===b)&&""===m.trim(f.replace(Mb,""))&&c.removeAttribute&&(c.removeAttribute("filter"),""===b||d&&!d.filter)||(c.filter=Mb.test(f)?f.replace(Mb,e):f+" "+e)}}),m.cssHooks.marginRight=Lb(k.reliableMarginRight,function(a,b){return b?m.swap(a,{display:"inline-block"},Jb,[a,"marginRight"]):void 0}),m.each({margin:"",padding:"",border:"Width"},function(a,b){m.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+T[d]+b]=f[d]||f[d-2]||f[0];return e}},Gb.test(a)||(m.cssHooks[a+b].set=Wb)}),m.fn.extend({css:function(a,b){return V(this,function(a,b,c){var d,e,f={},g=0;if(m.isArray(b)){for(d=Ib(a),e=b.length;e>g;g++)f[b[g]]=m.css(a,b[g],!1,d);return f}return void 0!==c?m.style(a,b,c):m.css(a,b)},a,b,arguments.length>1)},show:function(){return Vb(this,!0)},hide:function(){return Vb(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){U(this)?m(this).show():m(this).hide()})}});function Zb(a,b,c,d,e){return new Zb.prototype.init(a,b,c,d,e)}m.Tween=Zb,Zb.prototype={constructor:Zb,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||"swing",this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(m.cssNumber[c]?"":"px")
},cur:function(){var a=Zb.propHooks[this.prop];return a&&a.get?a.get(this):Zb.propHooks._default.get(this)},run:function(a){var b,c=Zb.propHooks[this.prop];return this.pos=b=this.options.duration?m.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):Zb.propHooks._default.set(this),this}},Zb.prototype.init.prototype=Zb.prototype,Zb.propHooks={_default:{get:function(a){var b;return null==a.elem[a.prop]||a.elem.style&&null!=a.elem.style[a.prop]?(b=m.css(a.elem,a.prop,""),b&&"auto"!==b?b:0):a.elem[a.prop]},set:function(a){m.fx.step[a.prop]?m.fx.step[a.prop](a):a.elem.style&&(null!=a.elem.style[m.cssProps[a.prop]]||m.cssHooks[a.prop])?m.style(a.elem,a.prop,a.now+a.unit):a.elem[a.prop]=a.now}}},Zb.propHooks.scrollTop=Zb.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},m.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2}},m.fx=Zb.prototype.init,m.fx.step={};var $b,_b,ac=/^(?:toggle|show|hide)$/,bc=new RegExp("^(?:([+-])=|)("+S+")([a-z%]*)$","i"),cc=/queueHooks$/,dc=[ic],ec={"*":[function(a,b){var c=this.createTween(a,b),d=c.cur(),e=bc.exec(b),f=e&&e[3]||(m.cssNumber[a]?"":"px"),g=(m.cssNumber[a]||"px"!==f&&+d)&&bc.exec(m.css(c.elem,a)),h=1,i=20;if(g&&g[3]!==f){f=f||g[3],e=e||[],g=+d||1;do h=h||".5",g/=h,m.style(c.elem,a,g+f);while(h!==(h=c.cur()/d)&&1!==h&&--i)}return e&&(g=c.start=+g||+d||0,c.unit=f,c.end=e[1]?g+(e[1]+1)*e[2]:+e[2]),c}]};function fc(){return setTimeout(function(){$b=void 0}),$b=m.now()}function gc(a,b){var c,d={height:a},e=0;for(b=b?1:0;4>e;e+=2-b)c=T[e],d["margin"+c]=d["padding"+c]=a;return b&&(d.opacity=d.width=a),d}function hc(a,b,c){for(var d,e=(ec[b]||[]).concat(ec["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function ic(a,b,c){var d,e,f,g,h,i,j,l,n=this,o={},p=a.style,q=a.nodeType&&U(a),r=m._data(a,"fxshow");c.queue||(h=m._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,n.always(function(){n.always(function(){h.unqueued--,m.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[p.overflow,p.overflowX,p.overflowY],j=m.css(a,"display"),l="none"===j?m._data(a,"olddisplay")||Fb(a.nodeName):j,"inline"===l&&"none"===m.css(a,"float")&&(k.inlineBlockNeedsLayout&&"inline"!==Fb(a.nodeName)?p.zoom=1:p.display="inline-block")),c.overflow&&(p.overflow="hidden",k.shrinkWrapBlocks()||n.always(function(){p.overflow=c.overflow[0],p.overflowX=c.overflow[1],p.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],ac.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(q?"hide":"show")){if("show"!==e||!r||void 0===r[d])continue;q=!0}o[d]=r&&r[d]||m.style(a,d)}else j=void 0;if(m.isEmptyObject(o))"inline"===("none"===j?Fb(a.nodeName):j)&&(p.display=j);else{r?"hidden"in r&&(q=r.hidden):r=m._data(a,"fxshow",{}),f&&(r.hidden=!q),q?m(a).show():n.done(function(){m(a).hide()}),n.done(function(){var b;m._removeData(a,"fxshow");for(b in o)m.style(a,b,o[b])});for(d in o)g=hc(q?r[d]:0,d,n),d in r||(r[d]=g.start,q&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function jc(a,b){var c,d,e,f,g;for(c in a)if(d=m.camelCase(c),e=b[d],f=a[c],m.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=m.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function kc(a,b,c){var d,e,f=0,g=dc.length,h=m.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=$b||fc(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:m.extend({},b),opts:m.extend(!0,{specialEasing:{}},c),originalProperties:b,originalOptions:c,startTime:$b||fc(),duration:c.duration,tweens:[],createTween:function(b,c){var d=m.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?h.resolveWith(a,[j,b]):h.rejectWith(a,[j,b]),this}}),k=j.props;for(jc(k,j.opts.specialEasing);g>f;f++)if(d=dc[f].call(j,a,k,j.opts))return d;return m.map(k,hc,j),m.isFunction(j.opts.start)&&j.opts.start.call(a,j),m.fx.timer(m.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}m.Animation=m.extend(kc,{tweener:function(a,b){m.isFunction(a)?(b=a,a=["*"]):a=a.split(" ");for(var c,d=0,e=a.length;e>d;d++)c=a[d],ec[c]=ec[c]||[],ec[c].unshift(b)},prefilter:function(a,b){b?dc.unshift(a):dc.push(a)}}),m.speed=function(a,b,c){var d=a&&"object"==typeof a?m.extend({},a):{complete:c||!c&&b||m.isFunction(a)&&a,duration:a,easing:c&&b||b&&!m.isFunction(b)&&b};return d.duration=m.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in m.fx.speeds?m.fx.speeds[d.duration]:m.fx.speeds._default,(null==d.queue||d.queue===!0)&&(d.queue="fx"),d.old=d.complete,d.complete=function(){m.isFunction(d.old)&&d.old.call(this),d.queue&&m.dequeue(this,d.queue)},d},m.fn.extend({fadeTo:function(a,b,c,d){return this.filter(U).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=m.isEmptyObject(a),f=m.speed(b,c,d),g=function(){var b=kc(this,m.extend({},a),f);(e||m._data(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=m.timers,g=m._data(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&cc.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));(b||!c)&&m.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=m._data(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=m.timers,g=d?d.length:0;for(c.finish=!0,m.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),m.each(["toggle","show","hide"],function(a,b){var c=m.fn[b];m.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(gc(b,!0),a,d,e)}}),m.each({slideDown:gc("show"),slideUp:gc("hide"),slideToggle:gc("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){m.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),m.timers=[],m.fx.tick=function(){var a,b=m.timers,c=0;for($b=m.now();c<b.length;c++)a=b[c],a()||b[c]!==a||b.splice(c--,1);b.length||m.fx.stop(),$b=void 0},m.fx.timer=function(a){m.timers.push(a),a()?m.fx.start():m.timers.pop()},m.fx.interval=13,m.fx.start=function(){_b||(_b=setInterval(m.fx.tick,m.fx.interval))},m.fx.stop=function(){clearInterval(_b),_b=null},m.fx.speeds={slow:600,fast:200,_default:400},m.fn.delay=function(a,b){return a=m.fx?m.fx.speeds[a]||a:a,b=b||"fx",this.queue(b,function(b,c){var d=setTimeout(b,a);c.stop=function(){clearTimeout(d)}})},function(){var a,b,c,d,e;b=y.createElement("div"),b.setAttribute("className","t"),b.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",d=b.getElementsByTagName("a")[0],c=y.createElement("select"),e=c.appendChild(y.createElement("option")),a=b.getElementsByTagName("input")[0],d.style.cssText="top:1px",k.getSetAttribute="t"!==b.className,k.style=/top/.test(d.getAttribute("style")),k.hrefNormalized="/a"===d.getAttribute("href"),k.checkOn=!!a.value,k.optSelected=e.selected,k.enctype=!!y.createElement("form").enctype,c.disabled=!0,k.optDisabled=!e.disabled,a=y.createElement("input"),a.setAttribute("value",""),k.input=""===a.getAttribute("value"),a.value="t",a.setAttribute("type","radio"),k.radioValue="t"===a.value}();var lc=/\r/g;m.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=m.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,m(this).val()):a,null==e?e="":"number"==typeof e?e+="":m.isArray(e)&&(e=m.map(e,function(a){return null==a?"":a+""})),b=m.valHooks[this.type]||m.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=m.valHooks[e.type]||m.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(lc,""):null==c?"":c)}}}),m.extend({valHooks:{option:{get:function(a){var b=m.find.attr(a,"value");return null!=b?b:m.trim(m.text(a))}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],!(!c.selected&&i!==e||(k.optDisabled?c.disabled:null!==c.getAttribute("disabled"))||c.parentNode.disabled&&m.nodeName(c.parentNode,"optgroup"))){if(b=m(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=m.makeArray(b),g=e.length;while(g--)if(d=e[g],m.inArray(m.valHooks.option.get(d),f)>=0)try{d.selected=c=!0}catch(h){d.scrollHeight}else d.selected=!1;return c||(a.selectedIndex=-1),e}}}}),m.each(["radio","checkbox"],function(){m.valHooks[this]={set:function(a,b){return m.isArray(b)?a.checked=m.inArray(m(a).val(),b)>=0:void 0}},k.checkOn||(m.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var mc,nc,oc=m.expr.attrHandle,pc=/^(?:checked|selected)$/i,qc=k.getSetAttribute,rc=k.input;m.fn.extend({attr:function(a,b){return V(this,m.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){m.removeAttr(this,a)})}}),m.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(a&&3!==f&&8!==f&&2!==f)return typeof a.getAttribute===K?m.prop(a,b,c):(1===f&&m.isXMLDoc(a)||(b=b.toLowerCase(),d=m.attrHooks[b]||(m.expr.match.bool.test(b)?nc:mc)),void 0===c?d&&"get"in d&&null!==(e=d.get(a,b))?e:(e=m.find.attr(a,b),null==e?void 0:e):null!==c?d&&"set"in d&&void 0!==(e=d.set(a,c,b))?e:(a.setAttribute(b,c+""),c):void m.removeAttr(a,b))},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(E);if(f&&1===a.nodeType)while(c=f[e++])d=m.propFix[c]||c,m.expr.match.bool.test(c)?rc&&qc||!pc.test(c)?a[d]=!1:a[m.camelCase("default-"+c)]=a[d]=!1:m.attr(a,c,""),a.removeAttribute(qc?c:d)},attrHooks:{type:{set:function(a,b){if(!k.radioValue&&"radio"===b&&m.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}}}),nc={set:function(a,b,c){return b===!1?m.removeAttr(a,c):rc&&qc||!pc.test(c)?a.setAttribute(!qc&&m.propFix[c]||c,c):a[m.camelCase("default-"+c)]=a[c]=!0,c}},m.each(m.expr.match.bool.source.match(/\w+/g),function(a,b){var c=oc[b]||m.find.attr;oc[b]=rc&&qc||!pc.test(b)?function(a,b,d){var e,f;return d||(f=oc[b],oc[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,oc[b]=f),e}:function(a,b,c){return c?void 0:a[m.camelCase("default-"+b)]?b.toLowerCase():null}}),rc&&qc||(m.attrHooks.value={set:function(a,b,c){return m.nodeName(a,"input")?void(a.defaultValue=b):mc&&mc.set(a,b,c)}}),qc||(mc={set:function(a,b,c){var d=a.getAttributeNode(c);return d||a.setAttributeNode(d=a.ownerDocument.createAttribute(c)),d.value=b+="","value"===c||b===a.getAttribute(c)?b:void 0}},oc.id=oc.name=oc.coords=function(a,b,c){var d;return c?void 0:(d=a.getAttributeNode(b))&&""!==d.value?d.value:null},m.valHooks.button={get:function(a,b){var c=a.getAttributeNode(b);return c&&c.specified?c.value:void 0},set:mc.set},m.attrHooks.contenteditable={set:function(a,b,c){mc.set(a,""===b?!1:b,c)}},m.each(["width","height"],function(a,b){m.attrHooks[b]={set:function(a,c){return""===c?(a.setAttribute(b,"auto"),c):void 0}}})),k.style||(m.attrHooks.style={get:function(a){return a.style.cssText||void 0},set:function(a,b){return a.style.cssText=b+""}});var sc=/^(?:input|select|textarea|button|object)$/i,tc=/^(?:a|area)$/i;m.fn.extend({prop:function(a,b){return V(this,m.prop,a,b,arguments.length>1)},removeProp:function(a){return a=m.propFix[a]||a,this.each(function(){try{this[a]=void 0,delete this[a]}catch(b){}})}}),m.extend({propFix:{"for":"htmlFor","class":"className"},prop:function(a,b,c){var d,e,f,g=a.nodeType;if(a&&3!==g&&8!==g&&2!==g)return f=1!==g||!m.isXMLDoc(a),f&&(b=m.propFix[b]||b,e=m.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=m.find.attr(a,"tabindex");return b?parseInt(b,10):sc.test(a.nodeName)||tc.test(a.nodeName)&&a.href?0:-1}}}}),k.hrefNormalized||m.each(["href","src"],function(a,b){m.propHooks[b]={get:function(a){return a.getAttribute(b,4)}}}),k.optSelected||(m.propHooks.selected={get:function(a){var b=a.parentNode;return b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex),null}}),m.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){m.propFix[this.toLowerCase()]=this}),k.enctype||(m.propFix.enctype="encoding");var uc=/[\t\r\n\f]/g;m.fn.extend({addClass:function(a){var b,c,d,e,f,g,h=0,i=this.length,j="string"==typeof a&&a;if(m.isFunction(a))return this.each(function(b){m(this).addClass(a.call(this,b,this.className))});if(j)for(b=(a||"").match(E)||[];i>h;h++)if(c=this[h],d=1===c.nodeType&&(c.className?(" "+c.className+" ").replace(uc," "):" ")){f=0;while(e=b[f++])d.indexOf(" "+e+" ")<0&&(d+=e+" ");g=m.trim(d),c.className!==g&&(c.className=g)}return this},removeClass:function(a){var b,c,d,e,f,g,h=0,i=this.length,j=0===arguments.length||"string"==typeof a&&a;if(m.isFunction(a))return this.each(function(b){m(this).removeClass(a.call(this,b,this.className))});if(j)for(b=(a||"").match(E)||[];i>h;h++)if(c=this[h],d=1===c.nodeType&&(c.className?(" "+c.className+" ").replace(uc," "):"")){f=0;while(e=b[f++])while(d.indexOf(" "+e+" ")>=0)d=d.replace(" "+e+" "," ");g=a?m.trim(d):"",c.className!==g&&(c.className=g)}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):this.each(m.isFunction(a)?function(c){m(this).toggleClass(a.call(this,c,this.className,b),b)}:function(){if("string"===c){var b,d=0,e=m(this),f=a.match(E)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else(c===K||"boolean"===c)&&(this.className&&m._data(this,"__className__",this.className),this.className=this.className||a===!1?"":m._data(this,"__className__")||"")})},hasClass:function(a){for(var b=" "+a+" ",c=0,d=this.length;d>c;c++)if(1===this[c].nodeType&&(" "+this[c].className+" ").replace(uc," ").indexOf(b)>=0)return!0;return!1}}),m.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){m.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),m.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)},bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}});var vc=m.now(),wc=/\?/,xc=/(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;m.parseJSON=function(b){if(a.JSON&&a.JSON.parse)return a.JSON.parse(b+"");var c,d=null,e=m.trim(b+"");return e&&!m.trim(e.replace(xc,function(a,b,e,f){return c&&b&&(d=0),0===d?a:(c=e||b,d+=!f-!e,"")}))?Function("return "+e)():m.error("Invalid JSON: "+b)},m.parseXML=function(b){var c,d;if(!b||"string"!=typeof b)return null;try{a.DOMParser?(d=new DOMParser,c=d.parseFromString(b,"text/xml")):(c=new ActiveXObject("Microsoft.XMLDOM"),c.async="false",c.loadXML(b))}catch(e){c=void 0}return c&&c.documentElement&&!c.getElementsByTagName("parsererror").length||m.error("Invalid XML: "+b),c};var yc,zc,Ac=/#.*$/,Bc=/([?&])_=[^&]*/,Cc=/^(.*?):[ \t]*([^\r\n]*)\r?$/gm,Dc=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Ec=/^(?:GET|HEAD)$/,Fc=/^\/\//,Gc=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,Hc={},Ic={},Jc="*/".concat("*");try{zc=location.href}catch(Kc){zc=y.createElement("a"),zc.href="",zc=zc.href}yc=Gc.exec(zc.toLowerCase())||[];function Lc(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(E)||[];if(m.isFunction(c))while(d=f[e++])"+"===d.charAt(0)?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function Mc(a,b,c,d){var e={},f=a===Ic;function g(h){var i;return e[h]=!0,m.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function Nc(a,b){var c,d,e=m.ajaxSettings.flatOptions||{};for(d in b)void 0!==b[d]&&((e[d]?a:c||(c={}))[d]=b[d]);return c&&m.extend(!0,a,c),a}function Oc(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===e&&(e=a.mimeType||b.getResponseHeader("Content-Type"));if(e)for(g in h)if(h[g]&&h[g].test(e)){i.unshift(g);break}if(i[0]in c)f=i[0];else{for(g in c){if(!i[0]||a.converters[g+" "+i[0]]){f=g;break}d||(d=g)}f=f||d}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function Pc(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}m.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:zc,type:"GET",isLocal:Dc.test(yc[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Jc,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":m.parseJSON,"text xml":m.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?Nc(Nc(a,m.ajaxSettings),b):Nc(m.ajaxSettings,a)},ajaxPrefilter:Lc(Hc),ajaxTransport:Lc(Ic),ajax:function(a,b){"object"==typeof a&&(b=a,a=void 0),b=b||{};var c,d,e,f,g,h,i,j,k=m.ajaxSetup({},b),l=k.context||k,n=k.context&&(l.nodeType||l.jquery)?m(l):m.event,o=m.Deferred(),p=m.Callbacks("once memory"),q=k.statusCode||{},r={},s={},t=0,u="canceled",v={readyState:0,getResponseHeader:function(a){var b;if(2===t){if(!j){j={};while(b=Cc.exec(f))j[b[1].toLowerCase()]=b[2]}b=j[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===t?f:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return t||(a=s[c]=s[c]||a,r[a]=b),this},overrideMimeType:function(a){return t||(k.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>t)for(b in a)q[b]=[q[b],a[b]];else v.always(a[v.status]);return this},abort:function(a){var b=a||u;return i&&i.abort(b),x(0,b),this}};if(o.promise(v).complete=p.add,v.success=v.done,v.error=v.fail,k.url=((a||k.url||zc)+"").replace(Ac,"").replace(Fc,yc[1]+"//"),k.type=b.method||b.type||k.method||k.type,k.dataTypes=m.trim(k.dataType||"*").toLowerCase().match(E)||[""],null==k.crossDomain&&(c=Gc.exec(k.url.toLowerCase()),k.crossDomain=!(!c||c[1]===yc[1]&&c[2]===yc[2]&&(c[3]||("http:"===c[1]?"80":"443"))===(yc[3]||("http:"===yc[1]?"80":"443")))),k.data&&k.processData&&"string"!=typeof k.data&&(k.data=m.param(k.data,k.traditional)),Mc(Hc,k,b,v),2===t)return v;h=k.global,h&&0===m.active++&&m.event.trigger("ajaxStart"),k.type=k.type.toUpperCase(),k.hasContent=!Ec.test(k.type),e=k.url,k.hasContent||(k.data&&(e=k.url+=(wc.test(e)?"&":"?")+k.data,delete k.data),k.cache===!1&&(k.url=Bc.test(e)?e.replace(Bc,"$1_="+vc++):e+(wc.test(e)?"&":"?")+"_="+vc++)),k.ifModified&&(m.lastModified[e]&&v.setRequestHeader("If-Modified-Since",m.lastModified[e]),m.etag[e]&&v.setRequestHeader("If-None-Match",m.etag[e])),(k.data&&k.hasContent&&k.contentType!==!1||b.contentType)&&v.setRequestHeader("Content-Type",k.contentType),v.setRequestHeader("Accept",k.dataTypes[0]&&k.accepts[k.dataTypes[0]]?k.accepts[k.dataTypes[0]]+("*"!==k.dataTypes[0]?", "+Jc+"; q=0.01":""):k.accepts["*"]);for(d in k.headers)v.setRequestHeader(d,k.headers[d]);if(k.beforeSend&&(k.beforeSend.call(l,v,k)===!1||2===t))return v.abort();u="abort";for(d in{success:1,error:1,complete:1})v[d](k[d]);if(i=Mc(Ic,k,b,v)){v.readyState=1,h&&n.trigger("ajaxSend",[v,k]),k.async&&k.timeout>0&&(g=setTimeout(function(){v.abort("timeout")},k.timeout));try{t=1,i.send(r,x)}catch(w){if(!(2>t))throw w;x(-1,w)}}else x(-1,"No Transport");function x(a,b,c,d){var j,r,s,u,w,x=b;2!==t&&(t=2,g&&clearTimeout(g),i=void 0,f=d||"",v.readyState=a>0?4:0,j=a>=200&&300>a||304===a,c&&(u=Oc(k,v,c)),u=Pc(k,u,v,j),j?(k.ifModified&&(w=v.getResponseHeader("Last-Modified"),w&&(m.lastModified[e]=w),w=v.getResponseHeader("etag"),w&&(m.etag[e]=w)),204===a||"HEAD"===k.type?x="nocontent":304===a?x="notmodified":(x=u.state,r=u.data,s=u.error,j=!s)):(s=x,(a||!x)&&(x="error",0>a&&(a=0))),v.status=a,v.statusText=(b||x)+"",j?o.resolveWith(l,[r,x,v]):o.rejectWith(l,[v,x,s]),v.statusCode(q),q=void 0,h&&n.trigger(j?"ajaxSuccess":"ajaxError",[v,k,j?r:s]),p.fireWith(l,[v,x]),h&&(n.trigger("ajaxComplete",[v,k]),--m.active||m.event.trigger("ajaxStop")))}return v},getJSON:function(a,b,c){return m.get(a,b,c,"json")},getScript:function(a,b){return m.get(a,void 0,b,"script")}}),m.each(["get","post"],function(a,b){m[b]=function(a,c,d,e){return m.isFunction(c)&&(e=e||d,d=c,c=void 0),m.ajax({url:a,type:b,dataType:e,data:c,success:d})}}),m.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){m.fn[b]=function(a){return this.on(b,a)}}),m._evalUrl=function(a){return m.ajax({url:a,type:"GET",dataType:"script",async:!1,global:!1,"throws":!0})},m.fn.extend({wrapAll:function(a){if(m.isFunction(a))return this.each(function(b){m(this).wrapAll(a.call(this,b))});if(this[0]){var b=m(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&1===a.firstChild.nodeType)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){return this.each(m.isFunction(a)?function(b){m(this).wrapInner(a.call(this,b))}:function(){var b=m(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=m.isFunction(a);return this.each(function(c){m(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){m.nodeName(this,"body")||m(this).replaceWith(this.childNodes)}).end()}}),m.expr.filters.hidden=function(a){return a.offsetWidth<=0&&a.offsetHeight<=0||!k.reliableHiddenOffsets()&&"none"===(a.style&&a.style.display||m.css(a,"display"))},m.expr.filters.visible=function(a){return!m.expr.filters.hidden(a)};var Qc=/%20/g,Rc=/\[\]$/,Sc=/\r?\n/g,Tc=/^(?:submit|button|image|reset|file)$/i,Uc=/^(?:input|select|textarea|keygen)/i;function Vc(a,b,c,d){var e;if(m.isArray(b))m.each(b,function(b,e){c||Rc.test(a)?d(a,e):Vc(a+"["+("object"==typeof e?b:"")+"]",e,c,d)});else if(c||"object"!==m.type(b))d(a,b);else for(e in b)Vc(a+"["+e+"]",b[e],c,d)}m.param=function(a,b){var c,d=[],e=function(a,b){b=m.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=m.ajaxSettings&&m.ajaxSettings.traditional),m.isArray(a)||a.jquery&&!m.isPlainObject(a))m.each(a,function(){e(this.name,this.value)});else for(c in a)Vc(c,a[c],b,e);return d.join("&").replace(Qc,"+")},m.fn.extend({serialize:function(){return m.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=m.prop(this,"elements");return a?m.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!m(this).is(":disabled")&&Uc.test(this.nodeName)&&!Tc.test(a)&&(this.checked||!W.test(a))}).map(function(a,b){var c=m(this).val();return null==c?null:m.isArray(c)?m.map(c,function(a){return{name:b.name,value:a.replace(Sc,"\r\n")}}):{name:b.name,value:c.replace(Sc,"\r\n")}}).get()}}),m.ajaxSettings.xhr=void 0!==a.ActiveXObject?function(){return!this.isLocal&&/^(get|post|head|put|delete|options)$/i.test(this.type)&&Zc()||$c()}:Zc;var Wc=0,Xc={},Yc=m.ajaxSettings.xhr();a.ActiveXObject&&m(a).on("unload",function(){for(var a in Xc)Xc[a](void 0,!0)}),k.cors=!!Yc&&"withCredentials"in Yc,Yc=k.ajax=!!Yc,Yc&&m.ajaxTransport(function(a){if(!a.crossDomain||k.cors){var b;return{send:function(c,d){var e,f=a.xhr(),g=++Wc;if(f.open(a.type,a.url,a.async,a.username,a.password),a.xhrFields)for(e in a.xhrFields)f[e]=a.xhrFields[e];a.mimeType&&f.overrideMimeType&&f.overrideMimeType(a.mimeType),a.crossDomain||c["X-Requested-With"]||(c["X-Requested-With"]="XMLHttpRequest");for(e in c)void 0!==c[e]&&f.setRequestHeader(e,c[e]+"");f.send(a.hasContent&&a.data||null),b=function(c,e){var h,i,j;if(b&&(e||4===f.readyState))if(delete Xc[g],b=void 0,f.onreadystatechange=m.noop,e)4!==f.readyState&&f.abort();else{j={},h=f.status,"string"==typeof f.responseText&&(j.text=f.responseText);try{i=f.statusText}catch(k){i=""}h||!a.isLocal||a.crossDomain?1223===h&&(h=204):h=j.text?200:404}j&&d(h,i,j,f.getAllResponseHeaders())},a.async?4===f.readyState?setTimeout(b):f.onreadystatechange=Xc[g]=b:b()},abort:function(){b&&b(void 0,!0)}}}});function Zc(){try{return new a.XMLHttpRequest}catch(b){}}function $c(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}}m.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/(?:java|ecma)script/},converters:{"text script":function(a){return m.globalEval(a),a}}}),m.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)}),m.ajaxTransport("script",function(a){if(a.crossDomain){var b,c=y.head||m("head")[0]||y.documentElement;return{send:function(d,e){b=y.createElement("script"),b.async=!0,a.scriptCharset&&(b.charset=a.scriptCharset),b.src=a.url,b.onload=b.onreadystatechange=function(a,c){(c||!b.readyState||/loaded|complete/.test(b.readyState))&&(b.onload=b.onreadystatechange=null,b.parentNode&&b.parentNode.removeChild(b),b=null,c||e(200,"success"))},c.insertBefore(b,c.firstChild)},abort:function(){b&&b.onload(void 0,!0)}}}});var _c=[],ad=/(=)\?(?=&|$)|\?\?/;m.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=_c.pop()||m.expando+"_"+vc++;return this[a]=!0,a}}),m.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(ad.test(b.url)?"url":"string"==typeof b.data&&!(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&ad.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=m.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(ad,"$1"+e):b.jsonp!==!1&&(b.url+=(wc.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||m.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,_c.push(e)),g&&m.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),m.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||y;var d=u.exec(a),e=!c&&[];return d?[b.createElement(d[1])]:(d=m.buildFragment([a],b,e),e&&e.length&&m(e).remove(),m.merge([],d.childNodes))};var bd=m.fn.load;m.fn.load=function(a,b,c){if("string"!=typeof a&&bd)return bd.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>=0&&(d=m.trim(a.slice(h,a.length)),a=a.slice(0,h)),m.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(f="POST"),g.length>0&&m.ajax({url:a,type:f,dataType:"html",data:b}).done(function(a){e=arguments,g.html(d?m("<div>").append(m.parseHTML(a)).find(d):a)}).complete(c&&function(a,b){g.each(c,e||[a.responseText,b,a])}),this},m.expr.filters.animated=function(a){return m.grep(m.timers,function(b){return a===b.elem}).length};var cd=a.document.documentElement;function dd(a){return m.isWindow(a)?a:9===a.nodeType?a.defaultView||a.parentWindow:!1}m.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=m.css(a,"position"),l=m(a),n={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=m.css(a,"top"),i=m.css(a,"left"),j=("absolute"===k||"fixed"===k)&&m.inArray("auto",[f,i])>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),m.isFunction(b)&&(b=b.call(a,c,h)),null!=b.top&&(n.top=b.top-h.top+g),null!=b.left&&(n.left=b.left-h.left+e),"using"in b?b.using.call(a,n):l.css(n)}},m.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){m.offset.setOffset(this,a,b)});var b,c,d={top:0,left:0},e=this[0],f=e&&e.ownerDocument;if(f)return b=f.documentElement,m.contains(b,e)?(typeof e.getBoundingClientRect!==K&&(d=e.getBoundingClientRect()),c=dd(f),{top:d.top+(c.pageYOffset||b.scrollTop)-(b.clientTop||0),left:d.left+(c.pageXOffset||b.scrollLeft)-(b.clientLeft||0)}):d},position:function(){if(this[0]){var a,b,c={top:0,left:0},d=this[0];return"fixed"===m.css(d,"position")?b=d.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),m.nodeName(a[0],"html")||(c=a.offset()),c.top+=m.css(a[0],"borderTopWidth",!0),c.left+=m.css(a[0],"borderLeftWidth",!0)),{top:b.top-c.top-m.css(d,"marginTop",!0),left:b.left-c.left-m.css(d,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent||cd;while(a&&!m.nodeName(a,"html")&&"static"===m.css(a,"position"))a=a.offsetParent;return a||cd})}}),m.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c=/Y/.test(b);m.fn[a]=function(d){return V(this,function(a,d,e){var f=dd(a);return void 0===e?f?b in f?f[b]:f.document.documentElement[d]:a[d]:void(f?f.scrollTo(c?m(f).scrollLeft():e,c?e:m(f).scrollTop()):a[d]=e)},a,d,arguments.length,null)}}),m.each(["top","left"],function(a,b){m.cssHooks[b]=Lb(k.pixelPosition,function(a,c){return c?(c=Jb(a,b),Hb.test(c)?m(a).position()[b]+"px":c):void 0})}),m.each({Height:"height",Width:"width"},function(a,b){m.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){m.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return V(this,function(b,c,d){var e;return m.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?m.css(b,c,g):m.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),m.fn.size=function(){return this.length},m.fn.andSelf=m.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return m});var ed=a.jQuery,fd=a.$;return m.noConflict=function(b){return a.$===m&&(a.$=fd),b&&a.jQuery===m&&(a.jQuery=ed),m},typeof b===K&&(a.jQuery=a.$=m),m});
/*!
 * jQuery Migrate - v1.2.1 - 2013-05-08
 * https://github.com/jquery/jquery-migrate
 * Copyright 2005, 2013 jQuery Foundation, Inc. and other contributors; Licensed MIT
 */
(function( jQuery, window, undefined ) {
// See http://bugs.jquery.com/ticket/13335
// "use strict";


var warnedAbout = {};

// List of warnings already given; public read only
jQuery.migrateWarnings = [];

// Set to true to prevent console output; migrateWarnings still maintained
// jQuery.migrateMute = false;

// Show a message on the console so devs know we're active
if ( !jQuery.migrateMute && window.console && window.console.log ) {
//	window.console.log("JQMIGRATE: Logging is active");
}

// Set to false to disable traces that appear with warnings
if ( jQuery.migrateTrace === undefined ) {
	jQuery.migrateTrace = true;
}

// Forget any warnings we've already given; public
jQuery.migrateReset = function() {
	warnedAbout = {};
	jQuery.migrateWarnings.length = 0;
};

function migrateWarn( msg) {
	var console = window.console;
	if ( !warnedAbout[ msg ] ) {
		warnedAbout[ msg ] = true;
		jQuery.migrateWarnings.push( msg );
//		if (typeof spnetENV_TARGET != 'undefined' || typeof spnetENV_STAGE != 'undefined') {
//			if ( console && console.warn && !jQuery.migrateMute ) {
//				console.warn( "JQMIGRATE: " + msg );
//				if ( jQuery.migrateTrace && console.trace ) {
//					console.trace();
//				}
//			}
//		}
		var timeout = 5000;
		if (typeof spNmAjax == 'function') timeout = 10;
		window.setTimeout(
			function() {
				try {
					spNmAjax({
						'sp.site': '102002',
						'sp.event': 'jqm-02',
						'sp.msg': msg
					});
				} catch (e) {}
			}, timeout);
	}
}

function migrateWarnProp( obj, prop, value, msg ) {
	if ( Object.defineProperty ) {
		// On ES5 browsers (non-oldIE), warn if the code tries to get prop;
		// allow property to be overwritten in case some other plugin wants it
		try {
			Object.defineProperty( obj, prop, {
				configurable: true,
				enumerable: true,
				get: function() {
					migrateWarn( msg );
					return value;
				},
				set: function( newValue ) {
					migrateWarn( msg );
					value = newValue;
				}
			});
			return;
		} catch( err ) {
			// IE8 is a dope about Object.defineProperty, can't warn there
		}
	}

	// Non-ES5 (or broken) browser; just set the property
	jQuery._definePropertyBroken = true;
	obj[ prop ] = value;
}

if ( document.compatMode === "BackCompat" ) {
	// jQuery has never supported or tested Quirks Mode
	migrateWarn( "jQuery is not compatible with Quirks Mode" );
}


var attrFn = jQuery( "<input/>", { size: 1 } ).attr("size") && jQuery.attrFn,
	oldAttr = jQuery.attr,
	valueAttrGet = jQuery.attrHooks.value && jQuery.attrHooks.value.get ||
		function() { return null; },
	valueAttrSet = jQuery.attrHooks.value && jQuery.attrHooks.value.set ||
		function() { return undefined; },
	rnoType = /^(?:input|button)$/i,
	rnoAttrNodeType = /^[238]$/,
	rboolean = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
	ruseDefault = /^(?:checked|selected)$/i;

// jQuery.attrFn
migrateWarnProp( jQuery, "attrFn", attrFn || {}, "jQuery.attrFn is deprecated" );

jQuery.attr = function( elem, name, value, pass ) {
	var lowerName = name.toLowerCase(),
		nType = elem && elem.nodeType;

	if ( pass ) {
		// Since pass is used internally, we only warn for new jQuery
		// versions where there isn't a pass arg in the formal params
		if ( oldAttr.length < 4 ) {
			migrateWarn("jQuery.fn.attr( props, pass ) is deprecated");
		}
		if ( elem && !rnoAttrNodeType.test( nType ) &&
			(attrFn ? name in attrFn : jQuery.isFunction(jQuery.fn[name])) ) {
			return jQuery( elem )[ name ]( value );
		}
	}

	// Warn if user tries to set `type`, since it breaks on IE 6/7/8; by checking
	// for disconnected elements we don't warn on $( "<button>", { type: "button" } ).
	if ( name === "type" && value !== undefined && rnoType.test( elem.nodeName ) && elem.parentNode ) {
		migrateWarn("Can't change the 'type' of an input or button in IE 6/7/8");
	}

	// Restore boolHook for boolean property/attribute synchronization
	if ( !jQuery.attrHooks[ lowerName ] && rboolean.test( lowerName ) ) {
		jQuery.attrHooks[ lowerName ] = {
			get: function( elem, name ) {
				// Align boolean attributes with corresponding properties
				// Fall back to attribute presence where some booleans are not supported
				var attrNode,
					property = jQuery.prop( elem, name );
				return property === true || typeof property !== "boolean" &&
					( attrNode = elem.getAttributeNode(name) ) && attrNode.nodeValue !== false ?

					name.toLowerCase() :
					undefined;
			},
			set: function( elem, value, name ) {
				var propName;
				if ( value === false ) {
					// Remove boolean attributes when set to false
					jQuery.removeAttr( elem, name );
				} else {
					// value is true since we know at this point it's type boolean and not false
					// Set boolean attributes to the same name and set the DOM property
					propName = jQuery.propFix[ name ] || name;
					if ( propName in elem ) {
						// Only set the IDL specifically if it already exists on the element
						elem[ propName ] = true;
					}

					elem.setAttribute( name, name.toLowerCase() );
				}
				return name;
			}
		};

		// Warn only for attributes that can remain distinct from their properties post-1.9
		if ( ruseDefault.test( lowerName ) ) {
			migrateWarn( "jQuery.fn.attr('" + lowerName + "') may use property instead of attribute" );
		}
	}

	return oldAttr.call( jQuery, elem, name, value );
};

// attrHooks: value
jQuery.attrHooks.value = {
	get: function( elem, name ) {
		var nodeName = ( elem.nodeName || "" ).toLowerCase();
		if ( nodeName === "button" ) {
			return valueAttrGet.apply( this, arguments );
		}
		if ( nodeName !== "input" && nodeName !== "option" ) {
			migrateWarn("jQuery.fn.attr('value') no longer gets properties");
		}
		return name in elem ?
			elem.value :
			null;
	},
	set: function( elem, value ) {
		var nodeName = ( elem.nodeName || "" ).toLowerCase();
		if ( nodeName === "button" ) {
			return valueAttrSet.apply( this, arguments );
		}
		if ( nodeName !== "input" && nodeName !== "option" ) {
			migrateWarn("jQuery.fn.attr('value', val) no longer sets properties");
		}
		// Does not return so that setAttribute is also used
		elem.value = value;
	}
};


var matched, browser,
	oldInit = jQuery.fn.init,
	oldParseJSON = jQuery.parseJSON,
	// Note: XSS check is done below after string is trimmed
	rquickExpr = /^([^<]*)(<[\w\W]+>)([^>]*)$/;

// $(html) "looks like html" rule change
jQuery.fn.init = function( selector, context, rootjQuery ) {
	var match;

	if ( selector && typeof selector === "string" && !jQuery.isPlainObject( context ) &&
			(match = rquickExpr.exec( jQuery.trim( selector ) )) && match[ 0 ] ) {
		// This is an HTML string according to the "old" rules; is it still?
		if ( selector.charAt( 0 ) !== "<" ) {
			migrateWarn("$(html) HTML strings must start with '<' character");
		}
		if ( match[ 3 ] ) {
			migrateWarn("$(html) HTML text after last tag is ignored");
		}
		// Consistently reject any HTML-like string starting with a hash (#9521)
		// Note that this may break jQuery 1.6.x code that otherwise would work.
		if ( match[ 0 ].charAt( 0 ) === "#" ) {
			migrateWarn("HTML string cannot start with a '#' character");
			jQuery.error("JQMIGRATE: Invalid selector string (XSS)");
		}
		// Now process using loose rules; let pre-1.8 play too
		if ( context && context.context ) {
			// jQuery object as context; parseHTML expects a DOM object
			context = context.context;
		}
		if ( jQuery.parseHTML ) {
			return oldInit.call( this, jQuery.parseHTML( match[ 2 ], context, true ),
					context, rootjQuery );
		}
	}
	return oldInit.apply( this, arguments );
};
jQuery.fn.init.prototype = jQuery.fn;

// Let $.parseJSON(falsy_value) return null
jQuery.parseJSON = function( json ) {
	if ( !json && json !== null ) {
		migrateWarn("jQuery.parseJSON requires a valid JSON string");
		return null;
	}
	return oldParseJSON.apply( this, arguments );
};

jQuery.uaMatch = function( ua ) {
	ua = ua.toLowerCase();

	var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
		/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
		/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
		/(msie) ([\w.]+)/.exec( ua ) ||
		ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
		[];

	return {
		browser: match[ 1 ] || "",
		version: match[ 2 ] || "0"
	};
};

// Don't clobber any existing jQuery.browser in case it's different
if ( !jQuery.browser ) {
	matched = jQuery.uaMatch( navigator.userAgent );
	browser = {};

	if ( matched.browser ) {
		browser[ matched.browser ] = true;
		browser.version = matched.version;
	}

	// Chrome is Webkit, but Webkit is also Safari.
	if ( browser.chrome ) {
		browser.webkit = true;
	} else if ( browser.webkit ) {
		browser.safari = true;
	}

	jQuery.browser = browser;
}

// Warn if the code tries to get jQuery.browser
migrateWarnProp( jQuery, "browser", jQuery.browser, "jQuery.browser is deprecated" );

jQuery.sub = function() {
	function jQuerySub( selector, context ) {
		return new jQuerySub.fn.init( selector, context );
	}
	jQuery.extend( true, jQuerySub, this );
	jQuerySub.superclass = this;
	jQuerySub.fn = jQuerySub.prototype = this();
	jQuerySub.fn.constructor = jQuerySub;
	jQuerySub.sub = this.sub;
	jQuerySub.fn.init = function init( selector, context ) {
		if ( context && context instanceof jQuery && !(context instanceof jQuerySub) ) {
			context = jQuerySub( context );
		}

		return jQuery.fn.init.call( this, selector, context, rootjQuerySub );
	};
	jQuerySub.fn.init.prototype = jQuerySub.fn;
	var rootjQuerySub = jQuerySub(document);
	migrateWarn( "jQuery.sub() is deprecated" );
	return jQuerySub;
};


// Ensure that $.ajax gets the new parseJSON defined in core.js
jQuery.ajaxSetup({
	converters: {
		"text json": jQuery.parseJSON
	}
});


var oldFnData = jQuery.fn.data;

jQuery.fn.data = function( name ) {
	var ret, evt,
		elem = this[0];

	// Handles 1.7 which has this behavior and 1.8 which doesn't
	if ( elem && name === "events" && arguments.length === 1 ) {
		ret = jQuery.data( elem, name );
		evt = jQuery._data( elem, name );
		if ( ( ret === undefined || ret === evt ) && evt !== undefined ) {
			migrateWarn("Use of jQuery.fn.data('events') is deprecated");
			return evt;
		}
	}
	return oldFnData.apply( this, arguments );
};


var rscriptType = /\/(java|ecma)script/i,
	oldSelf = jQuery.fn.andSelf || jQuery.fn.addBack;

jQuery.fn.andSelf = function() {
	migrateWarn("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()");
	return oldSelf.apply( this, arguments );
};

// Since jQuery.clean is used internally on older versions, we only shim if it's missing
if ( !jQuery.clean ) {
	jQuery.clean = function( elems, context, fragment, scripts ) {
		// Set context per 1.8 logic
		context = context || document;
		context = !context.nodeType && context[0] || context;
		context = context.ownerDocument || context;

		migrateWarn("jQuery.clean() is deprecated");

		var i, elem, handleScript, jsTags,
			ret = [];

		jQuery.merge( ret, jQuery.buildFragment( elems, context ).childNodes );

		// Complex logic lifted directly from jQuery 1.8
		if ( fragment ) {
			// Special handling of each script element
			handleScript = function( elem ) {
				// Check if we consider it executable
				if ( !elem.type || rscriptType.test( elem.type ) ) {
					// Detach the script and store it in the scripts array (if provided) or the fragment
					// Return truthy to indicate that it has been handled
					return scripts ?
						scripts.push( elem.parentNode ? elem.parentNode.removeChild( elem ) : elem ) :
						fragment.appendChild( elem );
				}
			};

			for ( i = 0; (elem = ret[i]) != null; i++ ) {
				// Check if we're done after handling an executable script
				if ( !( jQuery.nodeName( elem, "script" ) && handleScript( elem ) ) ) {
					// Append to fragment and handle embedded scripts
					fragment.appendChild( elem );
					if ( typeof elem.getElementsByTagName !== "undefined" ) {
						// handleScript alters the DOM, so use jQuery.merge to ensure snapshot iteration
						jsTags = jQuery.grep( jQuery.merge( [], elem.getElementsByTagName("script") ), handleScript );

						// Splice the scripts into ret after their former ancestor and advance our index beyond them
						ret.splice.apply( ret, [i + 1, 0].concat( jsTags ) );
						i += jsTags.length;
					}
				}
			}
		}

		return ret;
	};
}

var eventAdd = jQuery.event.add,
	eventRemove = jQuery.event.remove,
	eventTrigger = jQuery.event.trigger,
	oldToggle = jQuery.fn.toggle,
	oldLive = jQuery.fn.live,
	oldDie = jQuery.fn.die,
	ajaxEvents = "ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess",
	rajaxEvent = new RegExp( "\\b(?:" + ajaxEvents + ")\\b" ),
	rhoverHack = /(?:^|\s)hover(\.\S+|)\b/,
	hoverHack = function( events ) {
		if ( typeof( events ) !== "string" || jQuery.event.special.hover ) {
			return events;
		}
		if ( rhoverHack.test( events ) ) {
			migrateWarn("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'");
		}
		return events && events.replace( rhoverHack, "mouseenter$1 mouseleave$1" );
	};

// Event props removed in 1.9, put them back if needed; no practical way to warn them
if ( jQuery.event.props && jQuery.event.props[ 0 ] !== "attrChange" ) {
	jQuery.event.props.unshift( "attrChange", "attrName", "relatedNode", "srcElement" );
}

// Undocumented jQuery.event.handle was "deprecated" in jQuery 1.7
if ( jQuery.event.dispatch ) {
	migrateWarnProp( jQuery.event, "handle", jQuery.event.dispatch, "jQuery.event.handle is undocumented and deprecated" );
}

// Support for 'hover' pseudo-event and ajax event warnings
jQuery.event.add = function( elem, types, handler, data, selector ){
	if ( elem !== document && rajaxEvent.test( types ) ) {
		migrateWarn( "AJAX events should be attached to document: " + types );
	}
	eventAdd.call( this, elem, hoverHack( types || "" ), handler, data, selector );
};
jQuery.event.remove = function( elem, types, handler, selector, mappedTypes ){
	eventRemove.call( this, elem, hoverHack( types ) || "", handler, selector, mappedTypes );
};

jQuery.fn.error = function() {
	var args = Array.prototype.slice.call( arguments, 0);
	migrateWarn("jQuery.fn.error() is deprecated");
	args.splice( 0, 0, "error" );
	if ( arguments.length ) {
		return this.bind.apply( this, args );
	}
	// error event should not bubble to window, although it does pre-1.7
	this.triggerHandler.apply( this, args );
	return this;
};

jQuery.fn.toggle = function( fn, fn2 ) {

	// Don't mess with animation or css toggles
	if ( !jQuery.isFunction( fn ) || !jQuery.isFunction( fn2 ) ) {
		return oldToggle.apply( this, arguments );
	}
	migrateWarn("jQuery.fn.toggle(handler, handler...) is deprecated");

	// Save reference to arguments for access in closure
	var args = arguments,
		guid = fn.guid || jQuery.guid++,
		i = 0,
		toggler = function( event ) {
			// Figure out which function to execute
			var lastToggle = ( jQuery._data( this, "lastToggle" + fn.guid ) || 0 ) % i;
			jQuery._data( this, "lastToggle" + fn.guid, lastToggle + 1 );

			// Make sure that clicks stop
			event.preventDefault();

			// and execute the function
			return args[ lastToggle ].apply( this, arguments ) || false;
		};

	// link all the functions, so any of them can unbind this click handler
	toggler.guid = guid;
	while ( i < args.length ) {
		args[ i++ ].guid = guid;
	}

	return this.click( toggler );
};

jQuery.fn.live = function( types, data, fn ) {
	migrateWarn("jQuery.fn.live() is deprecated");
	if ( oldLive ) {
		return oldLive.apply( this, arguments );
	}
	jQuery( this.context ).on( types, this.selector, data, fn );
	return this;
};

jQuery.fn.die = function( types, fn ) {
	migrateWarn("jQuery.fn.die() is deprecated");
	if ( oldDie ) {
		return oldDie.apply( this, arguments );
	}
	jQuery( this.context ).off( types, this.selector || "**", fn );
	return this;
};

// Turn global events into document-triggered events
jQuery.event.trigger = function( event, data, elem, onlyHandlers  ){
	if ( !elem && !rajaxEvent.test( event ) ) {
		migrateWarn( "Global events are undocumented and deprecated" );
	}
	return eventTrigger.call( this,  event, data, elem || document, onlyHandlers  );
};
jQuery.each( ajaxEvents.split("|"),
	function( _, name ) {
		jQuery.event.special[ name ] = {
			setup: function() {
				var elem = this;

				// The document needs no shimming; must be !== for oldIE
				if ( elem !== document ) {
					jQuery.event.add( document, name + "." + jQuery.guid, function() {
						jQuery.event.trigger( name, null, elem, true );
					});
					jQuery._data( this, name, jQuery.guid++ );
				}
				return false;
			},
			teardown: function() {
				if ( this !== document ) {
					jQuery.event.remove( document, name + "." + jQuery._data( this, name ) );
				}
				return false;
			}
		};
	}
);


})( jQuery, window );
/*! jQuery UI - v1.9.2 - 2014-07-13
* http://jqueryui.com
* Includes: jquery.ui.core.js, jquery.ui.widget.js, jquery.ui.mouse.js, jquery.ui.position.js, jquery.ui.draggable.js, jquery.ui.droppable.js, jquery.ui.resizable.js, jquery.ui.selectable.js, jquery.ui.sortable.js, jquery.ui.accordion.js, jquery.ui.autocomplete.js, jquery.ui.button.js, jquery.ui.datepicker.js, jquery.ui.dialog.js, jquery.ui.menu.js, jquery.ui.progressbar.js, jquery.ui.slider.js, jquery.ui.spinner.js, jquery.ui.tabs.js, jquery.ui.tooltip.js, jquery.ui.effect.js, jquery.ui.effect-blind.js, jquery.ui.effect-bounce.js, jquery.ui.effect-clip.js, jquery.ui.effect-drop.js, jquery.ui.effect-explode.js, jquery.ui.effect-fade.js, jquery.ui.effect-fold.js, jquery.ui.effect-highlight.js, jquery.ui.effect-pulsate.js, jquery.ui.effect-scale.js, jquery.ui.effect-shake.js, jquery.ui.effect-slide.js, jquery.ui.effect-transfer.js
* Copyright 2014 jQuery Foundation and other contributors; Licensed MIT */

(function(e,t){function i(t,i){var a,n,o,r=t.nodeName.toLowerCase();return"area"===r?(a=t.parentNode,n=a.name,t.href&&n&&"map"===a.nodeName.toLowerCase()?(o=e("img[usemap=#"+n+"]")[0],!!o&&s(o)):!1):(/input|select|textarea|button|object/.test(r)?!t.disabled:"a"===r?t.href||i:i)&&s(t)}function s(t){return e.expr.filters.visible(t)&&!e(t).parents().andSelf().filter(function(){return"hidden"===e.css(this,"visibility")}).length}var a=0,n=/^ui-id-\d+$/;e.ui=e.ui||{},e.ui.version||(e.extend(e.ui,{version:"1.9.2",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}}),e.fn.extend({_focus:e.fn.focus,focus:function(t,i){return"number"==typeof t?this.each(function(){var s=this;setTimeout(function(){e(s).focus(),i&&i.call(s)},t)}):this._focus.apply(this,arguments)},scrollParent:function(){var t;return t=e.ui.ie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position"))?this.parents().filter(function(){return/(relative|absolute|fixed)/.test(e.css(this,"position"))&&/(auto|scroll)/.test(e.css(this,"overflow")+e.css(this,"overflow-y")+e.css(this,"overflow-x"))}).eq(0):this.parents().filter(function(){return/(auto|scroll)/.test(e.css(this,"overflow")+e.css(this,"overflow-y")+e.css(this,"overflow-x"))}).eq(0),/fixed/.test(this.css("position"))||!t.length?e(document):t},zIndex:function(i){if(i!==t)return this.css("zIndex",i);if(this.length)for(var s,a,n=e(this[0]);n.length&&n[0]!==document;){if(s=n.css("position"),("absolute"===s||"relative"===s||"fixed"===s)&&(a=parseInt(n.css("zIndex"),10),!isNaN(a)&&0!==a))return a;n=n.parent()}return 0},uniqueId:function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++a)})},removeUniqueId:function(){return this.each(function(){n.test(this.id)&&e(this).removeAttr("id")})}}),e.extend(e.expr[":"],{data:e.expr.createPseudo?e.expr.createPseudo(function(t){return function(i){return!!e.data(i,t)}}):function(t,i,s){return!!e.data(t,s[3])},focusable:function(t){return i(t,!isNaN(e.attr(t,"tabindex")))},tabbable:function(t){var s=e.attr(t,"tabindex"),a=isNaN(s);return(a||s>=0)&&i(t,!a)}}),e(function(){var t=document.body,i=t.appendChild(i=document.createElement("div"));i.offsetHeight,e.extend(i.style,{minHeight:"100px",height:"auto",padding:0,borderWidth:0}),e.support.minHeight=100===i.offsetHeight,e.support.selectstart="onselectstart"in i,t.removeChild(i).style.display="none"}),e("<a>").outerWidth(1).jquery||e.each(["Width","Height"],function(i,s){function a(t,i,s,a){return e.each(n,function(){i-=parseFloat(e.css(t,"padding"+this))||0,s&&(i-=parseFloat(e.css(t,"border"+this+"Width"))||0),a&&(i-=parseFloat(e.css(t,"margin"+this))||0)}),i}var n="Width"===s?["Left","Right"]:["Top","Bottom"],o=s.toLowerCase(),r={innerWidth:e.fn.innerWidth,innerHeight:e.fn.innerHeight,outerWidth:e.fn.outerWidth,outerHeight:e.fn.outerHeight};e.fn["inner"+s]=function(i){return i===t?r["inner"+s].call(this):this.each(function(){e(this).css(o,a(this,i)+"px")})},e.fn["outer"+s]=function(t,i){return"number"!=typeof t?r["outer"+s].call(this,t):this.each(function(){e(this).css(o,a(this,t,!0,i)+"px")})}}),e("<a>").data("a-b","a").removeData("a-b").data("a-b")&&(e.fn.removeData=function(t){return function(i){return arguments.length?t.call(this,e.camelCase(i)):t.call(this)}}(e.fn.removeData)),function(){var t=/msie ([\w.]+)/.exec(navigator.userAgent.toLowerCase())||[];e.ui.ie=t.length?!0:!1,e.ui.ie6=6===parseFloat(t[1],10)}(),e.fn.extend({disableSelection:function(){return this.bind((e.support.selectstart?"selectstart":"mousedown")+".ui-disableSelection",function(e){e.preventDefault()})},enableSelection:function(){return this.unbind(".ui-disableSelection")}}),e.extend(e.ui,{plugin:{add:function(t,i,s){var a,n=e.ui[t].prototype;for(a in s)n.plugins[a]=n.plugins[a]||[],n.plugins[a].push([i,s[a]])},call:function(e,t,i){var s,a=e.plugins[t];if(a&&e.element[0].parentNode&&11!==e.element[0].parentNode.nodeType)for(s=0;a.length>s;s++)e.options[a[s][0]]&&a[s][1].apply(e.element,i)}},contains:e.contains,hasScroll:function(t,i){if("hidden"===e(t).css("overflow"))return!1;var s=i&&"left"===i?"scrollLeft":"scrollTop",a=!1;return t[s]>0?!0:(t[s]=1,a=t[s]>0,t[s]=0,a)},isOverAxis:function(e,t,i){return e>t&&t+i>e},isOver:function(t,i,s,a,n,o){return e.ui.isOverAxis(t,s,n)&&e.ui.isOverAxis(i,a,o)}}))})(jQuery);(function(e,t){var i=0,s=Array.prototype.slice,a=e.cleanData;e.cleanData=function(t){for(var i,s=0;null!=(i=t[s]);s++)try{e(i).triggerHandler("remove")}catch(n){}a(t)},e.widget=function(i,s,a){var n,o,r,h,l=i.split(".")[0];i=i.split(".")[1],n=l+"-"+i,a||(a=s,s=e.Widget),e.expr[":"][n.toLowerCase()]=function(t){return!!e.data(t,n)},e[l]=e[l]||{},o=e[l][i],r=e[l][i]=function(e,i){return this._createWidget?(arguments.length&&this._createWidget(e,i),t):new r(e,i)},e.extend(r,o,{version:a.version,_proto:e.extend({},a),_childConstructors:[]}),h=new s,h.options=e.widget.extend({},h.options),e.each(a,function(t,i){e.isFunction(i)&&(a[t]=function(){var e=function(){return s.prototype[t].apply(this,arguments)},a=function(e){return s.prototype[t].apply(this,e)};return function(){var t,s=this._super,n=this._superApply;return this._super=e,this._superApply=a,t=i.apply(this,arguments),this._super=s,this._superApply=n,t}}())}),r.prototype=e.widget.extend(h,{widgetEventPrefix:o?h.widgetEventPrefix:i},a,{constructor:r,namespace:l,widgetName:i,widgetBaseClass:n,widgetFullName:n}),o?(e.each(o._childConstructors,function(t,i){var s=i.prototype;e.widget(s.namespace+"."+s.widgetName,r,i._proto)}),delete o._childConstructors):s._childConstructors.push(r),e.widget.bridge(i,r)},e.widget.extend=function(i){for(var a,n,o=s.call(arguments,1),r=0,h=o.length;h>r;r++)for(a in o[r])n=o[r][a],o[r].hasOwnProperty(a)&&n!==t&&(i[a]=e.isPlainObject(n)?e.isPlainObject(i[a])?e.widget.extend({},i[a],n):e.widget.extend({},n):n);return i},e.widget.bridge=function(i,a){var n=a.prototype.widgetFullName||i;e.fn[i]=function(o){var r="string"==typeof o,h=s.call(arguments,1),l=this;return o=!r&&h.length?e.widget.extend.apply(null,[o].concat(h)):o,r?this.each(function(){var s,a=e.data(this,n);return a?e.isFunction(a[o])&&"_"!==o.charAt(0)?(s=a[o].apply(a,h),s!==a&&s!==t?(l=s&&s.jquery?l.pushStack(s.get()):s,!1):t):e.error("no such method '"+o+"' for "+i+" widget instance"):e.error("cannot call methods on "+i+" prior to initialization; "+"attempted to call method '"+o+"'")}):this.each(function(){var t=e.data(this,n);t?t.option(o||{})._init():e.data(this,n,new a(o,this))}),l}},e.Widget=function(){},e.Widget._childConstructors=[],e.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{disabled:!1,create:null},_createWidget:function(t,s){s=e(s||this.defaultElement||this)[0],this.element=e(s),this.uuid=i++,this.eventNamespace="."+this.widgetName+this.uuid,this.options=e.widget.extend({},this.options,this._getCreateOptions(),t),this.bindings=e(),this.hoverable=e(),this.focusable=e(),s!==this&&(e.data(s,this.widgetName,this),e.data(s,this.widgetFullName,this),this._on(!0,this.element,{remove:function(e){e.target===s&&this.destroy()}}),this.document=e(s.style?s.ownerDocument:s.document||s),this.window=e(this.document[0].defaultView||this.document[0].parentWindow)),this._create(),this._trigger("create",null,this._getCreateEventData()),this._init()},_getCreateOptions:e.noop,_getCreateEventData:e.noop,_create:e.noop,_init:e.noop,destroy:function(){this._destroy(),this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(e.camelCase(this.widgetFullName)),this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName+"-disabled "+"ui-state-disabled"),this.bindings.unbind(this.eventNamespace),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")},_destroy:e.noop,widget:function(){return this.element},option:function(i,s){var a,n,o,r=i;if(0===arguments.length)return e.widget.extend({},this.options);if("string"==typeof i)if(r={},a=i.split("."),i=a.shift(),a.length){for(n=r[i]=e.widget.extend({},this.options[i]),o=0;a.length-1>o;o++)n[a[o]]=n[a[o]]||{},n=n[a[o]];if(i=a.pop(),s===t)return n[i]===t?null:n[i];n[i]=s}else{if(s===t)return this.options[i]===t?null:this.options[i];r[i]=s}return this._setOptions(r),this},_setOptions:function(e){var t;for(t in e)this._setOption(t,e[t]);return this},_setOption:function(e,t){return this.options[e]=t,"disabled"===e&&(this.widget().toggleClass(this.widgetFullName+"-disabled ui-state-disabled",!!t).attr("aria-disabled",t),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")),this},enable:function(){return this._setOption("disabled",!1)},disable:function(){return this._setOption("disabled",!0)},_on:function(i,s,a){var n,o=this;"boolean"!=typeof i&&(a=s,s=i,i=!1),a?(s=n=e(s),this.bindings=this.bindings.add(s)):(a=s,s=this.element,n=this.widget()),e.each(a,function(a,r){function h(){return i||o.options.disabled!==!0&&!e(this).hasClass("ui-state-disabled")?("string"==typeof r?o[r]:r).apply(o,arguments):t}"string"!=typeof r&&(h.guid=r.guid=r.guid||h.guid||e.guid++);var l=a.match(/^(\w+)\s*(.*)$/),u=l[1]+o.eventNamespace,d=l[2];d?n.delegate(d,u,h):s.bind(u,h)})},_off:function(e,t){t=(t||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace,e.unbind(t).undelegate(t)},_delay:function(e,t){function i(){return("string"==typeof e?s[e]:e).apply(s,arguments)}var s=this;return setTimeout(i,t||0)},_hoverable:function(t){this.hoverable=this.hoverable.add(t),this._on(t,{mouseenter:function(t){e(t.currentTarget).addClass("ui-state-hover")},mouseleave:function(t){e(t.currentTarget).removeClass("ui-state-hover")}})},_focusable:function(t){this.focusable=this.focusable.add(t),this._on(t,{focusin:function(t){e(t.currentTarget).addClass("ui-state-focus")},focusout:function(t){e(t.currentTarget).removeClass("ui-state-focus")}})},_trigger:function(t,i,s){var a,n,o=this.options[t];if(s=s||{},i=e.Event(i),i.type=(t===this.widgetEventPrefix?t:this.widgetEventPrefix+t).toLowerCase(),i.target=this.element[0],n=i.originalEvent)for(a in n)a in i||(i[a]=n[a]);return this.element.trigger(i,s),!(e.isFunction(o)&&o.apply(this.element[0],[i].concat(s))===!1||i.isDefaultPrevented())}},e.each({show:"fadeIn",hide:"fadeOut"},function(t,i){e.Widget.prototype["_"+t]=function(s,a,n){"string"==typeof a&&(a={effect:a});var o,r=a?a===!0||"number"==typeof a?i:a.effect||i:t;a=a||{},"number"==typeof a&&(a={duration:a}),o=!e.isEmptyObject(a),a.complete=n,a.delay&&s.delay(a.delay),o&&e.effects&&(e.effects.effect[r]||e.uiBackCompat!==!1&&e.effects[r])?s[t](a):r!==t&&s[r]?s[r](a.duration,a.easing,n):s.queue(function(i){e(this)[t](),n&&n.call(s[0]),i()})}}),e.uiBackCompat!==!1&&(e.Widget.prototype._getCreateOptions=function(){return e.metadata&&e.metadata.get(this.element[0])[this.widgetName]})})(jQuery);(function(e){var t=!1;e(document).mouseup(function(){t=!1}),e.widget("ui.mouse",{version:"1.9.2",options:{cancel:"input,textarea,button,select,option",distance:1,delay:0},_mouseInit:function(){var t=this;this.element.bind("mousedown."+this.widgetName,function(e){return t._mouseDown(e)}).bind("click."+this.widgetName,function(i){return!0===e.data(i.target,t.widgetName+".preventClickEvent")?(e.removeData(i.target,t.widgetName+".preventClickEvent"),i.stopImmediatePropagation(),!1):undefined}),this.started=!1},_mouseDestroy:function(){this.element.unbind("."+this.widgetName),this._mouseMoveDelegate&&e(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate)},_mouseDown:function(i){if(!t){this._mouseStarted&&this._mouseUp(i),this._mouseDownEvent=i;var s=this,a=1===i.which,n="string"==typeof this.options.cancel&&i.target.nodeName?e(i.target).closest(this.options.cancel).length:!1;return a&&!n&&this._mouseCapture(i)?(this.mouseDelayMet=!this.options.delay,this.mouseDelayMet||(this._mouseDelayTimer=setTimeout(function(){s.mouseDelayMet=!0},this.options.delay)),this._mouseDistanceMet(i)&&this._mouseDelayMet(i)&&(this._mouseStarted=this._mouseStart(i)!==!1,!this._mouseStarted)?(i.preventDefault(),!0):(!0===e.data(i.target,this.widgetName+".preventClickEvent")&&e.removeData(i.target,this.widgetName+".preventClickEvent"),this._mouseMoveDelegate=function(e){return s._mouseMove(e)},this._mouseUpDelegate=function(e){return s._mouseUp(e)},e(document).bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate),i.preventDefault(),t=!0,!0)):!0}},_mouseMove:function(t){return!e.ui.ie||document.documentMode>=9||t.button?this._mouseStarted?(this._mouseDrag(t),t.preventDefault()):(this._mouseDistanceMet(t)&&this._mouseDelayMet(t)&&(this._mouseStarted=this._mouseStart(this._mouseDownEvent,t)!==!1,this._mouseStarted?this._mouseDrag(t):this._mouseUp(t)),!this._mouseStarted):this._mouseUp(t)},_mouseUp:function(t){return e(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate),this._mouseStarted&&(this._mouseStarted=!1,t.target===this._mouseDownEvent.target&&e.data(t.target,this.widgetName+".preventClickEvent",!0),this._mouseStop(t)),!1},_mouseDistanceMet:function(e){return Math.max(Math.abs(this._mouseDownEvent.pageX-e.pageX),Math.abs(this._mouseDownEvent.pageY-e.pageY))>=this.options.distance},_mouseDelayMet:function(){return this.mouseDelayMet},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return!0}})})(jQuery);(function(e,t){function i(e,t,i){return[parseInt(e[0],10)*(c.test(e[0])?t/100:1),parseInt(e[1],10)*(c.test(e[1])?i/100:1)]}function s(t,i){return parseInt(e.css(t,i),10)||0}e.ui=e.ui||{};var a,n=Math.max,o=Math.abs,r=Math.round,h=/left|center|right/,l=/top|center|bottom/,u=/[\+\-]\d+%?/,d=/^\w+/,c=/%$/,p=e.fn.position;e.position={scrollbarWidth:function(){if(a!==t)return a;var i,s,n=e("<div style='display:block;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),o=n.children()[0];return e("body").append(n),i=o.offsetWidth,n.css("overflow","scroll"),s=o.offsetWidth,i===s&&(s=n[0].clientWidth),n.remove(),a=i-s},getScrollInfo:function(t){var i=t.isWindow?"":t.element.css("overflow-x"),s=t.isWindow?"":t.element.css("overflow-y"),a="scroll"===i||"auto"===i&&t.width<t.element[0].scrollWidth,n="scroll"===s||"auto"===s&&t.height<t.element[0].scrollHeight;return{width:a?e.position.scrollbarWidth():0,height:n?e.position.scrollbarWidth():0}},getWithinInfo:function(t){var i=e(t||window),s=e.isWindow(i[0]);return{element:i,isWindow:s,offset:i.offset()||{left:0,top:0},scrollLeft:i.scrollLeft(),scrollTop:i.scrollTop(),width:s?i.width():i.outerWidth(),height:s?i.height():i.outerHeight()}}},e.fn.position=function(t){if(!t||!t.of)return p.apply(this,arguments);t=e.extend({},t);var a,c,f,m,g,v=e(t.of),y=e.position.getWithinInfo(t.within),b=e.position.getScrollInfo(y),_=v[0],x=(t.collision||"flip").split(" "),w={};return 9===_.nodeType?(c=v.width(),f=v.height(),m={top:0,left:0}):e.isWindow(_)?(c=v.width(),f=v.height(),m={top:v.scrollTop(),left:v.scrollLeft()}):_.preventDefault?(t.at="left top",c=f=0,m={top:_.pageY,left:_.pageX}):(c=v.outerWidth(),f=v.outerHeight(),m=v.offset()),g=e.extend({},m),e.each(["my","at"],function(){var e,i,s=(t[this]||"").split(" ");1===s.length&&(s=h.test(s[0])?s.concat(["center"]):l.test(s[0])?["center"].concat(s):["center","center"]),s[0]=h.test(s[0])?s[0]:"center",s[1]=l.test(s[1])?s[1]:"center",e=u.exec(s[0]),i=u.exec(s[1]),w[this]=[e?e[0]:0,i?i[0]:0],t[this]=[d.exec(s[0])[0],d.exec(s[1])[0]]}),1===x.length&&(x[1]=x[0]),"right"===t.at[0]?g.left+=c:"center"===t.at[0]&&(g.left+=c/2),"bottom"===t.at[1]?g.top+=f:"center"===t.at[1]&&(g.top+=f/2),a=i(w.at,c,f),g.left+=a[0],g.top+=a[1],this.each(function(){var h,l,u=e(this),d=u.outerWidth(),p=u.outerHeight(),_=s(this,"marginLeft"),k=s(this,"marginTop"),D=d+_+s(this,"marginRight")+b.width,T=p+k+s(this,"marginBottom")+b.height,S=e.extend({},g),M=i(w.my,u.outerWidth(),u.outerHeight());"right"===t.my[0]?S.left-=d:"center"===t.my[0]&&(S.left-=d/2),"bottom"===t.my[1]?S.top-=p:"center"===t.my[1]&&(S.top-=p/2),S.left+=M[0],S.top+=M[1],e.support.offsetFractions||(S.left=r(S.left),S.top=r(S.top)),h={marginLeft:_,marginTop:k},e.each(["left","top"],function(i,s){e.ui.position[x[i]]&&e.ui.position[x[i]][s](S,{targetWidth:c,targetHeight:f,elemWidth:d,elemHeight:p,collisionPosition:h,collisionWidth:D,collisionHeight:T,offset:[a[0]+M[0],a[1]+M[1]],my:t.my,at:t.at,within:y,elem:u})}),e.fn.bgiframe&&u.bgiframe(),t.using&&(l=function(e){var i=m.left-S.left,s=i+c-d,a=m.top-S.top,r=a+f-p,h={target:{element:v,left:m.left,top:m.top,width:c,height:f},element:{element:u,left:S.left,top:S.top,width:d,height:p},horizontal:0>s?"left":i>0?"right":"center",vertical:0>r?"top":a>0?"bottom":"middle"};d>c&&c>o(i+s)&&(h.horizontal="center"),p>f&&f>o(a+r)&&(h.vertical="middle"),h.important=n(o(i),o(s))>n(o(a),o(r))?"horizontal":"vertical",t.using.call(this,e,h)}),u.offset(e.extend(S,{using:l}))})},e.ui.position={fit:{left:function(e,t){var i,s=t.within,a=s.isWindow?s.scrollLeft:s.offset.left,o=s.width,r=e.left-t.collisionPosition.marginLeft,h=a-r,l=r+t.collisionWidth-o-a;t.collisionWidth>o?h>0&&0>=l?(i=e.left+h+t.collisionWidth-o-a,e.left+=h-i):e.left=l>0&&0>=h?a:h>l?a+o-t.collisionWidth:a:h>0?e.left+=h:l>0?e.left-=l:e.left=n(e.left-r,e.left)},top:function(e,t){var i,s=t.within,a=s.isWindow?s.scrollTop:s.offset.top,o=t.within.height,r=e.top-t.collisionPosition.marginTop,h=a-r,l=r+t.collisionHeight-o-a;t.collisionHeight>o?h>0&&0>=l?(i=e.top+h+t.collisionHeight-o-a,e.top+=h-i):e.top=l>0&&0>=h?a:h>l?a+o-t.collisionHeight:a:h>0?e.top+=h:l>0?e.top-=l:e.top=n(e.top-r,e.top)}},flip:{left:function(e,t){var i,s,a=t.within,n=a.offset.left+a.scrollLeft,r=a.width,h=a.isWindow?a.scrollLeft:a.offset.left,l=e.left-t.collisionPosition.marginLeft,u=l-h,d=l+t.collisionWidth-r-h,c="left"===t.my[0]?-t.elemWidth:"right"===t.my[0]?t.elemWidth:0,p="left"===t.at[0]?t.targetWidth:"right"===t.at[0]?-t.targetWidth:0,f=-2*t.offset[0];0>u?(i=e.left+c+p+f+t.collisionWidth-r-n,(0>i||o(u)>i)&&(e.left+=c+p+f)):d>0&&(s=e.left-t.collisionPosition.marginLeft+c+p+f-h,(s>0||d>o(s))&&(e.left+=c+p+f))},top:function(e,t){var i,s,a=t.within,n=a.offset.top+a.scrollTop,r=a.height,h=a.isWindow?a.scrollTop:a.offset.top,l=e.top-t.collisionPosition.marginTop,u=l-h,d=l+t.collisionHeight-r-h,c="top"===t.my[1],p=c?-t.elemHeight:"bottom"===t.my[1]?t.elemHeight:0,f="top"===t.at[1]?t.targetHeight:"bottom"===t.at[1]?-t.targetHeight:0,m=-2*t.offset[1];0>u?(s=e.top+p+f+m+t.collisionHeight-r-n,e.top+p+f+m>u&&(0>s||o(u)>s)&&(e.top+=p+f+m)):d>0&&(i=e.top-t.collisionPosition.marginTop+p+f+m-h,e.top+p+f+m>d&&(i>0||d>o(i))&&(e.top+=p+f+m))}},flipfit:{left:function(){e.ui.position.flip.left.apply(this,arguments),e.ui.position.fit.left.apply(this,arguments)},top:function(){e.ui.position.flip.top.apply(this,arguments),e.ui.position.fit.top.apply(this,arguments)}}},function(){var t,i,s,a,n,o=document.getElementsByTagName("body")[0],r=document.createElement("div");t=document.createElement(o?"div":"body"),s={visibility:"hidden",width:0,height:0,border:0,margin:0,background:"none"},o&&e.extend(s,{position:"absolute",left:"-1000px",top:"-1000px"});for(n in s)t.style[n]=s[n];t.appendChild(r),i=o||document.documentElement,i.insertBefore(t,i.firstChild),r.style.cssText="position: absolute; left: 10.7432222px;",a=e(r).offset().left,e.support.offsetFractions=a>10&&11>a,t.innerHTML="",i.removeChild(t)}(),e.uiBackCompat!==!1&&function(e){var i=e.fn.position;e.fn.position=function(s){if(!s||!s.offset)return i.call(this,s);var a=s.offset.split(" "),n=s.at.split(" ");return 1===a.length&&(a[1]=a[0]),/^\d/.test(a[0])&&(a[0]="+"+a[0]),/^\d/.test(a[1])&&(a[1]="+"+a[1]),1===n.length&&(/left|center|right/.test(n[0])?n[1]="center":(n[1]=n[0],n[0]="center")),i.call(this,e.extend(s,{at:n[0]+a[0]+" "+n[1]+a[1],offset:t}))}}(jQuery)})(jQuery);(function(e){e.widget("ui.draggable",e.ui.mouse,{version:"1.9.2",widgetEventPrefix:"drag",options:{addClasses:!0,appendTo:"parent",axis:!1,connectToSortable:!1,containment:!1,cursor:"auto",cursorAt:!1,grid:!1,handle:!1,helper:"original",iframeFix:!1,opacity:!1,refreshPositions:!1,revert:!1,revertDuration:500,scope:"default",scroll:!0,scrollSensitivity:20,scrollSpeed:20,snap:!1,snapMode:"both",snapTolerance:20,stack:!1,zIndex:!1},_create:function(){"original"!=this.options.helper||/^(?:r|a|f)/.test(this.element.css("position"))||(this.element[0].style.position="relative"),this.options.addClasses&&this.element.addClass("ui-draggable"),this.options.disabled&&this.element.addClass("ui-draggable-disabled"),this._mouseInit()},_destroy:function(){this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"),this._mouseDestroy()},_mouseCapture:function(t){var i=this.options;return this.helper||i.disabled||e(t.target).is(".ui-resizable-handle")?!1:(this.handle=this._getHandle(t),this.handle?(e(i.iframeFix===!0?"iframe":i.iframeFix).each(function(){e('<div class="ui-draggable-iframeFix" style="background: #fff;"></div>').css({width:this.offsetWidth+"px",height:this.offsetHeight+"px",position:"absolute",opacity:"0.001",zIndex:1e3}).css(e(this).offset()).appendTo("body")}),!0):!1)},_mouseStart:function(t){var i=this.options;return this.helper=this._createHelper(t),this.helper.addClass("ui-draggable-dragging"),this._cacheHelperProportions(),e.ui.ddmanager&&(e.ui.ddmanager.current=this),this._cacheMargins(),this.cssPosition=this.helper.css("position"),this.scrollParent=this.helper.scrollParent(),this.offset=this.positionAbs=this.element.offset(),this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left},e.extend(this.offset,{click:{left:t.pageX-this.offset.left,top:t.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()}),this.originalPosition=this.position=this._generatePosition(t),this.originalPageX=t.pageX,this.originalPageY=t.pageY,i.cursorAt&&this._adjustOffsetFromHelper(i.cursorAt),i.containment&&this._setContainment(),this._trigger("start",t)===!1?(this._clear(),!1):(this._cacheHelperProportions(),e.ui.ddmanager&&!i.dropBehaviour&&e.ui.ddmanager.prepareOffsets(this,t),this._mouseDrag(t,!0),e.ui.ddmanager&&e.ui.ddmanager.dragStart(this,t),!0)},_mouseDrag:function(t,i){if(this.position=this._generatePosition(t),this.positionAbs=this._convertPositionTo("absolute"),!i){var s=this._uiHash();if(this._trigger("drag",t,s)===!1)return this._mouseUp({}),!1;this.position=s.position}return this.options.axis&&"y"==this.options.axis||(this.helper[0].style.left=this.position.left+"px"),this.options.axis&&"x"==this.options.axis||(this.helper[0].style.top=this.position.top+"px"),e.ui.ddmanager&&e.ui.ddmanager.drag(this,t),!1},_mouseStop:function(t){var i=!1;e.ui.ddmanager&&!this.options.dropBehaviour&&(i=e.ui.ddmanager.drop(this,t)),this.dropped&&(i=this.dropped,this.dropped=!1);for(var s=this.element[0],a=!1;s&&(s=s.parentNode);)s==document&&(a=!0);if(!a&&"original"===this.options.helper)return!1;if("invalid"==this.options.revert&&!i||"valid"==this.options.revert&&i||this.options.revert===!0||e.isFunction(this.options.revert)&&this.options.revert.call(this.element,i)){var n=this;e(this.helper).animate(this.originalPosition,parseInt(this.options.revertDuration,10),function(){n._trigger("stop",t)!==!1&&n._clear()})}else this._trigger("stop",t)!==!1&&this._clear();return!1},_mouseUp:function(t){return e("div.ui-draggable-iframeFix").each(function(){this.parentNode.removeChild(this)}),e.ui.ddmanager&&e.ui.ddmanager.dragStop(this,t),e.ui.mouse.prototype._mouseUp.call(this,t)},cancel:function(){return this.helper.is(".ui-draggable-dragging")?this._mouseUp({}):this._clear(),this},_getHandle:function(t){var i=this.options.handle&&e(this.options.handle,this.element).length?!1:!0;return e(this.options.handle,this.element).find("*").andSelf().each(function(){this==t.target&&(i=!0)}),i},_createHelper:function(t){var i=this.options,s=e.isFunction(i.helper)?e(i.helper.apply(this.element[0],[t])):"clone"==i.helper?this.element.clone().removeAttr("id"):this.element;return s.parents("body").length||s.appendTo("parent"==i.appendTo?this.element[0].parentNode:i.appendTo),s[0]==this.element[0]||/(fixed|absolute)/.test(s.css("position"))||s.css("position","absolute"),s},_adjustOffsetFromHelper:function(t){"string"==typeof t&&(t=t.split(" ")),e.isArray(t)&&(t={left:+t[0],top:+t[1]||0}),"left"in t&&(this.offset.click.left=t.left+this.margins.left),"right"in t&&(this.offset.click.left=this.helperProportions.width-t.right+this.margins.left),"top"in t&&(this.offset.click.top=t.top+this.margins.top),"bottom"in t&&(this.offset.click.top=this.helperProportions.height-t.bottom+this.margins.top)},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var t=this.offsetParent.offset();return"absolute"==this.cssPosition&&this.scrollParent[0]!=document&&e.contains(this.scrollParent[0],this.offsetParent[0])&&(t.left+=this.scrollParent.scrollLeft(),t.top+=this.scrollParent.scrollTop()),(this.offsetParent[0]==document.body||this.offsetParent[0].tagName&&"html"==this.offsetParent[0].tagName.toLowerCase()&&e.ui.ie)&&(t={top:0,left:0}),{top:t.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:t.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if("relative"==this.cssPosition){var e=this.element.position();return{top:e.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:e.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}}return{top:0,left:0}},_cacheMargins:function(){this.margins={left:parseInt(this.element.css("marginLeft"),10)||0,top:parseInt(this.element.css("marginTop"),10)||0,right:parseInt(this.element.css("marginRight"),10)||0,bottom:parseInt(this.element.css("marginBottom"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var t=this.options;if("parent"==t.containment&&(t.containment=this.helper[0].parentNode),("document"==t.containment||"window"==t.containment)&&(this.containment=["document"==t.containment?0:e(window).scrollLeft()-this.offset.relative.left-this.offset.parent.left,"document"==t.containment?0:e(window).scrollTop()-this.offset.relative.top-this.offset.parent.top,("document"==t.containment?0:e(window).scrollLeft())+e("document"==t.containment?document:window).width()-this.helperProportions.width-this.margins.left,("document"==t.containment?0:e(window).scrollTop())+(e("document"==t.containment?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top]),/^(document|window|parent)$/.test(t.containment)||t.containment.constructor==Array)t.containment.constructor==Array&&(this.containment=t.containment);else{var i=e(t.containment),s=i[0];if(!s)return;i.offset();var a="hidden"!=e(s).css("overflow");this.containment=[(parseInt(e(s).css("borderLeftWidth"),10)||0)+(parseInt(e(s).css("paddingLeft"),10)||0),(parseInt(e(s).css("borderTopWidth"),10)||0)+(parseInt(e(s).css("paddingTop"),10)||0),(a?Math.max(s.scrollWidth,s.offsetWidth):s.offsetWidth)-(parseInt(e(s).css("borderLeftWidth"),10)||0)-(parseInt(e(s).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left-this.margins.right,(a?Math.max(s.scrollHeight,s.offsetHeight):s.offsetHeight)-(parseInt(e(s).css("borderTopWidth"),10)||0)-(parseInt(e(s).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top-this.margins.bottom],this.relative_container=i}},_convertPositionTo:function(t,i){i||(i=this.position);var s="absolute"==t?1:-1,a=(this.options,"absolute"!=this.cssPosition||this.scrollParent[0]!=document&&e.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent),n=/(html|body)/i.test(a[0].tagName);return{top:i.top+this.offset.relative.top*s+this.offset.parent.top*s-("fixed"==this.cssPosition?-this.scrollParent.scrollTop():n?0:a.scrollTop())*s,left:i.left+this.offset.relative.left*s+this.offset.parent.left*s-("fixed"==this.cssPosition?-this.scrollParent.scrollLeft():n?0:a.scrollLeft())*s}},_generatePosition:function(t){var i=this.options,s="absolute"!=this.cssPosition||this.scrollParent[0]!=document&&e.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,a=/(html|body)/i.test(s[0].tagName),n=t.pageX,o=t.pageY;if(this.originalPosition){var r;if(this.containment){if(this.relative_container){var h=this.relative_container.offset();r=[this.containment[0]+h.left,this.containment[1]+h.top,this.containment[2]+h.left,this.containment[3]+h.top]}else r=this.containment;t.pageX-this.offset.click.left<r[0]&&(n=r[0]+this.offset.click.left),t.pageY-this.offset.click.top<r[1]&&(o=r[1]+this.offset.click.top),t.pageX-this.offset.click.left>r[2]&&(n=r[2]+this.offset.click.left),t.pageY-this.offset.click.top>r[3]&&(o=r[3]+this.offset.click.top)}if(i.grid){var l=i.grid[1]?this.originalPageY+Math.round((o-this.originalPageY)/i.grid[1])*i.grid[1]:this.originalPageY;o=r?l-this.offset.click.top<r[1]||l-this.offset.click.top>r[3]?l-this.offset.click.top<r[1]?l+i.grid[1]:l-i.grid[1]:l:l;var u=i.grid[0]?this.originalPageX+Math.round((n-this.originalPageX)/i.grid[0])*i.grid[0]:this.originalPageX;n=r?u-this.offset.click.left<r[0]||u-this.offset.click.left>r[2]?u-this.offset.click.left<r[0]?u+i.grid[0]:u-i.grid[0]:u:u}}return{top:o-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+("fixed"==this.cssPosition?-this.scrollParent.scrollTop():a?0:s.scrollTop()),left:n-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+("fixed"==this.cssPosition?-this.scrollParent.scrollLeft():a?0:s.scrollLeft())}},_clear:function(){this.helper.removeClass("ui-draggable-dragging"),this.helper[0]==this.element[0]||this.cancelHelperRemoval||this.helper.remove(),this.helper=null,this.cancelHelperRemoval=!1},_trigger:function(t,i,s){return s=s||this._uiHash(),e.ui.plugin.call(this,t,[i,s]),"drag"==t&&(this.positionAbs=this._convertPositionTo("absolute")),e.Widget.prototype._trigger.call(this,t,i,s)},plugins:{},_uiHash:function(){return{helper:this.helper,position:this.position,originalPosition:this.originalPosition,offset:this.positionAbs}}}),e.ui.plugin.add("draggable","connectToSortable",{start:function(t,i){var s=e(this).data("draggable"),a=s.options,n=e.extend({},i,{item:s.element});s.sortables=[],e(a.connectToSortable).each(function(){var i=e.data(this,"sortable");i&&!i.options.disabled&&(s.sortables.push({instance:i,shouldRevert:i.options.revert}),i.refreshPositions(),i._trigger("activate",t,n))})},stop:function(t,i){var s=e(this).data("draggable"),a=e.extend({},i,{item:s.element});e.each(s.sortables,function(){this.instance.isOver?(this.instance.isOver=0,s.cancelHelperRemoval=!0,this.instance.cancelHelperRemoval=!1,this.shouldRevert&&(this.instance.options.revert=!0),this.instance._mouseStop(t),this.instance.options.helper=this.instance.options._helper,"original"==s.options.helper&&this.instance.currentItem.css({top:"auto",left:"auto"})):(this.instance.cancelHelperRemoval=!1,this.instance._trigger("deactivate",t,a))})},drag:function(t,i){var s=e(this).data("draggable"),a=this;e.each(s.sortables,function(){var n=!1,o=this;this.instance.positionAbs=s.positionAbs,this.instance.helperProportions=s.helperProportions,this.instance.offset.click=s.offset.click,this.instance._intersectsWith(this.instance.containerCache)&&(n=!0,e.each(s.sortables,function(){return this.instance.positionAbs=s.positionAbs,this.instance.helperProportions=s.helperProportions,this.instance.offset.click=s.offset.click,this!=o&&this.instance._intersectsWith(this.instance.containerCache)&&e.ui.contains(o.instance.element[0],this.instance.element[0])&&(n=!1),n})),n?(this.instance.isOver||(this.instance.isOver=1,this.instance.currentItem=e(a).clone().removeAttr("id").appendTo(this.instance.element).data("sortable-item",!0),this.instance.options._helper=this.instance.options.helper,this.instance.options.helper=function(){return i.helper[0]},t.target=this.instance.currentItem[0],this.instance._mouseCapture(t,!0),this.instance._mouseStart(t,!0,!0),this.instance.offset.click.top=s.offset.click.top,this.instance.offset.click.left=s.offset.click.left,this.instance.offset.parent.left-=s.offset.parent.left-this.instance.offset.parent.left,this.instance.offset.parent.top-=s.offset.parent.top-this.instance.offset.parent.top,s._trigger("toSortable",t),s.dropped=this.instance.element,s.currentItem=s.element,this.instance.fromOutside=s),this.instance.currentItem&&this.instance._mouseDrag(t)):this.instance.isOver&&(this.instance.isOver=0,this.instance.cancelHelperRemoval=!0,this.instance.options.revert=!1,this.instance._trigger("out",t,this.instance._uiHash(this.instance)),this.instance._mouseStop(t,!0),this.instance.options.helper=this.instance.options._helper,this.instance.currentItem.remove(),this.instance.placeholder&&this.instance.placeholder.remove(),s._trigger("fromSortable",t),s.dropped=!1)})}}),e.ui.plugin.add("draggable","cursor",{start:function(){var t=e("body"),i=e(this).data("draggable").options;t.css("cursor")&&(i._cursor=t.css("cursor")),t.css("cursor",i.cursor)},stop:function(){var t=e(this).data("draggable").options;t._cursor&&e("body").css("cursor",t._cursor)}}),e.ui.plugin.add("draggable","opacity",{start:function(t,i){var s=e(i.helper),a=e(this).data("draggable").options;s.css("opacity")&&(a._opacity=s.css("opacity")),s.css("opacity",a.opacity)},stop:function(t,i){var s=e(this).data("draggable").options;s._opacity&&e(i.helper).css("opacity",s._opacity)}}),e.ui.plugin.add("draggable","scroll",{start:function(){var t=e(this).data("draggable");t.scrollParent[0]!=document&&"HTML"!=t.scrollParent[0].tagName&&(t.overflowOffset=t.scrollParent.offset())},drag:function(t){var i=e(this).data("draggable"),s=i.options,a=!1;i.scrollParent[0]!=document&&"HTML"!=i.scrollParent[0].tagName?(s.axis&&"x"==s.axis||(i.overflowOffset.top+i.scrollParent[0].offsetHeight-t.pageY<s.scrollSensitivity?i.scrollParent[0].scrollTop=a=i.scrollParent[0].scrollTop+s.scrollSpeed:t.pageY-i.overflowOffset.top<s.scrollSensitivity&&(i.scrollParent[0].scrollTop=a=i.scrollParent[0].scrollTop-s.scrollSpeed)),s.axis&&"y"==s.axis||(i.overflowOffset.left+i.scrollParent[0].offsetWidth-t.pageX<s.scrollSensitivity?i.scrollParent[0].scrollLeft=a=i.scrollParent[0].scrollLeft+s.scrollSpeed:t.pageX-i.overflowOffset.left<s.scrollSensitivity&&(i.scrollParent[0].scrollLeft=a=i.scrollParent[0].scrollLeft-s.scrollSpeed))):(s.axis&&"x"==s.axis||(t.pageY-e(document).scrollTop()<s.scrollSensitivity?a=e(document).scrollTop(e(document).scrollTop()-s.scrollSpeed):e(window).height()-(t.pageY-e(document).scrollTop())<s.scrollSensitivity&&(a=e(document).scrollTop(e(document).scrollTop()+s.scrollSpeed))),s.axis&&"y"==s.axis||(t.pageX-e(document).scrollLeft()<s.scrollSensitivity?a=e(document).scrollLeft(e(document).scrollLeft()-s.scrollSpeed):e(window).width()-(t.pageX-e(document).scrollLeft())<s.scrollSensitivity&&(a=e(document).scrollLeft(e(document).scrollLeft()+s.scrollSpeed)))),a!==!1&&e.ui.ddmanager&&!s.dropBehaviour&&e.ui.ddmanager.prepareOffsets(i,t)}}),e.ui.plugin.add("draggable","snap",{start:function(){var t=e(this).data("draggable"),i=t.options;t.snapElements=[],e(i.snap.constructor!=String?i.snap.items||":data(draggable)":i.snap).each(function(){var i=e(this),s=i.offset();this!=t.element[0]&&t.snapElements.push({item:this,width:i.outerWidth(),height:i.outerHeight(),top:s.top,left:s.left})})},drag:function(t,i){for(var s=e(this).data("draggable"),a=s.options,n=a.snapTolerance,o=i.offset.left,r=o+s.helperProportions.width,h=i.offset.top,l=h+s.helperProportions.height,u=s.snapElements.length-1;u>=0;u--){var d=s.snapElements[u].left,c=d+s.snapElements[u].width,p=s.snapElements[u].top,f=p+s.snapElements[u].height;if(o>d-n&&c+n>o&&h>p-n&&f+n>h||o>d-n&&c+n>o&&l>p-n&&f+n>l||r>d-n&&c+n>r&&h>p-n&&f+n>h||r>d-n&&c+n>r&&l>p-n&&f+n>l){if("inner"!=a.snapMode){var m=n>=Math.abs(p-l),g=n>=Math.abs(f-h),v=n>=Math.abs(d-r),y=n>=Math.abs(c-o);m&&(i.position.top=s._convertPositionTo("relative",{top:p-s.helperProportions.height,left:0}).top-s.margins.top),g&&(i.position.top=s._convertPositionTo("relative",{top:f,left:0}).top-s.margins.top),v&&(i.position.left=s._convertPositionTo("relative",{top:0,left:d-s.helperProportions.width}).left-s.margins.left),y&&(i.position.left=s._convertPositionTo("relative",{top:0,left:c}).left-s.margins.left)}var b=m||g||v||y;if("outer"!=a.snapMode){var m=n>=Math.abs(p-h),g=n>=Math.abs(f-l),v=n>=Math.abs(d-o),y=n>=Math.abs(c-r);m&&(i.position.top=s._convertPositionTo("relative",{top:p,left:0}).top-s.margins.top),g&&(i.position.top=s._convertPositionTo("relative",{top:f-s.helperProportions.height,left:0}).top-s.margins.top),v&&(i.position.left=s._convertPositionTo("relative",{top:0,left:d}).left-s.margins.left),y&&(i.position.left=s._convertPositionTo("relative",{top:0,left:c-s.helperProportions.width}).left-s.margins.left)}!s.snapElements[u].snapping&&(m||g||v||y||b)&&s.options.snap.snap&&s.options.snap.snap.call(s.element,t,e.extend(s._uiHash(),{snapItem:s.snapElements[u].item})),s.snapElements[u].snapping=m||g||v||y||b}else s.snapElements[u].snapping&&s.options.snap.release&&s.options.snap.release.call(s.element,t,e.extend(s._uiHash(),{snapItem:s.snapElements[u].item})),s.snapElements[u].snapping=!1}}}),e.ui.plugin.add("draggable","stack",{start:function(){var t=e(this).data("draggable").options,i=e.makeArray(e(t.stack)).sort(function(t,i){return(parseInt(e(t).css("zIndex"),10)||0)-(parseInt(e(i).css("zIndex"),10)||0)});if(i.length){var s=parseInt(i[0].style.zIndex)||0;e(i).each(function(e){this.style.zIndex=s+e}),this[0].style.zIndex=s+i.length}}}),e.ui.plugin.add("draggable","zIndex",{start:function(t,i){var s=e(i.helper),a=e(this).data("draggable").options;s.css("zIndex")&&(a._zIndex=s.css("zIndex")),s.css("zIndex",a.zIndex)},stop:function(t,i){var s=e(this).data("draggable").options;s._zIndex&&e(i.helper).css("zIndex",s._zIndex)}})})(jQuery);(function(e){e.widget("ui.droppable",{version:"1.9.2",widgetEventPrefix:"drop",options:{accept:"*",activeClass:!1,addClasses:!0,greedy:!1,hoverClass:!1,scope:"default",tolerance:"intersect"},_create:function(){var t=this.options,i=t.accept;this.isover=0,this.isout=1,this.accept=e.isFunction(i)?i:function(e){return e.is(i)},this.proportions={width:this.element[0].offsetWidth,height:this.element[0].offsetHeight},e.ui.ddmanager.droppables[t.scope]=e.ui.ddmanager.droppables[t.scope]||[],e.ui.ddmanager.droppables[t.scope].push(this),t.addClasses&&this.element.addClass("ui-droppable")},_destroy:function(){for(var t=e.ui.ddmanager.droppables[this.options.scope],i=0;t.length>i;i++)t[i]==this&&t.splice(i,1);this.element.removeClass("ui-droppable ui-droppable-disabled")},_setOption:function(t,i){"accept"==t&&(this.accept=e.isFunction(i)?i:function(e){return e.is(i)}),e.Widget.prototype._setOption.apply(this,arguments)},_activate:function(t){var i=e.ui.ddmanager.current;this.options.activeClass&&this.element.addClass(this.options.activeClass),i&&this._trigger("activate",t,this.ui(i))},_deactivate:function(t){var i=e.ui.ddmanager.current;this.options.activeClass&&this.element.removeClass(this.options.activeClass),i&&this._trigger("deactivate",t,this.ui(i))},_over:function(t){var i=e.ui.ddmanager.current;i&&(i.currentItem||i.element)[0]!=this.element[0]&&this.accept.call(this.element[0],i.currentItem||i.element)&&(this.options.hoverClass&&this.element.addClass(this.options.hoverClass),this._trigger("over",t,this.ui(i)))},_out:function(t){var i=e.ui.ddmanager.current;i&&(i.currentItem||i.element)[0]!=this.element[0]&&this.accept.call(this.element[0],i.currentItem||i.element)&&(this.options.hoverClass&&this.element.removeClass(this.options.hoverClass),this._trigger("out",t,this.ui(i)))},_drop:function(t,i){var s=i||e.ui.ddmanager.current;if(!s||(s.currentItem||s.element)[0]==this.element[0])return!1;var a=!1;return this.element.find(":data(droppable)").not(".ui-draggable-dragging").each(function(){var t=e.data(this,"droppable");return t.options.greedy&&!t.options.disabled&&t.options.scope==s.options.scope&&t.accept.call(t.element[0],s.currentItem||s.element)&&e.ui.intersect(s,e.extend(t,{offset:t.element.offset()}),t.options.tolerance)?(a=!0,!1):undefined}),a?!1:this.accept.call(this.element[0],s.currentItem||s.element)?(this.options.activeClass&&this.element.removeClass(this.options.activeClass),this.options.hoverClass&&this.element.removeClass(this.options.hoverClass),this._trigger("drop",t,this.ui(s)),this.element):!1},ui:function(e){return{draggable:e.currentItem||e.element,helper:e.helper,position:e.position,offset:e.positionAbs}}}),e.ui.intersect=function(t,i,s){if(!i.offset)return!1;var a=(t.positionAbs||t.position.absolute).left,n=a+t.helperProportions.width,o=(t.positionAbs||t.position.absolute).top,r=o+t.helperProportions.height,h=i.offset.left,l=h+i.proportions.width,u=i.offset.top,d=u+i.proportions.height;switch(s){case"fit":return a>=h&&l>=n&&o>=u&&d>=r;case"intersect":return a+t.helperProportions.width/2>h&&l>n-t.helperProportions.width/2&&o+t.helperProportions.height/2>u&&d>r-t.helperProportions.height/2;case"pointer":var c=(t.positionAbs||t.position.absolute).left+(t.clickOffset||t.offset.click).left,p=(t.positionAbs||t.position.absolute).top+(t.clickOffset||t.offset.click).top,f=e.ui.isOver(p,c,u,h,i.proportions.height,i.proportions.width);return f;case"touch":return(o>=u&&d>=o||r>=u&&d>=r||u>o&&r>d)&&(a>=h&&l>=a||n>=h&&l>=n||h>a&&n>l);default:return!1}},e.ui.ddmanager={current:null,droppables:{"default":[]},prepareOffsets:function(t,i){var s=e.ui.ddmanager.droppables[t.options.scope]||[],a=i?i.type:null,n=(t.currentItem||t.element).find(":data(droppable)").andSelf();e:for(var o=0;s.length>o;o++)if(!(s[o].options.disabled||t&&!s[o].accept.call(s[o].element[0],t.currentItem||t.element))){for(var r=0;n.length>r;r++)if(n[r]==s[o].element[0]){s[o].proportions.height=0;continue e}s[o].visible="none"!=s[o].element.css("display"),s[o].visible&&("mousedown"==a&&s[o]._activate.call(s[o],i),s[o].offset=s[o].element.offset(),s[o].proportions={width:s[o].element[0].offsetWidth,height:s[o].element[0].offsetHeight})}},drop:function(t,i){var s=!1;return e.each(e.ui.ddmanager.droppables[t.options.scope]||[],function(){this.options&&(!this.options.disabled&&this.visible&&e.ui.intersect(t,this,this.options.tolerance)&&(s=this._drop.call(this,i)||s),!this.options.disabled&&this.visible&&this.accept.call(this.element[0],t.currentItem||t.element)&&(this.isout=1,this.isover=0,this._deactivate.call(this,i)))}),s},dragStart:function(t,i){t.element.parentsUntil("body").bind("scroll.droppable",function(){t.options.refreshPositions||e.ui.ddmanager.prepareOffsets(t,i)})},drag:function(t,i){t.options.refreshPositions&&e.ui.ddmanager.prepareOffsets(t,i),e.each(e.ui.ddmanager.droppables[t.options.scope]||[],function(){if(!this.options.disabled&&!this.greedyChild&&this.visible){var s=e.ui.intersect(t,this,this.options.tolerance),a=s||1!=this.isover?s&&0==this.isover?"isover":null:"isout";if(a){var n;if(this.options.greedy){var o=this.options.scope,r=this.element.parents(":data(droppable)").filter(function(){return e.data(this,"droppable").options.scope===o});r.length&&(n=e.data(r[0],"droppable"),n.greedyChild="isover"==a?1:0)}n&&"isover"==a&&(n.isover=0,n.isout=1,n._out.call(n,i)),this[a]=1,this["isout"==a?"isover":"isout"]=0,this["isover"==a?"_over":"_out"].call(this,i),n&&"isout"==a&&(n.isout=0,n.isover=1,n._over.call(n,i))}}})},dragStop:function(t,i){t.element.parentsUntil("body").unbind("scroll.droppable"),t.options.refreshPositions||e.ui.ddmanager.prepareOffsets(t,i)}}})(jQuery);(function(e){e.widget("ui.resizable",e.ui.mouse,{version:"1.9.2",widgetEventPrefix:"resize",options:{alsoResize:!1,animate:!1,animateDuration:"slow",animateEasing:"swing",aspectRatio:!1,autoHide:!1,containment:!1,ghost:!1,grid:!1,handles:"e,s,se",helper:!1,maxHeight:null,maxWidth:null,minHeight:10,minWidth:10,zIndex:1e3},_create:function(){var t=this,i=this.options;if(this.element.addClass("ui-resizable"),e.extend(this,{_aspectRatio:!!i.aspectRatio,aspectRatio:i.aspectRatio,originalElement:this.element,_proportionallyResizeElements:[],_helper:i.helper||i.ghost||i.animate?i.helper||"ui-resizable-helper":null}),this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i)&&(this.element.wrap(e('<div class="ui-wrapper" style="overflow: hidden;"></div>').css({position:this.element.css("position"),width:this.element.outerWidth(),height:this.element.outerHeight(),top:this.element.css("top"),left:this.element.css("left")})),this.element=this.element.parent().data("resizable",this.element.data("resizable")),this.elementIsWrapper=!0,this.element.css({marginLeft:this.originalElement.css("marginLeft"),marginTop:this.originalElement.css("marginTop"),marginRight:this.originalElement.css("marginRight"),marginBottom:this.originalElement.css("marginBottom")}),this.originalElement.css({marginLeft:0,marginTop:0,marginRight:0,marginBottom:0}),this.originalResizeStyle=this.originalElement.css("resize"),this.originalElement.css("resize","none"),this._proportionallyResizeElements.push(this.originalElement.css({position:"static",zoom:1,display:"block"})),this.originalElement.css({margin:this.originalElement.css("margin")}),this._proportionallyResize()),this.handles=i.handles||(e(".ui-resizable-handle",this.element).length?{n:".ui-resizable-n",e:".ui-resizable-e",s:".ui-resizable-s",w:".ui-resizable-w",se:".ui-resizable-se",sw:".ui-resizable-sw",ne:".ui-resizable-ne",nw:".ui-resizable-nw"}:"e,s,se"),this.handles.constructor==String){"all"==this.handles&&(this.handles="n,e,s,w,se,sw,ne,nw");var s=this.handles.split(",");this.handles={};for(var a=0;s.length>a;a++){var n=e.trim(s[a]),o="ui-resizable-"+n,r=e('<div class="ui-resizable-handle '+o+'"></div>');r.css({zIndex:i.zIndex}),"se"==n&&r.addClass("ui-icon ui-icon-gripsmall-diagonal-se"),this.handles[n]=".ui-resizable-"+n,this.element.append(r)}}this._renderAxis=function(t){t=t||this.element;for(var i in this.handles){if(this.handles[i].constructor==String&&(this.handles[i]=e(this.handles[i],this.element).show()),this.elementIsWrapper&&this.originalElement[0].nodeName.match(/textarea|input|select|button/i)){var s=e(this.handles[i],this.element),a=0;a=/sw|ne|nw|se|n|s/.test(i)?s.outerHeight():s.outerWidth();var n=["padding",/ne|nw|n/.test(i)?"Top":/se|sw|s/.test(i)?"Bottom":/^e$/.test(i)?"Right":"Left"].join("");t.css(n,a),this._proportionallyResize()}e(this.handles[i]).length}},this._renderAxis(this.element),this._handles=e(".ui-resizable-handle",this.element).disableSelection(),this._handles.mouseover(function(){if(!t.resizing){if(this.className)var e=this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i);t.axis=e&&e[1]?e[1]:"se"}}),i.autoHide&&(this._handles.hide(),e(this.element).addClass("ui-resizable-autohide").mouseenter(function(){i.disabled||(e(this).removeClass("ui-resizable-autohide"),t._handles.show())}).mouseleave(function(){i.disabled||t.resizing||(e(this).addClass("ui-resizable-autohide"),t._handles.hide())})),this._mouseInit()},_destroy:function(){this._mouseDestroy();var t=function(t){e(t).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()};if(this.elementIsWrapper){t(this.element);var i=this.element;this.originalElement.css({position:i.css("position"),width:i.outerWidth(),height:i.outerHeight(),top:i.css("top"),left:i.css("left")}).insertAfter(i),i.remove()}return this.originalElement.css("resize",this.originalResizeStyle),t(this.originalElement),this},_mouseCapture:function(t){var i=!1;for(var s in this.handles)e(this.handles[s])[0]==t.target&&(i=!0);return!this.options.disabled&&i},_mouseStart:function(i){var s=this.options,a=this.element.position(),n=this.element;this.resizing=!0,this.documentScroll={top:e(document).scrollTop(),left:e(document).scrollLeft()},(n.is(".ui-draggable")||/absolute/.test(n.css("position")))&&n.css({position:"absolute",top:a.top,left:a.left}),this._renderProxy();var o=t(this.helper.css("left")),r=t(this.helper.css("top"));s.containment&&(o+=e(s.containment).scrollLeft()||0,r+=e(s.containment).scrollTop()||0),this.offset=this.helper.offset(),this.position={left:o,top:r},this.size=this._helper?{width:n.outerWidth(),height:n.outerHeight()}:{width:n.width(),height:n.height()},this.originalSize=this._helper?{width:n.outerWidth(),height:n.outerHeight()}:{width:n.width(),height:n.height()},this.originalPosition={left:o,top:r},this.sizeDiff={width:n.outerWidth()-n.width(),height:n.outerHeight()-n.height()},this.originalMousePosition={left:i.pageX,top:i.pageY},this.aspectRatio="number"==typeof s.aspectRatio?s.aspectRatio:this.originalSize.width/this.originalSize.height||1;var h=e(".ui-resizable-"+this.axis).css("cursor");return e("body").css("cursor","auto"==h?this.axis+"-resize":h),n.addClass("ui-resizable-resizing"),this._propagate("start",i),!0},_mouseDrag:function(e){var t=this.helper,i=(this.options,this.originalMousePosition),s=this.axis,a=e.pageX-i.left||0,n=e.pageY-i.top||0,o=this._change[s];if(!o)return!1;var r=o.apply(this,[e,a,n]);return this._updateVirtualBoundaries(e.shiftKey),(this._aspectRatio||e.shiftKey)&&(r=this._updateRatio(r,e)),r=this._respectSize(r,e),this._propagate("resize",e),t.css({top:this.position.top+"px",left:this.position.left+"px",width:this.size.width+"px",height:this.size.height+"px"}),!this._helper&&this._proportionallyResizeElements.length&&this._proportionallyResize(),this._updateCache(r),this._trigger("resize",e,this.ui()),!1},_mouseStop:function(t){this.resizing=!1;var i=this.options,s=this;if(this._helper){var a=this._proportionallyResizeElements,n=a.length&&/textarea/i.test(a[0].nodeName),o=n&&e.ui.hasScroll(a[0],"left")?0:s.sizeDiff.height,r=n?0:s.sizeDiff.width,h={width:s.helper.width()-r,height:s.helper.height()-o},l=parseInt(s.element.css("left"),10)+(s.position.left-s.originalPosition.left)||null,u=parseInt(s.element.css("top"),10)+(s.position.top-s.originalPosition.top)||null;i.animate||this.element.css(e.extend(h,{top:u,left:l})),s.helper.height(s.size.height),s.helper.width(s.size.width),this._helper&&!i.animate&&this._proportionallyResize()}return e("body").css("cursor","auto"),this.element.removeClass("ui-resizable-resizing"),this._propagate("stop",t),this._helper&&this.helper.remove(),!1},_updateVirtualBoundaries:function(e){var t,s,a,n,o,r=this.options;o={minWidth:i(r.minWidth)?r.minWidth:0,maxWidth:i(r.maxWidth)?r.maxWidth:1/0,minHeight:i(r.minHeight)?r.minHeight:0,maxHeight:i(r.maxHeight)?r.maxHeight:1/0},(this._aspectRatio||e)&&(t=o.minHeight*this.aspectRatio,a=o.minWidth/this.aspectRatio,s=o.maxHeight*this.aspectRatio,n=o.maxWidth/this.aspectRatio,t>o.minWidth&&(o.minWidth=t),a>o.minHeight&&(o.minHeight=a),o.maxWidth>s&&(o.maxWidth=s),o.maxHeight>n&&(o.maxHeight=n)),this._vBoundaries=o},_updateCache:function(e){this.options,this.offset=this.helper.offset(),i(e.left)&&(this.position.left=e.left),i(e.top)&&(this.position.top=e.top),i(e.height)&&(this.size.height=e.height),i(e.width)&&(this.size.width=e.width)},_updateRatio:function(e){var t=(this.options,this.position),s=this.size,a=this.axis;return i(e.height)?e.width=e.height*this.aspectRatio:i(e.width)&&(e.height=e.width/this.aspectRatio),"sw"==a&&(e.left=t.left+(s.width-e.width),e.top=null),"nw"==a&&(e.top=t.top+(s.height-e.height),e.left=t.left+(s.width-e.width)),e},_respectSize:function(e,t){var s=(this.helper,this._vBoundaries),a=(this._aspectRatio||t.shiftKey,this.axis),n=i(e.width)&&s.maxWidth&&s.maxWidth<e.width,o=i(e.height)&&s.maxHeight&&s.maxHeight<e.height,r=i(e.width)&&s.minWidth&&s.minWidth>e.width,h=i(e.height)&&s.minHeight&&s.minHeight>e.height;r&&(e.width=s.minWidth),h&&(e.height=s.minHeight),n&&(e.width=s.maxWidth),o&&(e.height=s.maxHeight);var l=this.originalPosition.left+this.originalSize.width,u=this.position.top+this.size.height,d=/sw|nw|w/.test(a),c=/nw|ne|n/.test(a);r&&d&&(e.left=l-s.minWidth),n&&d&&(e.left=l-s.maxWidth),h&&c&&(e.top=u-s.minHeight),o&&c&&(e.top=u-s.maxHeight);var p=!e.width&&!e.height;return p&&!e.left&&e.top?e.top=null:p&&!e.top&&e.left&&(e.left=null),e},_proportionallyResize:function(){if(this.options,this._proportionallyResizeElements.length)for(var t=this.helper||this.element,i=0;this._proportionallyResizeElements.length>i;i++){var s=this._proportionallyResizeElements[i];if(!this.borderDif){var a=[s.css("borderTopWidth"),s.css("borderRightWidth"),s.css("borderBottomWidth"),s.css("borderLeftWidth")],n=[s.css("paddingTop"),s.css("paddingRight"),s.css("paddingBottom"),s.css("paddingLeft")];this.borderDif=e.map(a,function(e,t){var i=parseInt(e,10)||0,s=parseInt(n[t],10)||0;return i+s})}s.css({height:t.height()-this.borderDif[0]-this.borderDif[2]||0,width:t.width()-this.borderDif[1]-this.borderDif[3]||0})}},_renderProxy:function(){var t=this.element,i=this.options;if(this.elementOffset=t.offset(),this._helper){this.helper=this.helper||e('<div style="overflow:hidden;"></div>');var s=e.ui.ie6?1:0,a=e.ui.ie6?2:-1;this.helper.addClass(this._helper).css({width:this.element.outerWidth()+a,height:this.element.outerHeight()+a,position:"absolute",left:this.elementOffset.left-s+"px",top:this.elementOffset.top-s+"px",zIndex:++i.zIndex}),this.helper.appendTo("body").disableSelection()}else this.helper=this.element},_change:{e:function(e,t){return{width:this.originalSize.width+t}},w:function(e,t){var i=(this.options,this.originalSize),s=this.originalPosition;return{left:s.left+t,width:i.width-t}},n:function(e,t,i){var s=(this.options,this.originalSize),a=this.originalPosition;return{top:a.top+i,height:s.height-i}},s:function(e,t,i){return{height:this.originalSize.height+i}},se:function(t,i,s){return e.extend(this._change.s.apply(this,arguments),this._change.e.apply(this,[t,i,s]))},sw:function(t,i,s){return e.extend(this._change.s.apply(this,arguments),this._change.w.apply(this,[t,i,s]))},ne:function(t,i,s){return e.extend(this._change.n.apply(this,arguments),this._change.e.apply(this,[t,i,s]))},nw:function(t,i,s){return e.extend(this._change.n.apply(this,arguments),this._change.w.apply(this,[t,i,s]))}},_propagate:function(t,i){e.ui.plugin.call(this,t,[i,this.ui()]),"resize"!=t&&this._trigger(t,i,this.ui())},plugins:{},ui:function(){return{originalElement:this.originalElement,element:this.element,helper:this.helper,position:this.position,size:this.size,originalSize:this.originalSize,originalPosition:this.originalPosition}}}),e.ui.plugin.add("resizable","alsoResize",{start:function(){var t=e(this).data("resizable"),i=t.options,s=function(t){e(t).each(function(){var t=e(this);t.data("resizable-alsoresize",{width:parseInt(t.width(),10),height:parseInt(t.height(),10),left:parseInt(t.css("left"),10),top:parseInt(t.css("top"),10)})})};"object"!=typeof i.alsoResize||i.alsoResize.parentNode?s(i.alsoResize):i.alsoResize.length?(i.alsoResize=i.alsoResize[0],s(i.alsoResize)):e.each(i.alsoResize,function(e){s(e)})},resize:function(t,i){var s=e(this).data("resizable"),a=s.options,n=s.originalSize,o=s.originalPosition,r={height:s.size.height-n.height||0,width:s.size.width-n.width||0,top:s.position.top-o.top||0,left:s.position.left-o.left||0},h=function(t,s){e(t).each(function(){var t=e(this),a=e(this).data("resizable-alsoresize"),n={},o=s&&s.length?s:t.parents(i.originalElement[0]).length?["width","height"]:["width","height","top","left"];e.each(o,function(e,t){var i=(a[t]||0)+(r[t]||0);i&&i>=0&&(n[t]=i||null)}),t.css(n)})};"object"!=typeof a.alsoResize||a.alsoResize.nodeType?h(a.alsoResize):e.each(a.alsoResize,function(e,t){h(e,t)})},stop:function(){e(this).removeData("resizable-alsoresize")}}),e.ui.plugin.add("resizable","animate",{stop:function(t){var i=e(this).data("resizable"),s=i.options,a=i._proportionallyResizeElements,n=a.length&&/textarea/i.test(a[0].nodeName),o=n&&e.ui.hasScroll(a[0],"left")?0:i.sizeDiff.height,r=n?0:i.sizeDiff.width,h={width:i.size.width-r,height:i.size.height-o},l=parseInt(i.element.css("left"),10)+(i.position.left-i.originalPosition.left)||null,u=parseInt(i.element.css("top"),10)+(i.position.top-i.originalPosition.top)||null;i.element.animate(e.extend(h,u&&l?{top:u,left:l}:{}),{duration:s.animateDuration,easing:s.animateEasing,step:function(){var s={width:parseInt(i.element.css("width"),10),height:parseInt(i.element.css("height"),10),top:parseInt(i.element.css("top"),10),left:parseInt(i.element.css("left"),10)};a&&a.length&&e(a[0]).css({width:s.width,height:s.height}),i._updateCache(s),i._propagate("resize",t)}})}}),e.ui.plugin.add("resizable","containment",{start:function(){var i=e(this).data("resizable"),s=i.options,a=i.element,n=s.containment,o=n instanceof e?n.get(0):/parent/.test(n)?a.parent().get(0):n;if(o)if(i.containerElement=e(o),/document/.test(n)||n==document)i.containerOffset={left:0,top:0},i.containerPosition={left:0,top:0},i.parentData={element:e(document),left:0,top:0,width:e(document).width(),height:e(document).height()||document.body.parentNode.scrollHeight};else{var r=e(o),h=[];e(["Top","Right","Left","Bottom"]).each(function(e,i){h[e]=t(r.css("padding"+i))}),i.containerOffset=r.offset(),i.containerPosition=r.position(),i.containerSize={height:r.innerHeight()-h[3],width:r.innerWidth()-h[1]};var l=i.containerOffset,u=i.containerSize.height,d=i.containerSize.width,c=e.ui.hasScroll(o,"left")?o.scrollWidth:d,p=e.ui.hasScroll(o)?o.scrollHeight:u;i.parentData={element:o,left:l.left,top:l.top,width:c,height:p}}},resize:function(t){var i=e(this).data("resizable"),s=i.options,a=(i.containerSize,i.containerOffset),n=(i.size,i.position),o=i._aspectRatio||t.shiftKey,r={top:0,left:0},h=i.containerElement;h[0]!=document&&/static/.test(h.css("position"))&&(r=a),n.left<(i._helper?a.left:0)&&(i.size.width=i.size.width+(i._helper?i.position.left-a.left:i.position.left-r.left),o&&(i.size.height=i.size.width/i.aspectRatio),i.position.left=s.helper?a.left:0),n.top<(i._helper?a.top:0)&&(i.size.height=i.size.height+(i._helper?i.position.top-a.top:i.position.top),o&&(i.size.width=i.size.height*i.aspectRatio),i.position.top=i._helper?a.top:0),i.offset.left=i.parentData.left+i.position.left,i.offset.top=i.parentData.top+i.position.top;var l=Math.abs((i._helper?i.offset.left-r.left:i.offset.left-r.left)+i.sizeDiff.width),u=Math.abs((i._helper?i.offset.top-r.top:i.offset.top-a.top)+i.sizeDiff.height),d=i.containerElement.get(0)==i.element.parent().get(0),c=/relative|absolute/.test(i.containerElement.css("position"));d&&c&&(l-=i.parentData.left),l+i.size.width>=i.parentData.width&&(i.size.width=i.parentData.width-l,o&&(i.size.height=i.size.width/i.aspectRatio)),u+i.size.height>=i.parentData.height&&(i.size.height=i.parentData.height-u,o&&(i.size.width=i.size.height*i.aspectRatio))},stop:function(){var t=e(this).data("resizable"),i=t.options,s=(t.position,t.containerOffset),a=t.containerPosition,n=t.containerElement,o=e(t.helper),r=o.offset(),h=o.outerWidth()-t.sizeDiff.width,l=o.outerHeight()-t.sizeDiff.height;t._helper&&!i.animate&&/relative/.test(n.css("position"))&&e(this).css({left:r.left-a.left-s.left,width:h,height:l}),t._helper&&!i.animate&&/static/.test(n.css("position"))&&e(this).css({left:r.left-a.left-s.left,width:h,height:l})}}),e.ui.plugin.add("resizable","ghost",{start:function(){var t=e(this).data("resizable"),i=t.options,s=t.size;t.ghost=t.originalElement.clone(),t.ghost.css({opacity:.25,display:"block",position:"relative",height:s.height,width:s.width,margin:0,left:0,top:0}).addClass("ui-resizable-ghost").addClass("string"==typeof i.ghost?i.ghost:""),t.ghost.appendTo(t.helper)},resize:function(){var t=e(this).data("resizable");t.options,t.ghost&&t.ghost.css({position:"relative",height:t.size.height,width:t.size.width})},stop:function(){var t=e(this).data("resizable");t.options,t.ghost&&t.helper&&t.helper.get(0).removeChild(t.ghost.get(0))}}),e.ui.plugin.add("resizable","grid",{resize:function(t){var i=e(this).data("resizable"),s=i.options,a=i.size,n=i.originalSize,o=i.originalPosition,r=i.axis;s._aspectRatio||t.shiftKey,s.grid="number"==typeof s.grid?[s.grid,s.grid]:s.grid;var h=Math.round((a.width-n.width)/(s.grid[0]||1))*(s.grid[0]||1),l=Math.round((a.height-n.height)/(s.grid[1]||1))*(s.grid[1]||1);/^(se|s|e)$/.test(r)?(i.size.width=n.width+h,i.size.height=n.height+l):/^(ne)$/.test(r)?(i.size.width=n.width+h,i.size.height=n.height+l,i.position.top=o.top-l):/^(sw)$/.test(r)?(i.size.width=n.width+h,i.size.height=n.height+l,i.position.left=o.left-h):(i.size.width=n.width+h,i.size.height=n.height+l,i.position.top=o.top-l,i.position.left=o.left-h)}});var t=function(e){return parseInt(e,10)||0},i=function(e){return!isNaN(parseInt(e,10))}})(jQuery);(function(e){e.widget("ui.selectable",e.ui.mouse,{version:"1.9.2",options:{appendTo:"body",autoRefresh:!0,distance:0,filter:"*",tolerance:"touch"},_create:function(){var t=this;this.element.addClass("ui-selectable"),this.dragged=!1;var i;this.refresh=function(){i=e(t.options.filter,t.element[0]),i.addClass("ui-selectee"),i.each(function(){var t=e(this),i=t.offset();e.data(this,"selectable-item",{element:this,$element:t,left:i.left,top:i.top,right:i.left+t.outerWidth(),bottom:i.top+t.outerHeight(),startselected:!1,selected:t.hasClass("ui-selected"),selecting:t.hasClass("ui-selecting"),unselecting:t.hasClass("ui-unselecting")})})},this.refresh(),this.selectees=i.addClass("ui-selectee"),this._mouseInit(),this.helper=e("<div class='ui-selectable-helper'></div>")},_destroy:function(){this.selectees.removeClass("ui-selectee").removeData("selectable-item"),this.element.removeClass("ui-selectable ui-selectable-disabled"),this._mouseDestroy()},_mouseStart:function(t){var i=this;if(this.opos=[t.pageX,t.pageY],!this.options.disabled){var s=this.options;this.selectees=e(s.filter,this.element[0]),this._trigger("start",t),e(s.appendTo).append(this.helper),this.helper.css({left:t.clientX,top:t.clientY,width:0,height:0}),s.autoRefresh&&this.refresh(),this.selectees.filter(".ui-selected").each(function(){var s=e.data(this,"selectable-item");s.startselected=!0,t.metaKey||t.ctrlKey||(s.$element.removeClass("ui-selected"),s.selected=!1,s.$element.addClass("ui-unselecting"),s.unselecting=!0,i._trigger("unselecting",t,{unselecting:s.element}))}),e(t.target).parents().andSelf().each(function(){var s=e.data(this,"selectable-item");if(s){var a=!t.metaKey&&!t.ctrlKey||!s.$element.hasClass("ui-selected");return s.$element.removeClass(a?"ui-unselecting":"ui-selected").addClass(a?"ui-selecting":"ui-unselecting"),s.unselecting=!a,s.selecting=a,s.selected=a,a?i._trigger("selecting",t,{selecting:s.element}):i._trigger("unselecting",t,{unselecting:s.element}),!1}})}},_mouseDrag:function(t){var i=this;if(this.dragged=!0,!this.options.disabled){var s=this.options,a=this.opos[0],n=this.opos[1],o=t.pageX,r=t.pageY;if(a>o){var h=o;o=a,a=h}if(n>r){var h=r;r=n,n=h}return this.helper.css({left:a,top:n,width:o-a,height:r-n}),this.selectees.each(function(){var h=e.data(this,"selectable-item");if(h&&h.element!=i.element[0]){var l=!1;"touch"==s.tolerance?l=!(h.left>o||a>h.right||h.top>r||n>h.bottom):"fit"==s.tolerance&&(l=h.left>a&&o>h.right&&h.top>n&&r>h.bottom),l?(h.selected&&(h.$element.removeClass("ui-selected"),h.selected=!1),h.unselecting&&(h.$element.removeClass("ui-unselecting"),h.unselecting=!1),h.selecting||(h.$element.addClass("ui-selecting"),h.selecting=!0,i._trigger("selecting",t,{selecting:h.element}))):(h.selecting&&((t.metaKey||t.ctrlKey)&&h.startselected?(h.$element.removeClass("ui-selecting"),h.selecting=!1,h.$element.addClass("ui-selected"),h.selected=!0):(h.$element.removeClass("ui-selecting"),h.selecting=!1,h.startselected&&(h.$element.addClass("ui-unselecting"),h.unselecting=!0),i._trigger("unselecting",t,{unselecting:h.element}))),h.selected&&(t.metaKey||t.ctrlKey||h.startselected||(h.$element.removeClass("ui-selected"),h.selected=!1,h.$element.addClass("ui-unselecting"),h.unselecting=!0,i._trigger("unselecting",t,{unselecting:h.element}))))}}),!1}},_mouseStop:function(t){var i=this;return this.dragged=!1,this.options,e(".ui-unselecting",this.element[0]).each(function(){var s=e.data(this,"selectable-item");s.$element.removeClass("ui-unselecting"),s.unselecting=!1,s.startselected=!1,i._trigger("unselected",t,{unselected:s.element})}),e(".ui-selecting",this.element[0]).each(function(){var s=e.data(this,"selectable-item");s.$element.removeClass("ui-selecting").addClass("ui-selected"),s.selecting=!1,s.selected=!0,s.startselected=!0,i._trigger("selected",t,{selected:s.element})}),this._trigger("stop",t),this.helper.remove(),!1}})})(jQuery);(function(e){e.widget("ui.sortable",e.ui.mouse,{version:"1.9.2",widgetEventPrefix:"sort",ready:!1,options:{appendTo:"parent",axis:!1,connectWith:!1,containment:!1,cursor:"auto",cursorAt:!1,dropOnEmpty:!0,forcePlaceholderSize:!1,forceHelperSize:!1,grid:!1,handle:!1,helper:"original",items:"> *",opacity:!1,placeholder:!1,revert:!1,scroll:!0,scrollSensitivity:20,scrollSpeed:20,scope:"default",tolerance:"intersect",zIndex:1e3},_create:function(){var e=this.options;this.containerCache={},this.element.addClass("ui-sortable"),this.refresh(),this.floating=this.items.length?"x"===e.axis||/left|right/.test(this.items[0].item.css("float"))||/inline|table-cell/.test(this.items[0].item.css("display")):!1,this.offset=this.element.offset(),this._mouseInit(),this.ready=!0},_destroy:function(){this.element.removeClass("ui-sortable ui-sortable-disabled"),this._mouseDestroy();for(var e=this.items.length-1;e>=0;e--)this.items[e].item.removeData(this.widgetName+"-item");return this},_setOption:function(t,i){"disabled"===t?(this.options[t]=i,this.widget().toggleClass("ui-sortable-disabled",!!i)):e.Widget.prototype._setOption.apply(this,arguments)},_mouseCapture:function(t,i){var s=this;if(this.reverting)return!1;if(this.options.disabled||"static"==this.options.type)return!1;this._refreshItems(t);var a=null;if(e(t.target).parents().each(function(){return e.data(this,s.widgetName+"-item")==s?(a=e(this),!1):undefined}),e.data(t.target,s.widgetName+"-item")==s&&(a=e(t.target)),!a)return!1;if(this.options.handle&&!i){var n=!1;if(e(this.options.handle,a).find("*").andSelf().each(function(){this==t.target&&(n=!0)}),!n)return!1}return this.currentItem=a,this._removeCurrentsFromItems(),!0},_mouseStart:function(t,i,s){var a=this.options;if(this.currentContainer=this,this.refreshPositions(),this.helper=this._createHelper(t),this._cacheHelperProportions(),this._cacheMargins(),this.scrollParent=this.helper.scrollParent(),this.offset=this.currentItem.offset(),this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left},e.extend(this.offset,{click:{left:t.pageX-this.offset.left,top:t.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()}),this.helper.css("position","absolute"),this.cssPosition=this.helper.css("position"),this.originalPosition=this._generatePosition(t),this.originalPageX=t.pageX,this.originalPageY=t.pageY,a.cursorAt&&this._adjustOffsetFromHelper(a.cursorAt),this.domPosition={prev:this.currentItem.prev()[0],parent:this.currentItem.parent()[0]},this.helper[0]!=this.currentItem[0]&&this.currentItem.hide(),this._createPlaceholder(),a.containment&&this._setContainment(),a.cursor&&(e("body").css("cursor")&&(this._storedCursor=e("body").css("cursor")),e("body").css("cursor",a.cursor)),a.opacity&&(this.helper.css("opacity")&&(this._storedOpacity=this.helper.css("opacity")),this.helper.css("opacity",a.opacity)),a.zIndex&&(this.helper.css("zIndex")&&(this._storedZIndex=this.helper.css("zIndex")),this.helper.css("zIndex",a.zIndex)),this.scrollParent[0]!=document&&"HTML"!=this.scrollParent[0].tagName&&(this.overflowOffset=this.scrollParent.offset()),this._trigger("start",t,this._uiHash()),this._preserveHelperProportions||this._cacheHelperProportions(),!s)for(var n=this.containers.length-1;n>=0;n--)this.containers[n]._trigger("activate",t,this._uiHash(this));return e.ui.ddmanager&&(e.ui.ddmanager.current=this),e.ui.ddmanager&&!a.dropBehaviour&&e.ui.ddmanager.prepareOffsets(this,t),this.dragging=!0,this.helper.addClass("ui-sortable-helper"),this._mouseDrag(t),!0},_mouseDrag:function(t){if(this.position=this._generatePosition(t),this.positionAbs=this._convertPositionTo("absolute"),this.lastPositionAbs||(this.lastPositionAbs=this.positionAbs),this.options.scroll){var i=this.options,s=!1;this.scrollParent[0]!=document&&"HTML"!=this.scrollParent[0].tagName?(this.overflowOffset.top+this.scrollParent[0].offsetHeight-t.pageY<i.scrollSensitivity?this.scrollParent[0].scrollTop=s=this.scrollParent[0].scrollTop+i.scrollSpeed:t.pageY-this.overflowOffset.top<i.scrollSensitivity&&(this.scrollParent[0].scrollTop=s=this.scrollParent[0].scrollTop-i.scrollSpeed),this.overflowOffset.left+this.scrollParent[0].offsetWidth-t.pageX<i.scrollSensitivity?this.scrollParent[0].scrollLeft=s=this.scrollParent[0].scrollLeft+i.scrollSpeed:t.pageX-this.overflowOffset.left<i.scrollSensitivity&&(this.scrollParent[0].scrollLeft=s=this.scrollParent[0].scrollLeft-i.scrollSpeed)):(t.pageY-e(document).scrollTop()<i.scrollSensitivity?s=e(document).scrollTop(e(document).scrollTop()-i.scrollSpeed):e(window).height()-(t.pageY-e(document).scrollTop())<i.scrollSensitivity&&(s=e(document).scrollTop(e(document).scrollTop()+i.scrollSpeed)),t.pageX-e(document).scrollLeft()<i.scrollSensitivity?s=e(document).scrollLeft(e(document).scrollLeft()-i.scrollSpeed):e(window).width()-(t.pageX-e(document).scrollLeft())<i.scrollSensitivity&&(s=e(document).scrollLeft(e(document).scrollLeft()+i.scrollSpeed))),s!==!1&&e.ui.ddmanager&&!i.dropBehaviour&&e.ui.ddmanager.prepareOffsets(this,t)}this.positionAbs=this._convertPositionTo("absolute"),this.options.axis&&"y"==this.options.axis||(this.helper[0].style.left=this.position.left+"px"),this.options.axis&&"x"==this.options.axis||(this.helper[0].style.top=this.position.top+"px");for(var a=this.items.length-1;a>=0;a--){var n=this.items[a],o=n.item[0],r=this._intersectsWithPointer(n);if(r&&n.instance===this.currentContainer&&o!=this.currentItem[0]&&this.placeholder[1==r?"next":"prev"]()[0]!=o&&!e.contains(this.placeholder[0],o)&&("semi-dynamic"==this.options.type?!e.contains(this.element[0],o):!0)){if(this.direction=1==r?"down":"up","pointer"!=this.options.tolerance&&!this._intersectsWithSides(n))break;this._rearrange(t,n),this._trigger("change",t,this._uiHash());break}}return this._contactContainers(t),e.ui.ddmanager&&e.ui.ddmanager.drag(this,t),this._trigger("sort",t,this._uiHash()),this.lastPositionAbs=this.positionAbs,!1},_mouseStop:function(t,i){if(t){if(e.ui.ddmanager&&!this.options.dropBehaviour&&e.ui.ddmanager.drop(this,t),this.options.revert){var s=this,a=this.placeholder.offset();this.reverting=!0,e(this.helper).animate({left:a.left-this.offset.parent.left-this.margins.left+(this.offsetParent[0]==document.body?0:this.offsetParent[0].scrollLeft),top:a.top-this.offset.parent.top-this.margins.top+(this.offsetParent[0]==document.body?0:this.offsetParent[0].scrollTop)},parseInt(this.options.revert,10)||500,function(){s._clear(t)})}else this._clear(t,i);return!1}},cancel:function(){if(this.dragging){this._mouseUp({target:null}),"original"==this.options.helper?this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper"):this.currentItem.show();for(var t=this.containers.length-1;t>=0;t--)this.containers[t]._trigger("deactivate",null,this._uiHash(this)),this.containers[t].containerCache.over&&(this.containers[t]._trigger("out",null,this._uiHash(this)),this.containers[t].containerCache.over=0)}return this.placeholder&&(this.placeholder[0].parentNode&&this.placeholder[0].parentNode.removeChild(this.placeholder[0]),"original"!=this.options.helper&&this.helper&&this.helper[0].parentNode&&this.helper.remove(),e.extend(this,{helper:null,dragging:!1,reverting:!1,_noFinalSort:null}),this.domPosition.prev?e(this.domPosition.prev).after(this.currentItem):e(this.domPosition.parent).prepend(this.currentItem)),this},serialize:function(t){var i=this._getItemsAsjQuery(t&&t.connected),s=[];return t=t||{},e(i).each(function(){var i=(e(t.item||this).attr(t.attribute||"id")||"").match(t.expression||/(.+)[-=_](.+)/);i&&s.push((t.key||i[1]+"[]")+"="+(t.key&&t.expression?i[1]:i[2]))}),!s.length&&t.key&&s.push(t.key+"="),s.join("&")},toArray:function(t){var i=this._getItemsAsjQuery(t&&t.connected),s=[];return t=t||{},i.each(function(){s.push(e(t.item||this).attr(t.attribute||"id")||"")}),s},_intersectsWith:function(e){var t=this.positionAbs.left,i=t+this.helperProportions.width,s=this.positionAbs.top,a=s+this.helperProportions.height,n=e.left,o=n+e.width,r=e.top,h=r+e.height,l=this.offset.click.top,u=this.offset.click.left,d=s+l>r&&h>s+l&&t+u>n&&o>t+u;return"pointer"==this.options.tolerance||this.options.forcePointerForContainers||"pointer"!=this.options.tolerance&&this.helperProportions[this.floating?"width":"height"]>e[this.floating?"width":"height"]?d:t+this.helperProportions.width/2>n&&o>i-this.helperProportions.width/2&&s+this.helperProportions.height/2>r&&h>a-this.helperProportions.height/2},_intersectsWithPointer:function(t){var i="x"===this.options.axis||e.ui.isOverAxis(this.positionAbs.top+this.offset.click.top,t.top,t.height),s="y"===this.options.axis||e.ui.isOverAxis(this.positionAbs.left+this.offset.click.left,t.left,t.width),a=i&&s,n=this._getDragVerticalDirection(),o=this._getDragHorizontalDirection();return a?this.floating?o&&"right"==o||"down"==n?2:1:n&&("down"==n?2:1):!1},_intersectsWithSides:function(t){var i=e.ui.isOverAxis(this.positionAbs.top+this.offset.click.top,t.top+t.height/2,t.height),s=e.ui.isOverAxis(this.positionAbs.left+this.offset.click.left,t.left+t.width/2,t.width),a=this._getDragVerticalDirection(),n=this._getDragHorizontalDirection();return this.floating&&n?"right"==n&&s||"left"==n&&!s:a&&("down"==a&&i||"up"==a&&!i)},_getDragVerticalDirection:function(){var e=this.positionAbs.top-this.lastPositionAbs.top;return 0!=e&&(e>0?"down":"up")},_getDragHorizontalDirection:function(){var e=this.positionAbs.left-this.lastPositionAbs.left;return 0!=e&&(e>0?"right":"left")},refresh:function(e){return this._refreshItems(e),this.refreshPositions(),this},_connectWith:function(){var e=this.options;return e.connectWith.constructor==String?[e.connectWith]:e.connectWith},_getItemsAsjQuery:function(t){var i=[],s=[],a=this._connectWith();if(a&&t)for(var n=a.length-1;n>=0;n--)for(var o=e(a[n]),r=o.length-1;r>=0;r--){var h=e.data(o[r],this.widgetName);h&&h!=this&&!h.options.disabled&&s.push([e.isFunction(h.options.items)?h.options.items.call(h.element):e(h.options.items,h.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),h])}s.push([e.isFunction(this.options.items)?this.options.items.call(this.element,null,{options:this.options,item:this.currentItem}):e(this.options.items,this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),this]);for(var n=s.length-1;n>=0;n--)s[n][0].each(function(){i.push(this)});return e(i)},_removeCurrentsFromItems:function(){var t=this.currentItem.find(":data("+this.widgetName+"-item)");this.items=e.grep(this.items,function(e){for(var i=0;t.length>i;i++)if(t[i]==e.item[0])return!1;return!0})},_refreshItems:function(t){this.items=[],this.containers=[this];var i=this.items,s=[[e.isFunction(this.options.items)?this.options.items.call(this.element[0],t,{item:this.currentItem}):e(this.options.items,this.element),this]],a=this._connectWith();if(a&&this.ready)for(var n=a.length-1;n>=0;n--)for(var o=e(a[n]),r=o.length-1;r>=0;r--){var h=e.data(o[r],this.widgetName);h&&h!=this&&!h.options.disabled&&(s.push([e.isFunction(h.options.items)?h.options.items.call(h.element[0],t,{item:this.currentItem}):e(h.options.items,h.element),h]),this.containers.push(h))}for(var n=s.length-1;n>=0;n--)for(var l=s[n][1],u=s[n][0],r=0,d=u.length;d>r;r++){var c=e(u[r]);c.data(this.widgetName+"-item",l),i.push({item:c,instance:l,width:0,height:0,left:0,top:0})}},refreshPositions:function(t){this.offsetParent&&this.helper&&(this.offset.parent=this._getParentOffset());for(var i=this.items.length-1;i>=0;i--){var s=this.items[i];if(s.instance==this.currentContainer||!this.currentContainer||s.item[0]==this.currentItem[0]){var a=this.options.toleranceElement?e(this.options.toleranceElement,s.item):s.item;t||(s.width=a.outerWidth(),s.height=a.outerHeight());var n=a.offset();s.left=n.left,s.top=n.top}}if(this.options.custom&&this.options.custom.refreshContainers)this.options.custom.refreshContainers.call(this);else for(var i=this.containers.length-1;i>=0;i--){var n=this.containers[i].element.offset();this.containers[i].containerCache.left=n.left,this.containers[i].containerCache.top=n.top,this.containers[i].containerCache.width=this.containers[i].element.outerWidth(),this.containers[i].containerCache.height=this.containers[i].element.outerHeight()}return this},_createPlaceholder:function(t){t=t||this;var i=t.options;if(!i.placeholder||i.placeholder.constructor==String){var s=i.placeholder;i.placeholder={element:function(){var i=e(document.createElement(t.currentItem[0].nodeName)).addClass(s||t.currentItem[0].className+" ui-sortable-placeholder").removeClass("ui-sortable-helper")[0];return s||(i.style.visibility="hidden"),i},update:function(e,a){(!s||i.forcePlaceholderSize)&&(a.height()||a.height(t.currentItem.innerHeight()-parseInt(t.currentItem.css("paddingTop")||0,10)-parseInt(t.currentItem.css("paddingBottom")||0,10)),a.width()||a.width(t.currentItem.innerWidth()-parseInt(t.currentItem.css("paddingLeft")||0,10)-parseInt(t.currentItem.css("paddingRight")||0,10)))}}}t.placeholder=e(i.placeholder.element.call(t.element,t.currentItem)),t.currentItem.after(t.placeholder),i.placeholder.update(t,t.placeholder)},_contactContainers:function(t){for(var i=null,s=null,a=this.containers.length-1;a>=0;a--)if(!e.contains(this.currentItem[0],this.containers[a].element[0]))if(this._intersectsWith(this.containers[a].containerCache)){if(i&&e.contains(this.containers[a].element[0],i.element[0]))continue;i=this.containers[a],s=a}else this.containers[a].containerCache.over&&(this.containers[a]._trigger("out",t,this._uiHash(this)),this.containers[a].containerCache.over=0);if(i)if(1===this.containers.length)this.containers[s]._trigger("over",t,this._uiHash(this)),this.containers[s].containerCache.over=1;else{for(var n=1e4,o=null,r=this.containers[s].floating?"left":"top",h=this.containers[s].floating?"width":"height",l=this.positionAbs[r]+this.offset.click[r],u=this.items.length-1;u>=0;u--)if(e.contains(this.containers[s].element[0],this.items[u].item[0])&&this.items[u].item[0]!=this.currentItem[0]){var d=this.items[u].item.offset()[r],c=!1;Math.abs(d-l)>Math.abs(d+this.items[u][h]-l)&&(c=!0,d+=this.items[u][h]),n>Math.abs(d-l)&&(n=Math.abs(d-l),o=this.items[u],this.direction=c?"up":"down")}if(!o&&!this.options.dropOnEmpty)return;this.currentContainer=this.containers[s],o?this._rearrange(t,o,null,!0):this._rearrange(t,null,this.containers[s].element,!0),this._trigger("change",t,this._uiHash()),this.containers[s]._trigger("change",t,this._uiHash(this)),this.options.placeholder.update(this.currentContainer,this.placeholder),this.containers[s]._trigger("over",t,this._uiHash(this)),this.containers[s].containerCache.over=1}},_createHelper:function(t){var i=this.options,s=e.isFunction(i.helper)?e(i.helper.apply(this.element[0],[t,this.currentItem])):"clone"==i.helper?this.currentItem.clone():this.currentItem;return s.parents("body").length||e("parent"!=i.appendTo?i.appendTo:this.currentItem[0].parentNode)[0].appendChild(s[0]),s[0]==this.currentItem[0]&&(this._storedCSS={width:this.currentItem[0].style.width,height:this.currentItem[0].style.height,position:this.currentItem.css("position"),top:this.currentItem.css("top"),left:this.currentItem.css("left")}),(""==s[0].style.width||i.forceHelperSize)&&s.width(this.currentItem.width()),(""==s[0].style.height||i.forceHelperSize)&&s.height(this.currentItem.height()),s},_adjustOffsetFromHelper:function(t){"string"==typeof t&&(t=t.split(" ")),e.isArray(t)&&(t={left:+t[0],top:+t[1]||0}),"left"in t&&(this.offset.click.left=t.left+this.margins.left),"right"in t&&(this.offset.click.left=this.helperProportions.width-t.right+this.margins.left),"top"in t&&(this.offset.click.top=t.top+this.margins.top),"bottom"in t&&(this.offset.click.top=this.helperProportions.height-t.bottom+this.margins.top)},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var t=this.offsetParent.offset();return"absolute"==this.cssPosition&&this.scrollParent[0]!=document&&e.contains(this.scrollParent[0],this.offsetParent[0])&&(t.left+=this.scrollParent.scrollLeft(),t.top+=this.scrollParent.scrollTop()),(this.offsetParent[0]==document.body||this.offsetParent[0].tagName&&"html"==this.offsetParent[0].tagName.toLowerCase()&&e.ui.ie)&&(t={top:0,left:0}),{top:t.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:t.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if("relative"==this.cssPosition){var e=this.currentItem.position();return{top:e.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:e.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}}return{top:0,left:0}},_cacheMargins:function(){this.margins={left:parseInt(this.currentItem.css("marginLeft"),10)||0,top:parseInt(this.currentItem.css("marginTop"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var t=this.options;if("parent"==t.containment&&(t.containment=this.helper[0].parentNode),("document"==t.containment||"window"==t.containment)&&(this.containment=[0-this.offset.relative.left-this.offset.parent.left,0-this.offset.relative.top-this.offset.parent.top,e("document"==t.containment?document:window).width()-this.helperProportions.width-this.margins.left,(e("document"==t.containment?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top]),!/^(document|window|parent)$/.test(t.containment)){var i=e(t.containment)[0],s=e(t.containment).offset(),a="hidden"!=e(i).css("overflow");this.containment=[s.left+(parseInt(e(i).css("borderLeftWidth"),10)||0)+(parseInt(e(i).css("paddingLeft"),10)||0)-this.margins.left,s.top+(parseInt(e(i).css("borderTopWidth"),10)||0)+(parseInt(e(i).css("paddingTop"),10)||0)-this.margins.top,s.left+(a?Math.max(i.scrollWidth,i.offsetWidth):i.offsetWidth)-(parseInt(e(i).css("borderLeftWidth"),10)||0)-(parseInt(e(i).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left,s.top+(a?Math.max(i.scrollHeight,i.offsetHeight):i.offsetHeight)-(parseInt(e(i).css("borderTopWidth"),10)||0)-(parseInt(e(i).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top]}},_convertPositionTo:function(t,i){i||(i=this.position);var s="absolute"==t?1:-1,a=(this.options,"absolute"!=this.cssPosition||this.scrollParent[0]!=document&&e.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent),n=/(html|body)/i.test(a[0].tagName);return{top:i.top+this.offset.relative.top*s+this.offset.parent.top*s-("fixed"==this.cssPosition?-this.scrollParent.scrollTop():n?0:a.scrollTop())*s,left:i.left+this.offset.relative.left*s+this.offset.parent.left*s-("fixed"==this.cssPosition?-this.scrollParent.scrollLeft():n?0:a.scrollLeft())*s}},_generatePosition:function(t){var i=this.options,s="absolute"!=this.cssPosition||this.scrollParent[0]!=document&&e.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,a=/(html|body)/i.test(s[0].tagName);"relative"!=this.cssPosition||this.scrollParent[0]!=document&&this.scrollParent[0]!=this.offsetParent[0]||(this.offset.relative=this._getRelativeOffset());var n=t.pageX,o=t.pageY;if(this.originalPosition&&(this.containment&&(t.pageX-this.offset.click.left<this.containment[0]&&(n=this.containment[0]+this.offset.click.left),t.pageY-this.offset.click.top<this.containment[1]&&(o=this.containment[1]+this.offset.click.top),t.pageX-this.offset.click.left>this.containment[2]&&(n=this.containment[2]+this.offset.click.left),t.pageY-this.offset.click.top>this.containment[3]&&(o=this.containment[3]+this.offset.click.top)),i.grid)){var r=this.originalPageY+Math.round((o-this.originalPageY)/i.grid[1])*i.grid[1];o=this.containment?r-this.offset.click.top<this.containment[1]||r-this.offset.click.top>this.containment[3]?r-this.offset.click.top<this.containment[1]?r+i.grid[1]:r-i.grid[1]:r:r;var h=this.originalPageX+Math.round((n-this.originalPageX)/i.grid[0])*i.grid[0];n=this.containment?h-this.offset.click.left<this.containment[0]||h-this.offset.click.left>this.containment[2]?h-this.offset.click.left<this.containment[0]?h+i.grid[0]:h-i.grid[0]:h:h}return{top:o-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+("fixed"==this.cssPosition?-this.scrollParent.scrollTop():a?0:s.scrollTop()),left:n-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+("fixed"==this.cssPosition?-this.scrollParent.scrollLeft():a?0:s.scrollLeft())}},_rearrange:function(e,t,i,s){i?i[0].appendChild(this.placeholder[0]):t.item[0].parentNode.insertBefore(this.placeholder[0],"down"==this.direction?t.item[0]:t.item[0].nextSibling),this.counter=this.counter?++this.counter:1;var a=this.counter;this._delay(function(){a==this.counter&&this.refreshPositions(!s)})},_clear:function(t,i){this.reverting=!1;var s=[];if(!this._noFinalSort&&this.currentItem.parent().length&&this.placeholder.before(this.currentItem),this._noFinalSort=null,this.helper[0]==this.currentItem[0]){for(var a in this._storedCSS)("auto"==this._storedCSS[a]||"static"==this._storedCSS[a])&&(this._storedCSS[a]="");this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")}else this.currentItem.show();this.fromOutside&&!i&&s.push(function(e){this._trigger("receive",e,this._uiHash(this.fromOutside))}),!this.fromOutside&&this.domPosition.prev==this.currentItem.prev().not(".ui-sortable-helper")[0]&&this.domPosition.parent==this.currentItem.parent()[0]||i||s.push(function(e){this._trigger("update",e,this._uiHash())}),this!==this.currentContainer&&(i||(s.push(function(e){this._trigger("remove",e,this._uiHash())}),s.push(function(e){return function(t){e._trigger("receive",t,this._uiHash(this))}}.call(this,this.currentContainer)),s.push(function(e){return function(t){e._trigger("update",t,this._uiHash(this))}}.call(this,this.currentContainer))));for(var a=this.containers.length-1;a>=0;a--)i||s.push(function(e){return function(t){e._trigger("deactivate",t,this._uiHash(this))}}.call(this,this.containers[a])),this.containers[a].containerCache.over&&(s.push(function(e){return function(t){e._trigger("out",t,this._uiHash(this))}}.call(this,this.containers[a])),this.containers[a].containerCache.over=0);if(this._storedCursor&&e("body").css("cursor",this._storedCursor),this._storedOpacity&&this.helper.css("opacity",this._storedOpacity),this._storedZIndex&&this.helper.css("zIndex","auto"==this._storedZIndex?"":this._storedZIndex),this.dragging=!1,this.cancelHelperRemoval){if(!i){this._trigger("beforeStop",t,this._uiHash());for(var a=0;s.length>a;a++)s[a].call(this,t);this._trigger("stop",t,this._uiHash())}return this.fromOutside=!1,!1}if(i||this._trigger("beforeStop",t,this._uiHash()),this.placeholder[0].parentNode.removeChild(this.placeholder[0]),this.helper[0]!=this.currentItem[0]&&this.helper.remove(),this.helper=null,!i){for(var a=0;s.length>a;a++)s[a].call(this,t);this._trigger("stop",t,this._uiHash())}return this.fromOutside=!1,!0},_trigger:function(){e.Widget.prototype._trigger.apply(this,arguments)===!1&&this.cancel()},_uiHash:function(t){var i=t||this;return{helper:i.helper,placeholder:i.placeholder||e([]),position:i.position,originalPosition:i.originalPosition,offset:i.positionAbs,item:i.currentItem,sender:t?t.element:null}}})})(jQuery);(function(e){var t=0,i={},s={};i.height=i.paddingTop=i.paddingBottom=i.borderTopWidth=i.borderBottomWidth="hide",s.height=s.paddingTop=s.paddingBottom=s.borderTopWidth=s.borderBottomWidth="show",e.widget("ui.accordion",{version:"1.9.2",options:{active:0,animate:{},collapsible:!1,event:"click",header:"> li > :first-child,> :not(li):even",heightStyle:"auto",icons:{activeHeader:"ui-icon-triangle-1-s",header:"ui-icon-triangle-1-e"},activate:null,beforeActivate:null},_create:function(){var i=this.accordionId="ui-accordion-"+(this.element.attr("id")||++t),s=this.options;this.prevShow=this.prevHide=e(),this.element.addClass("ui-accordion ui-widget ui-helper-reset"),this.headers=this.element.find(s.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"),this._hoverable(this.headers),this._focusable(this.headers),this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").hide(),s.collapsible||s.active!==!1&&null!=s.active||(s.active=0),0>s.active&&(s.active+=this.headers.length),this.active=this._findActive(s.active).addClass("ui-accordion-header-active ui-state-active").toggleClass("ui-corner-all ui-corner-top"),this.active.next().addClass("ui-accordion-content-active").show(),this._createIcons(),this.refresh(),this.element.attr("role","tablist"),this.headers.attr("role","tab").each(function(t){var s=e(this),a=s.attr("id"),n=s.next(),o=n.attr("id");a||(a=i+"-header-"+t,s.attr("id",a)),o||(o=i+"-panel-"+t,n.attr("id",o)),s.attr("aria-controls",o),n.attr("aria-labelledby",a)}).next().attr("role","tabpanel"),this.headers.not(this.active).attr({"aria-selected":"false",tabIndex:-1}).next().attr({"aria-expanded":"false","aria-hidden":"true"}).hide(),this.active.length?this.active.attr({"aria-selected":"true",tabIndex:0}).next().attr({"aria-expanded":"true","aria-hidden":"false"}):this.headers.eq(0).attr("tabIndex",0),this._on(this.headers,{keydown:"_keydown"}),this._on(this.headers.next(),{keydown:"_panelKeyDown"}),this._setupEvents(s.event)},_getCreateEventData:function(){return{header:this.active,content:this.active.length?this.active.next():e()}},_createIcons:function(){var t=this.options.icons;t&&(e("<span>").addClass("ui-accordion-header-icon ui-icon "+t.header).prependTo(this.headers),this.active.children(".ui-accordion-header-icon").removeClass(t.header).addClass(t.activeHeader),this.headers.addClass("ui-accordion-icons"))},_destroyIcons:function(){this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove()},_destroy:function(){var e;this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role"),this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").each(function(){/^ui-accordion/.test(this.id)&&this.removeAttribute("id")}),this._destroyIcons(),e=this.headers.next().css("display","").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").each(function(){/^ui-accordion/.test(this.id)&&this.removeAttribute("id")}),"content"!==this.options.heightStyle&&e.css("height","")},_setOption:function(e,t){return"active"===e?(this._activate(t),undefined):("event"===e&&(this.options.event&&this._off(this.headers,this.options.event),this._setupEvents(t)),this._super(e,t),"collapsible"!==e||t||this.options.active!==!1||this._activate(0),"icons"===e&&(this._destroyIcons(),t&&this._createIcons()),"disabled"===e&&this.headers.add(this.headers.next()).toggleClass("ui-state-disabled",!!t),undefined)},_keydown:function(t){if(!t.altKey&&!t.ctrlKey){var i=e.ui.keyCode,s=this.headers.length,a=this.headers.index(t.target),n=!1;switch(t.keyCode){case i.RIGHT:case i.DOWN:n=this.headers[(a+1)%s];break;case i.LEFT:case i.UP:n=this.headers[(a-1+s)%s];break;case i.SPACE:case i.ENTER:this._eventHandler(t);break;case i.HOME:n=this.headers[0];break;case i.END:n=this.headers[s-1]}n&&(e(t.target).attr("tabIndex",-1),e(n).attr("tabIndex",0),n.focus(),t.preventDefault())}},_panelKeyDown:function(t){t.keyCode===e.ui.keyCode.UP&&t.ctrlKey&&e(t.currentTarget).prev().focus()},refresh:function(){var t,i,s=this.options.heightStyle,a=this.element.parent();"fill"===s?(e.support.minHeight||(i=a.css("overflow"),a.css("overflow","hidden")),t=a.height(),this.element.siblings(":visible").each(function(){var i=e(this),s=i.css("position");"absolute"!==s&&"fixed"!==s&&(t-=i.outerHeight(!0))}),i&&a.css("overflow",i),this.headers.each(function(){t-=e(this).outerHeight(!0)}),this.headers.next().each(function(){e(this).height(Math.max(0,t-e(this).innerHeight()+e(this).height()))}).css("overflow","auto")):"auto"===s&&(t=0,this.headers.next().each(function(){t=Math.max(t,e(this).css("height","").height())}).height(t))},_activate:function(t){var i=this._findActive(t)[0];i!==this.active[0]&&(i=i||this.active[0],this._eventHandler({target:i,currentTarget:i,preventDefault:e.noop}))},_findActive:function(t){return"number"==typeof t?this.headers.eq(t):e()},_setupEvents:function(t){var i={};t&&(e.each(t.split(" "),function(e,t){i[t]="_eventHandler"}),this._on(this.headers,i))},_eventHandler:function(t){var i=this.options,s=this.active,a=e(t.currentTarget),n=a[0]===s[0],o=n&&i.collapsible,r=o?e():a.next(),h=s.next(),l={oldHeader:s,oldPanel:h,newHeader:o?e():a,newPanel:r};t.preventDefault(),n&&!i.collapsible||this._trigger("beforeActivate",t,l)===!1||(i.active=o?!1:this.headers.index(a),this.active=n?e():a,this._toggle(l),s.removeClass("ui-accordion-header-active ui-state-active"),i.icons&&s.children(".ui-accordion-header-icon").removeClass(i.icons.activeHeader).addClass(i.icons.header),n||(a.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top"),i.icons&&a.children(".ui-accordion-header-icon").removeClass(i.icons.header).addClass(i.icons.activeHeader),a.next().addClass("ui-accordion-content-active")))},_toggle:function(t){var i=t.newPanel,s=this.prevShow.length?this.prevShow:t.oldPanel;this.prevShow.add(this.prevHide).stop(!0,!0),this.prevShow=i,this.prevHide=s,this.options.animate?this._animate(i,s,t):(s.hide(),i.show(),this._toggleComplete(t)),s.attr({"aria-expanded":"false","aria-hidden":"true"}),s.prev().attr("aria-selected","false"),i.length&&s.length?s.prev().attr("tabIndex",-1):i.length&&this.headers.filter(function(){return 0===e(this).attr("tabIndex")}).attr("tabIndex",-1),i.attr({"aria-expanded":"true","aria-hidden":"false"}).prev().attr({"aria-selected":"true",tabIndex:0})},_animate:function(e,t,a){var n,o,r,h=this,l=0,u=e.length&&(!t.length||e.index()<t.index()),d=this.options.animate||{},c=u&&d.down||d,p=function(){h._toggleComplete(a)};return"number"==typeof c&&(r=c),"string"==typeof c&&(o=c),o=o||c.easing||d.easing,r=r||c.duration||d.duration,t.length?e.length?(n=e.show().outerHeight(),t.animate(i,{duration:r,easing:o,step:function(e,t){t.now=Math.round(e)}}),e.hide().animate(s,{duration:r,easing:o,complete:p,step:function(e,i){i.now=Math.round(e),"height"!==i.prop?l+=i.now:"content"!==h.options.heightStyle&&(i.now=Math.round(n-t.outerHeight()-l),l=0)}}),undefined):t.animate(i,r,o,p):e.animate(s,r,o,p)},_toggleComplete:function(e){var t=e.oldPanel;t.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all"),t.length&&(t.parent()[0].className=t.parent()[0].className),this._trigger("activate",null,e)}}),e.uiBackCompat!==!1&&(function(e,t){e.extend(t.options,{navigation:!1,navigationFilter:function(){return this.href.toLowerCase()===location.href.toLowerCase()}});var i=t._create;t._create=function(){if(this.options.navigation){var t=this,s=this.element.find(this.options.header),a=s.next(),n=s.add(a).find("a").filter(this.options.navigationFilter)[0];n&&s.add(a).each(function(i){return e.contains(this,n)?(t.options.active=Math.floor(i/2),!1):undefined})}i.call(this)}}(jQuery,jQuery.ui.accordion.prototype),function(e,t){e.extend(t.options,{heightStyle:null,autoHeight:!0,clearStyle:!1,fillSpace:!1});var i=t._create,s=t._setOption;e.extend(t,{_create:function(){this.options.heightStyle=this.options.heightStyle||this._mergeHeightStyle(),i.call(this)},_setOption:function(e){("autoHeight"===e||"clearStyle"===e||"fillSpace"===e)&&(this.options.heightStyle=this._mergeHeightStyle()),s.apply(this,arguments)},_mergeHeightStyle:function(){var e=this.options;return e.fillSpace?"fill":e.clearStyle?"content":e.autoHeight?"auto":undefined}})}(jQuery,jQuery.ui.accordion.prototype),function(e,t){e.extend(t.options.icons,{activeHeader:null,headerSelected:"ui-icon-triangle-1-s"});var i=t._createIcons;t._createIcons=function(){this.options.icons&&(this.options.icons.activeHeader=this.options.icons.activeHeader||this.options.icons.headerSelected),i.call(this)}}(jQuery,jQuery.ui.accordion.prototype),function(e,t){t.activate=t._activate;var i=t._findActive;t._findActive=function(e){return-1===e&&(e=!1),e&&"number"!=typeof e&&(e=this.headers.index(this.headers.filter(e)),-1===e&&(e=!1)),i.call(this,e)}}(jQuery,jQuery.ui.accordion.prototype),jQuery.ui.accordion.prototype.resize=jQuery.ui.accordion.prototype.refresh,function(e,t){e.extend(t.options,{change:null,changestart:null});var i=t._trigger;t._trigger=function(e,t,s){var a=i.apply(this,arguments);return a?("beforeActivate"===e?a=i.call(this,"changestart",t,{oldHeader:s.oldHeader,oldContent:s.oldPanel,newHeader:s.newHeader,newContent:s.newPanel}):"activate"===e&&(a=i.call(this,"change",t,{oldHeader:s.oldHeader,oldContent:s.oldPanel,newHeader:s.newHeader,newContent:s.newPanel})),a):!1}}(jQuery,jQuery.ui.accordion.prototype),function(e,t){e.extend(t.options,{animate:null,animated:"slide"});var i=t._create;t._create=function(){var e=this.options;null===e.animate&&(e.animate=e.animated?"slide"===e.animated?300:"bounceslide"===e.animated?{duration:200,down:{easing:"easeOutBounce",duration:1e3}}:e.animated:!1),i.call(this)}}(jQuery,jQuery.ui.accordion.prototype))})(jQuery);(function(e){var t=0;e.widget("ui.autocomplete",{version:"1.9.2",defaultElement:"<input>",options:{appendTo:"body",autoFocus:!1,delay:300,minLength:1,position:{my:"left top",at:"left bottom",collision:"none"},source:null,change:null,close:null,focus:null,open:null,response:null,search:null,select:null},pending:0,_create:function(){var t,i,s;this.isMultiLine=this._isMultiLine(),this.valueMethod=this.element[this.element.is("input,textarea")?"val":"text"],this.isNewMenu=!0,this.element.addClass("ui-autocomplete-input").attr("autocomplete","off"),this._on(this.element,{keydown:function(a){if(this.element.prop("readOnly"))return t=!0,s=!0,i=!0,undefined;t=!1,s=!1,i=!1;var n=e.ui.keyCode;switch(a.keyCode){case n.PAGE_UP:t=!0,this._move("previousPage",a);break;case n.PAGE_DOWN:t=!0,this._move("nextPage",a);break;case n.UP:t=!0,this._keyEvent("previous",a);break;case n.DOWN:t=!0,this._keyEvent("next",a);break;case n.ENTER:case n.NUMPAD_ENTER:this.menu.active&&(t=!0,a.preventDefault(),this.menu.select(a));break;case n.TAB:this.menu.active&&this.menu.select(a);break;case n.ESCAPE:this.menu.element.is(":visible")&&(this._value(this.term),this.close(a),a.preventDefault());break;default:i=!0,this._searchTimeout(a)}},keypress:function(s){if(t)return t=!1,s.preventDefault(),undefined;if(!i){var a=e.ui.keyCode;switch(s.keyCode){case a.PAGE_UP:this._move("previousPage",s);break;case a.PAGE_DOWN:this._move("nextPage",s);break;case a.UP:this._keyEvent("previous",s);break;case a.DOWN:this._keyEvent("next",s)}}},input:function(e){return s?(s=!1,e.preventDefault(),undefined):(this._searchTimeout(e),undefined)},focus:function(){this.selectedItem=null,this.previous=this._value()},blur:function(e){return this.cancelBlur?(delete this.cancelBlur,undefined):(clearTimeout(this.searching),this.close(e),this._change(e),undefined)}}),this._initSource(),this.menu=e("<ul>").addClass("ui-autocomplete").appendTo(this.document.find(this.options.appendTo||"body")[0]).menu({input:e(),role:null}).zIndex(this.element.zIndex()+1).hide().data("menu"),this._on(this.menu.element,{mousedown:function(t){t.preventDefault(),this.cancelBlur=!0,this._delay(function(){delete this.cancelBlur});var i=this.menu.element[0];e(t.target).closest(".ui-menu-item").length||this._delay(function(){var t=this;this.document.one("mousedown",function(s){s.target===t.element[0]||s.target===i||e.contains(i,s.target)||t.close()})})},menufocus:function(t,i){if(this.isNewMenu&&(this.isNewMenu=!1,t.originalEvent&&/^mouse/.test(t.originalEvent.type)))return this.menu.blur(),this.document.one("mousemove",function(){e(t.target).trigger(t.originalEvent)}),undefined;var s=i.item.data("ui-autocomplete-item")||i.item.data("item.autocomplete");!1!==this._trigger("focus",t,{item:s})?t.originalEvent&&/^key/.test(t.originalEvent.type)&&this._value(s.value):this.liveRegion.text(s.value)},menuselect:function(e,t){var i=t.item.data("ui-autocomplete-item")||t.item.data("item.autocomplete"),s=this.previous;this.element[0]!==this.document[0].activeElement&&(this.element.focus(),this.previous=s,this._delay(function(){this.previous=s,this.selectedItem=i})),!1!==this._trigger("select",e,{item:i})&&this._value(i.value),this.term=this._value(),this.close(e),this.selectedItem=i}}),this.liveRegion=e("<span>",{role:"status","aria-live":"polite"}).addClass("ui-helper-hidden-accessible").insertAfter(this.element),e.fn.bgiframe&&this.menu.element.bgiframe(),this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_destroy:function(){clearTimeout(this.searching),this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"),this.menu.element.remove(),this.liveRegion.remove()},_setOption:function(e,t){this._super(e,t),"source"===e&&this._initSource(),"appendTo"===e&&this.menu.element.appendTo(this.document.find(t||"body")[0]),"disabled"===e&&t&&this.xhr&&this.xhr.abort()},_isMultiLine:function(){return this.element.is("textarea")?!0:this.element.is("input")?!1:this.element.prop("isContentEditable")},_initSource:function(){var t,i,s=this;e.isArray(this.options.source)?(t=this.options.source,this.source=function(i,s){s(e.ui.autocomplete.filter(t,i.term))}):"string"==typeof this.options.source?(i=this.options.source,this.source=function(t,a){s.xhr&&s.xhr.abort(),s.xhr=e.ajax({url:i,data:t,dataType:"json",success:function(e){a(e)},error:function(){a([])}})}):this.source=this.options.source},_searchTimeout:function(e){clearTimeout(this.searching),this.searching=this._delay(function(){this.term!==this._value()&&(this.selectedItem=null,this.search(null,e))},this.options.delay)},search:function(e,t){return e=null!=e?e:this._value(),this.term=this._value(),e.length<this.options.minLength?this.close(t):this._trigger("search",t)!==!1?this._search(e):undefined},_search:function(e){this.pending++,this.element.addClass("ui-autocomplete-loading"),this.cancelSearch=!1,this.source({term:e},this._response())},_response:function(){var e=this,i=++t;return function(s){i===t&&e.__response(s),e.pending--,e.pending||e.element.removeClass("ui-autocomplete-loading")}},__response:function(e){e&&(e=this._normalize(e)),this._trigger("response",null,{content:e}),!this.options.disabled&&e&&e.length&&!this.cancelSearch?(this._suggest(e),this._trigger("open")):this._close()},close:function(e){this.cancelSearch=!0,this._close(e)},_close:function(e){this.menu.element.is(":visible")&&(this.menu.element.hide(),this.menu.blur(),this.isNewMenu=!0,this._trigger("close",e))},_change:function(e){this.previous!==this._value()&&this._trigger("change",e,{item:this.selectedItem})},_normalize:function(t){return t.length&&t[0].label&&t[0].value?t:e.map(t,function(t){return"string"==typeof t?{label:t,value:t}:e.extend({label:t.label||t.value,value:t.value||t.label},t)})},_suggest:function(t){var i=this.menu.element.empty().zIndex(this.element.zIndex()+1);this._renderMenu(i,t),this.menu.refresh(),i.show(),this._resizeMenu(),i.position(e.extend({of:this.element},this.options.position)),this.options.autoFocus&&this.menu.next()},_resizeMenu:function(){var e=this.menu.element;e.outerWidth(Math.max(e.width("").outerWidth()+1,this.element.outerWidth()))},_renderMenu:function(t,i){var s=this;e.each(i,function(e,i){s._renderItemData(t,i)})},_renderItemData:function(e,t){return this._renderItem(e,t).data("ui-autocomplete-item",t)},_renderItem:function(t,i){return e("<li>").append(e("<a>").text(i.label)).appendTo(t)},_move:function(e,t){return this.menu.element.is(":visible")?this.menu.isFirstItem()&&/^previous/.test(e)||this.menu.isLastItem()&&/^next/.test(e)?(this._value(this.term),this.menu.blur(),undefined):(this.menu[e](t),undefined):(this.search(null,t),undefined)},widget:function(){return this.menu.element},_value:function(){return this.valueMethod.apply(this.element,arguments)},_keyEvent:function(e,t){(!this.isMultiLine||this.menu.element.is(":visible"))&&(this._move(e,t),t.preventDefault())}}),e.extend(e.ui.autocomplete,{escapeRegex:function(e){return e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")},filter:function(t,i){var s=RegExp(e.ui.autocomplete.escapeRegex(i),"i");return e.grep(t,function(e){return s.test(e.label||e.value||e)})}}),e.widget("ui.autocomplete",e.ui.autocomplete,{options:{messages:{noResults:"No search results.",results:function(e){return e+(e>1?" results are":" result is")+" available, use up and down arrow keys to navigate."}}},__response:function(e){var t;this._superApply(arguments),this.options.disabled||this.cancelSearch||(t=e&&e.length?this.options.messages.results(e.length):this.options.messages.noResults,this.liveRegion.text(t))}})})(jQuery);(function(e){var t,i,s,a,n="ui-button ui-widget ui-state-default ui-corner-all",o="ui-state-hover ui-state-active ",r="ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only",h=function(){var t=e(this).find(":ui-button");setTimeout(function(){t.button("refresh")},1)},l=function(t){var i=t.name,s=t.form,a=e([]);return i&&(a=s?e(s).find("[name='"+i+"']"):e("[name='"+i+"']",t.ownerDocument).filter(function(){return!this.form})),a};e.widget("ui.button",{version:"1.9.2",defaultElement:"<button>",options:{disabled:null,text:!0,label:null,icons:{primary:null,secondary:null}},_create:function(){this.element.closest("form").unbind("reset"+this.eventNamespace).bind("reset"+this.eventNamespace,h),"boolean"!=typeof this.options.disabled?this.options.disabled=!!this.element.prop("disabled"):this.element.prop("disabled",this.options.disabled),this._determineButtonType(),this.hasTitle=!!this.buttonElement.attr("title");var o=this,r=this.options,u="checkbox"===this.type||"radio"===this.type,d=u?"":"ui-state-active",c="ui-state-focus";null===r.label&&(r.label="input"===this.type?this.buttonElement.val():this.buttonElement.html()),this._hoverable(this.buttonElement),this.buttonElement.addClass(n).attr("role","button").bind("mouseenter"+this.eventNamespace,function(){r.disabled||this===t&&e(this).addClass("ui-state-active")}).bind("mouseleave"+this.eventNamespace,function(){r.disabled||e(this).removeClass(d)}).bind("click"+this.eventNamespace,function(e){r.disabled&&(e.preventDefault(),e.stopImmediatePropagation())}),this.element.bind("focus"+this.eventNamespace,function(){o.buttonElement.addClass(c)}).bind("blur"+this.eventNamespace,function(){o.buttonElement.removeClass(c)}),u&&(this.element.bind("change"+this.eventNamespace,function(){a||o.refresh()}),this.buttonElement.bind("mousedown"+this.eventNamespace,function(e){r.disabled||(a=!1,i=e.pageX,s=e.pageY)}).bind("mouseup"+this.eventNamespace,function(e){r.disabled||(i!==e.pageX||s!==e.pageY)&&(a=!0)})),"checkbox"===this.type?this.buttonElement.bind("click"+this.eventNamespace,function(){return r.disabled||a?!1:(e(this).toggleClass("ui-state-active"),o.buttonElement.attr("aria-pressed",o.element[0].checked),undefined)}):"radio"===this.type?this.buttonElement.bind("click"+this.eventNamespace,function(){if(r.disabled||a)return!1;e(this).addClass("ui-state-active"),o.buttonElement.attr("aria-pressed","true");var t=o.element[0];l(t).not(t).map(function(){return e(this).button("widget")[0]}).removeClass("ui-state-active").attr("aria-pressed","false")}):(this.buttonElement.bind("mousedown"+this.eventNamespace,function(){return r.disabled?!1:(e(this).addClass("ui-state-active"),t=this,o.document.one("mouseup",function(){t=null}),undefined)}).bind("mouseup"+this.eventNamespace,function(){return r.disabled?!1:(e(this).removeClass("ui-state-active"),undefined)}).bind("keydown"+this.eventNamespace,function(t){return r.disabled?!1:((t.keyCode===e.ui.keyCode.SPACE||t.keyCode===e.ui.keyCode.ENTER)&&e(this).addClass("ui-state-active"),undefined)}).bind("keyup"+this.eventNamespace,function(){e(this).removeClass("ui-state-active")}),this.buttonElement.is("a")&&this.buttonElement.keyup(function(t){t.keyCode===e.ui.keyCode.SPACE&&e(this).click()})),this._setOption("disabled",r.disabled),this._resetButton()},_determineButtonType:function(){var e,t,i;this.type=this.element.is("[type=checkbox]")?"checkbox":this.element.is("[type=radio]")?"radio":this.element.is("input")?"input":"button","checkbox"===this.type||"radio"===this.type?(e=this.element.parents().last(),t="label[for='"+this.element.attr("id")+"']",this.buttonElement=e.find(t),this.buttonElement.length||(e=e.length?e.siblings():this.element.siblings(),this.buttonElement=e.filter(t),this.buttonElement.length||(this.buttonElement=e.find(t))),this.element.addClass("ui-helper-hidden-accessible"),i=this.element.is(":checked"),i&&this.buttonElement.addClass("ui-state-active"),this.buttonElement.prop("aria-pressed",i)):this.buttonElement=this.element},widget:function(){return this.buttonElement},_destroy:function(){this.element.removeClass("ui-helper-hidden-accessible"),this.buttonElement.removeClass(n+" "+o+" "+r).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html()),this.hasTitle||this.buttonElement.removeAttr("title")},_setOption:function(e,t){return this._super(e,t),"disabled"===e?(t?this.element.prop("disabled",!0):this.element.prop("disabled",!1),undefined):(this._resetButton(),undefined)},refresh:function(){var t=this.element.is("input, button")?this.element.is(":disabled"):this.element.hasClass("ui-button-disabled");t!==this.options.disabled&&this._setOption("disabled",t),"radio"===this.type?l(this.element[0]).each(function(){e(this).is(":checked")?e(this).button("widget").addClass("ui-state-active").attr("aria-pressed","true"):e(this).button("widget").removeClass("ui-state-active").attr("aria-pressed","false")}):"checkbox"===this.type&&(this.element.is(":checked")?this.buttonElement.addClass("ui-state-active").attr("aria-pressed","true"):this.buttonElement.removeClass("ui-state-active").attr("aria-pressed","false"))},_resetButton:function(){if("input"===this.type)return this.options.label&&this.element.val(this.options.label),undefined;var t=this.buttonElement.removeClass(r),i=e("<span></span>",this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(t.empty()).text(),s=this.options.icons,a=s.primary&&s.secondary,n=[];s.primary||s.secondary?(this.options.text&&n.push("ui-button-text-icon"+(a?"s":s.primary?"-primary":"-secondary")),s.primary&&t.prepend("<span class='ui-button-icon-primary ui-icon "+s.primary+"'></span>"),s.secondary&&t.append("<span class='ui-button-icon-secondary ui-icon "+s.secondary+"'></span>"),this.options.text||(n.push(a?"ui-button-icons-only":"ui-button-icon-only"),this.hasTitle||t.attr("title",e.trim(i)))):n.push("ui-button-text-only"),t.addClass(n.join(" "))}}),e.widget("ui.buttonset",{version:"1.9.2",options:{items:"button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(button)"},_create:function(){this.element.addClass("ui-buttonset")},_init:function(){this.refresh()},_setOption:function(e,t){"disabled"===e&&this.buttons.button("option",e,t),this._super(e,t)},refresh:function(){var t="rtl"===this.element.css("direction");this.buttons=this.element.find(this.options.items).filter(":ui-button").button("refresh").end().not(":ui-button").button().end().map(function(){return e(this).button("widget")[0]}).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(t?"ui-corner-right":"ui-corner-left").end().filter(":last").addClass(t?"ui-corner-left":"ui-corner-right").end().end()},_destroy:function(){this.element.removeClass("ui-buttonset"),this.buttons.map(function(){return e(this).button("widget")[0]}).removeClass("ui-corner-left ui-corner-right").end().button("destroy")}})})(jQuery);(function($,undefined){function Datepicker(){this.debug=!1,this._curInst=null,this._keyEvent=!1,this._disabledInputs=[],this._datepickerShowing=!1,this._inDialog=!1,this._mainDivId="ui-datepicker-div",this._inlineClass="ui-datepicker-inline",this._appendClass="ui-datepicker-append",this._triggerClass="ui-datepicker-trigger",this._dialogClass="ui-datepicker-dialog",this._disableClass="ui-datepicker-disabled",this._unselectableClass="ui-datepicker-unselectable",this._currentClass="ui-datepicker-current-day",this._dayOverClass="ui-datepicker-days-cell-over",this.regional=[],this.regional[""]={closeText:"Done",prevText:"Prev",nextText:"Next",currentText:"Today",monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],dayNamesShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],dayNamesMin:["Su","Mo","Tu","We","Th","Fr","Sa"],weekHeader:"Wk",dateFormat:"mm/dd/yy",firstDay:0,isRTL:!1,showMonthAfterYear:!1,yearSuffix:""},this._defaults={showOn:"focus",showAnim:"fadeIn",showOptions:{},defaultDate:null,appendText:"",buttonText:"...",buttonImage:"",buttonImageOnly:!1,hideIfNoPrevNext:!1,navigationAsDateFormat:!1,gotoCurrent:!1,changeMonth:!1,changeYear:!1,yearRange:"c-10:c+10",showOtherMonths:!1,selectOtherMonths:!1,showWeek:!1,calculateWeek:this.iso8601Week,shortYearCutoff:"+10",minDate:null,maxDate:null,duration:"fast",beforeShowDay:null,beforeShow:null,onSelect:null,onChangeMonthYear:null,onClose:null,numberOfMonths:1,showCurrentAtPos:0,stepMonths:1,stepBigMonths:12,altField:"",altFormat:"",constrainInput:!0,showButtonPanel:!1,autoSize:!1,disabled:!1},$.extend(this._defaults,this.regional[""]),this.dpDiv=bindHover($('<div id="'+this._mainDivId+'" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>'))}function bindHover(e){var t="button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";return e.delegate(t,"mouseout",function(){$(this).removeClass("ui-state-hover"),-1!=this.className.indexOf("ui-datepicker-prev")&&$(this).removeClass("ui-datepicker-prev-hover"),-1!=this.className.indexOf("ui-datepicker-next")&&$(this).removeClass("ui-datepicker-next-hover")}).delegate(t,"mouseover",function(){$.datepicker._isDisabledDatepicker(instActive.inline?e.parent()[0]:instActive.input[0])||($(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"),$(this).addClass("ui-state-hover"),-1!=this.className.indexOf("ui-datepicker-prev")&&$(this).addClass("ui-datepicker-prev-hover"),-1!=this.className.indexOf("ui-datepicker-next")&&$(this).addClass("ui-datepicker-next-hover"))})}function extendRemove(e,t){$.extend(e,t);for(var i in t)(null==t[i]||t[i]==undefined)&&(e[i]=t[i]);return e}$.extend($.ui,{datepicker:{version:"1.9.2"}});var PROP_NAME="datepicker",dpuuid=(new Date).getTime(),instActive;$.extend(Datepicker.prototype,{markerClassName:"hasDatepicker",maxRows:4,log:function(){this.debug&&console.log.apply("",arguments)},_widgetDatepicker:function(){return this.dpDiv},setDefaults:function(e){return extendRemove(this._defaults,e||{}),this},_attachDatepicker:function(target,settings){var inlineSettings=null;for(var attrName in this._defaults){var attrValue=target.getAttribute("date:"+attrName);if(attrValue){inlineSettings=inlineSettings||{};try{inlineSettings[attrName]=eval(attrValue)}catch(err){inlineSettings[attrName]=attrValue}}}var nodeName=target.nodeName.toLowerCase(),inline="div"==nodeName||"span"==nodeName;target.id||(this.uuid+=1,target.id="dp"+this.uuid);var inst=this._newInst($(target),inline);inst.settings=$.extend({},settings||{},inlineSettings||{}),"input"==nodeName?this._connectDatepicker(target,inst):inline&&this._inlineDatepicker(target,inst)},_newInst:function(e,t){var i=e[0].id.replace(/([^A-Za-z0-9_-])/g,"\\\\$1");return{id:i,input:e,selectedDay:0,selectedMonth:0,selectedYear:0,drawMonth:0,drawYear:0,inline:t,dpDiv:t?bindHover($('<div class="'+this._inlineClass+' ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>')):this.dpDiv}},_connectDatepicker:function(e,t){var i=$(e);t.append=$([]),t.trigger=$([]),i.hasClass(this.markerClassName)||(this._attachments(i,t),i.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp).bind("setData.datepicker",function(e,i,s){t.settings[i]=s}).bind("getData.datepicker",function(e,i){return this._get(t,i)}),this._autoSize(t),$.data(e,PROP_NAME,t),t.settings.disabled&&this._disableDatepicker(e))},_attachments:function(e,t){var i=this._get(t,"appendText"),s=this._get(t,"isRTL");t.append&&t.append.remove(),i&&(t.append=$('<span class="'+this._appendClass+'">'+i+"</span>"),e[s?"before":"after"](t.append)),e.unbind("focus",this._showDatepicker),t.trigger&&t.trigger.remove();var a=this._get(t,"showOn");if(("focus"==a||"both"==a)&&e.focus(this._showDatepicker),"button"==a||"both"==a){var n=this._get(t,"buttonText"),r=this._get(t,"buttonImage");t.trigger=$(this._get(t,"buttonImageOnly")?$("<img/>").addClass(this._triggerClass).attr({src:r,alt:n,title:n}):$('<button type="button"></button>').addClass(this._triggerClass).html(""==r?n:$("<img/>").attr({src:r,alt:n,title:n}))),e[s?"before":"after"](t.trigger),t.trigger.click(function(){return $.datepicker._datepickerShowing&&$.datepicker._lastInput==e[0]?$.datepicker._hideDatepicker():$.datepicker._datepickerShowing&&$.datepicker._lastInput!=e[0]?($.datepicker._hideDatepicker(),$.datepicker._showDatepicker(e[0])):$.datepicker._showDatepicker(e[0]),!1})}},_autoSize:function(e){if(this._get(e,"autoSize")&&!e.inline){var t=new Date(2009,11,20),i=this._get(e,"dateFormat");if(i.match(/[DM]/)){var s=function(e){for(var t=0,i=0,s=0;e.length>s;s++)e[s].length>t&&(t=e[s].length,i=s);return i};t.setMonth(s(this._get(e,i.match(/MM/)?"monthNames":"monthNamesShort"))),t.setDate(s(this._get(e,i.match(/DD/)?"dayNames":"dayNamesShort"))+20-t.getDay())}e.input.attr("size",this._formatDate(e,t).length)}},_inlineDatepicker:function(e,t){var i=$(e);i.hasClass(this.markerClassName)||(i.addClass(this.markerClassName).append(t.dpDiv).bind("setData.datepicker",function(e,i,s){t.settings[i]=s}).bind("getData.datepicker",function(e,i){return this._get(t,i)}),$.data(e,PROP_NAME,t),this._setDate(t,this._getDefaultDate(t),!0),this._updateDatepicker(t),this._updateAlternate(t),t.settings.disabled&&this._disableDatepicker(e),t.dpDiv.css("display","block"))},_dialogDatepicker:function(e,t,i,s,a){var n=this._dialogInst;if(!n){this.uuid+=1;var r="dp"+this.uuid;this._dialogInput=$('<input type="text" id="'+r+'" style="position: absolute; top: -100px; width: 0px;"/>'),this._dialogInput.keydown(this._doKeyDown),$("body").append(this._dialogInput),n=this._dialogInst=this._newInst(this._dialogInput,!1),n.settings={},$.data(this._dialogInput[0],PROP_NAME,n)}if(extendRemove(n.settings,s||{}),t=t&&t.constructor==Date?this._formatDate(n,t):t,this._dialogInput.val(t),this._pos=a?a.length?a:[a.pageX,a.pageY]:null,!this._pos){var o=document.documentElement.clientWidth,h=document.documentElement.clientHeight,l=document.documentElement.scrollLeft||document.body.scrollLeft,u=document.documentElement.scrollTop||document.body.scrollTop;this._pos=[o/2-100+l,h/2-150+u]}return this._dialogInput.css("left",this._pos[0]+20+"px").css("top",this._pos[1]+"px"),n.settings.onSelect=i,this._inDialog=!0,this.dpDiv.addClass(this._dialogClass),this._showDatepicker(this._dialogInput[0]),$.blockUI&&$.blockUI(this.dpDiv),$.data(this._dialogInput[0],PROP_NAME,n),this},_destroyDatepicker:function(e){var t=$(e),i=$.data(e,PROP_NAME);if(t.hasClass(this.markerClassName)){var s=e.nodeName.toLowerCase();$.removeData(e,PROP_NAME),"input"==s?(i.append.remove(),i.trigger.remove(),t.removeClass(this.markerClassName).unbind("focus",this._showDatepicker).unbind("keydown",this._doKeyDown).unbind("keypress",this._doKeyPress).unbind("keyup",this._doKeyUp)):("div"==s||"span"==s)&&t.removeClass(this.markerClassName).empty()}},_enableDatepicker:function(e){var t=$(e),i=$.data(e,PROP_NAME);if(t.hasClass(this.markerClassName)){var s=e.nodeName.toLowerCase();if("input"==s)e.disabled=!1,i.trigger.filter("button").each(function(){this.disabled=!1}).end().filter("img").css({opacity:"1.0",cursor:""});else if("div"==s||"span"==s){var a=t.children("."+this._inlineClass);a.children().removeClass("ui-state-disabled"),a.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!1)}this._disabledInputs=$.map(this._disabledInputs,function(t){return t==e?null:t})}},_disableDatepicker:function(e){var t=$(e),i=$.data(e,PROP_NAME);if(t.hasClass(this.markerClassName)){var s=e.nodeName.toLowerCase();if("input"==s)e.disabled=!0,i.trigger.filter("button").each(function(){this.disabled=!0}).end().filter("img").css({opacity:"0.5",cursor:"default"});else if("div"==s||"span"==s){var a=t.children("."+this._inlineClass);a.children().addClass("ui-state-disabled"),a.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!0)}this._disabledInputs=$.map(this._disabledInputs,function(t){return t==e?null:t}),this._disabledInputs[this._disabledInputs.length]=e}},_isDisabledDatepicker:function(e){if(!e)return!1;for(var t=0;this._disabledInputs.length>t;t++)if(this._disabledInputs[t]==e)return!0;return!1},_getInst:function(e){try{return $.data(e,PROP_NAME)}catch(t){throw"Missing instance data for this datepicker"}},_optionDatepicker:function(e,t,i){var s=this._getInst(e);if(2==arguments.length&&"string"==typeof t)return"defaults"==t?$.extend({},$.datepicker._defaults):s?"all"==t?$.extend({},s.settings):this._get(s,t):null;var a=t||{};if("string"==typeof t&&(a={},a[t]=i),s){this._curInst==s&&this._hideDatepicker();var n=this._getDateDatepicker(e,!0),r=this._getMinMaxDate(s,"min"),o=this._getMinMaxDate(s,"max");extendRemove(s.settings,a),null!==r&&a.dateFormat!==undefined&&a.minDate===undefined&&(s.settings.minDate=this._formatDate(s,r)),null!==o&&a.dateFormat!==undefined&&a.maxDate===undefined&&(s.settings.maxDate=this._formatDate(s,o)),this._attachments($(e),s),this._autoSize(s),this._setDate(s,n),this._updateAlternate(s),this._updateDatepicker(s)}},_changeDatepicker:function(e,t,i){this._optionDatepicker(e,t,i)},_refreshDatepicker:function(e){var t=this._getInst(e);t&&this._updateDatepicker(t)},_setDateDatepicker:function(e,t){var i=this._getInst(e);i&&(this._setDate(i,t),this._updateDatepicker(i),this._updateAlternate(i))},_getDateDatepicker:function(e,t){var i=this._getInst(e);return i&&!i.inline&&this._setDateFromField(i,t),i?this._getDate(i):null},_doKeyDown:function(e){var t=$.datepicker._getInst(e.target),i=!0,s=t.dpDiv.is(".ui-datepicker-rtl");if(t._keyEvent=!0,$.datepicker._datepickerShowing)switch(e.keyCode){case 9:$.datepicker._hideDatepicker(),i=!1;break;case 13:var a=$("td."+$.datepicker._dayOverClass+":not(."+$.datepicker._currentClass+")",t.dpDiv);a[0]&&$.datepicker._selectDay(e.target,t.selectedMonth,t.selectedYear,a[0]);var n=$.datepicker._get(t,"onSelect");if(n){var r=$.datepicker._formatDate(t);n.apply(t.input?t.input[0]:null,[r,t])}else $.datepicker._hideDatepicker();return!1;case 27:$.datepicker._hideDatepicker();break;case 33:$.datepicker._adjustDate(e.target,e.ctrlKey?-$.datepicker._get(t,"stepBigMonths"):-$.datepicker._get(t,"stepMonths"),"M");break;case 34:$.datepicker._adjustDate(e.target,e.ctrlKey?+$.datepicker._get(t,"stepBigMonths"):+$.datepicker._get(t,"stepMonths"),"M");break;case 35:(e.ctrlKey||e.metaKey)&&$.datepicker._clearDate(e.target),i=e.ctrlKey||e.metaKey;break;case 36:(e.ctrlKey||e.metaKey)&&$.datepicker._gotoToday(e.target),i=e.ctrlKey||e.metaKey;break;case 37:(e.ctrlKey||e.metaKey)&&$.datepicker._adjustDate(e.target,s?1:-1,"D"),i=e.ctrlKey||e.metaKey,e.originalEvent.altKey&&$.datepicker._adjustDate(e.target,e.ctrlKey?-$.datepicker._get(t,"stepBigMonths"):-$.datepicker._get(t,"stepMonths"),"M");break;case 38:(e.ctrlKey||e.metaKey)&&$.datepicker._adjustDate(e.target,-7,"D"),i=e.ctrlKey||e.metaKey;break;case 39:(e.ctrlKey||e.metaKey)&&$.datepicker._adjustDate(e.target,s?-1:1,"D"),i=e.ctrlKey||e.metaKey,e.originalEvent.altKey&&$.datepicker._adjustDate(e.target,e.ctrlKey?+$.datepicker._get(t,"stepBigMonths"):+$.datepicker._get(t,"stepMonths"),"M");break;case 40:(e.ctrlKey||e.metaKey)&&$.datepicker._adjustDate(e.target,7,"D"),i=e.ctrlKey||e.metaKey;break;default:i=!1}else 36==e.keyCode&&e.ctrlKey?$.datepicker._showDatepicker(this):i=!1;i&&(e.preventDefault(),e.stopPropagation())},_doKeyPress:function(e){var t=$.datepicker._getInst(e.target);if($.datepicker._get(t,"constrainInput")){var i=$.datepicker._possibleChars($.datepicker._get(t,"dateFormat")),s=String.fromCharCode(e.charCode==undefined?e.keyCode:e.charCode);return e.ctrlKey||e.metaKey||" ">s||!i||i.indexOf(s)>-1}},_doKeyUp:function(e){var t=$.datepicker._getInst(e.target);if(t.input.val()!=t.lastVal)try{var i=$.datepicker.parseDate($.datepicker._get(t,"dateFormat"),t.input?t.input.val():null,$.datepicker._getFormatConfig(t));i&&($.datepicker._setDateFromField(t),$.datepicker._updateAlternate(t),$.datepicker._updateDatepicker(t))}catch(s){$.datepicker.log(s)}return!0},_showDatepicker:function(e){if(e=e.target||e,"input"!=e.nodeName.toLowerCase()&&(e=$("input",e.parentNode)[0]),!$.datepicker._isDisabledDatepicker(e)&&$.datepicker._lastInput!=e){var t=$.datepicker._getInst(e);$.datepicker._curInst&&$.datepicker._curInst!=t&&($.datepicker._curInst.dpDiv.stop(!0,!0),t&&$.datepicker._datepickerShowing&&$.datepicker._hideDatepicker($.datepicker._curInst.input[0]));var i=$.datepicker._get(t,"beforeShow"),s=i?i.apply(e,[e,t]):{};if(s!==!1){extendRemove(t.settings,s),t.lastVal=null,$.datepicker._lastInput=e,$.datepicker._setDateFromField(t),$.datepicker._inDialog&&(e.value=""),$.datepicker._pos||($.datepicker._pos=$.datepicker._findPos(e),$.datepicker._pos[1]+=e.offsetHeight);var a=!1;$(e).parents().each(function(){return a|="fixed"==$(this).css("position"),!a});var n={left:$.datepicker._pos[0],top:$.datepicker._pos[1]};if($.datepicker._pos=null,t.dpDiv.empty(),t.dpDiv.css({position:"absolute",display:"block",top:"-1000px"}),$.datepicker._updateDatepicker(t),n=$.datepicker._checkOffset(t,n,a),t.dpDiv.css({position:$.datepicker._inDialog&&$.blockUI?"static":a?"fixed":"absolute",display:"none",left:n.left+"px",top:n.top+"px"}),!t.inline){var r=$.datepicker._get(t,"showAnim"),o=$.datepicker._get(t,"duration"),h=function(){var e=t.dpDiv.find("iframe.ui-datepicker-cover");if(e.length){var i=$.datepicker._getBorders(t.dpDiv);e.css({left:-i[0],top:-i[1],width:t.dpDiv.outerWidth(),height:t.dpDiv.outerHeight()})}};t.dpDiv.zIndex($(e).zIndex()+1),$.datepicker._datepickerShowing=!0,$.effects&&($.effects.effect[r]||$.effects[r])?t.dpDiv.show(r,$.datepicker._get(t,"showOptions"),o,h):t.dpDiv[r||"show"](r?o:null,h),r&&o||h(),t.input.is(":visible")&&!t.input.is(":disabled")&&t.input.focus(),$.datepicker._curInst=t}}}},_updateDatepicker:function(e){this.maxRows=4;var t=$.datepicker._getBorders(e.dpDiv);instActive=e,e.dpDiv.empty().append(this._generateHTML(e)),this._attachHandlers(e);var i=e.dpDiv.find("iframe.ui-datepicker-cover");i.length&&i.css({left:-t[0],top:-t[1],width:e.dpDiv.outerWidth(),height:e.dpDiv.outerHeight()}),e.dpDiv.find("."+this._dayOverClass+" a").mouseover();var s=this._getNumberOfMonths(e),a=s[1],n=17;if(e.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""),a>1&&e.dpDiv.addClass("ui-datepicker-multi-"+a).css("width",n*a+"em"),e.dpDiv[(1!=s[0]||1!=s[1]?"add":"remove")+"Class"]("ui-datepicker-multi"),e.dpDiv[(this._get(e,"isRTL")?"add":"remove")+"Class"]("ui-datepicker-rtl"),e==$.datepicker._curInst&&$.datepicker._datepickerShowing&&e.input&&e.input.is(":visible")&&!e.input.is(":disabled")&&e.input[0]!=document.activeElement&&e.input.focus(),e.yearshtml){var r=e.yearshtml;setTimeout(function(){r===e.yearshtml&&e.yearshtml&&e.dpDiv.find("select.ui-datepicker-year:first").replaceWith(e.yearshtml),r=e.yearshtml=null},0)}},_getBorders:function(e){var t=function(e){return{thin:1,medium:2,thick:3}[e]||e};return[parseFloat(t(e.css("border-left-width"))),parseFloat(t(e.css("border-top-width")))]},_checkOffset:function(e,t,i){var s=e.dpDiv.outerWidth(),a=e.dpDiv.outerHeight(),n=e.input?e.input.outerWidth():0,r=e.input?e.input.outerHeight():0,o=document.documentElement.clientWidth+(i?0:$(document).scrollLeft()),h=document.documentElement.clientHeight+(i?0:$(document).scrollTop());return t.left-=this._get(e,"isRTL")?s-n:0,t.left-=i&&t.left==e.input.offset().left?$(document).scrollLeft():0,t.top-=i&&t.top==e.input.offset().top+r?$(document).scrollTop():0,t.left-=Math.min(t.left,t.left+s>o&&o>s?Math.abs(t.left+s-o):0),t.top-=Math.min(t.top,t.top+a>h&&h>a?Math.abs(a+r):0),t},_findPos:function(e){for(var t=this._getInst(e),i=this._get(t,"isRTL");e&&("hidden"==e.type||1!=e.nodeType||$.expr.filters.hidden(e));)e=e[i?"previousSibling":"nextSibling"];var s=$(e).offset();return[s.left,s.top]},_hideDatepicker:function(e){var t=this._curInst;if(t&&(!e||t==$.data(e,PROP_NAME))&&this._datepickerShowing){var i=this._get(t,"showAnim"),s=this._get(t,"duration"),a=function(){$.datepicker._tidyDialog(t)};$.effects&&($.effects.effect[i]||$.effects[i])?t.dpDiv.hide(i,$.datepicker._get(t,"showOptions"),s,a):t.dpDiv["slideDown"==i?"slideUp":"fadeIn"==i?"fadeOut":"hide"](i?s:null,a),i||a(),this._datepickerShowing=!1;var n=this._get(t,"onClose");n&&n.apply(t.input?t.input[0]:null,[t.input?t.input.val():"",t]),this._lastInput=null,this._inDialog&&(this._dialogInput.css({position:"absolute",left:"0",top:"-100px"}),$.blockUI&&($.unblockUI(),$("body").append(this.dpDiv))),this._inDialog=!1}},_tidyDialog:function(e){e.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")},_checkExternalClick:function(e){if($.datepicker._curInst){var t=$(e.target),i=$.datepicker._getInst(t[0]);(t[0].id!=$.datepicker._mainDivId&&0==t.parents("#"+$.datepicker._mainDivId).length&&!t.hasClass($.datepicker.markerClassName)&&!t.closest("."+$.datepicker._triggerClass).length&&$.datepicker._datepickerShowing&&(!$.datepicker._inDialog||!$.blockUI)||t.hasClass($.datepicker.markerClassName)&&$.datepicker._curInst!=i)&&$.datepicker._hideDatepicker()}},_adjustDate:function(e,t,i){var s=$(e),a=this._getInst(s[0]);this._isDisabledDatepicker(s[0])||(this._adjustInstDate(a,t+("M"==i?this._get(a,"showCurrentAtPos"):0),i),this._updateDatepicker(a))},_gotoToday:function(e){var t=$(e),i=this._getInst(t[0]);if(this._get(i,"gotoCurrent")&&i.currentDay)i.selectedDay=i.currentDay,i.drawMonth=i.selectedMonth=i.currentMonth,i.drawYear=i.selectedYear=i.currentYear;else{var s=new Date;i.selectedDay=s.getDate(),i.drawMonth=i.selectedMonth=s.getMonth(),i.drawYear=i.selectedYear=s.getFullYear()}this._notifyChange(i),this._adjustDate(t)},_selectMonthYear:function(e,t,i){var s=$(e),a=this._getInst(s[0]);a["selected"+("M"==i?"Month":"Year")]=a["draw"+("M"==i?"Month":"Year")]=parseInt(t.options[t.selectedIndex].value,10),this._notifyChange(a),this._adjustDate(s)},_selectDay:function(e,t,i,s){var a=$(e);if(!$(s).hasClass(this._unselectableClass)&&!this._isDisabledDatepicker(a[0])){var n=this._getInst(a[0]);n.selectedDay=n.currentDay=$("a",s).html(),n.selectedMonth=n.currentMonth=t,n.selectedYear=n.currentYear=i,this._selectDate(e,this._formatDate(n,n.currentDay,n.currentMonth,n.currentYear))}},_clearDate:function(e){var t=$(e);this._getInst(t[0]),this._selectDate(t,"")},_selectDate:function(e,t){var i=$(e),s=this._getInst(i[0]);t=null!=t?t:this._formatDate(s),s.input&&s.input.val(t),this._updateAlternate(s);var a=this._get(s,"onSelect");a?a.apply(s.input?s.input[0]:null,[t,s]):s.input&&s.input.trigger("change"),s.inline?this._updateDatepicker(s):(this._hideDatepicker(),this._lastInput=s.input[0],"object"!=typeof s.input[0]&&s.input.focus(),this._lastInput=null)},_updateAlternate:function(e){var t=this._get(e,"altField");if(t){var i=this._get(e,"altFormat")||this._get(e,"dateFormat"),s=this._getDate(e),a=this.formatDate(i,s,this._getFormatConfig(e));$(t).each(function(){$(this).val(a)})}},noWeekends:function(e){var t=e.getDay();return[t>0&&6>t,""]},iso8601Week:function(e){var t=new Date(e.getTime());t.setDate(t.getDate()+4-(t.getDay()||7));var i=t.getTime();return t.setMonth(0),t.setDate(1),Math.floor(Math.round((i-t)/864e5)/7)+1},parseDate:function(e,t,i){if(null==e||null==t)throw"Invalid arguments";if(t="object"==typeof t?""+t:t+"",""==t)return null;var s=(i?i.shortYearCutoff:null)||this._defaults.shortYearCutoff;s="string"!=typeof s?s:(new Date).getFullYear()%100+parseInt(s,10);for(var a=(i?i.dayNamesShort:null)||this._defaults.dayNamesShort,n=(i?i.dayNames:null)||this._defaults.dayNames,r=(i?i.monthNamesShort:null)||this._defaults.monthNamesShort,o=(i?i.monthNames:null)||this._defaults.monthNames,h=-1,l=-1,u=-1,d=-1,c=!1,p=function(t){var i=e.length>y+1&&e.charAt(y+1)==t;return i&&y++,i},m=function(e){var i=p(e),s="@"==e?14:"!"==e?20:"y"==e&&i?4:"o"==e?3:2,a=RegExp("^\\d{1,"+s+"}"),n=t.substring(v).match(a);if(!n)throw"Missing number at position "+v;return v+=n[0].length,parseInt(n[0],10)},f=function(e,i,s){var a=$.map(p(e)?s:i,function(e,t){return[[t,e]]}).sort(function(e,t){return-(e[1].length-t[1].length)}),n=-1;if($.each(a,function(e,i){var s=i[1];return t.substr(v,s.length).toLowerCase()==s.toLowerCase()?(n=i[0],v+=s.length,!1):undefined}),-1!=n)return n+1;throw"Unknown name at position "+v},g=function(){if(t.charAt(v)!=e.charAt(y))throw"Unexpected literal at position "+v;v++},v=0,y=0;e.length>y;y++)if(c)"'"!=e.charAt(y)||p("'")?g():c=!1;else switch(e.charAt(y)){case"d":u=m("d");break;case"D":f("D",a,n);break;case"o":d=m("o");break;case"m":l=m("m");break;case"M":l=f("M",r,o);break;case"y":h=m("y");break;case"@":var b=new Date(m("@"));h=b.getFullYear(),l=b.getMonth()+1,u=b.getDate();break;case"!":var b=new Date((m("!")-this._ticksTo1970)/1e4);h=b.getFullYear(),l=b.getMonth()+1,u=b.getDate();break;case"'":p("'")?g():c=!0;break;default:g()}if(t.length>v){var _=t.substr(v);if(!/^\s+/.test(_))throw"Extra/unparsed characters found in date: "+_}if(-1==h?h=(new Date).getFullYear():100>h&&(h+=(new Date).getFullYear()-(new Date).getFullYear()%100+(s>=h?0:-100)),d>-1)for(l=1,u=d;;){var x=this._getDaysInMonth(h,l-1);if(x>=u)break;l++,u-=x}var b=this._daylightSavingAdjust(new Date(h,l-1,u));if(b.getFullYear()!=h||b.getMonth()+1!=l||b.getDate()!=u)throw"Invalid date";return b},ATOM:"yy-mm-dd",COOKIE:"D, dd M yy",ISO_8601:"yy-mm-dd",RFC_822:"D, d M y",RFC_850:"DD, dd-M-y",RFC_1036:"D, d M y",RFC_1123:"D, d M yy",RFC_2822:"D, d M yy",RSS:"D, d M y",TICKS:"!",TIMESTAMP:"@",W3C:"yy-mm-dd",_ticksTo1970:1e7*60*60*24*(718685+Math.floor(492.5)-Math.floor(19.7)+Math.floor(4.925)),formatDate:function(e,t,i){if(!t)return"";var s=(i?i.dayNamesShort:null)||this._defaults.dayNamesShort,a=(i?i.dayNames:null)||this._defaults.dayNames,n=(i?i.monthNamesShort:null)||this._defaults.monthNamesShort,r=(i?i.monthNames:null)||this._defaults.monthNames,o=function(t){var i=e.length>c+1&&e.charAt(c+1)==t;return i&&c++,i},h=function(e,t,i){var s=""+t;if(o(e))for(;i>s.length;)s="0"+s;return s},l=function(e,t,i,s){return o(e)?s[t]:i[t]},u="",d=!1;if(t)for(var c=0;e.length>c;c++)if(d)"'"!=e.charAt(c)||o("'")?u+=e.charAt(c):d=!1;else switch(e.charAt(c)){case"d":u+=h("d",t.getDate(),2);break;case"D":u+=l("D",t.getDay(),s,a);break;case"o":u+=h("o",Math.round((new Date(t.getFullYear(),t.getMonth(),t.getDate()).getTime()-new Date(t.getFullYear(),0,0).getTime())/864e5),3);break;case"m":u+=h("m",t.getMonth()+1,2);break;case"M":u+=l("M",t.getMonth(),n,r);break;case"y":u+=o("y")?t.getFullYear():(10>t.getYear()%100?"0":"")+t.getYear()%100;break;case"@":u+=t.getTime();break;case"!":u+=1e4*t.getTime()+this._ticksTo1970;break;case"'":o("'")?u+="'":d=!0;break;default:u+=e.charAt(c)}return u},_possibleChars:function(e){for(var t="",i=!1,s=function(t){var i=e.length>a+1&&e.charAt(a+1)==t;return i&&a++,i},a=0;e.length>a;a++)if(i)"'"!=e.charAt(a)||s("'")?t+=e.charAt(a):i=!1;else switch(e.charAt(a)){case"d":case"m":case"y":case"@":t+="0123456789";break;case"D":case"M":return null;case"'":s("'")?t+="'":i=!0;break;default:t+=e.charAt(a)}return t},_get:function(e,t){return e.settings[t]!==undefined?e.settings[t]:this._defaults[t]},_setDateFromField:function(e,t){if(e.input.val()!=e.lastVal){var i,s,a=this._get(e,"dateFormat"),n=e.lastVal=e.input?e.input.val():null;i=s=this._getDefaultDate(e);var r=this._getFormatConfig(e);try{i=this.parseDate(a,n,r)||s}catch(o){this.log(o),n=t?"":n}e.selectedDay=i.getDate(),e.drawMonth=e.selectedMonth=i.getMonth(),e.drawYear=e.selectedYear=i.getFullYear(),e.currentDay=n?i.getDate():0,e.currentMonth=n?i.getMonth():0,e.currentYear=n?i.getFullYear():0,this._adjustInstDate(e)}},_getDefaultDate:function(e){return this._restrictMinMax(e,this._determineDate(e,this._get(e,"defaultDate"),new Date))},_determineDate:function(e,t,i){var s=function(e){var t=new Date;return t.setDate(t.getDate()+e),t},a=function(t){try{return $.datepicker.parseDate($.datepicker._get(e,"dateFormat"),t,$.datepicker._getFormatConfig(e))}catch(i){}for(var s=(t.toLowerCase().match(/^c/)?$.datepicker._getDate(e):null)||new Date,a=s.getFullYear(),n=s.getMonth(),r=s.getDate(),o=/([+-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g,h=o.exec(t);h;){switch(h[2]||"d"){case"d":case"D":r+=parseInt(h[1],10);break;case"w":case"W":r+=7*parseInt(h[1],10);break;case"m":case"M":n+=parseInt(h[1],10),r=Math.min(r,$.datepicker._getDaysInMonth(a,n));break;case"y":case"Y":a+=parseInt(h[1],10),r=Math.min(r,$.datepicker._getDaysInMonth(a,n))}h=o.exec(t)}return new Date(a,n,r)},n=null==t||""===t?i:"string"==typeof t?a(t):"number"==typeof t?isNaN(t)?i:s(t):new Date(t.getTime());return n=n&&"Invalid Date"==""+n?i:n,n&&(n.setHours(0),n.setMinutes(0),n.setSeconds(0),n.setMilliseconds(0)),this._daylightSavingAdjust(n)},_daylightSavingAdjust:function(e){return e?(e.setHours(e.getHours()>12?e.getHours()+2:0),e):null},_setDate:function(e,t,i){var s=!t,a=e.selectedMonth,n=e.selectedYear,r=this._restrictMinMax(e,this._determineDate(e,t,new Date));e.selectedDay=e.currentDay=r.getDate(),e.drawMonth=e.selectedMonth=e.currentMonth=r.getMonth(),e.drawYear=e.selectedYear=e.currentYear=r.getFullYear(),a==e.selectedMonth&&n==e.selectedYear||i||this._notifyChange(e),this._adjustInstDate(e),e.input&&e.input.val(s?"":this._formatDate(e))},_getDate:function(e){var t=!e.currentYear||e.input&&""==e.input.val()?null:this._daylightSavingAdjust(new Date(e.currentYear,e.currentMonth,e.currentDay));return t},_attachHandlers:function(e){var t=this._get(e,"stepMonths"),i="#"+e.id.replace(/\\\\/g,"\\");e.dpDiv.find("[data-handler]").map(function(){var e={prev:function(){window["DP_jQuery_"+dpuuid].datepicker._adjustDate(i,-t,"M")},next:function(){window["DP_jQuery_"+dpuuid].datepicker._adjustDate(i,+t,"M")},hide:function(){window["DP_jQuery_"+dpuuid].datepicker._hideDatepicker()},today:function(){window["DP_jQuery_"+dpuuid].datepicker._gotoToday(i)},selectDay:function(){return window["DP_jQuery_"+dpuuid].datepicker._selectDay(i,+this.getAttribute("data-month"),+this.getAttribute("data-year"),this),!1},selectMonth:function(){return window["DP_jQuery_"+dpuuid].datepicker._selectMonthYear(i,this,"M"),!1},selectYear:function(){return window["DP_jQuery_"+dpuuid].datepicker._selectMonthYear(i,this,"Y"),!1}};$(this).bind(this.getAttribute("data-event"),e[this.getAttribute("data-handler")])})},_generateHTML:function(e){var t=new Date;t=this._daylightSavingAdjust(new Date(t.getFullYear(),t.getMonth(),t.getDate()));var i=this._get(e,"isRTL"),s=this._get(e,"showButtonPanel"),a=this._get(e,"hideIfNoPrevNext"),n=this._get(e,"navigationAsDateFormat"),r=this._getNumberOfMonths(e),o=this._get(e,"showCurrentAtPos"),h=this._get(e,"stepMonths"),l=1!=r[0]||1!=r[1],u=this._daylightSavingAdjust(e.currentDay?new Date(e.currentYear,e.currentMonth,e.currentDay):new Date(9999,9,9)),d=this._getMinMaxDate(e,"min"),c=this._getMinMaxDate(e,"max"),p=e.drawMonth-o,m=e.drawYear;if(0>p&&(p+=12,m--),c){var f=this._daylightSavingAdjust(new Date(c.getFullYear(),c.getMonth()-r[0]*r[1]+1,c.getDate()));for(f=d&&d>f?d:f;this._daylightSavingAdjust(new Date(m,p,1))>f;)p--,0>p&&(p=11,m--)}e.drawMonth=p,e.drawYear=m;var g=this._get(e,"prevText");g=n?this.formatDate(g,this._daylightSavingAdjust(new Date(m,p-h,1)),this._getFormatConfig(e)):g;var v=this._canAdjustMonth(e,-1,m,p)?'<a class="ui-datepicker-prev ui-corner-all" data-handler="prev" data-event="click" title="'+g+'"><span class="ui-icon ui-icon-circle-triangle-'+(i?"e":"w")+'">'+g+"</span></a>":a?"":'<a class="ui-datepicker-prev ui-corner-all ui-state-disabled" title="'+g+'"><span class="ui-icon ui-icon-circle-triangle-'+(i?"e":"w")+'">'+g+"</span></a>",y=this._get(e,"nextText");y=n?this.formatDate(y,this._daylightSavingAdjust(new Date(m,p+h,1)),this._getFormatConfig(e)):y;var b=this._canAdjustMonth(e,1,m,p)?'<a class="ui-datepicker-next ui-corner-all" data-handler="next" data-event="click" title="'+y+'"><span class="ui-icon ui-icon-circle-triangle-'+(i?"w":"e")+'">'+y+"</span></a>":a?"":'<a class="ui-datepicker-next ui-corner-all ui-state-disabled" title="'+y+'"><span class="ui-icon ui-icon-circle-triangle-'+(i?"w":"e")+'">'+y+"</span></a>",_=this._get(e,"currentText"),x=this._get(e,"gotoCurrent")&&e.currentDay?u:t;_=n?this.formatDate(_,x,this._getFormatConfig(e)):_;var k=e.inline?"":'<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" data-handler="hide" data-event="click">'+this._get(e,"closeText")+"</button>",w=s?'<div class="ui-datepicker-buttonpane ui-widget-content">'+(i?k:"")+(this._isInRange(e,x)?'<button type="button" class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" data-handler="today" data-event="click">'+_+"</button>":"")+(i?"":k)+"</div>":"",D=parseInt(this._get(e,"firstDay"),10);D=isNaN(D)?0:D;var T=this._get(e,"showWeek"),S=this._get(e,"dayNames");this._get(e,"dayNamesShort");var M=this._get(e,"dayNamesMin"),N=this._get(e,"monthNames"),C=this._get(e,"monthNamesShort"),A=this._get(e,"beforeShowDay"),I=this._get(e,"showOtherMonths"),P=this._get(e,"selectOtherMonths");this._get(e,"calculateWeek")||this.iso8601Week;for(var z=this._getDefaultDate(e),H="",F=0;r[0]>F;F++){var E="";this.maxRows=4;for(var O=0;r[1]>O;O++){var j=this._daylightSavingAdjust(new Date(m,p,e.selectedDay)),W=" ui-corner-all",L="";if(l){if(L+='<div class="ui-datepicker-group',r[1]>1)switch(O){case 0:L+=" ui-datepicker-group-first",W=" ui-corner-"+(i?"right":"left");break;case r[1]-1:L+=" ui-datepicker-group-last",W=" ui-corner-"+(i?"left":"right");break;default:L+=" ui-datepicker-group-middle",W=""}L+='">'}L+='<div class="ui-datepicker-header ui-widget-header ui-helper-clearfix'+W+'">'+(/all|left/.test(W)&&0==F?i?b:v:"")+(/all|right/.test(W)&&0==F?i?v:b:"")+this._generateMonthYearHeader(e,p,m,d,c,F>0||O>0,N,C)+'</div><table class="ui-datepicker-calendar"><thead>'+"<tr>";for(var R=T?'<th class="ui-datepicker-week-col">'+this._get(e,"weekHeader")+"</th>":"",Y=0;7>Y;Y++){var J=(Y+D)%7;R+="<th"+((Y+D+6)%7>=5?' class="ui-datepicker-week-end"':"")+">"+'<span title="'+S[J]+'">'+M[J]+"</span></th>"}L+=R+"</tr></thead><tbody>";var B=this._getDaysInMonth(m,p);m==e.selectedYear&&p==e.selectedMonth&&(e.selectedDay=Math.min(e.selectedDay,B));var K=(this._getFirstDayOfMonth(m,p)-D+7)%7,V=Math.ceil((K+B)/7),U=l?this.maxRows>V?this.maxRows:V:V;this.maxRows=U;for(var q=this._daylightSavingAdjust(new Date(m,p,1-K)),G=0;U>G;G++){L+="<tr>";for(var Q=T?'<td class="ui-datepicker-week-col">'+this._get(e,"calculateWeek")(q)+"</td>":"",Y=0;7>Y;Y++){var X=A?A.apply(e.input?e.input[0]:null,[q]):[!0,""],Z=q.getMonth()!=p,et=Z&&!P||!X[0]||d&&d>q||c&&q>c;Q+='<td class="'+((Y+D+6)%7>=5?" ui-datepicker-week-end":"")+(Z?" ui-datepicker-other-month":"")+(q.getTime()==j.getTime()&&p==e.selectedMonth&&e._keyEvent||z.getTime()==q.getTime()&&z.getTime()==j.getTime()?" "+this._dayOverClass:"")+(et?" "+this._unselectableClass+" ui-state-disabled":"")+(Z&&!I?"":" "+X[1]+(q.getTime()==u.getTime()?" "+this._currentClass:"")+(q.getTime()==t.getTime()?" ui-datepicker-today":""))+'"'+(Z&&!I||!X[2]?"":' title="'+X[2]+'"')+(et?"":' data-handler="selectDay" data-event="click" data-month="'+q.getMonth()+'" data-year="'+q.getFullYear()+'"')+">"+(Z&&!I?"&#xa0;":et?'<span class="ui-state-default">'+q.getDate()+"</span>":'<a class="ui-state-default'+(q.getTime()==t.getTime()?" ui-state-highlight":"")+(q.getTime()==u.getTime()?" ui-state-active":"")+(Z?" ui-priority-secondary":"")+'" href="#">'+q.getDate()+"</a>")+"</td>",q.setDate(q.getDate()+1),q=this._daylightSavingAdjust(q)
}L+=Q+"</tr>"}p++,p>11&&(p=0,m++),L+="</tbody></table>"+(l?"</div>"+(r[0]>0&&O==r[1]-1?'<div class="ui-datepicker-row-break"></div>':""):""),E+=L}H+=E}return H+=w+($.ui.ie6&&!e.inline?'<iframe src="javascript:false;" class="ui-datepicker-cover" frameborder="0"></iframe>':""),e._keyEvent=!1,H},_generateMonthYearHeader:function(e,t,i,s,a,n,r,o){var h=this._get(e,"changeMonth"),l=this._get(e,"changeYear"),u=this._get(e,"showMonthAfterYear"),d='<div class="ui-datepicker-title">',c="";if(n||!h)c+='<span class="ui-datepicker-month">'+r[t]+"</span>";else{var p=s&&s.getFullYear()==i,m=a&&a.getFullYear()==i;c+='<select class="ui-datepicker-month" data-handler="selectMonth" data-event="change">';for(var f=0;12>f;f++)(!p||f>=s.getMonth())&&(!m||a.getMonth()>=f)&&(c+='<option value="'+f+'"'+(f==t?' selected="selected"':"")+">"+o[f]+"</option>");c+="</select>"}if(u||(d+=c+(!n&&h&&l?"":"&#xa0;")),!e.yearshtml)if(e.yearshtml="",n||!l)d+='<span class="ui-datepicker-year">'+i+"</span>";else{var g=this._get(e,"yearRange").split(":"),v=(new Date).getFullYear(),y=function(e){var t=e.match(/c[+-].*/)?i+parseInt(e.substring(1),10):e.match(/[+-].*/)?v+parseInt(e,10):parseInt(e,10);return isNaN(t)?v:t},b=y(g[0]),_=Math.max(b,y(g[1]||""));for(b=s?Math.max(b,s.getFullYear()):b,_=a?Math.min(_,a.getFullYear()):_,e.yearshtml+='<select class="ui-datepicker-year" data-handler="selectYear" data-event="change">';_>=b;b++)e.yearshtml+='<option value="'+b+'"'+(b==i?' selected="selected"':"")+">"+b+"</option>";e.yearshtml+="</select>",d+=e.yearshtml,e.yearshtml=null}return d+=this._get(e,"yearSuffix"),u&&(d+=(!n&&h&&l?"":"&#xa0;")+c),d+="</div>"},_adjustInstDate:function(e,t,i){var s=e.drawYear+("Y"==i?t:0),a=e.drawMonth+("M"==i?t:0),n=Math.min(e.selectedDay,this._getDaysInMonth(s,a))+("D"==i?t:0),r=this._restrictMinMax(e,this._daylightSavingAdjust(new Date(s,a,n)));e.selectedDay=r.getDate(),e.drawMonth=e.selectedMonth=r.getMonth(),e.drawYear=e.selectedYear=r.getFullYear(),("M"==i||"Y"==i)&&this._notifyChange(e)},_restrictMinMax:function(e,t){var i=this._getMinMaxDate(e,"min"),s=this._getMinMaxDate(e,"max"),a=i&&i>t?i:t;return a=s&&a>s?s:a},_notifyChange:function(e){var t=this._get(e,"onChangeMonthYear");t&&t.apply(e.input?e.input[0]:null,[e.selectedYear,e.selectedMonth+1,e])},_getNumberOfMonths:function(e){var t=this._get(e,"numberOfMonths");return null==t?[1,1]:"number"==typeof t?[1,t]:t},_getMinMaxDate:function(e,t){return this._determineDate(e,this._get(e,t+"Date"),null)},_getDaysInMonth:function(e,t){return 32-this._daylightSavingAdjust(new Date(e,t,32)).getDate()},_getFirstDayOfMonth:function(e,t){return new Date(e,t,1).getDay()},_canAdjustMonth:function(e,t,i,s){var a=this._getNumberOfMonths(e),n=this._daylightSavingAdjust(new Date(i,s+(0>t?t:a[0]*a[1]),1));return 0>t&&n.setDate(this._getDaysInMonth(n.getFullYear(),n.getMonth())),this._isInRange(e,n)},_isInRange:function(e,t){var i=this._getMinMaxDate(e,"min"),s=this._getMinMaxDate(e,"max");return(!i||t.getTime()>=i.getTime())&&(!s||t.getTime()<=s.getTime())},_getFormatConfig:function(e){var t=this._get(e,"shortYearCutoff");return t="string"!=typeof t?t:(new Date).getFullYear()%100+parseInt(t,10),{shortYearCutoff:t,dayNamesShort:this._get(e,"dayNamesShort"),dayNames:this._get(e,"dayNames"),monthNamesShort:this._get(e,"monthNamesShort"),monthNames:this._get(e,"monthNames")}},_formatDate:function(e,t,i,s){t||(e.currentDay=e.selectedDay,e.currentMonth=e.selectedMonth,e.currentYear=e.selectedYear);var a=t?"object"==typeof t?t:this._daylightSavingAdjust(new Date(s,i,t)):this._daylightSavingAdjust(new Date(e.currentYear,e.currentMonth,e.currentDay));return this.formatDate(this._get(e,"dateFormat"),a,this._getFormatConfig(e))}}),$.fn.datepicker=function(e){if(!this.length)return this;$.datepicker.initialized||($(document).mousedown($.datepicker._checkExternalClick).find(document.body).append($.datepicker.dpDiv),$.datepicker.initialized=!0);var t=Array.prototype.slice.call(arguments,1);return"string"!=typeof e||"isDisabled"!=e&&"getDate"!=e&&"widget"!=e?"option"==e&&2==arguments.length&&"string"==typeof arguments[1]?$.datepicker["_"+e+"Datepicker"].apply($.datepicker,[this[0]].concat(t)):this.each(function(){"string"==typeof e?$.datepicker["_"+e+"Datepicker"].apply($.datepicker,[this].concat(t)):$.datepicker._attachDatepicker(this,e)}):$.datepicker["_"+e+"Datepicker"].apply($.datepicker,[this[0]].concat(t))},$.datepicker=new Datepicker,$.datepicker.initialized=!1,$.datepicker.uuid=(new Date).getTime(),$.datepicker.version="1.9.2",window["DP_jQuery_"+dpuuid]=$})(jQuery);(function(e,t){var i="ui-dialog ui-widget ui-widget-content ui-corner-all ",s={buttons:!0,height:!0,maxHeight:!0,maxWidth:!0,minHeight:!0,minWidth:!0,width:!0},a={maxHeight:!0,maxWidth:!0,minHeight:!0,minWidth:!0};e.widget("ui.dialog",{version:"1.9.2",options:{autoOpen:!0,buttons:{},closeOnEscape:!0,closeText:"close",dialogClass:"",draggable:!0,hide:null,height:"auto",maxHeight:!1,maxWidth:!1,minHeight:150,minWidth:150,modal:!1,position:{my:"center",at:"center",of:window,collision:"fit",using:function(t){var i=e(this).css(t).offset().top;0>i&&e(this).css("top",t.top-i)}},resizable:!0,show:null,stack:!0,title:"",width:300,zIndex:1e3},_create:function(){this.originalTitle=this.element.attr("title"),"string"!=typeof this.originalTitle&&(this.originalTitle=""),this.oldPosition={parent:this.element.parent(),index:this.element.parent().children().index(this.element)},this.options.title=this.options.title||this.originalTitle;var s,a,n,r,o,h=this,l=this.options,u=l.title||"&#160;";s=(this.uiDialog=e("<div>")).addClass(i+l.dialogClass).css({display:"none",outline:0,zIndex:l.zIndex}).attr("tabIndex",-1).keydown(function(t){l.closeOnEscape&&!t.isDefaultPrevented()&&t.keyCode&&t.keyCode===e.ui.keyCode.ESCAPE&&(h.close(t),t.preventDefault())}).mousedown(function(e){h.moveToTop(!1,e)}).appendTo("body"),this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(s),a=(this.uiDialogTitlebar=e("<div>")).addClass("ui-dialog-titlebar  ui-widget-header  ui-corner-all  ui-helper-clearfix").bind("mousedown",function(){s.focus()}).prependTo(s),n=e("<a href='#'></a>").addClass("ui-dialog-titlebar-close  ui-corner-all").attr("role","button").click(function(e){e.preventDefault(),h.close(e)}).appendTo(a),(this.uiDialogTitlebarCloseText=e("<span>")).addClass("ui-icon ui-icon-closethick").text(l.closeText).appendTo(n),r=e("<span>").uniqueId().addClass("ui-dialog-title").html(u).prependTo(a),o=(this.uiDialogButtonPane=e("<div>")).addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"),(this.uiButtonSet=e("<div>")).addClass("ui-dialog-buttonset").appendTo(o),s.attr({role:"dialog","aria-labelledby":r.attr("id")}),a.find("*").add(a).disableSelection(),this._hoverable(n),this._focusable(n),l.draggable&&e.fn.draggable&&this._makeDraggable(),l.resizable&&e.fn.resizable&&this._makeResizable(),this._createButtons(l.buttons),this._isOpen=!1,e.fn.bgiframe&&s.bgiframe(),this._on(s,{keydown:function(i){if(l.modal&&i.keyCode===e.ui.keyCode.TAB){var a=e(":tabbable",s),n=a.filter(":first"),r=a.filter(":last");return i.target!==r[0]||i.shiftKey?i.target===n[0]&&i.shiftKey?(r.focus(1),!1):t:(n.focus(1),!1)}}})},_init:function(){this.options.autoOpen&&this.open()},_destroy:function(){var e,t=this.oldPosition;this.overlay&&this.overlay.destroy(),this.uiDialog.hide(),this.element.removeClass("ui-dialog-content ui-widget-content").hide().appendTo("body"),this.uiDialog.remove(),this.originalTitle&&this.element.attr("title",this.originalTitle),e=t.parent.children().eq(t.index),e.length&&e[0]!==this.element[0]?e.before(this.element):t.parent.append(this.element)},widget:function(){return this.uiDialog},close:function(t){var i,s,a=this;if(this._isOpen&&!1!==this._trigger("beforeClose",t))return this._isOpen=!1,this.overlay&&this.overlay.destroy(),this.options.hide?this._hide(this.uiDialog,this.options.hide,function(){a._trigger("close",t)}):(this.uiDialog.hide(),this._trigger("close",t)),e.ui.dialog.overlay.resize(),this.options.modal&&(i=0,e(".ui-dialog").each(function(){this!==a.uiDialog[0]&&(s=e(this).css("z-index"),isNaN(s)||(i=Math.max(i,s)))}),e.ui.dialog.maxZ=i),this},isOpen:function(){return this._isOpen},moveToTop:function(t,i){var s,a=this.options;return a.modal&&!t||!a.stack&&!a.modal?this._trigger("focus",i):(a.zIndex>e.ui.dialog.maxZ&&(e.ui.dialog.maxZ=a.zIndex),this.overlay&&(e.ui.dialog.maxZ+=1,e.ui.dialog.overlay.maxZ=e.ui.dialog.maxZ,this.overlay.$el.css("z-index",e.ui.dialog.overlay.maxZ)),s={scrollTop:this.element.scrollTop(),scrollLeft:this.element.scrollLeft()},e.ui.dialog.maxZ+=1,this.uiDialog.css("z-index",e.ui.dialog.maxZ),this.element.attr(s),this._trigger("focus",i),this)},open:function(){if(!this._isOpen){var t,i=this.options,s=this.uiDialog;return this._size(),this._position(i.position),s.show(i.show),this.overlay=i.modal?new e.ui.dialog.overlay(this):null,this.moveToTop(!0),t=this.element.find(":tabbable"),t.length||(t=this.uiDialogButtonPane.find(":tabbable"),t.length||(t=s)),t.eq(0).focus(),this._isOpen=!0,this._trigger("open"),this}},_createButtons:function(t){var i=this,s=!1;this.uiDialogButtonPane.remove(),this.uiButtonSet.empty(),"object"==typeof t&&null!==t&&e.each(t,function(){return!(s=!0)}),s?(e.each(t,function(t,s){var a,n;s=e.isFunction(s)?{click:s,text:t}:s,s=e.extend({type:"button"},s),n=s.click,s.click=function(){n.apply(i.element[0],arguments)},a=e("<button></button>",s).appendTo(i.uiButtonSet),e.fn.button&&a.button()}),this.uiDialog.addClass("ui-dialog-buttons"),this.uiDialogButtonPane.appendTo(this.uiDialog)):this.uiDialog.removeClass("ui-dialog-buttons")},_makeDraggable:function(){function t(e){return{position:e.position,offset:e.offset}}var i=this,s=this.options;this.uiDialog.draggable({cancel:".ui-dialog-content, .ui-dialog-titlebar-close",handle:".ui-dialog-titlebar",containment:"document",start:function(s,a){e(this).addClass("ui-dialog-dragging"),i._trigger("dragStart",s,t(a))},drag:function(e,s){i._trigger("drag",e,t(s))},stop:function(a,n){s.position=[n.position.left-i.document.scrollLeft(),n.position.top-i.document.scrollTop()],e(this).removeClass("ui-dialog-dragging"),i._trigger("dragStop",a,t(n)),e.ui.dialog.overlay.resize()}})},_makeResizable:function(i){function s(e){return{originalPosition:e.originalPosition,originalSize:e.originalSize,position:e.position,size:e.size}}i=i===t?this.options.resizable:i;var a=this,n=this.options,r=this.uiDialog.css("position"),o="string"==typeof i?i:"n,e,s,w,se,sw,ne,nw";this.uiDialog.resizable({cancel:".ui-dialog-content",containment:"document",alsoResize:this.element,maxWidth:n.maxWidth,maxHeight:n.maxHeight,minWidth:n.minWidth,minHeight:this._minHeight(),handles:o,start:function(t,i){e(this).addClass("ui-dialog-resizing"),a._trigger("resizeStart",t,s(i))},resize:function(e,t){a._trigger("resize",e,s(t))},stop:function(t,i){e(this).removeClass("ui-dialog-resizing"),n.height=e(this).height(),n.width=e(this).width(),a._trigger("resizeStop",t,s(i)),e.ui.dialog.overlay.resize()}}).css("position",r).find(".ui-resizable-se").addClass("ui-icon ui-icon-grip-diagonal-se")},_minHeight:function(){var e=this.options;return"auto"===e.height?e.minHeight:Math.min(e.minHeight,e.height)},_position:function(t){var i,s=[],a=[0,0];t?(("string"==typeof t||"object"==typeof t&&"0"in t)&&(s=t.split?t.split(" "):[t[0],t[1]],1===s.length&&(s[1]=s[0]),e.each(["left","top"],function(e,t){+s[e]===s[e]&&(a[e]=s[e],s[e]=t)}),t={my:s[0]+(0>a[0]?a[0]:"+"+a[0])+" "+s[1]+(0>a[1]?a[1]:"+"+a[1]),at:s.join(" ")}),t=e.extend({},e.ui.dialog.prototype.options.position,t)):t=e.ui.dialog.prototype.options.position,i=this.uiDialog.is(":visible"),i||this.uiDialog.show(),this.uiDialog.position(t),i||this.uiDialog.hide()},_setOptions:function(t){var i=this,n={},r=!1;e.each(t,function(e,t){i._setOption(e,t),e in s&&(r=!0),e in a&&(n[e]=t)}),r&&this._size(),this.uiDialog.is(":data(resizable)")&&this.uiDialog.resizable("option",n)},_setOption:function(t,s){var a,n,r=this.uiDialog;switch(t){case"buttons":this._createButtons(s);break;case"closeText":this.uiDialogTitlebarCloseText.text(""+s);break;case"dialogClass":r.removeClass(this.options.dialogClass).addClass(i+s);break;case"disabled":s?r.addClass("ui-dialog-disabled"):r.removeClass("ui-dialog-disabled");break;case"draggable":a=r.is(":data(draggable)"),a&&!s&&r.draggable("destroy"),!a&&s&&this._makeDraggable();break;case"position":this._position(s);break;case"resizable":n=r.is(":data(resizable)"),n&&!s&&r.resizable("destroy"),n&&"string"==typeof s&&r.resizable("option","handles",s),n||s===!1||this._makeResizable(s);break;case"title":e(".ui-dialog-title",this.uiDialogTitlebar).html(""+(s||"&#160;"))}this._super(t,s)},_size:function(){var t,i,s,a=this.options,n=this.uiDialog.is(":visible");this.element.show().css({width:"auto",minHeight:0,height:0}),a.minWidth>a.width&&(a.width=a.minWidth),t=this.uiDialog.css({height:"auto",width:a.width}).outerHeight(),i=Math.max(0,a.minHeight-t),"auto"===a.height?e.support.minHeight?this.element.css({minHeight:i,height:"auto"}):(this.uiDialog.show(),s=this.element.css("height","auto").height(),n||this.uiDialog.hide(),this.element.height(Math.max(s,i))):this.element.height(Math.max(a.height-t,0)),this.uiDialog.is(":data(resizable)")&&this.uiDialog.resizable("option","minHeight",this._minHeight())}}),e.extend(e.ui.dialog,{uuid:0,maxZ:0,getTitleId:function(e){var t=e.attr("id");return t||(this.uuid+=1,t=this.uuid),"ui-dialog-title-"+t},overlay:function(t){this.$el=e.ui.dialog.overlay.create(t)}}),e.extend(e.ui.dialog.overlay,{instances:[],oldInstances:[],maxZ:0,events:e.map("focus,mousedown,mouseup,keydown,keypress,click".split(","),function(e){return e+".dialog-overlay"}).join(" "),create:function(i){0===this.instances.length&&(setTimeout(function(){e.ui.dialog.overlay.instances.length&&e(document).bind(e.ui.dialog.overlay.events,function(i){return e(i.target).zIndex()<e.ui.dialog.overlay.maxZ?!1:t})},1),e(window).bind("resize.dialog-overlay",e.ui.dialog.overlay.resize));var s=this.oldInstances.pop()||e("<div>").addClass("ui-widget-overlay");return e(document).bind("keydown.dialog-overlay",function(t){var a=e.ui.dialog.overlay.instances;0!==a.length&&a[a.length-1]===s&&i.options.closeOnEscape&&!t.isDefaultPrevented()&&t.keyCode&&t.keyCode===e.ui.keyCode.ESCAPE&&(i.close(t),t.preventDefault())}),s.appendTo(document.body).css({width:this.width(),height:this.height()}),e.fn.bgiframe&&s.bgiframe(),this.instances.push(s),s},destroy:function(t){var i=e.inArray(t,this.instances),s=0;-1!==i&&this.oldInstances.push(this.instances.splice(i,1)[0]),0===this.instances.length&&e([document,window]).unbind(".dialog-overlay"),t.height(0).width(0).remove(),e.each(this.instances,function(){s=Math.max(s,this.css("z-index"))}),this.maxZ=s},height:function(){var t,i;return e.ui.ie?(t=Math.max(document.documentElement.scrollHeight,document.body.scrollHeight),i=Math.max(document.documentElement.offsetHeight,document.body.offsetHeight),i>t?e(window).height()+"px":t+"px"):e(document).height()+"px"},width:function(){var t,i;return e.ui.ie?(t=Math.max(document.documentElement.scrollWidth,document.body.scrollWidth),i=Math.max(document.documentElement.offsetWidth,document.body.offsetWidth),i>t?e(window).width()+"px":t+"px"):e(document).width()+"px"},resize:function(){var t=e([]);e.each(e.ui.dialog.overlay.instances,function(){t=t.add(this)}),t.css({width:0,height:0}).css({width:e.ui.dialog.overlay.width(),height:e.ui.dialog.overlay.height()})}}),e.extend(e.ui.dialog.overlay.prototype,{destroy:function(){e.ui.dialog.overlay.destroy(this.$el)}})})(jQuery);(function(e){var t=!1;e.widget("ui.menu",{version:"1.9.2",defaultElement:"<ul>",delay:300,options:{icons:{submenu:"ui-icon-carat-1-e"},menus:"ul",position:{my:"left top",at:"right top"},role:"menu",blur:null,focus:null,select:null},_create:function(){this.activeMenu=this.element,this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content ui-corner-all").toggleClass("ui-menu-icons",!!this.element.find(".ui-icon").length).attr({role:this.options.role,tabIndex:0}).bind("click"+this.eventNamespace,e.proxy(function(e){this.options.disabled&&e.preventDefault()},this)),this.options.disabled&&this.element.addClass("ui-state-disabled").attr("aria-disabled","true"),this._on({"mousedown .ui-menu-item > a":function(e){e.preventDefault()},"click .ui-state-disabled > a":function(e){e.preventDefault()},"click .ui-menu-item:has(a)":function(i){var s=e(i.target).closest(".ui-menu-item");!t&&s.not(".ui-state-disabled").length&&(t=!0,this.select(i),s.has(".ui-menu").length?this.expand(i):this.element.is(":focus")||(this.element.trigger("focus",[!0]),this.active&&1===this.active.parents(".ui-menu").length&&clearTimeout(this.timer)))},"mouseenter .ui-menu-item":function(t){var i=e(t.currentTarget);i.siblings().children(".ui-state-active").removeClass("ui-state-active"),this.focus(t,i)},mouseleave:"collapseAll","mouseleave .ui-menu":"collapseAll",focus:function(e,t){var i=this.active||this.element.children(".ui-menu-item").eq(0);t||this.focus(e,i)},blur:function(t){this._delay(function(){e.contains(this.element[0],this.document[0].activeElement)||this.collapseAll(t)})},keydown:"_keydown"}),this.refresh(),this._on(this.document,{click:function(i){e(i.target).closest(".ui-menu").length||this.collapseAll(i),t=!1}})},_destroy:function(){this.element.removeAttr("aria-activedescendant").find(".ui-menu").andSelf().removeClass("ui-menu ui-widget ui-widget-content ui-corner-all ui-menu-icons").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(),this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").children("a").removeUniqueId().removeClass("ui-corner-all ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function(){var t=e(this);t.data("ui-menu-submenu-carat")&&t.remove()}),this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")},_keydown:function(t){function i(e){return e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")}var s,a,n,o,r,h=!0;switch(t.keyCode){case e.ui.keyCode.PAGE_UP:this.previousPage(t);break;case e.ui.keyCode.PAGE_DOWN:this.nextPage(t);break;case e.ui.keyCode.HOME:this._move("first","first",t);break;case e.ui.keyCode.END:this._move("last","last",t);break;case e.ui.keyCode.UP:this.previous(t);break;case e.ui.keyCode.DOWN:this.next(t);break;case e.ui.keyCode.LEFT:this.collapse(t);break;case e.ui.keyCode.RIGHT:this.active&&!this.active.is(".ui-state-disabled")&&this.expand(t);break;case e.ui.keyCode.ENTER:case e.ui.keyCode.SPACE:this._activate(t);break;case e.ui.keyCode.ESCAPE:this.collapse(t);break;default:h=!1,a=this.previousFilter||"",n=String.fromCharCode(t.keyCode),o=!1,clearTimeout(this.filterTimer),n===a?o=!0:n=a+n,r=RegExp("^"+i(n),"i"),s=this.activeMenu.children(".ui-menu-item").filter(function(){return r.test(e(this).children("a").text())}),s=o&&-1!==s.index(this.active.next())?this.active.nextAll(".ui-menu-item"):s,s.length||(n=String.fromCharCode(t.keyCode),r=RegExp("^"+i(n),"i"),s=this.activeMenu.children(".ui-menu-item").filter(function(){return r.test(e(this).children("a").text())})),s.length?(this.focus(t,s),s.length>1?(this.previousFilter=n,this.filterTimer=this._delay(function(){delete this.previousFilter},1e3)):delete this.previousFilter):delete this.previousFilter}h&&t.preventDefault()},_activate:function(e){this.active.is(".ui-state-disabled")||(this.active.children("a[aria-haspopup='true']").length?this.expand(e):this.select(e))},refresh:function(){var t,i=this.options.icons.submenu,s=this.element.find(this.options.menus);s.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-corner-all").hide().attr({role:this.options.role,"aria-hidden":"true","aria-expanded":"false"}).each(function(){var t=e(this),s=t.prev("a"),a=e("<span>").addClass("ui-menu-icon ui-icon "+i).data("ui-menu-submenu-carat",!0);s.attr("aria-haspopup","true").prepend(a),t.attr("aria-labelledby",s.attr("id"))}),t=s.add(this.element),t.children(":not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role","presentation").children("a").uniqueId().addClass("ui-corner-all").attr({tabIndex:-1,role:this._itemRole()}),t.children(":not(.ui-menu-item)").each(function(){var t=e(this);/[^\-—–\s]/.test(t.text())||t.addClass("ui-widget-content ui-menu-divider")}),t.children(".ui-state-disabled").attr("aria-disabled","true"),this.active&&!e.contains(this.element[0],this.active[0])&&this.blur()},_itemRole:function(){return{menu:"menuitem",listbox:"option"}[this.options.role]},focus:function(e,t){var i,s;this.blur(e,e&&"focus"===e.type),this._scrollIntoView(t),this.active=t.first(),s=this.active.children("a").addClass("ui-state-focus"),this.options.role&&this.element.attr("aria-activedescendant",s.attr("id")),this.active.parent().closest(".ui-menu-item").children("a:first").addClass("ui-state-active"),e&&"keydown"===e.type?this._close():this.timer=this._delay(function(){this._close()},this.delay),i=t.children(".ui-menu"),i.length&&/^mouse/.test(e.type)&&this._startOpening(i),this.activeMenu=t.parent(),this._trigger("focus",e,{item:t})},_scrollIntoView:function(t){var i,s,a,n,o,r;this._hasScroll()&&(i=parseFloat(e.css(this.activeMenu[0],"borderTopWidth"))||0,s=parseFloat(e.css(this.activeMenu[0],"paddingTop"))||0,a=t.offset().top-this.activeMenu.offset().top-i-s,n=this.activeMenu.scrollTop(),o=this.activeMenu.height(),r=t.height(),0>a?this.activeMenu.scrollTop(n+a):a+r>o&&this.activeMenu.scrollTop(n+a-o+r))},blur:function(e,t){t||clearTimeout(this.timer),this.active&&(this.active.children("a").removeClass("ui-state-focus"),this.active=null,this._trigger("blur",e,{item:this.active}))},_startOpening:function(e){clearTimeout(this.timer),"true"===e.attr("aria-hidden")&&(this.timer=this._delay(function(){this._close(),this._open(e)},this.delay))},_open:function(t){var i=e.extend({of:this.active},this.options.position);clearTimeout(this.timer),this.element.find(".ui-menu").not(t.parents(".ui-menu")).hide().attr("aria-hidden","true"),t.show().removeAttr("aria-hidden").attr("aria-expanded","true").position(i)},collapseAll:function(t,i){clearTimeout(this.timer),this.timer=this._delay(function(){var s=i?this.element:e(t&&t.target).closest(this.element.find(".ui-menu"));s.length||(s=this.element),this._close(s),this.blur(t),this.activeMenu=s},this.delay)},_close:function(e){e||(e=this.active?this.active.parent():this.element),e.find(".ui-menu").hide().attr("aria-hidden","true").attr("aria-expanded","false").end().find("a.ui-state-active").removeClass("ui-state-active")},collapse:function(e){var t=this.active&&this.active.parent().closest(".ui-menu-item",this.element);t&&t.length&&(this._close(),this.focus(e,t))},expand:function(e){var t=this.active&&this.active.children(".ui-menu ").children(".ui-menu-item").first();t&&t.length&&(this._open(t.parent()),this._delay(function(){this.focus(e,t)}))},next:function(e){this._move("next","first",e)},previous:function(e){this._move("prev","last",e)},isFirstItem:function(){return this.active&&!this.active.prevAll(".ui-menu-item").length},isLastItem:function(){return this.active&&!this.active.nextAll(".ui-menu-item").length},_move:function(e,t,i){var s;this.active&&(s="first"===e||"last"===e?this.active["first"===e?"prevAll":"nextAll"](".ui-menu-item").eq(-1):this.active[e+"All"](".ui-menu-item").eq(0)),s&&s.length&&this.active||(s=this.activeMenu.children(".ui-menu-item")[t]()),this.focus(i,s)},nextPage:function(t){var i,s,a;return this.active?(this.isLastItem()||(this._hasScroll()?(s=this.active.offset().top,a=this.element.height(),this.active.nextAll(".ui-menu-item").each(function(){return i=e(this),0>i.offset().top-s-a}),this.focus(t,i)):this.focus(t,this.activeMenu.children(".ui-menu-item")[this.active?"last":"first"]())),undefined):(this.next(t),undefined)},previousPage:function(t){var i,s,a;return this.active?(this.isFirstItem()||(this._hasScroll()?(s=this.active.offset().top,a=this.element.height(),this.active.prevAll(".ui-menu-item").each(function(){return i=e(this),i.offset().top-s+a>0}),this.focus(t,i)):this.focus(t,this.activeMenu.children(".ui-menu-item").first())),undefined):(this.next(t),undefined)},_hasScroll:function(){return this.element.outerHeight()<this.element.prop("scrollHeight")},select:function(t){this.active=this.active||e(t.target).closest(".ui-menu-item");var i={item:this.active};this.active.has(".ui-menu").length||this.collapseAll(t,!0),this._trigger("select",t,i)}})})(jQuery);(function(e,t){e.widget("ui.progressbar",{version:"1.9.2",options:{value:0,max:100},min:0,_create:function(){this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({role:"progressbar","aria-valuemin":this.min,"aria-valuemax":this.options.max,"aria-valuenow":this._value()}),this.valueDiv=e("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element),this.oldValue=this._value(),this._refreshValue()},_destroy:function(){this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"),this.valueDiv.remove()},value:function(e){return e===t?this._value():(this._setOption("value",e),this)},_setOption:function(e,t){"value"===e&&(this.options.value=t,this._refreshValue(),this._value()===this.options.max&&this._trigger("complete")),this._super(e,t)},_value:function(){var e=this.options.value;return"number"!=typeof e&&(e=0),Math.min(this.options.max,Math.max(this.min,e))},_percentage:function(){return 100*this._value()/this.options.max},_refreshValue:function(){var e=this.value(),t=this._percentage();this.oldValue!==e&&(this.oldValue=e,this._trigger("change")),this.valueDiv.toggle(e>this.min).toggleClass("ui-corner-right",e===this.options.max).width(t.toFixed(0)+"%"),this.element.attr("aria-valuenow",e)}})})(jQuery);(function(e){var t=5;e.widget("ui.slider",e.ui.mouse,{version:"1.9.2",widgetEventPrefix:"slide",options:{animate:!1,distance:0,max:100,min:0,orientation:"horizontal",range:!1,step:1,value:0,values:null},_create:function(){var i,s,a=this.options,n=this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),o="<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>",r=[];for(this._keySliding=!1,this._mouseSliding=!1,this._animateOff=!0,this._handleIndex=null,this._detectOrientation(),this._mouseInit(),this.element.addClass("ui-slider ui-slider-"+this.orientation+" ui-widget"+" ui-widget-content"+" ui-corner-all"+(a.disabled?" ui-slider-disabled ui-disabled":"")),this.range=e([]),a.range&&(a.range===!0&&(a.values||(a.values=[this._valueMin(),this._valueMin()]),a.values.length&&2!==a.values.length&&(a.values=[a.values[0],a.values[0]])),this.range=e("<div></div>").appendTo(this.element).addClass("ui-slider-range ui-widget-header"+("min"===a.range||"max"===a.range?" ui-slider-range-"+a.range:""))),s=a.values&&a.values.length||1,i=n.length;s>i;i++)r.push(o);this.handles=n.add(e(r.join("")).appendTo(this.element)),this.handle=this.handles.eq(0),this.handles.add(this.range).filter("a").click(function(e){e.preventDefault()}).mouseenter(function(){a.disabled||e(this).addClass("ui-state-hover")}).mouseleave(function(){e(this).removeClass("ui-state-hover")}).focus(function(){a.disabled?e(this).blur():(e(".ui-slider .ui-state-focus").removeClass("ui-state-focus"),e(this).addClass("ui-state-focus"))}).blur(function(){e(this).removeClass("ui-state-focus")}),this.handles.each(function(t){e(this).data("ui-slider-handle-index",t)}),this._on(this.handles,{keydown:function(i){var s,a,n,o,r=e(i.target).data("ui-slider-handle-index");switch(i.keyCode){case e.ui.keyCode.HOME:case e.ui.keyCode.END:case e.ui.keyCode.PAGE_UP:case e.ui.keyCode.PAGE_DOWN:case e.ui.keyCode.UP:case e.ui.keyCode.RIGHT:case e.ui.keyCode.DOWN:case e.ui.keyCode.LEFT:if(i.preventDefault(),!this._keySliding&&(this._keySliding=!0,e(i.target).addClass("ui-state-active"),s=this._start(i,r),s===!1))return}switch(o=this.options.step,a=n=this.options.values&&this.options.values.length?this.values(r):this.value(),i.keyCode){case e.ui.keyCode.HOME:n=this._valueMin();break;case e.ui.keyCode.END:n=this._valueMax();break;case e.ui.keyCode.PAGE_UP:n=this._trimAlignValue(a+(this._valueMax()-this._valueMin())/t);break;case e.ui.keyCode.PAGE_DOWN:n=this._trimAlignValue(a-(this._valueMax()-this._valueMin())/t);break;case e.ui.keyCode.UP:case e.ui.keyCode.RIGHT:if(a===this._valueMax())return;n=this._trimAlignValue(a+o);break;case e.ui.keyCode.DOWN:case e.ui.keyCode.LEFT:if(a===this._valueMin())return;n=this._trimAlignValue(a-o)}this._slide(i,r,n)},keyup:function(t){var i=e(t.target).data("ui-slider-handle-index");this._keySliding&&(this._keySliding=!1,this._stop(t,i),this._change(t,i),e(t.target).removeClass("ui-state-active"))}}),this._refreshValue(),this._animateOff=!1},_destroy:function(){this.handles.remove(),this.range.remove(),this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-slider-disabled ui-widget ui-widget-content ui-corner-all"),this._mouseDestroy()},_mouseCapture:function(t){var i,s,a,n,o,r,h,l,u=this,d=this.options;return d.disabled?!1:(this.elementSize={width:this.element.outerWidth(),height:this.element.outerHeight()},this.elementOffset=this.element.offset(),i={x:t.pageX,y:t.pageY},s=this._normValueFromMouse(i),a=this._valueMax()-this._valueMin()+1,this.handles.each(function(t){var i=Math.abs(s-u.values(t));a>i&&(a=i,n=e(this),o=t)}),d.range===!0&&this.values(1)===d.min&&(o+=1,n=e(this.handles[o])),r=this._start(t,o),r===!1?!1:(this._mouseSliding=!0,this._handleIndex=o,n.addClass("ui-state-active").focus(),h=n.offset(),l=!e(t.target).parents().andSelf().is(".ui-slider-handle"),this._clickOffset=l?{left:0,top:0}:{left:t.pageX-h.left-n.width()/2,top:t.pageY-h.top-n.height()/2-(parseInt(n.css("borderTopWidth"),10)||0)-(parseInt(n.css("borderBottomWidth"),10)||0)+(parseInt(n.css("marginTop"),10)||0)},this.handles.hasClass("ui-state-hover")||this._slide(t,o,s),this._animateOff=!0,!0))},_mouseStart:function(){return!0},_mouseDrag:function(e){var t={x:e.pageX,y:e.pageY},i=this._normValueFromMouse(t);return this._slide(e,this._handleIndex,i),!1},_mouseStop:function(e){return this.handles.removeClass("ui-state-active"),this._mouseSliding=!1,this._stop(e,this._handleIndex),this._change(e,this._handleIndex),this._handleIndex=null,this._clickOffset=null,this._animateOff=!1,!1},_detectOrientation:function(){this.orientation="vertical"===this.options.orientation?"vertical":"horizontal"},_normValueFromMouse:function(e){var t,i,s,a,n;return"horizontal"===this.orientation?(t=this.elementSize.width,i=e.x-this.elementOffset.left-(this._clickOffset?this._clickOffset.left:0)):(t=this.elementSize.height,i=e.y-this.elementOffset.top-(this._clickOffset?this._clickOffset.top:0)),s=i/t,s>1&&(s=1),0>s&&(s=0),"vertical"===this.orientation&&(s=1-s),a=this._valueMax()-this._valueMin(),n=this._valueMin()+s*a,this._trimAlignValue(n)},_start:function(e,t){var i={handle:this.handles[t],value:this.value()};return this.options.values&&this.options.values.length&&(i.value=this.values(t),i.values=this.values()),this._trigger("start",e,i)},_slide:function(e,t,i){var s,a,n;this.options.values&&this.options.values.length?(s=this.values(t?0:1),2===this.options.values.length&&this.options.range===!0&&(0===t&&i>s||1===t&&s>i)&&(i=s),i!==this.values(t)&&(a=this.values(),a[t]=i,n=this._trigger("slide",e,{handle:this.handles[t],value:i,values:a}),s=this.values(t?0:1),n!==!1&&this.values(t,i,!0))):i!==this.value()&&(n=this._trigger("slide",e,{handle:this.handles[t],value:i}),n!==!1&&this.value(i))},_stop:function(e,t){var i={handle:this.handles[t],value:this.value()};this.options.values&&this.options.values.length&&(i.value=this.values(t),i.values=this.values()),this._trigger("stop",e,i)},_change:function(e,t){if(!this._keySliding&&!this._mouseSliding){var i={handle:this.handles[t],value:this.value()};this.options.values&&this.options.values.length&&(i.value=this.values(t),i.values=this.values()),this._trigger("change",e,i)}},value:function(e){return arguments.length?(this.options.value=this._trimAlignValue(e),this._refreshValue(),this._change(null,0),undefined):this._value()},values:function(t,i){var s,a,n;if(arguments.length>1)return this.options.values[t]=this._trimAlignValue(i),this._refreshValue(),this._change(null,t),undefined;if(!arguments.length)return this._values();if(!e.isArray(arguments[0]))return this.options.values&&this.options.values.length?this._values(t):this.value();for(s=this.options.values,a=arguments[0],n=0;s.length>n;n+=1)s[n]=this._trimAlignValue(a[n]),this._change(null,n);this._refreshValue()},_setOption:function(t,i){var s,a=0;switch(e.isArray(this.options.values)&&(a=this.options.values.length),e.Widget.prototype._setOption.apply(this,arguments),t){case"disabled":i?(this.handles.filter(".ui-state-focus").blur(),this.handles.removeClass("ui-state-hover"),this.handles.prop("disabled",!0),this.element.addClass("ui-disabled")):(this.handles.prop("disabled",!1),this.element.removeClass("ui-disabled"));break;case"orientation":this._detectOrientation(),this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-"+this.orientation),this._refreshValue();break;case"value":this._animateOff=!0,this._refreshValue(),this._change(null,0),this._animateOff=!1;break;case"values":for(this._animateOff=!0,this._refreshValue(),s=0;a>s;s+=1)this._change(null,s);this._animateOff=!1;break;case"min":case"max":this._animateOff=!0,this._refreshValue(),this._animateOff=!1}},_value:function(){var e=this.options.value;return e=this._trimAlignValue(e)},_values:function(e){var t,i,s;if(arguments.length)return t=this.options.values[e],t=this._trimAlignValue(t);for(i=this.options.values.slice(),s=0;i.length>s;s+=1)i[s]=this._trimAlignValue(i[s]);return i},_trimAlignValue:function(e){if(this._valueMin()>=e)return this._valueMin();if(e>=this._valueMax())return this._valueMax();var t=this.options.step>0?this.options.step:1,i=(e-this._valueMin())%t,s=e-i;return 2*Math.abs(i)>=t&&(s+=i>0?t:-t),parseFloat(s.toFixed(5))},_valueMin:function(){return this.options.min},_valueMax:function(){return this.options.max},_refreshValue:function(){var t,i,s,a,n,o=this.options.range,r=this.options,h=this,l=this._animateOff?!1:r.animate,u={};this.options.values&&this.options.values.length?this.handles.each(function(s){i=100*((h.values(s)-h._valueMin())/(h._valueMax()-h._valueMin())),u["horizontal"===h.orientation?"left":"bottom"]=i+"%",e(this).stop(1,1)[l?"animate":"css"](u,r.animate),h.options.range===!0&&("horizontal"===h.orientation?(0===s&&h.range.stop(1,1)[l?"animate":"css"]({left:i+"%"},r.animate),1===s&&h.range[l?"animate":"css"]({width:i-t+"%"},{queue:!1,duration:r.animate})):(0===s&&h.range.stop(1,1)[l?"animate":"css"]({bottom:i+"%"},r.animate),1===s&&h.range[l?"animate":"css"]({height:i-t+"%"},{queue:!1,duration:r.animate}))),t=i}):(s=this.value(),a=this._valueMin(),n=this._valueMax(),i=n!==a?100*((s-a)/(n-a)):0,u["horizontal"===this.orientation?"left":"bottom"]=i+"%",this.handle.stop(1,1)[l?"animate":"css"](u,r.animate),"min"===o&&"horizontal"===this.orientation&&this.range.stop(1,1)[l?"animate":"css"]({width:i+"%"},r.animate),"max"===o&&"horizontal"===this.orientation&&this.range[l?"animate":"css"]({width:100-i+"%"},{queue:!1,duration:r.animate}),"min"===o&&"vertical"===this.orientation&&this.range.stop(1,1)[l?"animate":"css"]({height:i+"%"},r.animate),"max"===o&&"vertical"===this.orientation&&this.range[l?"animate":"css"]({height:100-i+"%"},{queue:!1,duration:r.animate}))}})})(jQuery);(function(e){function t(e){return function(){var t=this.element.val();e.apply(this,arguments),this._refresh(),t!==this.element.val()&&this._trigger("change")}}e.widget("ui.spinner",{version:"1.9.2",defaultElement:"<input>",widgetEventPrefix:"spin",options:{culture:null,icons:{down:"ui-icon-triangle-1-s",up:"ui-icon-triangle-1-n"},incremental:!0,max:null,min:null,numberFormat:null,page:10,step:1,change:null,spin:null,start:null,stop:null},_create:function(){this._setOption("max",this.options.max),this._setOption("min",this.options.min),this._setOption("step",this.options.step),this._value(this.element.val(),!0),this._draw(),this._on(this._events),this._refresh(),this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_getCreateOptions:function(){var t={},i=this.element;return e.each(["min","max","step"],function(e,s){var a=i.attr(s);void 0!==a&&a.length&&(t[s]=a)}),t},_events:{keydown:function(e){this._start(e)&&this._keydown(e)&&e.preventDefault()},keyup:"_stop",focus:function(){this.previous=this.element.val()},blur:function(e){return this.cancelBlur?(delete this.cancelBlur,void 0):(this._refresh(),this.previous!==this.element.val()&&this._trigger("change",e),void 0)},mousewheel:function(e,t){if(t){if(!this.spinning&&!this._start(e))return!1;this._spin((t>0?1:-1)*this.options.step,e),clearTimeout(this.mousewheelTimer),this.mousewheelTimer=this._delay(function(){this.spinning&&this._stop(e)},100),e.preventDefault()}},"mousedown .ui-spinner-button":function(t){function i(){var e=this.element[0]===this.document[0].activeElement;e||(this.element.focus(),this.previous=s,this._delay(function(){this.previous=s}))}var s;s=this.element[0]===this.document[0].activeElement?this.previous:this.element.val(),t.preventDefault(),i.call(this),this.cancelBlur=!0,this._delay(function(){delete this.cancelBlur,i.call(this)}),this._start(t)!==!1&&this._repeat(null,e(t.currentTarget).hasClass("ui-spinner-up")?1:-1,t)},"mouseup .ui-spinner-button":"_stop","mouseenter .ui-spinner-button":function(t){return e(t.currentTarget).hasClass("ui-state-active")?this._start(t)===!1?!1:(this._repeat(null,e(t.currentTarget).hasClass("ui-spinner-up")?1:-1,t),void 0):void 0},"mouseleave .ui-spinner-button":"_stop"},_draw:function(){var e=this.uiSpinner=this.element.addClass("ui-spinner-input").attr("autocomplete","off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());this.element.attr("role","spinbutton"),this.buttons=e.find(".ui-spinner-button").attr("tabIndex",-1).button().removeClass("ui-corner-all"),this.buttons.height()>Math.ceil(.5*e.height())&&e.height()>0&&e.height(e.height()),this.options.disabled&&this.disable()},_keydown:function(t){var i=this.options,s=e.ui.keyCode;switch(t.keyCode){case s.UP:return this._repeat(null,1,t),!0;case s.DOWN:return this._repeat(null,-1,t),!0;case s.PAGE_UP:return this._repeat(null,i.page,t),!0;case s.PAGE_DOWN:return this._repeat(null,-i.page,t),!0}return!1},_uiSpinnerHtml:function(){return"<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>"},_buttonHtml:function(){return"<a class='ui-spinner-button ui-spinner-up ui-corner-tr'><span class='ui-icon "+this.options.icons.up+"'>&#9650;</span>"+"</a>"+"<a class='ui-spinner-button ui-spinner-down ui-corner-br'>"+"<span class='ui-icon "+this.options.icons.down+"'>&#9660;</span>"+"</a>"},_start:function(e){return this.spinning||this._trigger("start",e)!==!1?(this.counter||(this.counter=1),this.spinning=!0,!0):!1},_repeat:function(e,t,i){e=e||500,clearTimeout(this.timer),this.timer=this._delay(function(){this._repeat(40,t,i)},e),this._spin(t*this.options.step,i)},_spin:function(e,t){var i=this.value()||0;this.counter||(this.counter=1),i=this._adjustValue(i+e*this._increment(this.counter)),this.spinning&&this._trigger("spin",t,{value:i})===!1||(this._value(i),this.counter++)},_increment:function(t){var i=this.options.incremental;return i?e.isFunction(i)?i(t):Math.floor(t*t*t/5e4-t*t/500+17*t/200+1):1},_precision:function(){var e=this._precisionOf(this.options.step);return null!==this.options.min&&(e=Math.max(e,this._precisionOf(this.options.min))),e},_precisionOf:function(e){var t=""+e,i=t.indexOf(".");return-1===i?0:t.length-i-1},_adjustValue:function(e){var t,i,s=this.options;return t=null!==s.min?s.min:0,i=e-t,i=Math.round(i/s.step)*s.step,e=t+i,e=parseFloat(e.toFixed(this._precision())),null!==s.max&&e>s.max?s.max:null!==s.min&&s.min>e?s.min:e},_stop:function(e){this.spinning&&(clearTimeout(this.timer),clearTimeout(this.mousewheelTimer),this.counter=0,this.spinning=!1,this._trigger("stop",e))},_setOption:function(e,t){if("culture"===e||"numberFormat"===e){var i=this._parse(this.element.val());return this.options[e]=t,this.element.val(this._format(i)),void 0}("max"===e||"min"===e||"step"===e)&&"string"==typeof t&&(t=this._parse(t)),this._super(e,t),"disabled"===e&&(t?(this.element.prop("disabled",!0),this.buttons.button("disable")):(this.element.prop("disabled",!1),this.buttons.button("enable")))},_setOptions:t(function(e){this._super(e),this._value(this.element.val())}),_parse:function(e){return"string"==typeof e&&""!==e&&(e=window.Globalize&&this.options.numberFormat?Globalize.parseFloat(e,10,this.options.culture):+e),""===e||isNaN(e)?null:e},_format:function(e){return""===e?"":window.Globalize&&this.options.numberFormat?Globalize.format(e,this.options.numberFormat,this.options.culture):e},_refresh:function(){this.element.attr({"aria-valuemin":this.options.min,"aria-valuemax":this.options.max,"aria-valuenow":this._parse(this.element.val())})},_value:function(e,t){var i;""!==e&&(i=this._parse(e),null!==i&&(t||(i=this._adjustValue(i)),e=this._format(i))),this.element.val(e),this._refresh()},_destroy:function(){this.element.removeClass("ui-spinner-input").prop("disabled",!1).removeAttr("autocomplete").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"),this.uiSpinner.replaceWith(this.element)},stepUp:t(function(e){this._stepUp(e)}),_stepUp:function(e){this._spin((e||1)*this.options.step)},stepDown:t(function(e){this._stepDown(e)}),_stepDown:function(e){this._spin((e||1)*-this.options.step)},pageUp:t(function(e){this._stepUp((e||1)*this.options.page)}),pageDown:t(function(e){this._stepDown((e||1)*this.options.page)}),value:function(e){return arguments.length?(t(this._value).call(this,e),void 0):this._parse(this.element.val())},widget:function(){return this.uiSpinner}})})(jQuery);(function(e,t){function i(){return++a}function s(e){return e.hash.length>1&&e.href.replace(n,"")===location.href.replace(n,"").replace(/\s/g,"%20")}var a=0,n=/#.*$/;e.widget("ui.tabs",{version:"1.9.2",delay:300,options:{active:null,collapsible:!1,event:"click",heightStyle:"content",hide:null,show:null,activate:null,beforeActivate:null,beforeLoad:null,load:null},_create:function(){var i=this,s=this.options,a=s.active,n=location.hash.substring(1);this.running=!1,this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all").toggleClass("ui-tabs-collapsible",s.collapsible).delegate(".ui-tabs-nav > li","mousedown"+this.eventNamespace,function(t){e(this).is(".ui-state-disabled")&&t.preventDefault()}).delegate(".ui-tabs-anchor","focus"+this.eventNamespace,function(){e(this).closest("li").is(".ui-state-disabled")&&this.blur()}),this._processTabs(),null===a&&(n&&this.tabs.each(function(i,s){return e(s).attr("aria-controls")===n?(a=i,!1):t}),null===a&&(a=this.tabs.index(this.tabs.filter(".ui-tabs-active"))),(null===a||-1===a)&&(a=this.tabs.length?0:!1)),a!==!1&&(a=this.tabs.index(this.tabs.eq(a)),-1===a&&(a=s.collapsible?!1:0)),s.active=a,!s.collapsible&&s.active===!1&&this.anchors.length&&(s.active=0),e.isArray(s.disabled)&&(s.disabled=e.unique(s.disabled.concat(e.map(this.tabs.filter(".ui-state-disabled"),function(e){return i.tabs.index(e)}))).sort()),this.active=this.options.active!==!1&&this.anchors.length?this._findActive(this.options.active):e(),this._refresh(),this.active.length&&this.load(s.active)},_getCreateEventData:function(){return{tab:this.active,panel:this.active.length?this._getPanelForTab(this.active):e()}},_tabKeydown:function(i){var s=e(this.document[0].activeElement).closest("li"),a=this.tabs.index(s),n=!0;if(!this._handlePageNav(i)){switch(i.keyCode){case e.ui.keyCode.RIGHT:case e.ui.keyCode.DOWN:a++;break;case e.ui.keyCode.UP:case e.ui.keyCode.LEFT:n=!1,a--;break;case e.ui.keyCode.END:a=this.anchors.length-1;break;case e.ui.keyCode.HOME:a=0;break;case e.ui.keyCode.SPACE:return i.preventDefault(),clearTimeout(this.activating),this._activate(a),t;case e.ui.keyCode.ENTER:return i.preventDefault(),clearTimeout(this.activating),this._activate(a===this.options.active?!1:a),t;default:return}i.preventDefault(),clearTimeout(this.activating),a=this._focusNextTab(a,n),i.ctrlKey||(s.attr("aria-selected","false"),this.tabs.eq(a).attr("aria-selected","true"),this.activating=this._delay(function(){this.option("active",a)},this.delay))}},_panelKeydown:function(t){this._handlePageNav(t)||t.ctrlKey&&t.keyCode===e.ui.keyCode.UP&&(t.preventDefault(),this.active.focus())},_handlePageNav:function(i){return i.altKey&&i.keyCode===e.ui.keyCode.PAGE_UP?(this._activate(this._focusNextTab(this.options.active-1,!1)),!0):i.altKey&&i.keyCode===e.ui.keyCode.PAGE_DOWN?(this._activate(this._focusNextTab(this.options.active+1,!0)),!0):t},_findNextTab:function(t,i){function s(){return t>a&&(t=0),0>t&&(t=a),t}for(var a=this.tabs.length-1;-1!==e.inArray(s(),this.options.disabled);)t=i?t+1:t-1;return t},_focusNextTab:function(e,t){return e=this._findNextTab(e,t),this.tabs.eq(e).focus(),e},_setOption:function(e,i){return"active"===e?(this._activate(i),t):"disabled"===e?(this._setupDisabled(i),t):(this._super(e,i),"collapsible"===e&&(this.element.toggleClass("ui-tabs-collapsible",i),i||this.options.active!==!1||this._activate(0)),"event"===e&&this._setupEvents(i),"heightStyle"===e&&this._setupHeightStyle(i),t)},_tabId:function(e){return e.attr("aria-controls")||"ui-tabs-"+i()},_sanitizeSelector:function(e){return e?e.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g,"\\$&"):""},refresh:function(){var t=this.options,i=this.tablist.children(":has(a[href])");t.disabled=e.map(i.filter(".ui-state-disabled"),function(e){return i.index(e)}),this._processTabs(),t.active!==!1&&this.anchors.length?this.active.length&&!e.contains(this.tablist[0],this.active[0])?this.tabs.length===t.disabled.length?(t.active=!1,this.active=e()):this._activate(this._findNextTab(Math.max(0,t.active-1),!1)):t.active=this.tabs.index(this.active):(t.active=!1,this.active=e()),this._refresh()},_refresh:function(){this._setupDisabled(this.options.disabled),this._setupEvents(this.options.event),this._setupHeightStyle(this.options.heightStyle),this.tabs.not(this.active).attr({"aria-selected":"false",tabIndex:-1}),this.panels.not(this._getPanelForTab(this.active)).hide().attr({"aria-expanded":"false","aria-hidden":"true"}),this.active.length?(this.active.addClass("ui-tabs-active ui-state-active").attr({"aria-selected":"true",tabIndex:0}),this._getPanelForTab(this.active).show().attr({"aria-expanded":"true","aria-hidden":"false"})):this.tabs.eq(0).attr("tabIndex",0)},_processTabs:function(){var t=this;this.tablist=this._getList().addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").attr("role","tablist"),this.tabs=this.tablist.find("> li:has(a[href])").addClass("ui-state-default ui-corner-top").attr({role:"tab",tabIndex:-1}),this.anchors=this.tabs.map(function(){return e("a",this)[0]}).addClass("ui-tabs-anchor").attr({role:"presentation",tabIndex:-1}),this.panels=e(),this.anchors.each(function(i,a){var n,o,r,h=e(a).uniqueId().attr("id"),l=e(a).closest("li"),u=l.attr("aria-controls");s(a)?(n=a.hash,o=t.element.find(t._sanitizeSelector(n))):(r=t._tabId(l),n="#"+r,o=t.element.find(n),o.length||(o=t._createPanel(r),o.insertAfter(t.panels[i-1]||t.tablist)),o.attr("aria-live","polite")),o.length&&(t.panels=t.panels.add(o)),u&&l.data("ui-tabs-aria-controls",u),l.attr({"aria-controls":n.substring(1),"aria-labelledby":h}),o.attr("aria-labelledby",h)}),this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").attr("role","tabpanel")},_getList:function(){return this.element.find("ol,ul").eq(0)},_createPanel:function(t){return e("<div>").attr("id",t).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy",!0)},_setupDisabled:function(t){e.isArray(t)&&(t.length?t.length===this.anchors.length&&(t=!0):t=!1);for(var i,s=0;i=this.tabs[s];s++)t===!0||-1!==e.inArray(s,t)?e(i).addClass("ui-state-disabled").attr("aria-disabled","true"):e(i).removeClass("ui-state-disabled").removeAttr("aria-disabled");this.options.disabled=t},_setupEvents:function(t){var i={click:function(e){e.preventDefault()}};t&&e.each(t.split(" "),function(e,t){i[t]="_eventHandler"}),this._off(this.anchors.add(this.tabs).add(this.panels)),this._on(this.anchors,i),this._on(this.tabs,{keydown:"_tabKeydown"}),this._on(this.panels,{keydown:"_panelKeydown"}),this._focusable(this.tabs),this._hoverable(this.tabs)},_setupHeightStyle:function(t){var i,s,a=this.element.parent();"fill"===t?(e.support.minHeight||(s=a.css("overflow"),a.css("overflow","hidden")),i=a.height(),this.element.siblings(":visible").each(function(){var t=e(this),s=t.css("position");"absolute"!==s&&"fixed"!==s&&(i-=t.outerHeight(!0))}),s&&a.css("overflow",s),this.element.children().not(this.panels).each(function(){i-=e(this).outerHeight(!0)}),this.panels.each(function(){e(this).height(Math.max(0,i-e(this).innerHeight()+e(this).height()))}).css("overflow","auto")):"auto"===t&&(i=0,this.panels.each(function(){i=Math.max(i,e(this).height("").height())}).height(i))},_eventHandler:function(t){var i=this.options,s=this.active,a=e(t.currentTarget),n=a.closest("li"),o=n[0]===s[0],r=o&&i.collapsible,h=r?e():this._getPanelForTab(n),l=s.length?this._getPanelForTab(s):e(),u={oldTab:s,oldPanel:l,newTab:r?e():n,newPanel:h};t.preventDefault(),n.hasClass("ui-state-disabled")||n.hasClass("ui-tabs-loading")||this.running||o&&!i.collapsible||this._trigger("beforeActivate",t,u)===!1||(i.active=r?!1:this.tabs.index(n),this.active=o?e():n,this.xhr&&this.xhr.abort(),l.length||h.length||e.error("jQuery UI Tabs: Mismatching fragment identifier."),h.length&&this.load(this.tabs.index(n),t),this._toggle(t,u))},_toggle:function(t,i){function s(){n.running=!1,n._trigger("activate",t,i)}function a(){i.newTab.closest("li").addClass("ui-tabs-active ui-state-active"),o.length&&n.options.show?n._show(o,n.options.show,s):(o.show(),s())}var n=this,o=i.newPanel,r=i.oldPanel;this.running=!0,r.length&&this.options.hide?this._hide(r,this.options.hide,function(){i.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"),a()}):(i.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"),r.hide(),a()),r.attr({"aria-expanded":"false","aria-hidden":"true"}),i.oldTab.attr("aria-selected","false"),o.length&&r.length?i.oldTab.attr("tabIndex",-1):o.length&&this.tabs.filter(function(){return 0===e(this).attr("tabIndex")}).attr("tabIndex",-1),o.attr({"aria-expanded":"true","aria-hidden":"false"}),i.newTab.attr({"aria-selected":"true",tabIndex:0})},_activate:function(t){var i,s=this._findActive(t);s[0]!==this.active[0]&&(s.length||(s=this.active),i=s.find(".ui-tabs-anchor")[0],this._eventHandler({target:i,currentTarget:i,preventDefault:e.noop}))},_findActive:function(t){return t===!1?e():this.tabs.eq(t)},_getIndex:function(e){return"string"==typeof e&&(e=this.anchors.index(this.anchors.filter("[href$='"+e+"']"))),e},_destroy:function(){this.xhr&&this.xhr.abort(),this.element.removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible"),this.tablist.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").removeAttr("role"),this.anchors.removeClass("ui-tabs-anchor").removeAttr("role").removeAttr("tabIndex").removeData("href.tabs").removeData("load.tabs").removeUniqueId(),this.tabs.add(this.panels).each(function(){e.data(this,"ui-tabs-destroy")?e(this).remove():e(this).removeClass("ui-state-default ui-state-active ui-state-disabled ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel").removeAttr("tabIndex").removeAttr("aria-live").removeAttr("aria-busy").removeAttr("aria-selected").removeAttr("aria-labelledby").removeAttr("aria-hidden").removeAttr("aria-expanded").removeAttr("role")}),this.tabs.each(function(){var t=e(this),i=t.data("ui-tabs-aria-controls");i?t.attr("aria-controls",i):t.removeAttr("aria-controls")}),this.panels.show(),"content"!==this.options.heightStyle&&this.panels.css("height","")},enable:function(i){var s=this.options.disabled;s!==!1&&(i===t?s=!1:(i=this._getIndex(i),s=e.isArray(s)?e.map(s,function(e){return e!==i?e:null}):e.map(this.tabs,function(e,t){return t!==i?t:null})),this._setupDisabled(s))},disable:function(i){var s=this.options.disabled;if(s!==!0){if(i===t)s=!0;else{if(i=this._getIndex(i),-1!==e.inArray(i,s))return;s=e.isArray(s)?e.merge([i],s).sort():[i]}this._setupDisabled(s)}},load:function(t,i){t=this._getIndex(t);var a=this,n=this.tabs.eq(t),o=n.find(".ui-tabs-anchor"),r=this._getPanelForTab(n),h={tab:n,panel:r};s(o[0])||(this.xhr=e.ajax(this._ajaxSettings(o,i,h)),this.xhr&&"canceled"!==this.xhr.statusText&&(n.addClass("ui-tabs-loading"),r.attr("aria-busy","true"),this.xhr.success(function(e){setTimeout(function(){r.html(e),a._trigger("load",i,h)},1)}).complete(function(e,t){setTimeout(function(){"abort"===t&&a.panels.stop(!1,!0),n.removeClass("ui-tabs-loading"),r.removeAttr("aria-busy"),e===a.xhr&&delete a.xhr},1)})))},_ajaxSettings:function(t,i,s){var a=this;return{url:t.attr("href"),beforeSend:function(t,n){return a._trigger("beforeLoad",i,e.extend({jqXHR:t,ajaxSettings:n},s))}}},_getPanelForTab:function(t){var i=e(t).attr("aria-controls");return this.element.find(this._sanitizeSelector("#"+i))}}),e.uiBackCompat!==!1&&(e.ui.tabs.prototype._ui=function(e,t){return{tab:e,panel:t,index:this.anchors.index(e)}},e.widget("ui.tabs",e.ui.tabs,{url:function(e,t){this.anchors.eq(e).attr("href",t)}}),e.widget("ui.tabs",e.ui.tabs,{options:{ajaxOptions:null,cache:!1},_create:function(){this._super();var i=this;this._on({tabsbeforeload:function(s,a){return e.data(a.tab[0],"cache.tabs")?(s.preventDefault(),t):(a.jqXHR.success(function(){i.options.cache&&e.data(a.tab[0],"cache.tabs",!0)}),t)}})},_ajaxSettings:function(t,i,s){var a=this.options.ajaxOptions;return e.extend({},a,{error:function(e,t){try{a.error(e,t,s.tab.closest("li").index(),s.tab[0])}catch(i){}}},this._superApply(arguments))},_setOption:function(e,t){"cache"===e&&t===!1&&this.anchors.removeData("cache.tabs"),this._super(e,t)},_destroy:function(){this.anchors.removeData("cache.tabs"),this._super()},url:function(e){this.anchors.eq(e).removeData("cache.tabs"),this._superApply(arguments)}}),e.widget("ui.tabs",e.ui.tabs,{abort:function(){this.xhr&&this.xhr.abort()}}),e.widget("ui.tabs",e.ui.tabs,{options:{spinner:"<em>Loading&#8230;</em>"},_create:function(){this._super(),this._on({tabsbeforeload:function(e,t){if(e.target===this.element[0]&&this.options.spinner){var i=t.tab.find("span"),s=i.html();i.html(this.options.spinner),t.jqXHR.complete(function(){i.html(s)})}}})}}),e.widget("ui.tabs",e.ui.tabs,{options:{enable:null,disable:null},enable:function(t){var i,s=this.options;(t&&s.disabled===!0||e.isArray(s.disabled)&&-1!==e.inArray(t,s.disabled))&&(i=!0),this._superApply(arguments),i&&this._trigger("enable",null,this._ui(this.anchors[t],this.panels[t]))},disable:function(t){var i,s=this.options;(t&&s.disabled===!1||e.isArray(s.disabled)&&-1===e.inArray(t,s.disabled))&&(i=!0),this._superApply(arguments),i&&this._trigger("disable",null,this._ui(this.anchors[t],this.panels[t]))}}),e.widget("ui.tabs",e.ui.tabs,{options:{add:null,remove:null,tabTemplate:"<li><a href='#{href}'><span>#{label}</span></a></li>"},add:function(i,s,a){a===t&&(a=this.anchors.length);var n,o,r=this.options,h=e(r.tabTemplate.replace(/#\{href\}/g,i).replace(/#\{label\}/g,s)),l=i.indexOf("#")?this._tabId(h):i.replace("#","");return h.addClass("ui-state-default ui-corner-top").data("ui-tabs-destroy",!0),h.attr("aria-controls",l),n=a>=this.tabs.length,o=this.element.find("#"+l),o.length||(o=this._createPanel(l),n?a>0?o.insertAfter(this.panels.eq(-1)):o.appendTo(this.element):o.insertBefore(this.panels[a])),o.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").hide(),n?h.appendTo(this.tablist):h.insertBefore(this.tabs[a]),r.disabled=e.map(r.disabled,function(e){return e>=a?++e:e}),this.refresh(),1===this.tabs.length&&r.active===!1&&this.option("active",0),this._trigger("add",null,this._ui(this.anchors[a],this.panels[a])),this},remove:function(t){t=this._getIndex(t);var i=this.options,s=this.tabs.eq(t).remove(),a=this._getPanelForTab(s).remove();return s.hasClass("ui-tabs-active")&&this.anchors.length>2&&this._activate(t+(this.anchors.length>t+1?1:-1)),i.disabled=e.map(e.grep(i.disabled,function(e){return e!==t}),function(e){return e>=t?--e:e}),this.refresh(),this._trigger("remove",null,this._ui(s.find("a")[0],a[0])),this}}),e.widget("ui.tabs",e.ui.tabs,{length:function(){return this.anchors.length}}),e.widget("ui.tabs",e.ui.tabs,{options:{idPrefix:"ui-tabs-"},_tabId:function(t){var s=t.is("li")?t.find("a[href]"):t;return s=s[0],e(s).closest("li").attr("aria-controls")||s.title&&s.title.replace(/\s/g,"_").replace(/[^\w\u00c0-\uFFFF\-]/g,"")||this.options.idPrefix+i()}}),e.widget("ui.tabs",e.ui.tabs,{options:{panelTemplate:"<div></div>"},_createPanel:function(t){return e(this.options.panelTemplate).attr("id",t).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy",!0)}}),e.widget("ui.tabs",e.ui.tabs,{_create:function(){var e=this.options;null===e.active&&e.selected!==t&&(e.active=-1===e.selected?!1:e.selected),this._super(),e.selected=e.active,e.selected===!1&&(e.selected=-1)},_setOption:function(e,t){if("selected"!==e)return this._super(e,t);var i=this.options;this._super("active",-1===t?!1:t),i.selected=i.active,i.selected===!1&&(i.selected=-1)},_eventHandler:function(){this._superApply(arguments),this.options.selected=this.options.active,this.options.selected===!1&&(this.options.selected=-1)}}),e.widget("ui.tabs",e.ui.tabs,{options:{show:null,select:null},_create:function(){this._super(),this.options.active!==!1&&this._trigger("show",null,this._ui(this.active.find(".ui-tabs-anchor")[0],this._getPanelForTab(this.active)[0]))},_trigger:function(e,t,i){var s,a,n=this._superApply(arguments);return n?("beforeActivate"===e?(s=i.newTab.length?i.newTab:i.oldTab,a=i.newPanel.length?i.newPanel:i.oldPanel,n=this._super("select",t,{tab:s.find(".ui-tabs-anchor")[0],panel:a[0],index:s.closest("li").index()})):"activate"===e&&i.newTab.length&&(n=this._super("show",t,{tab:i.newTab.find(".ui-tabs-anchor")[0],panel:i.newPanel[0],index:i.newTab.closest("li").index()})),n):!1}}),e.widget("ui.tabs",e.ui.tabs,{select:function(e){if(e=this._getIndex(e),-1===e){if(!this.options.collapsible||-1===this.options.selected)return;e=this.options.selected}this.anchors.eq(e).trigger(this.options.event+this.eventNamespace)}}),function(){var t=0;e.widget("ui.tabs",e.ui.tabs,{options:{cookie:null},_create:function(){var e,t=this.options;null==t.active&&t.cookie&&(e=parseInt(this._cookie(),10),-1===e&&(e=!1),t.active=e),this._super()},_cookie:function(i){var s=[this.cookie||(this.cookie=this.options.cookie.name||"ui-tabs-"+ ++t)];return arguments.length&&(s.push(i===!1?-1:i),s.push(this.options.cookie)),e.cookie.apply(null,s)},_refresh:function(){this._super(),this.options.cookie&&this._cookie(this.options.active,this.options.cookie)},_eventHandler:function(){this._superApply(arguments),this.options.cookie&&this._cookie(this.options.active,this.options.cookie)},_destroy:function(){this._super(),this.options.cookie&&this._cookie(null,this.options.cookie)}})}(),e.widget("ui.tabs",e.ui.tabs,{_trigger:function(t,i,s){var a=e.extend({},s);return"load"===t&&(a.panel=a.panel[0],a.tab=a.tab.find(".ui-tabs-anchor")[0]),this._super(t,i,a)}}),e.widget("ui.tabs",e.ui.tabs,{options:{fx:null},_getFx:function(){var t,i,s=this.options.fx;return s&&(e.isArray(s)?(t=s[0],i=s[1]):t=i=s),s?{show:i,hide:t}:null},_toggle:function(e,i){function s(){n.running=!1,n._trigger("activate",e,i)}function a(){i.newTab.closest("li").addClass("ui-tabs-active ui-state-active"),o.length&&h.show?o.animate(h.show,h.show.duration,function(){s()}):(o.show(),s())}var n=this,o=i.newPanel,r=i.oldPanel,h=this._getFx();return h?(n.running=!0,r.length&&h.hide?r.animate(h.hide,h.hide.duration,function(){i.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"),a()}):(i.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"),r.hide(),a()),t):this._super(e,i)}}))})(jQuery);(function(e){function t(t,i){var s=(t.attr("aria-describedby")||"").split(/\s+/);s.push(i),t.data("ui-tooltip-id",i).attr("aria-describedby",e.trim(s.join(" ")))}function i(t){var i=t.data("ui-tooltip-id"),s=(t.attr("aria-describedby")||"").split(/\s+/),a=e.inArray(i,s);-1!==a&&s.splice(a,1),t.removeData("ui-tooltip-id"),s=e.trim(s.join(" ")),s?t.attr("aria-describedby",s):t.removeAttr("aria-describedby")}var s=0;e.widget("ui.tooltip",{version:"1.9.2",options:{content:function(){return e(this).attr("title")},hide:!0,items:"[title]:not([disabled])",position:{my:"left top+15",at:"left bottom",collision:"flipfit flip"},show:!0,tooltipClass:null,track:!1,close:null,open:null},_create:function(){this._on({mouseover:"open",focusin:"open"}),this.tooltips={},this.parents={},this.options.disabled&&this._disable()},_setOption:function(t,i){var s=this;return"disabled"===t?(this[i?"_disable":"_enable"](),this.options[t]=i,void 0):(this._super(t,i),"content"===t&&e.each(this.tooltips,function(e,t){s._updateContent(t)}),void 0)},_disable:function(){var t=this;e.each(this.tooltips,function(i,s){var a=e.Event("blur");a.target=a.currentTarget=s[0],t.close(a,!0)}),this.element.find(this.options.items).andSelf().each(function(){var t=e(this);t.is("[title]")&&t.data("ui-tooltip-title",t.attr("title")).attr("title","")})},_enable:function(){this.element.find(this.options.items).andSelf().each(function(){var t=e(this);t.data("ui-tooltip-title")&&t.attr("title",t.data("ui-tooltip-title"))})},open:function(t){var i=this,s=e(t?t.target:this.element).closest(this.options.items);s.length&&!s.data("ui-tooltip-id")&&(s.attr("title")&&s.data("ui-tooltip-title",s.attr("title")),s.data("ui-tooltip-open",!0),t&&"mouseover"===t.type&&s.parents().each(function(){var t,s=e(this);s.data("ui-tooltip-open")&&(t=e.Event("blur"),t.target=t.currentTarget=this,i.close(t,!0)),s.attr("title")&&(s.uniqueId(),i.parents[this.id]={element:this,title:s.attr("title")},s.attr("title",""))}),this._updateContent(s,t))},_updateContent:function(e,t){var i,s=this.options.content,a=this,n=t?t.type:null;return"string"==typeof s?this._open(t,e,s):(i=s.call(e[0],function(i){e.data("ui-tooltip-open")&&a._delay(function(){t&&(t.type=n),this._open(t,e,i)})}),i&&this._open(t,e,i),void 0)},_open:function(i,s,a){function n(e){l.of=e,o.is(":hidden")||o.position(l)}var o,r,h,l=e.extend({},this.options.position);if(a){if(o=this._find(s),o.length)return o.find(".ui-tooltip-content").html(a),void 0;s.is("[title]")&&(i&&"mouseover"===i.type?s.attr("title",""):s.removeAttr("title")),o=this._tooltip(s),t(s,o.attr("id")),o.find(".ui-tooltip-content").html(a),this.options.track&&i&&/^mouse/.test(i.type)?(this._on(this.document,{mousemove:n}),n(i)):o.position(e.extend({of:s},this.options.position)),o.hide(),this._show(o,this.options.show),this.options.show&&this.options.show.delay&&(h=setInterval(function(){o.is(":visible")&&(n(l.of),clearInterval(h))},e.fx.interval)),this._trigger("open",i,{tooltip:o}),r={keyup:function(t){if(t.keyCode===e.ui.keyCode.ESCAPE){var i=e.Event(t);i.currentTarget=s[0],this.close(i,!0)}},remove:function(){this._removeTooltip(o)}},i&&"mouseover"!==i.type||(r.mouseleave="close"),i&&"focusin"!==i.type||(r.focusout="close"),this._on(!0,s,r)}},close:function(t){var s=this,a=e(t?t.currentTarget:this.element),n=this._find(a);this.closing||(a.data("ui-tooltip-title")&&a.attr("title",a.data("ui-tooltip-title")),i(a),n.stop(!0),this._hide(n,this.options.hide,function(){s._removeTooltip(e(this))}),a.removeData("ui-tooltip-open"),this._off(a,"mouseleave focusout keyup"),a[0]!==this.element[0]&&this._off(a,"remove"),this._off(this.document,"mousemove"),t&&"mouseleave"===t.type&&e.each(this.parents,function(t,i){e(i.element).attr("title",i.title),delete s.parents[t]}),this.closing=!0,this._trigger("close",t,{tooltip:n}),this.closing=!1)},_tooltip:function(t){var i="ui-tooltip-"+s++,a=e("<div>").attr({id:i,role:"tooltip"}).addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content "+(this.options.tooltipClass||""));return e("<div>").addClass("ui-tooltip-content").appendTo(a),a.appendTo(this.document[0].body),e.fn.bgiframe&&a.bgiframe(),this.tooltips[i]=t,a},_find:function(t){var i=t.data("ui-tooltip-id");return i?e("#"+i):e()},_removeTooltip:function(e){e.remove(),delete this.tooltips[e.attr("id")]},_destroy:function(){var t=this;e.each(this.tooltips,function(i,s){var a=e.Event("blur");a.target=a.currentTarget=s[0],t.close(a,!0),e("#"+i).remove(),s.data("ui-tooltip-title")&&(s.attr("title",s.data("ui-tooltip-title")),s.removeData("ui-tooltip-title"))})}})})(jQuery);jQuery.effects||function(e,t){var i=e.uiBackCompat!==!1,s="ui-effects-";e.effects={effect:{}},function(t,i){function s(e,t,i){var s=c[t.type]||{};return null==e?i||!t.def?null:t.def:(e=s.floor?~~e:parseFloat(e),isNaN(e)?t.def:s.mod?(e+s.mod)%s.mod:0>e?0:e>s.max?s.max:e)}function a(e){var s=u(),a=s._rgba=[];return e=e.toLowerCase(),m(l,function(t,n){var o,r=n.re.exec(e),h=r&&n.parse(r),l=n.space||"rgba";return h?(o=s[l](h),s[d[l].cache]=o[d[l].cache],a=s._rgba=o._rgba,!1):i}),a.length?("0,0,0,0"===a.join()&&t.extend(a,o.transparent),s):o[e]}function n(e,t,i){return i=(i+1)%1,1>6*i?e+6*(t-e)*i:1>2*i?t:2>3*i?e+6*(t-e)*(2/3-i):e}var o,r="backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor".split(" "),h=/^([\-+])=\s*(\d+\.?\d*)/,l=[{re:/rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,parse:function(e){return[e[1],e[2],e[3],e[4]]}},{re:/rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,parse:function(e){return[2.55*e[1],2.55*e[2],2.55*e[3],e[4]]}},{re:/#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,parse:function(e){return[parseInt(e[1],16),parseInt(e[2],16),parseInt(e[3],16)]}},{re:/#([a-f0-9])([a-f0-9])([a-f0-9])/,parse:function(e){return[parseInt(e[1]+e[1],16),parseInt(e[2]+e[2],16),parseInt(e[3]+e[3],16)]}},{re:/hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,space:"hsla",parse:function(e){return[e[1],e[2]/100,e[3]/100,e[4]]}}],u=t.Color=function(e,i,s,a){return new t.Color.fn.parse(e,i,s,a)},d={rgba:{props:{red:{idx:0,type:"byte"},green:{idx:1,type:"byte"},blue:{idx:2,type:"byte"}}},hsla:{props:{hue:{idx:0,type:"degrees"},saturation:{idx:1,type:"percent"},lightness:{idx:2,type:"percent"}}}},c={"byte":{floor:!0,max:255},percent:{max:1},degrees:{mod:360,floor:!0}},p=u.support={},f=t("<p>")[0],m=t.each;f.style.cssText="background-color:rgba(1,1,1,.5)",p.rgba=f.style.backgroundColor.indexOf("rgba")>-1,m(d,function(e,t){t.cache="_"+e,t.props.alpha={idx:3,type:"percent",def:1}}),u.fn=t.extend(u.prototype,{parse:function(n,r,h,l){if(n===i)return this._rgba=[null,null,null,null],this;(n.jquery||n.nodeType)&&(n=t(n).css(r),r=i);var c=this,p=t.type(n),f=this._rgba=[];return r!==i&&(n=[n,r,h,l],p="array"),"string"===p?this.parse(a(n)||o._default):"array"===p?(m(d.rgba.props,function(e,t){f[t.idx]=s(n[t.idx],t)}),this):"object"===p?(n instanceof u?m(d,function(e,t){n[t.cache]&&(c[t.cache]=n[t.cache].slice())}):m(d,function(t,i){var a=i.cache;m(i.props,function(e,t){if(!c[a]&&i.to){if("alpha"===e||null==n[e])return;c[a]=i.to(c._rgba)}c[a][t.idx]=s(n[e],t,!0)}),c[a]&&0>e.inArray(null,c[a].slice(0,3))&&(c[a][3]=1,i.from&&(c._rgba=i.from(c[a])))}),this):i},is:function(e){var t=u(e),s=!0,a=this;return m(d,function(e,n){var o,r=t[n.cache];return r&&(o=a[n.cache]||n.to&&n.to(a._rgba)||[],m(n.props,function(e,t){return null!=r[t.idx]?s=r[t.idx]===o[t.idx]:i})),s}),s},_space:function(){var e=[],t=this;return m(d,function(i,s){t[s.cache]&&e.push(i)}),e.pop()},transition:function(e,t){var i=u(e),a=i._space(),n=d[a],o=0===this.alpha()?u("transparent"):this,r=o[n.cache]||n.to(o._rgba),h=r.slice();return i=i[n.cache],m(n.props,function(e,a){var n=a.idx,o=r[n],l=i[n],u=c[a.type]||{};null!==l&&(null===o?h[n]=l:(u.mod&&(l-o>u.mod/2?o+=u.mod:o-l>u.mod/2&&(o-=u.mod)),h[n]=s((l-o)*t+o,a)))}),this[a](h)},blend:function(e){if(1===this._rgba[3])return this;var i=this._rgba.slice(),s=i.pop(),a=u(e)._rgba;return u(t.map(i,function(e,t){return(1-s)*a[t]+s*e}))},toRgbaString:function(){var e="rgba(",i=t.map(this._rgba,function(e,t){return null==e?t>2?1:0:e});return 1===i[3]&&(i.pop(),e="rgb("),e+i.join()+")"},toHslaString:function(){var e="hsla(",i=t.map(this.hsla(),function(e,t){return null==e&&(e=t>2?1:0),t&&3>t&&(e=Math.round(100*e)+"%"),e});return 1===i[3]&&(i.pop(),e="hsl("),e+i.join()+")"},toHexString:function(e){var i=this._rgba.slice(),s=i.pop();return e&&i.push(~~(255*s)),"#"+t.map(i,function(e){return e=(e||0).toString(16),1===e.length?"0"+e:e}).join("")},toString:function(){return 0===this._rgba[3]?"transparent":this.toRgbaString()}}),u.fn.parse.prototype=u.fn,d.hsla.to=function(e){if(null==e[0]||null==e[1]||null==e[2])return[null,null,null,e[3]];var t,i,s=e[0]/255,a=e[1]/255,n=e[2]/255,o=e[3],r=Math.max(s,a,n),h=Math.min(s,a,n),l=r-h,u=r+h,d=.5*u;return t=h===r?0:s===r?60*(a-n)/l+360:a===r?60*(n-s)/l+120:60*(s-a)/l+240,i=0===d||1===d?d:.5>=d?l/u:l/(2-u),[Math.round(t)%360,i,d,null==o?1:o]},d.hsla.from=function(e){if(null==e[0]||null==e[1]||null==e[2])return[null,null,null,e[3]];var t=e[0]/360,i=e[1],s=e[2],a=e[3],o=.5>=s?s*(1+i):s+i-s*i,r=2*s-o;return[Math.round(255*n(r,o,t+1/3)),Math.round(255*n(r,o,t)),Math.round(255*n(r,o,t-1/3)),a]},m(d,function(e,a){var n=a.props,o=a.cache,r=a.to,l=a.from;u.fn[e]=function(e){if(r&&!this[o]&&(this[o]=r(this._rgba)),e===i)return this[o].slice();var a,h=t.type(e),d="array"===h||"object"===h?e:arguments,c=this[o].slice();return m(n,function(e,t){var i=d["object"===h?e:t.idx];null==i&&(i=c[t.idx]),c[t.idx]=s(i,t)}),l?(a=u(l(c)),a[o]=c,a):u(c)},m(n,function(i,s){u.fn[i]||(u.fn[i]=function(a){var n,o=t.type(a),r="alpha"===i?this._hsla?"hsla":"rgba":e,l=this[r](),u=l[s.idx];return"undefined"===o?u:("function"===o&&(a=a.call(this,u),o=t.type(a)),null==a&&s.empty?this:("string"===o&&(n=h.exec(a),n&&(a=u+parseFloat(n[2])*("+"===n[1]?1:-1))),l[s.idx]=a,this[r](l)))})})}),m(r,function(e,i){t.cssHooks[i]={set:function(e,s){var n,o,r="";if("string"!==t.type(s)||(n=a(s))){if(s=u(n||s),!p.rgba&&1!==s._rgba[3]){for(o="backgroundColor"===i?e.parentNode:e;(""===r||"transparent"===r)&&o&&o.style;)try{r=t.css(o,"backgroundColor"),o=o.parentNode}catch(h){}s=s.blend(r&&"transparent"!==r?r:"_default")}s=s.toRgbaString()}try{e.style[i]=s}catch(l){}}},t.fx.step[i]=function(e){e.colorInit||(e.start=u(e.elem,i),e.end=u(e.end),e.colorInit=!0),t.cssHooks[i].set(e.elem,e.start.transition(e.end,e.pos))}}),t.cssHooks.borderColor={expand:function(e){var t={};return m(["Top","Right","Bottom","Left"],function(i,s){t["border"+s+"Color"]=e}),t}},o=t.Color.names={aqua:"#00ffff",black:"#000000",blue:"#0000ff",fuchsia:"#ff00ff",gray:"#808080",green:"#008000",lime:"#00ff00",maroon:"#800000",navy:"#000080",olive:"#808000",purple:"#800080",red:"#ff0000",silver:"#c0c0c0",teal:"#008080",white:"#ffffff",yellow:"#ffff00",transparent:[null,null,null,0],_default:"#ffffff"}}(jQuery),function(){function i(){var t,i,s=this.ownerDocument.defaultView?this.ownerDocument.defaultView.getComputedStyle(this,null):this.currentStyle,a={};if(s&&s.length&&s[0]&&s[s[0]])for(i=s.length;i--;)t=s[i],"string"==typeof s[t]&&(a[e.camelCase(t)]=s[t]);else for(t in s)"string"==typeof s[t]&&(a[t]=s[t]);return a}function s(t,i){var s,a,o={};for(s in i)a=i[s],t[s]!==a&&(n[s]||(e.fx.step[s]||!isNaN(parseFloat(a)))&&(o[s]=a));return o}var a=["add","remove","toggle"],n={border:1,borderBottom:1,borderColor:1,borderLeft:1,borderRight:1,borderTop:1,borderWidth:1,margin:1,padding:1};e.each(["borderLeftStyle","borderRightStyle","borderBottomStyle","borderTopStyle"],function(t,i){e.fx.step[i]=function(e){("none"!==e.end&&!e.setAttr||1===e.pos&&!e.setAttr)&&(jQuery.style(e.elem,i,e.end),e.setAttr=!0)}}),e.effects.animateClass=function(t,n,o,r){var h=e.speed(n,o,r);return this.queue(function(){var n,o=e(this),r=o.attr("class")||"",l=h.children?o.find("*").andSelf():o;l=l.map(function(){var t=e(this);return{el:t,start:i.call(this)}}),n=function(){e.each(a,function(e,i){t[i]&&o[i+"Class"](t[i])})},n(),l=l.map(function(){return this.end=i.call(this.el[0]),this.diff=s(this.start,this.end),this}),o.attr("class",r),l=l.map(function(){var t=this,i=e.Deferred(),s=jQuery.extend({},h,{queue:!1,complete:function(){i.resolve(t)}});return this.el.animate(this.diff,s),i.promise()}),e.when.apply(e,l.get()).done(function(){n(),e.each(arguments,function(){var t=this.el;e.each(this.diff,function(e){t.css(e,"")})}),h.complete.call(o[0])})})},e.fn.extend({_addClass:e.fn.addClass,addClass:function(t,i,s,a){return i?e.effects.animateClass.call(this,{add:t},i,s,a):this._addClass(t)},_removeClass:e.fn.removeClass,removeClass:function(t,i,s,a){return i?e.effects.animateClass.call(this,{remove:t},i,s,a):this._removeClass(t)},_toggleClass:e.fn.toggleClass,toggleClass:function(i,s,a,n,o){return"boolean"==typeof s||s===t?a?e.effects.animateClass.call(this,s?{add:i}:{remove:i},a,n,o):this._toggleClass(i,s):e.effects.animateClass.call(this,{toggle:i},s,a,n)},switchClass:function(t,i,s,a,n){return e.effects.animateClass.call(this,{add:i,remove:t},s,a,n)}})}(),function(){function a(t,i,s,a){return e.isPlainObject(t)&&(i=t,t=t.effect),t={effect:t},null==i&&(i={}),e.isFunction(i)&&(a=i,s=null,i={}),("number"==typeof i||e.fx.speeds[i])&&(a=s,s=i,i={}),e.isFunction(s)&&(a=s,s=null),i&&e.extend(t,i),s=s||i.duration,t.duration=e.fx.off?0:"number"==typeof s?s:s in e.fx.speeds?e.fx.speeds[s]:e.fx.speeds._default,t.complete=a||i.complete,t}function n(t){return!t||"number"==typeof t||e.fx.speeds[t]?!0:"string"!=typeof t||e.effects.effect[t]?!1:i&&e.effects[t]?!1:!0}e.extend(e.effects,{version:"1.9.2",save:function(e,t){for(var i=0;t.length>i;i++)null!==t[i]&&e.data(s+t[i],e[0].style[t[i]])},restore:function(e,i){var a,n;for(n=0;i.length>n;n++)null!==i[n]&&(a=e.data(s+i[n]),a===t&&(a=""),e.css(i[n],a))},setMode:function(e,t){return"toggle"===t&&(t=e.is(":hidden")?"show":"hide"),t},getBaseline:function(e,t){var i,s;switch(e[0]){case"top":i=0;break;case"middle":i=.5;break;case"bottom":i=1;break;default:i=e[0]/t.height}switch(e[1]){case"left":s=0;break;case"center":s=.5;break;case"right":s=1;break;default:s=e[1]/t.width}return{x:s,y:i}},createWrapper:function(t){if(t.parent().is(".ui-effects-wrapper"))return t.parent();var i={width:t.outerWidth(!0),height:t.outerHeight(!0),"float":t.css("float")},s=e("<div></div>").addClass("ui-effects-wrapper").css({fontSize:"100%",background:"transparent",border:"none",margin:0,padding:0}),a={width:t.width(),height:t.height()},n=document.activeElement;try{n.id}catch(o){n=document.body}return t.wrap(s),(t[0]===n||e.contains(t[0],n))&&e(n).focus(),s=t.parent(),"static"===t.css("position")?(s.css({position:"relative"}),t.css({position:"relative"})):(e.extend(i,{position:t.css("position"),zIndex:t.css("z-index")}),e.each(["top","left","bottom","right"],function(e,s){i[s]=t.css(s),isNaN(parseInt(i[s],10))&&(i[s]="auto")}),t.css({position:"relative",top:0,left:0,right:"auto",bottom:"auto"})),t.css(a),s.css(i).show()},removeWrapper:function(t){var i=document.activeElement;return t.parent().is(".ui-effects-wrapper")&&(t.parent().replaceWith(t),(t[0]===i||e.contains(t[0],i))&&e(i).focus()),t},setTransition:function(t,i,s,a){return a=a||{},e.each(i,function(e,i){var n=t.cssUnit(i);n[0]>0&&(a[i]=n[0]*s+n[1])}),a}}),e.fn.extend({effect:function(){function t(t){function i(){e.isFunction(n)&&n.call(a[0]),e.isFunction(t)&&t()}var a=e(this),n=s.complete,o=s.mode;(a.is(":hidden")?"hide"===o:"show"===o)?i():r.call(a[0],s,i)}var s=a.apply(this,arguments),n=s.mode,o=s.queue,r=e.effects.effect[s.effect],h=!r&&i&&e.effects[s.effect];return e.fx.off||!r&&!h?n?this[n](s.duration,s.complete):this.each(function(){s.complete&&s.complete.call(this)}):r?o===!1?this.each(t):this.queue(o||"fx",t):h.call(this,{options:s,duration:s.duration,callback:s.complete,mode:s.mode})},_show:e.fn.show,show:function(e){if(n(e))return this._show.apply(this,arguments);var t=a.apply(this,arguments);return t.mode="show",this.effect.call(this,t)},_hide:e.fn.hide,hide:function(e){if(n(e))return this._hide.apply(this,arguments);var t=a.apply(this,arguments);return t.mode="hide",this.effect.call(this,t)},__toggle:e.fn.toggle,toggle:function(t){if(n(t)||"boolean"==typeof t||e.isFunction(t))return this.__toggle.apply(this,arguments);var i=a.apply(this,arguments);return i.mode="toggle",this.effect.call(this,i)},cssUnit:function(t){var i=this.css(t),s=[];return e.each(["em","px","%","pt"],function(e,t){i.indexOf(t)>0&&(s=[parseFloat(i),t])}),s}})}(),function(){var t={};e.each(["Quad","Cubic","Quart","Quint","Expo"],function(e,i){t[i]=function(t){return Math.pow(t,e+2)}}),e.extend(t,{Sine:function(e){return 1-Math.cos(e*Math.PI/2)},Circ:function(e){return 1-Math.sqrt(1-e*e)},Elastic:function(e){return 0===e||1===e?e:-Math.pow(2,8*(e-1))*Math.sin((80*(e-1)-7.5)*Math.PI/15)},Back:function(e){return e*e*(3*e-2)},Bounce:function(e){for(var t,i=4;((t=Math.pow(2,--i))-1)/11>e;);return 1/Math.pow(4,3-i)-7.5625*Math.pow((3*t-2)/22-e,2)}}),e.each(t,function(t,i){e.easing["easeIn"+t]=i,e.easing["easeOut"+t]=function(e){return 1-i(1-e)},e.easing["easeInOut"+t]=function(e){return.5>e?i(2*e)/2:1-i(-2*e+2)/2}})}()}(jQuery);(function(e){var t=/up|down|vertical/,i=/up|left|vertical|horizontal/;e.effects.effect.blind=function(s,a){var n,o,r,h=e(this),l=["position","top","bottom","left","right","height","width"],u=e.effects.setMode(h,s.mode||"hide"),d=s.direction||"up",c=t.test(d),p=c?"height":"width",f=c?"top":"left",m=i.test(d),g={},v="show"===u;h.parent().is(".ui-effects-wrapper")?e.effects.save(h.parent(),l):e.effects.save(h,l),h.show(),n=e.effects.createWrapper(h).css({overflow:"hidden"}),o=n[p](),r=parseFloat(n.css(f))||0,g[p]=v?o:0,m||(h.css(c?"bottom":"right",0).css(c?"top":"left","auto").css({position:"absolute"}),g[f]=v?r:o+r),v&&(n.css(p,0),m||n.css(f,r+o)),n.animate(g,{duration:s.duration,easing:s.easing,queue:!1,complete:function(){"hide"===u&&h.hide(),e.effects.restore(h,l),e.effects.removeWrapper(h),a()}})}})(jQuery);(function(e){e.effects.effect.bounce=function(t,i){var s,a,n,o=e(this),r=["position","top","bottom","left","right","height","width"],h=e.effects.setMode(o,t.mode||"effect"),l="hide"===h,u="show"===h,d=t.direction||"up",c=t.distance,p=t.times||5,f=2*p+(u||l?1:0),m=t.duration/f,g=t.easing,v="up"===d||"down"===d?"top":"left",y="up"===d||"left"===d,b=o.queue(),_=b.length;for((u||l)&&r.push("opacity"),e.effects.save(o,r),o.show(),e.effects.createWrapper(o),c||(c=o["top"===v?"outerHeight":"outerWidth"]()/3),u&&(n={opacity:1},n[v]=0,o.css("opacity",0).css(v,y?2*-c:2*c).animate(n,m,g)),l&&(c/=Math.pow(2,p-1)),n={},n[v]=0,s=0;p>s;s++)a={},a[v]=(y?"-=":"+=")+c,o.animate(a,m,g).animate(n,m,g),c=l?2*c:c/2;l&&(a={opacity:0},a[v]=(y?"-=":"+=")+c,o.animate(a,m,g)),o.queue(function(){l&&o.hide(),e.effects.restore(o,r),e.effects.removeWrapper(o),i()}),_>1&&b.splice.apply(b,[1,0].concat(b.splice(_,f+1))),o.dequeue()}})(jQuery);(function(e){e.effects.effect.clip=function(t,i){var s,a,n,o=e(this),r=["position","top","bottom","left","right","height","width"],h=e.effects.setMode(o,t.mode||"hide"),l="show"===h,u=t.direction||"vertical",d="vertical"===u,c=d?"height":"width",p=d?"top":"left",f={};e.effects.save(o,r),o.show(),s=e.effects.createWrapper(o).css({overflow:"hidden"}),a="IMG"===o[0].tagName?s:o,n=a[c](),l&&(a.css(c,0),a.css(p,n/2)),f[c]=l?n:0,f[p]=l?0:n/2,a.animate(f,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){l||o.hide(),e.effects.restore(o,r),e.effects.removeWrapper(o),i()}})}})(jQuery);(function(e){e.effects.effect.drop=function(t,i){var s,a=e(this),n=["position","top","bottom","left","right","opacity","height","width"],o=e.effects.setMode(a,t.mode||"hide"),r="show"===o,h=t.direction||"left",l="up"===h||"down"===h?"top":"left",u="up"===h||"left"===h?"pos":"neg",d={opacity:r?1:0};e.effects.save(a,n),a.show(),e.effects.createWrapper(a),s=t.distance||a["top"===l?"outerHeight":"outerWidth"](!0)/2,r&&a.css("opacity",0).css(l,"pos"===u?-s:s),d[l]=(r?"pos"===u?"+=":"-=":"pos"===u?"-=":"+=")+s,a.animate(d,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){"hide"===o&&a.hide(),e.effects.restore(a,n),e.effects.removeWrapper(a),i()}})}})(jQuery);(function(e){e.effects.effect.explode=function(t,i){function s(){b.push(this),b.length===d*c&&a()}function a(){p.css({visibility:"visible"}),e(b).remove(),m||p.hide(),i()}var n,o,r,h,l,u,d=t.pieces?Math.round(Math.sqrt(t.pieces)):3,c=d,p=e(this),f=e.effects.setMode(p,t.mode||"hide"),m="show"===f,g=p.show().css("visibility","hidden").offset(),v=Math.ceil(p.outerWidth()/c),y=Math.ceil(p.outerHeight()/d),b=[];for(n=0;d>n;n++)for(h=g.top+n*y,u=n-(d-1)/2,o=0;c>o;o++)r=g.left+o*v,l=o-(c-1)/2,p.clone().appendTo("body").wrap("<div></div>").css({position:"absolute",visibility:"visible",left:-o*v,top:-n*y}).parent().addClass("ui-effects-explode").css({position:"absolute",overflow:"hidden",width:v,height:y,left:r+(m?l*v:0),top:h+(m?u*y:0),opacity:m?0:1}).animate({left:r+(m?0:l*v),top:h+(m?0:u*y),opacity:m?1:0},t.duration||500,t.easing,s)}})(jQuery);(function(e){e.effects.effect.fade=function(t,i){var s=e(this),a=e.effects.setMode(s,t.mode||"toggle");s.animate({opacity:a},{queue:!1,duration:t.duration,easing:t.easing,complete:i})}})(jQuery);(function(e){e.effects.effect.fold=function(t,i){var s,a,n=e(this),o=["position","top","bottom","left","right","height","width"],r=e.effects.setMode(n,t.mode||"hide"),h="show"===r,l="hide"===r,u=t.size||15,d=/([0-9]+)%/.exec(u),c=!!t.horizFirst,p=h!==c,f=p?["width","height"]:["height","width"],m=t.duration/2,g={},v={};e.effects.save(n,o),n.show(),s=e.effects.createWrapper(n).css({overflow:"hidden"}),a=p?[s.width(),s.height()]:[s.height(),s.width()],d&&(u=parseInt(d[1],10)/100*a[l?0:1]),h&&s.css(c?{height:0,width:u}:{height:u,width:0}),g[f[0]]=h?a[0]:u,v[f[1]]=h?a[1]:0,s.animate(g,m,t.easing).animate(v,m,t.easing,function(){l&&n.hide(),e.effects.restore(n,o),e.effects.removeWrapper(n),i()})}})(jQuery);(function(e){e.effects.effect.highlight=function(t,i){var s=e(this),a=["backgroundImage","backgroundColor","opacity"],n=e.effects.setMode(s,t.mode||"show"),o={backgroundColor:s.css("backgroundColor")};"hide"===n&&(o.opacity=0),e.effects.save(s,a),s.show().css({backgroundImage:"none",backgroundColor:t.color||"#ffff99"}).animate(o,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){"hide"===n&&s.hide(),e.effects.restore(s,a),i()}})}})(jQuery);(function(e){e.effects.effect.pulsate=function(t,i){var s,a=e(this),n=e.effects.setMode(a,t.mode||"show"),o="show"===n,r="hide"===n,h=o||"hide"===n,l=2*(t.times||5)+(h?1:0),u=t.duration/l,d=0,c=a.queue(),p=c.length;for((o||!a.is(":visible"))&&(a.css("opacity",0).show(),d=1),s=1;l>s;s++)a.animate({opacity:d},u,t.easing),d=1-d;a.animate({opacity:d},u,t.easing),a.queue(function(){r&&a.hide(),i()}),p>1&&c.splice.apply(c,[1,0].concat(c.splice(p,l+1))),a.dequeue()}})(jQuery);(function(e){e.effects.effect.puff=function(t,i){var s=e(this),a=e.effects.setMode(s,t.mode||"hide"),n="hide"===a,o=parseInt(t.percent,10)||150,r=o/100,h={height:s.height(),width:s.width(),outerHeight:s.outerHeight(),outerWidth:s.outerWidth()};e.extend(t,{effect:"scale",queue:!1,fade:!0,mode:a,complete:i,percent:n?o:100,from:n?h:{height:h.height*r,width:h.width*r,outerHeight:h.outerHeight*r,outerWidth:h.outerWidth*r}}),s.effect(t)},e.effects.effect.scale=function(t,i){var s=e(this),a=e.extend(!0,{},t),n=e.effects.setMode(s,t.mode||"effect"),o=parseInt(t.percent,10)||(0===parseInt(t.percent,10)?0:"hide"===n?0:100),r=t.direction||"both",h=t.origin,l={height:s.height(),width:s.width(),outerHeight:s.outerHeight(),outerWidth:s.outerWidth()},u={y:"horizontal"!==r?o/100:1,x:"vertical"!==r?o/100:1};a.effect="size",a.queue=!1,a.complete=i,"effect"!==n&&(a.origin=h||["middle","center"],a.restore=!0),a.from=t.from||("show"===n?{height:0,width:0,outerHeight:0,outerWidth:0}:l),a.to={height:l.height*u.y,width:l.width*u.x,outerHeight:l.outerHeight*u.y,outerWidth:l.outerWidth*u.x},a.fade&&("show"===n&&(a.from.opacity=0,a.to.opacity=1),"hide"===n&&(a.from.opacity=1,a.to.opacity=0)),s.effect(a)},e.effects.effect.size=function(t,i){var s,a,n,o=e(this),r=["position","top","bottom","left","right","width","height","overflow","opacity"],h=["position","top","bottom","left","right","overflow","opacity"],l=["width","height","overflow"],u=["fontSize"],d=["borderTopWidth","borderBottomWidth","paddingTop","paddingBottom"],c=["borderLeftWidth","borderRightWidth","paddingLeft","paddingRight"],p=e.effects.setMode(o,t.mode||"effect"),f=t.restore||"effect"!==p,m=t.scale||"both",g=t.origin||["middle","center"],v=o.css("position"),y=f?r:h,b={height:0,width:0,outerHeight:0,outerWidth:0};"show"===p&&o.show(),s={height:o.height(),width:o.width(),outerHeight:o.outerHeight(),outerWidth:o.outerWidth()},"toggle"===t.mode&&"show"===p?(o.from=t.to||b,o.to=t.from||s):(o.from=t.from||("show"===p?b:s),o.to=t.to||("hide"===p?b:s)),n={from:{y:o.from.height/s.height,x:o.from.width/s.width},to:{y:o.to.height/s.height,x:o.to.width/s.width}},("box"===m||"both"===m)&&(n.from.y!==n.to.y&&(y=y.concat(d),o.from=e.effects.setTransition(o,d,n.from.y,o.from),o.to=e.effects.setTransition(o,d,n.to.y,o.to)),n.from.x!==n.to.x&&(y=y.concat(c),o.from=e.effects.setTransition(o,c,n.from.x,o.from),o.to=e.effects.setTransition(o,c,n.to.x,o.to))),("content"===m||"both"===m)&&n.from.y!==n.to.y&&(y=y.concat(u).concat(l),o.from=e.effects.setTransition(o,u,n.from.y,o.from),o.to=e.effects.setTransition(o,u,n.to.y,o.to)),e.effects.save(o,y),o.show(),e.effects.createWrapper(o),o.css("overflow","hidden").css(o.from),g&&(a=e.effects.getBaseline(g,s),o.from.top=(s.outerHeight-o.outerHeight())*a.y,o.from.left=(s.outerWidth-o.outerWidth())*a.x,o.to.top=(s.outerHeight-o.to.outerHeight)*a.y,o.to.left=(s.outerWidth-o.to.outerWidth)*a.x),o.css(o.from),("content"===m||"both"===m)&&(d=d.concat(["marginTop","marginBottom"]).concat(u),c=c.concat(["marginLeft","marginRight"]),l=r.concat(d).concat(c),o.find("*[width]").each(function(){var i=e(this),s={height:i.height(),width:i.width(),outerHeight:i.outerHeight(),outerWidth:i.outerWidth()};f&&e.effects.save(i,l),i.from={height:s.height*n.from.y,width:s.width*n.from.x,outerHeight:s.outerHeight*n.from.y,outerWidth:s.outerWidth*n.from.x},i.to={height:s.height*n.to.y,width:s.width*n.to.x,outerHeight:s.height*n.to.y,outerWidth:s.width*n.to.x},n.from.y!==n.to.y&&(i.from=e.effects.setTransition(i,d,n.from.y,i.from),i.to=e.effects.setTransition(i,d,n.to.y,i.to)),n.from.x!==n.to.x&&(i.from=e.effects.setTransition(i,c,n.from.x,i.from),i.to=e.effects.setTransition(i,c,n.to.x,i.to)),i.css(i.from),i.animate(i.to,t.duration,t.easing,function(){f&&e.effects.restore(i,l)})})),o.animate(o.to,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){0===o.to.opacity&&o.css("opacity",o.from.opacity),"hide"===p&&o.hide(),e.effects.restore(o,y),f||("static"===v?o.css({position:"relative",top:o.to.top,left:o.to.left}):e.each(["top","left"],function(e,t){o.css(t,function(t,i){var s=parseInt(i,10),a=e?o.to.left:o.to.top;return"auto"===i?a+"px":s+a+"px"})})),e.effects.removeWrapper(o),i()}})}})(jQuery);(function(e){e.effects.effect.shake=function(t,i){var s,a=e(this),n=["position","top","bottom","left","right","height","width"],o=e.effects.setMode(a,t.mode||"effect"),r=t.direction||"left",h=t.distance||20,l=t.times||3,u=2*l+1,d=Math.round(t.duration/u),c="up"===r||"down"===r?"top":"left",p="up"===r||"left"===r,f={},m={},g={},v=a.queue(),y=v.length;for(e.effects.save(a,n),a.show(),e.effects.createWrapper(a),f[c]=(p?"-=":"+=")+h,m[c]=(p?"+=":"-=")+2*h,g[c]=(p?"-=":"+=")+2*h,a.animate(f,d,t.easing),s=1;l>s;s++)a.animate(m,d,t.easing).animate(g,d,t.easing);a.animate(m,d,t.easing).animate(f,d/2,t.easing).queue(function(){"hide"===o&&a.hide(),e.effects.restore(a,n),e.effects.removeWrapper(a),i()}),y>1&&v.splice.apply(v,[1,0].concat(v.splice(y,u+1))),a.dequeue()}})(jQuery);(function(e){e.effects.effect.slide=function(t,i){var s,a=e(this),n=["position","top","bottom","left","right","width","height"],o=e.effects.setMode(a,t.mode||"show"),r="show"===o,h=t.direction||"left",l="up"===h||"down"===h?"top":"left",u="up"===h||"left"===h,d={};e.effects.save(a,n),a.show(),s=t.distance||a["top"===l?"outerHeight":"outerWidth"](!0),e.effects.createWrapper(a).css({overflow:"hidden"}),r&&a.css(l,u?isNaN(s)?"-"+s:-s:s),d[l]=(r?u?"+=":"-=":u?"-=":"+=")+s,a.animate(d,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){"hide"===o&&a.hide(),e.effects.restore(a,n),e.effects.removeWrapper(a),i()}})}})(jQuery);(function(e){e.effects.effect.transfer=function(t,i){var s=e(this),a=e(t.to),n="fixed"===a.css("position"),o=e("body"),r=n?o.scrollTop():0,h=n?o.scrollLeft():0,l=a.offset(),u={top:l.top-r,left:l.left-h,height:a.innerHeight(),width:a.innerWidth()},d=s.offset(),c=e('<div class="ui-effects-transfer"></div>').appendTo(document.body).addClass(t.className).css({top:d.top-r,left:d.left-h,height:s.innerHeight(),width:s.innerWidth(),position:n?"fixed":"absolute"}).animate(u,t.duration,t.easing,function(){c.remove(),i()})}})(jQuery);/*!
 * jQuery Cookie Plugin
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2011, Klaus Hartl
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/GPL-2.0
 */
(function($) {
    $.cookie = function(key, value, options) {

        // key and at least value given, set cookie...
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
            options = $.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path    ? '; path=' + options.path : '',
                options.domain  ? '; domain=' + options.domain : '',
                options.secure  ? '; secure' : ''
            ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var decode = options.raw ? function(s) { return s; } : decodeURIComponent;

        var pairs = document.cookie.split('; ');
        for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
            if (decode(pair[0]) === key) return decode(pair[1] || ''); // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
        }
        return null;
    };
})(jQuery);
/*
 Sticky-kit v1.0.4 | WTFPL | Leaf Corcoran 2014 | http://leafo.net
*/
(function(){var b,m;b=this.jQuery;m=b(window);b.fn.stick_in_parent=function(e){var u,n,f,s,B,l,C;null==e&&(e={});s=e.sticky_class;u=e.inner_scrolling;f=e.parent;n=e.offset_top;null==n&&(n=0);null==f&&(f=void 0);null==u&&(u=!0);null==s&&(s="is_stuck");B=function(a,e,l,v,y,p,t){var q,z,k,w,c,d,A,x,g,h;if(!a.data("sticky_kit")){a.data("sticky_kit",!0);d=a.parent();null!=f&&(d=d.closest(f));if(!d.length)throw"failed to find stick parent";q=k=!1;g=b("<div />");g.css("position",a.css("position"));A=function(){var c,
b;c=parseInt(d.css("border-top-width"),10);b=parseInt(d.css("padding-top"),10);e=parseInt(d.css("padding-bottom"),10);l=d.offset().top+c+b;v=d.height();c=k?(k=!1,q=!1,a.insertAfter(g).css({position:"",top:"",width:"",bottom:""}),g.detach(),!0):void 0;y=a.offset().top-parseInt(a.css("margin-top"),10)-n;p=a.outerHeight(!0);t=a.css("float");g.css({width:a.outerWidth(!0),height:p,display:a.css("display"),"vertical-align":a.css("vertical-align"),"float":t});if(c)return h()};A();if(p!==v)return w=void 0,
c=n,h=function(){var b,h,r,f;r=m.scrollTop();null!=w&&(h=r-w);w=r;k?(f=r+p+c>v+l,q&&!f&&(q=!1,a.css({position:"fixed",bottom:"",top:c}).trigger("sticky_kit:unbottom")),r<y&&(k=!1,c=n,"left"!==t&&"right"!==t||a.insertAfter(g),g.detach(),b={position:"",width:"",top:""},a.css(b).removeClass(s).trigger("sticky_kit:unstick")),u&&(b=m.height(),p>b&&!q&&(c-=h,c=Math.max(b-p,c),c=Math.min(n,c),k&&a.css({top:c+"px"})))):r>y&&(k=!0,b={position:"fixed",top:c},b.width="border-box"===a.css("box-sizing")?a.outerWidth()+
"px":a.width()+"px",a.css(b).addClass(s).after(g),"left"!==t&&"right"!==t||g.append(a),a.trigger("sticky_kit:stick"));if(k&&(null==f&&(f=r+p+c>v+l),!q&&f))return q=!0,"static"===d.css("position")&&d.css({position:"relative"}),a.css({position:"absolute",bottom:e,top:"auto"}).trigger("sticky_kit:bottom")},x=function(){A();return h()},z=function(){m.off("scroll",h);b(document.body).off("sticky_kit:recalc",x);a.off("sticky_kit:detach",z);a.removeData("sticky_kit");a.css({position:"",bottom:"",top:""});
d.position("position","");if(k)return a.insertAfter(g).removeClass(s),g.remove()},m.on("touchmove",h),m.on("scroll",h),m.on("resize",x),b(document.body).on("sticky_kit:recalc",x),a.on("sticky_kit:detach",z),setTimeout(h,0)}};l=0;for(C=this.length;l<C;l++)e=this[l],B(b(e));return this}}).call(this);function popup(seite,breite,hoehe,scroll,rsize) {
	sbars = (scroll==1)? "yes" : "no";
	rsize = (rsize==1)? "yes" : "no";
	if(seite.substr(0,1) == '/') seite = spnetENV_SERVER + seite;
	var win_name = breite+hoehe;
	var win_attr = "menubar=no,location=no,directories=no,toolbar=no,screenX=0,screenY=0";
	win_attr += ",width=" + breite + ",height=" + hoehe + ",scrollbars=" + sbars + ",resizable=" + rsize;
	showWindow = open(seite,win_name,win_attr);
	showWindow.focus();
}


/////////////////////////////////////////////////////////

var Mmonline = Mmonline || {};

(function() {
	Mmonline.UI = {

	// ......................................................

		articleToolbarClose: function() {
			$('.article-toolbar').find('.article-toolbar-top').removeClass('open').addClass('close');
			$('.article-toolbar').find('.social, .service').hide('300');
		},
			
		articleToolbar: function() {
			
			if ( $('.article-toolbar').length ) {
				var $bar = $('.article-toolbar');
				var $top = $bar.find('.article-toolbar-top');

				// Top-Link in toolbar
				$bar.find('.lever').click(function(e) {
					$('html, body').animate({ scrollTop: '0px' }, 600);
					return false;
				});

				// Toolbar open/close
				$top.click(function(e) {
					e.preventDefault();
					if ( $top.hasClass('open') ) {
						$bar.find('.social, .service').hide('300');
						$top.removeClass('open').addClass('close');
						$(this).attr('title', 'aufklappen');
					} else if ( $top.hasClass('close') ) {
						$bar.find('.social, .service').show('300');
						$top.removeClass('close').addClass('open');
						$(this).attr('title', 'einklappen');				
					}
				});

				// "Plus"-symbol opens more social-links
				$bar.find('.more').hover(function(){
					var $moreSocial = $(this).find('.more-social');
					var count = $(this).find('.more-social li').length;
					if ( count > 0 ) {
						var getWidth = $moreSocial.find('li:first').width() + 5;
						$moreSocial.css({
							width: (getWidth * count)
						}).show();
					}
				}, function() {
					var $moreSocial = $(this).find('.more-social');
						$moreSocial.css({
							width: '0'
						}).hide();
				});

				// keeps toolbar fixed inside content area
				// uses jquery-sticky-kit.min.js
				// css(margin-top, 0) needed for IE8 :-(
				$bar.css("margin-top", "0").stick_in_parent({
					parent: '#content-main',
					offset_top: 70
				});
			}
		},
		
	// ......................................................

		accordionBox: function() {
			
			if ( $('.accordion').length ) {
				// uses jQueryUI
				$('.accordion').accordion({
					collapsible: true,
					header: ".acc-section-title",
					heightStyle: "content",
					icons: false
				});
			}
		},

	// ......................................................

		tabsBox: function() {
			
			if ( $('.tabs').length ) {
				// uses jQueryUI
				$('.tabs-container').tabs({
					heightStyle: "content"
				});
			}
		},

	// ......................................................

		mainNavSticky: function() {
			
			// uses jquery-sticky-kit.min.js
			// css(margin-top, 0) needed for IE8 :-(
			$('.main-nav').css("margin-top", "0").stick_in_parent({
				parent: '#wrapper',
				sticky_class: 'scroll-active',
				bottoming: false
			});
		},
		
	// ......................................................

		headerSearch: function() {
			if ( $('#header').length ) { var $header = $('#header'); }
			if ( $('#header-compact').length ) { var $header = $('#header-compact'); }

			if ( $header && $header.find('.search').length ) {
				var $showSearch = $header.find('.search');
				var $searchBox = $header.find('.search-overlay');
				var $toptopics = $header.find('.top-topics');
				
				if ( $toptopics.length ) {
					// copy top-topics into search-overlay
					$('.cpy-breadcrumb').html( $toptopics.clone() );
				}

				// show search-overlay on click
				 $showSearch.click(function(e){
					e.preventDefault();
					if ( $searchBox.is(':visible') ) {
						$searchBox.slideUp('200');
						$(this).find('a').removeClass('is-active');
					} else {
						$(this).find('a').addClass('is-active');
						$searchBox.slideDown('200');
					}
				});
			}
			// show subnav > hide searchbox
			$('.caret').hover(function(){
				$searchBox.hide();
				$showSearch.find('a').removeClass('is-active');
			});
		},
		
	// ......................................................
		
		// Compact Header, e.g. on BiGa
		headerCompact: function() {
			$('#header-compact .mobile-button').click( function() {
				$(this).closest('.main-nav').find('.mobile-menu').toggleClass('mobile-menu-shown');
			});
			// close menu if user clicks anywhere else on the page
			$('body').bind( 'click', function(e) {
				if ( ! $(e.target).is('#header-compact .mobile-button, #header-compact .mobile-menu' )) {
					$('#header-compact').find('.mobile-menu').removeClass('mobile-menu-show');
				}
			});
		},
		
	// ......................................................
		
		// toggle image-overview on BiGa
		showBigaOverview: function() {
			var gallery_sprite_status = 0;
			
			$(".sprite-overview").click(function() {
				if (gallery_sprite_status) {
					$(".gallery-overview").slideUp(300);
					gallery_sprite_status = 0;
				}
				else {
					$(".gallery-overview").slideDown(100);
					gallery_sprite_status = 1;
				}
			});
		},

	// ......................................................

		// Button Share article on Facebook / shows number of shares
		shareSocial: function() {
			$.each($('.api .share-fb'), function(i, v){
	            var url = $(this).attr('data-url');
	            var href= $(this).attr('href');
	            var $btn = $(this);
//	            ------- span z.Zt. unsichtbar in share_page.jsp L20 -------
//	            if ( url !== undefined ) {
//	                $.getJSON("http://graph.facebook.com/?ids="+url+"&callback=?", function(data){
//	                	if (data[url] && data[url].shares !== undefined) {
//	                        $btn.next('.cnt').text(data[url].shares);
//	                    } else {
//	                    	$btn.next('.cnt').text( "0" );
//	                    }
//	                });
//	            }
	            $btn.click(function(e){
	            	e.preventDefault();
	            	popup(href, 650, 350, false, false);
	            });
	        });

			// Button Share article on Twitter / shows number of shares
			$.each($('.api .share-tw'), function(i, v){
                var url = $(this).attr('data-url');
                var $btn= $(this);
                var tweetUrl = 'https://twitter.com/intent/tweet?original_referer=' + $(location).attr('href') + '&text=' + url;
//                if (url !== undefined) {
//                	$.getJSON("http://urls.api.twitter.com/1/urls/count.json?url="+url+"&callback=?",
//                		function (data) {
//                            if (data && data.count !== undefined) {
//                            	$btn.next('.cnt').text(data.count);
//                            	$btn.attr('href', tweetUrl);
//                            }
//                    	}
//                	);
//                }
                $btn.click(function(e){
	            	e.preventDefault();
	            	popup(tweetUrl, 650, 350, false, false);
	            });
            });
			
			// twitter and fb: do only process once:
			$('.api').removeClass('api');
			
			// Button Share article on google+ / native google-code >> https://developers.google.com/+/web/+1button/
			/*
			(function() {
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			*/
			
			// article-toolbar > social-Buttons
			// facebook, twitter, g+
			if ( $('.article-toolbar').find('.js-popup').length ) {
				$('.article-toolbar').find('.js-popup a').click(function(e){
					var url = $(this).attr('href');
					e.preventDefault();
	            	popup(url, 650, 350, false, false);
				});
			}
		},

		// ......................................................
		// focus / blur default Text on input-fields
		inputDefaultValues: function() {
			$('.js-defaultval').on('focus', function() {
				if ( !$(this).data('defaultText') ) $(this).data('defaultText', $(this).val());
				if ($(this).val()==$(this).data('defaultText')) $(this).val('').removeClass('hasdefault');
			});
		    $('.js-defaultval').on('blur', function() {
		        if ( $(this).val()=='' ) $(this).val($(this).data('defaultText')).addClass('hasdefault');
		    });
		},

		// ......................................................
		// foto/video mumelei tabs
		showPhotoVideoTabs: function() {
			var $mumeleiNav = $('.photovideo-nav');

			if ( $mumeleiNav && $mumeleiNav.length ) {
				$mumeleiNav.find('.show-photos').click(function(e) {
					e.preventDefault();
					$('.video-box').hide();
					$('.photo-box').show();
					if ( ! $(this).hasClass('is-active') ) { $(this).addClass('is-active'); }
					$('.show-videos').removeClass('is-active');
				});
				$mumeleiNav.find('.show-videos').click(function(e) {
					e.preventDefault();
					$('.photo-box').hide();
					$('.video-box').show();
					if ( ! $(this).hasClass('is-active') ) { $(this).addClass('is-active'); }
					$('.show-photos').removeClass('is-active');
				});
			}
		},

		// ......................................................
		// nicer scrollbar, e.g. video-recommendations, "Schwerpunktthemen > Alle Themen"
		customScrollbar: function() {
			if ( $('.custom-scrollbar').length ) {
				$('.custom-scrollbar').delay(5000).jScrollPane({ showArrows: true, horizontalGutter: 0 });
			}
			
			// when scrollbar placed inside jQueryUI Tabs >> initialize on tab-click
			$('.ui-tabs-anchor').click(function() {
				if ( $('.custom-scrollbar').length ) {
					$('.custom-scrollbar').delay(5000).jScrollPane({ showArrows: true, horizontalGutter: 0 });
				}
			});
		},

		// ......................................................
		// better link-usability on list-items > make complete <li> clickable
		enlargeClickArea: function() {
			if ( $('.js-click').length ) {
				$('.js-click').click(function(){
					var url = $(this).find('a:first').attr('href');
					if (url) { window.location.href = url; }
				});
			}
		},

		// ......................................................
		
		// find first asset in column-small
		findFirstColAsset: function() {
			$('.column-small .asset-box').first().addClass('is-first');
		},

		
		// ......................................................
		
		// "manager magazin folgen" > toggle newsletter-form
		mmoFollowToggleNewsletter: function() {
			if ( $('.follow-mmo').length ) {
				$('.socialicons .ml').click(function(e){
					e.preventDefault();
					$(this).toggleClass('is-active');
					$(this).parent().next('.nl-form-box').toggle();
				});
			}
		},

		// ......................................................
		
		// /magazin > Inhaltsverzeichnis > toggle hidden content
		toggleHiddenEpubContent: function() {
			$('.js-show-epub-contents-hidden').click(function(e){
				e.preventDefault();
				$('.epub-contents-hidden').toggle();
				
				// tell sticky main-nav new page-size
				$(document.body).trigger("sticky_kit:recalc");
			});
		},

		// ......................................................

		// Table-rows
		oddEvenTables: function() {
			if ( ! $("tr:even").hasClass('even') ) {
				$("tr:even").addClass('even');
			}
		},
		
		// ......................................................

		// ugly IE-Bug: hover on (google+)iframe in social overlay kills focus on overlay
		
		fixIEiFrameHover: function(){
		    if( $.browser.msie  || !!navigator.userAgent.match(/Trident\/7\./) ){
		    	$('.btn-share iframe').live('mouseenter',function() {
		            $(this).parents('.tooltip-container, .js-click').addClass('hover');
		        });
		    	$('.btn-share iframe').live('mouseout',function() {
		    		$(this).parents('.tooltip-container, .js-click').removeClass('hover');
		    	});
		    }
		},

		// ......................................................

		// move hidden author-overlay to correct position
		
		changeAuthorOverlayDOM: function() {
			$('.js-author-lines .js-tooltip-insert').each(function(){
				var boxID = $(this).attr('id');
				// $('.author-name, .author').find('[data-authorid='+ boxID +']').append( $(this) );
				$('.js-author-insert').find('[data-authorid='+ boxID +']').append( $(this) );
			});
			$('.js-tooltip-insert').removeClass('js-tooltip-insert');
			$('.js-author-insert').removeClass('js-author-insert');
		},
		
		
		// ......................................................
		
		// Anchor-Link on comments-bubble needs some offset because of sticky search-bar
		
		blogAnchor: function() {
			$('a[href^="#new-blog-comment"]').on('click', function(e) {
				e.preventDefault();
				var pos = $('.article-comments-box').position();
				$('html, body').animate({ scrollTop: ( pos.top + 100 ) + 'px' }, 600);
			});
		},

		// ......................................................
		
		// special html-asset > toggle image-box
		
		toggleImageAsset: function() {
			$('.toggle-image li').first().addClass('is-active');
			
			$('.toggle-image li').hover( function(){
				$(this).parents().find('li').removeClass('is-active');
				$(this).addClass('is-active');
			});
		},
		

		// ......................................................
		
		// Top-Link (deprecated, e.g. in "Datenschutz")
		
		oldTopLink: function() {
			$(document).find('.mmTop').click(function(e) {
				$('html, body').animate({ scrollTop: '0px' }, 600);
				return false;
			});
		},

		// ......................................................
		
		// wenn article-meta + author mehrzeilig > border entfernen
		
		noBorderArticleInfo: function() {
			$.each( $('.article-info.has-author'), function(i, v){
				var pos = $(this).position();
				if ( pos.left < 10 ) {
					$(this).css({
						'border': 'none',
						'padding-left': '0'
					});
				}
			});
		},
		
		
	// ......................................................
	// ......................................................
		init : function() {
			this.articleToolbar();
			this.accordionBox();
			this.tabsBox();
			this.mainNavSticky();
			this.headerSearch();
			this.headerCompact();
			this.showBigaOverview();
			this.shareSocial();
			this.inputDefaultValues();
			this.showPhotoVideoTabs();
			this.customScrollbar();
			this.enlargeClickArea();
			this.findFirstColAsset();
			this.mmoFollowToggleNewsletter();
			this.toggleHiddenEpubContent();
			this.oddEvenTables();
			this.fixIEiFrameHover();
			this.changeAuthorOverlayDOM();
			this.blogAnchor();
			this.toggleImageAsset();
			this.oldTopLink();
			this.noBorderArticleInfo();
		}
	
	};
})();  // function


$(document).ready(function() {
	Mmonline.UI.init();	
});
function mmBlogLoginCheck(fromElement) {
	//console.log("BogLogin: Check");
	if (mmMSisLoggedIn()) { 
		mmBlogShowMessage('msg',mmoBlogCommentsPhrase.pruefelogin);
		var loggedIn = false;
   	 	mmAjax("/meinmm/forum/checkalias.json",null,function(response) {
   			if (response.ajaxSuccess) {
   				if (response.data.statuscode == 0) {
   	   	 			mmBlogOpenForm(fromElement, response.data)
   	   	 		} else if (response.data.statuscode == 1) {
   	   	 			mmBlogShowMessage('err', "Die Sitzung ist abgelaufen, bitte loggen Sie sich erneut ein.");
   	   	 			mmBlogLogin(fromElement);
   	   	 		} else if (response.data.statuscode == 2 || response.data.statuscode == 3) {
   	   	 			mmBlogShowMessage('err', response.data.message);
   	   	 			mmBlogLogin(fromElement);
   	   	 		} else {
   	   	 			mmBlogShowMessage('err', 'Fehler - Statuscode ' + response.data.statuscode + ' - '+response.data.message);
   	   	 		}
   			} else {
   	 			mmBlogShowMessage('err', 'Ajax-Fehler: ' + response.errormessage);
   			}
   		});   		
	} 
	else { 
		mmBlogLogin(fromElement);
	}
	mmBlogShowMessage('msg', '');
	return false;
}

function mmBlogLogin(fromElement) {
	$(document).unbind("mmLoggedIn");
	$(document).bind('mmLoggedIn', function(){fromElement.click();});
	$(document).bind('mmLoggedIn', mmEventLoggedIn);
	mmMSLogin(fromElement,mmStripHashParams(window.location.href)+'#new-blog-comment',null, mmoBlogCommentsPhrase.language);
}

function mmBlogOpenForm(fromElement, forumresponse) {
	$(".js-login-check").unbind('click');
	$("#blog-comment-submit").bind('click', function(event){mmBlogFormSubmit($("#js-article-comments-form form"));});
	$("#js-article-comments-form form").unbind('submit').bind('submit', function(event){mmBlogFormSubmit($("#js-article-comments-form form"));});
	$("#js-default-message").hide();

	if (!forumresponse.isForumuser) {
		mmBlogShowMessage('msg', mmoBlogCommentsPhrase.ersterbeitrag);
		$(".blog-comment-alias").show();
		$(".blog-comment-alias input").val(forumresponse.suggestedAlias);
	} else {
		mmBlogShowMessage('msg', mmoBlogCommentsPhrase.willkommen + ' ' + forumresponse.forumAlias);
	}
	fromElement.focus();
	fromElement.click();
}

function mmBlogFormSubmit(form) {
	if (mmMSisLoggedIn()) {
		// Submit deaktivieren
		$("#js-article-comments-form form").unbind('submit');
		$("#blog-comment-submit").unbind('click');
		$("#blog-comment-submit").addClass('mmInaktiv');

		mmBlogShowMessage('msg', mmoBlogCommentsPhrase.sendebeitrag);
		var formdata = $(form).serialize();	
		mmAjax("/meinmm/forum/postblogcomment.json",formdata,function(response) {
			if (response.ajaxSuccess) {
    	 		if (response.data.statuscode == 0) {
            	 	mmBlogShowMessage('ok', mmoBlogCommentsPhrase.sendebeitragok);
            		$("#js-article-comments-form form").hide();
            		$("#js-default-message").hide();
    	 		} else if (response.data.statuscode == 2 || response.data.statuscode == 3) {
    	 			mmBlogShowMessage('err', mmoBlogCommentsPhrase.loginexpired);
    	 			$("#js-default-message").show();    	 			
    	 			mmBlogLogin($("#blog-comment-submit"));
    	 		} else {
    	 			mmBlogShowMessage('err', response.data.message);
    	 			for (nr in response.data.errors) {
    	 				mmBlogShowMessage('err', response.data.errors[nr].message);
    	 			}
    	 		}
			} else {
 				mmBlogShowMessage('err', 'Ajax-Fehler: ' + response.error + ' - ' + response.text);
			}
		});
	} else {
		mmBlogShowMessage('err', mmoBlogCommentsPhrase.loginexpired);
		$("#js-default-message").show();		
		mmBlogLogin($("#blog-comment-submit"));
	}
	return false;
}

function mmBlogShowMessage(cssClass, message) {
	// console.log('mmBlogShowMessage: ' + message);
	if (message == '') {
		$(".info-message").html('');
	} else if (cssClass == 'err') {
		$(".info-message").html('');		
		alert(message);
		// Submit wieder aktivieren
		$("#blog-comment-submit").unbind('click').bind('click', function(event){mmBlogFormSubmit($("#js-article-comments-form form"));});
		$("#js-article-comments-form form").unbind('submit').bind('submit', function(event){mmBlogFormSubmit($("#js-article-comments-form form"));});
		$("#blog-comment-submit").removeClass('mmInaktiv');
	} else {
		$(".info-message").html('<span class="mmMessageEntry_'+cssClass+'">'+message+'</span>');
	}
}

function mmShowComment(quote) {
	$(quote).parent().parent().find(".mmBlogPostTeaser").hide();
	$(quote).parent().parent().find(".mmBlogPostText").show();
}

function mmPrepareQuotes() {
	$(".spForumQuoteHeadline").each( function() {
		var quote = $(this).html();
		quote = '[' + quote + '<a class="forum-quote-show-link" onclick="mmShowQuote(this);">' + mmoBlogCommentsPhrase.showquote + '</a>]';
		$(this).html(quote);
	});
	$(".mmBlogPostTeaser.mmMore").each( function() {
		var teaser = $(this).html();
		teaser = teaser + '<a class="mmForumShowFulltextLink" title="' + mmoBlogCommentsPhrase.showcomment + '" onclick="mmShowComment(this);">&nbsp;</a>';
		$(this).html(teaser);
	});
}

function mmShowQuote(quote) {
	$(quote).parent().parent().find(".spForumQuoteText").css("display","block").show();
	$(quote).hide();
}


/* Paginator */

var mmBCPager = function(element, forum, threadid, entries) {
	var pp = this;

	this.pager = element;
	this.step=5;
	this.entries=entries;
	this.forum = forum;
	this.threadid = threadid;
	this.pageElement = $(element).find(".comments-box-page");
	this.currentOffset=null;
	this.setPos(1);
	this.comments = false;

	$(pp.pager).find(".js-article-comments-box-nav .prev").each( function() {
		$(this).bind("click", function(ev) { pp.prev() });
	});
	$(pp.pager).find(".js-article-comments-box-nav .next").each( function() {
		$(this).bind("click", function(ev) { pp.next() });
	});
	$(pp.pager).find(".js-article-comments-box-nav .first").each( function() {
		$(this).bind("click", function(ev) { pp.first() });
	});
	$(pp.pager).find(".js-article-comments-box-nav .last").each( function() {
		$(this).bind("click", function(ev) { pp.last() });
	});
}

mmBCPager.prototype = {
		offset: function(pos) {
			var pp = this;

			if (pos > this.entries) pos=this.entries - this.entries % this.step;
			if (pos < 1) pos=1;
			
			if (pos != this.currentOffset) {
				var url = "/fragments/community/" + this.forum + "-" + this.threadid + "-" + pos + ".html";
				$("body").addClass("mmWait");
				$.ajax({
					type: "GET",
					url: url,
					success: function(data) {
								$(pp.pageElement).each(function(){
									$(this).html(data);
									mmPrepareQuotes();
								});
								pp.setPos(pos);
								$("body").removeClass("mmWait");

							},
					error: function(data) {
								$("body").removeClass("mmWait");
							}
				});
			}
		},
		next: function() {
			this.offset(this.currentOffset + this.step);
		},
		prev: function() {
			this.offset(this.currentOffset - this.step);
		},
		first: function() {
			this.offset(1);
		},
		last: function() {
			this.offset((this.entries-1) - ((this.entries-1) % this.step) + 1);
		},
		setpage: function(page) {
			var p = parseInt(page);
			if (!isNaN(p)) {
				var newoffset = (p-1) * this.step + 1;
				if (newoffset < 1 ) this.first()
				else if (newoffset > this.entries) this.last()
				else this.offset(newoffset);
			}
		},
		setPos: function(pos) {
			var pp = this;
			if (pos == 1) {
				$(pp.pager).find(".js-article-comments-box-nav .prev").each( function() {
					$(this).hide();
				});				
				$(pp.pager).find(".js-article-comments-box-nav .first").each( function() {
					$(this).hide();
				});				
			} else if (pp.currentOffset == null || pp.currentOffset == 1) {
				$(pp.pager).find(".js-article-comments-box-nav .prev").each( function() {
					$(this).show();
				});
				$(pp.pager).find(".js-article-comments-box-nav .first").each( function() {
					$(this).show();
				});				
			}
			if (pos > pp.entries-pp.step) {
				$(pp.pager).find(".js-article-comments-box-nav .next").each( function() {
					$(this).hide();
				});				
				$(pp.pager).find(".js-article-comments-box-nav last").each( function() {
					$(this).hide();
				});				
			} else if (pp.currentOffset == null || pp.currentOffset > pp.entries-pp.step) {
				$(pp.pager).find(".js-article-comments-box-nav .next").each( function() {
					$(this).show();
				});
				$(pp.pager).find(".js-article-comments-box-nav .last").each( function() {
					$(this).show();
				});
			}
			var techpos = pos-1;
			var techentries = this.entries-1;
			var seite  = (techpos - techpos % this.step) / this.step + 1;
			var seiten = (techentries - techentries % this.step) / this.step + 1;

			$(pp.pager).find(".comments-box-page-count").each( function() {
				$(this).html(mmoBlogCommentsPhrase.seite + ' ' + seite + ' ' + mmoBlogCommentsPhrase.von + ' ' + seiten);
			});				
			this.currentOffset = pos;
		}
}

function mmReplaceCommentsBoxPageNavBarAnchors(){
	if($('.js-article-comments-box-nav > .js-pager-right').length > 0){
		$('.js-article-comments-box-nav > .js-pager-right').each(function(index){
		    if(index < 1){
		    	var countLinks = $(this).find('a').length;
		    	$('.js-article-comments-box-nav > .js-pager-right').find('a').each(function(index){
		            if(index < countLinks){
		                $(this).attr('href', 'javascript:void(0);');
		            }
		    	});
			}
		});
	}
}

//ready
$(document).ready(function() {
	$(".js-login-check").bind('click', function(event){ mmBlogLoginCheck(this); });
	$("#js-article-comments-form form").bind('submit', function(event){ mmBlogLoginCheck(this); });
	mmPrepareQuotes();
	$("#js-article-comments-form").show();
	mmReplaceCommentsBoxPageNavBarAnchors();
});
/*! Copyright (c) 2013 Brandon Aaron (http://brandon.aaron.sh)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Version: 3.1.9
 *
 * Requires: jQuery 1.2.2+
 */

(function (factory) {
    if ( typeof define === 'function' && define.amd ) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS style for Browserify
        module.exports = factory;
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var toFix  = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'],
        toBind = ( 'onwheel' in document || document.documentMode >= 9 ) ?
                    ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'],
        slice  = Array.prototype.slice,
        nullLowestDeltaTimeout, lowestDelta;

    if ( $.event.fixHooks ) {
        for ( var i = toFix.length; i; ) {
            $.event.fixHooks[ toFix[--i] ] = $.event.mouseHooks;
        }
    }

    var special = $.event.special.mousewheel = {
        version: '3.1.9',

        setup: function() {
            if ( this.addEventListener ) {
                for ( var i = toBind.length; i; ) {
                    this.addEventListener( toBind[--i], handler, false );
                }
            } else {
                this.onmousewheel = handler;
            }
            // Store the line height and page height for this particular element
            $.data(this, 'mousewheel-line-height', special.getLineHeight(this));
            $.data(this, 'mousewheel-page-height', special.getPageHeight(this));
        },

        teardown: function() {
            if ( this.removeEventListener ) {
                for ( var i = toBind.length; i; ) {
                    this.removeEventListener( toBind[--i], handler, false );
                }
            } else {
                this.onmousewheel = null;
            }
        },

        getLineHeight: function(elem) {
            return parseInt($(elem)['offsetParent' in $.fn ? 'offsetParent' : 'parent']().css('fontSize'), 10);
        },

        getPageHeight: function(elem) {
            return $(elem).height();
        },

        settings: {
            adjustOldDeltas: true
        }
    };

    $.fn.extend({
        mousewheel: function(fn) {
            return fn ? this.bind('mousewheel', fn) : this.trigger('mousewheel');
        },

        unmousewheel: function(fn) {
            return this.unbind('mousewheel', fn);
        }
    });


    function handler(event) {
        var orgEvent   = event || window.event,
            args       = slice.call(arguments, 1),
            delta      = 0,
            deltaX     = 0,
            deltaY     = 0,
            absDelta   = 0;
        event = $.event.fix(orgEvent);
        event.type = 'mousewheel';

        // Old school scrollwheel delta
        if ( 'detail'      in orgEvent ) { deltaY = orgEvent.detail * -1;      }
        if ( 'wheelDelta'  in orgEvent ) { deltaY = orgEvent.wheelDelta;       }
        if ( 'wheelDeltaY' in orgEvent ) { deltaY = orgEvent.wheelDeltaY;      }
        if ( 'wheelDeltaX' in orgEvent ) { deltaX = orgEvent.wheelDeltaX * -1; }

        // Firefox < 17 horizontal scrolling related to DOMMouseScroll event
        if ( 'axis' in orgEvent && orgEvent.axis === orgEvent.HORIZONTAL_AXIS ) {
            deltaX = deltaY * -1;
            deltaY = 0;
        }

        // Set delta to be deltaY or deltaX if deltaY is 0 for backwards compatabilitiy
        delta = deltaY === 0 ? deltaX : deltaY;

        // New school wheel delta (wheel event)
        if ( 'deltaY' in orgEvent ) {
            deltaY = orgEvent.deltaY * -1;
            delta  = deltaY;
        }
        if ( 'deltaX' in orgEvent ) {
            deltaX = orgEvent.deltaX;
            if ( deltaY === 0 ) { delta  = deltaX * -1; }
        }

        // No change actually happened, no reason to go any further
        if ( deltaY === 0 && deltaX === 0 ) { return; }

        // Need to convert lines and pages to pixels if we aren't already in pixels
        // There are three delta modes:
        //   * deltaMode 0 is by pixels, nothing to do
        //   * deltaMode 1 is by lines
        //   * deltaMode 2 is by pages
        if ( orgEvent.deltaMode === 1 ) {
            var lineHeight = $.data(this, 'mousewheel-line-height');
            delta  *= lineHeight;
            deltaY *= lineHeight;
            deltaX *= lineHeight;
        } else if ( orgEvent.deltaMode === 2 ) {
            var pageHeight = $.data(this, 'mousewheel-page-height');
            delta  *= pageHeight;
            deltaY *= pageHeight;
            deltaX *= pageHeight;
        }

        // Store lowest absolute delta to normalize the delta values
        absDelta = Math.max( Math.abs(deltaY), Math.abs(deltaX) );

        if ( !lowestDelta || absDelta < lowestDelta ) {
            lowestDelta = absDelta;

            // Adjust older deltas if necessary
            if ( shouldAdjustOldDeltas(orgEvent, absDelta) ) {
                lowestDelta /= 40;
            }
        }

        // Adjust older deltas if necessary
        if ( shouldAdjustOldDeltas(orgEvent, absDelta) ) {
            // Divide all the things by 40!
            delta  /= 40;
            deltaX /= 40;
            deltaY /= 40;
        }

        // Get a whole, normalized value for the deltas
        delta  = Math[ delta  >= 1 ? 'floor' : 'ceil' ](delta  / lowestDelta);
        deltaX = Math[ deltaX >= 1 ? 'floor' : 'ceil' ](deltaX / lowestDelta);
        deltaY = Math[ deltaY >= 1 ? 'floor' : 'ceil' ](deltaY / lowestDelta);

        // Add information to the event object
        event.deltaX = deltaX;
        event.deltaY = deltaY;
        event.deltaFactor = lowestDelta;
        // Go ahead and set deltaMode to 0 since we converted to pixels
        // Although this is a little odd since we overwrite the deltaX/Y
        // properties with normalized deltas.
        event.deltaMode = 0;

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        // Clearout lowestDelta after sometime to better
        // handle multiple device types that give different
        // a different lowestDelta
        // Ex: trackpad = 3 and mouse wheel = 120
        if (nullLowestDeltaTimeout) { clearTimeout(nullLowestDeltaTimeout); }
        nullLowestDeltaTimeout = setTimeout(nullLowestDelta, 200);

        return ($.event.dispatch || $.event.handle).apply(this, args);
    }

    function nullLowestDelta() {
        lowestDelta = null;
    }

    function shouldAdjustOldDeltas(orgEvent, absDelta) {
        // If this is an older event and the delta is divisable by 120,
        // then we are assuming that the browser is treating this as an
        // older mouse wheel event and that we should divide the deltas
        // by 40 to try and get a more usable deltaFactor.
        // Side note, this actually impacts the reported scroll distance
        // in older browsers and can cause scrolling to be slower than native.
        // Turn this off by setting $.event.special.mousewheel.settings.adjustOldDeltas to false.
        return special.settings.adjustOldDeltas && orgEvent.type === 'mousewheel' && absDelta % 120 === 0;
    }

}));/*!
 * jScrollPane - v2.0.19 - 2013-11-16
 * http://jscrollpane.kelvinluck.com/
 *
 * Copyright (c) 2013 Kelvin Luck
 * Dual licensed under the MIT or GPL licenses.
 */
!function(a,b){var c=function(c){return a(c,b)};"function"==typeof define&&define.amd?define(["jquery"],c):"object"==typeof exports?module.exports=c:c(jQuery)}(function(a,b,c){a.fn.jScrollPane=function(d){function e(d,e){function f(b){var e,h,j,l,m,n,q=!1,r=!1;if(P=b,Q===c)m=d.scrollTop(),n=d.scrollLeft(),d.css({overflow:"hidden",padding:0}),R=d.innerWidth()+tb,S=d.innerHeight(),d.width(R),Q=a('<div class="jspPane" />').css("padding",sb).append(d.children()),T=a('<div class="jspContainer" />').css({width:R+"px",height:S+"px"}).append(Q).appendTo(d);else{if(d.css("width",""),q=P.stickToBottom&&C(),r=P.stickToRight&&D(),l=d.innerWidth()+tb!=R||d.outerHeight()!=S,l&&(R=d.innerWidth()+tb,S=d.innerHeight(),T.css({width:R+"px",height:S+"px"})),!l&&ub==U&&Q.outerHeight()==V)return void d.width(R);ub=U,Q.css("width",""),d.width(R),T.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()}Q.css("overflow","auto"),U=b.contentWidth?b.contentWidth:Q[0].scrollWidth,V=Q[0].scrollHeight,Q.css("overflow",""),W=U/R,X=V/S,Y=X>1,Z=W>1,Z||Y?(d.addClass("jspScrollable"),e=P.maintainPosition&&(ab||db),e&&(h=A(),j=B()),g(),i(),k(),e&&(y(r?U-R:h,!1),x(q?V-S:j,!1)),H(),E(),N(),P.enableKeyboardNavigation&&J(),P.clickOnTrack&&o(),L(),P.hijackInternalLinks&&M()):(d.removeClass("jspScrollable"),Q.css({top:0,left:0,width:T.width()-tb}),F(),I(),K(),p()),P.autoReinitialise&&!rb?rb=setInterval(function(){f(P)},P.autoReinitialiseDelay):!P.autoReinitialise&&rb&&clearInterval(rb),m&&d.scrollTop(0)&&x(m,!1),n&&d.scrollLeft(0)&&y(n,!1),d.trigger("jsp-initialised",[Z||Y])}function g(){Y&&(T.append(a('<div class="jspVerticalBar" />').append(a('<div class="jspCap jspCapTop" />'),a('<div class="jspTrack" />').append(a('<div class="jspDrag" />').append(a('<div class="jspDragTop" />'),a('<div class="jspDragBottom" />'))),a('<div class="jspCap jspCapBottom" />'))),eb=T.find(">.jspVerticalBar"),fb=eb.find(">.jspTrack"),$=fb.find(">.jspDrag"),P.showArrows&&(jb=a('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp",m(0,-1)).bind("click.jsp",G),kb=a('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp",m(0,1)).bind("click.jsp",G),P.arrowScrollOnHover&&(jb.bind("mouseover.jsp",m(0,-1,jb)),kb.bind("mouseover.jsp",m(0,1,kb))),l(fb,P.verticalArrowPositions,jb,kb)),hb=S,T.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function(){hb-=a(this).outerHeight()}),$.hover(function(){$.addClass("jspHover")},function(){$.removeClass("jspHover")}).bind("mousedown.jsp",function(b){a("html").bind("dragstart.jsp selectstart.jsp",G),$.addClass("jspActive");var c=b.pageY-$.position().top;return a("html").bind("mousemove.jsp",function(a){r(a.pageY-c,!1)}).bind("mouseup.jsp mouseleave.jsp",q),!1}),h())}function h(){fb.height(hb+"px"),ab=0,gb=P.verticalGutter+fb.outerWidth(),Q.width(R-gb-tb);try{0===eb.position().left&&Q.css("margin-left",gb+"px")}catch(a){}}function i(){Z&&(T.append(a('<div class="jspHorizontalBar" />').append(a('<div class="jspCap jspCapLeft" />'),a('<div class="jspTrack" />').append(a('<div class="jspDrag" />').append(a('<div class="jspDragLeft" />'),a('<div class="jspDragRight" />'))),a('<div class="jspCap jspCapRight" />'))),lb=T.find(">.jspHorizontalBar"),mb=lb.find(">.jspTrack"),bb=mb.find(">.jspDrag"),P.showArrows&&(pb=a('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp",m(-1,0)).bind("click.jsp",G),qb=a('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp",m(1,0)).bind("click.jsp",G),P.arrowScrollOnHover&&(pb.bind("mouseover.jsp",m(-1,0,pb)),qb.bind("mouseover.jsp",m(1,0,qb))),l(mb,P.horizontalArrowPositions,pb,qb)),bb.hover(function(){bb.addClass("jspHover")},function(){bb.removeClass("jspHover")}).bind("mousedown.jsp",function(b){a("html").bind("dragstart.jsp selectstart.jsp",G),bb.addClass("jspActive");var c=b.pageX-bb.position().left;return a("html").bind("mousemove.jsp",function(a){t(a.pageX-c,!1)}).bind("mouseup.jsp mouseleave.jsp",q),!1}),nb=T.innerWidth(),j())}function j(){T.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function(){nb-=a(this).outerWidth()}),mb.width(nb+"px"),db=0}function k(){if(Z&&Y){var b=mb.outerHeight(),c=fb.outerWidth();hb-=b,a(lb).find(">.jspCap:visible,>.jspArrow").each(function(){nb+=a(this).outerWidth()}),nb-=c,S-=c,R-=b,mb.parent().append(a('<div class="jspCorner" />').css("width",b+"px")),h(),j()}Z&&Q.width(T.outerWidth()-tb+"px"),V=Q.outerHeight(),X=V/S,Z&&(ob=Math.ceil(1/W*nb),ob>P.horizontalDragMaxWidth?ob=P.horizontalDragMaxWidth:ob<P.horizontalDragMinWidth&&(ob=P.horizontalDragMinWidth),bb.width(ob+"px"),cb=nb-ob,u(db)),Y&&(ib=Math.ceil(1/X*hb),ib>P.verticalDragMaxHeight?ib=P.verticalDragMaxHeight:ib<P.verticalDragMinHeight&&(ib=P.verticalDragMinHeight),$.height(ib+"px"),_=hb-ib,s(ab))}function l(a,b,c,d){var e,f="before",g="after";"os"==b&&(b=/Mac/.test(navigator.platform)?"after":"split"),b==f?g=b:b==g&&(f=b,e=c,c=d,d=e),a[f](c)[g](d)}function m(a,b,c){return function(){return n(a,b,this,c),this.blur(),!1}}function n(b,c,d,e){d=a(d).addClass("jspActive");var f,g,h=!0,i=function(){0!==b&&vb.scrollByX(b*P.arrowButtonSpeed),0!==c&&vb.scrollByY(c*P.arrowButtonSpeed),g=setTimeout(i,h?P.initialDelay:P.arrowRepeatFreq),h=!1};i(),f=e?"mouseout.jsp":"mouseup.jsp",e=e||a("html"),e.bind(f,function(){d.removeClass("jspActive"),g&&clearTimeout(g),g=null,e.unbind(f)})}function o(){p(),Y&&fb.bind("mousedown.jsp",function(b){if(b.originalTarget===c||b.originalTarget==b.currentTarget){var d,e=a(this),f=e.offset(),g=b.pageY-f.top-ab,h=!0,i=function(){var a=e.offset(),c=b.pageY-a.top-ib/2,f=S*P.scrollPagePercent,k=_*f/(V-S);if(0>g)ab-k>c?vb.scrollByY(-f):r(c);else{if(!(g>0))return void j();c>ab+k?vb.scrollByY(f):r(c)}d=setTimeout(i,h?P.initialDelay:P.trackClickRepeatFreq),h=!1},j=function(){d&&clearTimeout(d),d=null,a(document).unbind("mouseup.jsp",j)};return i(),a(document).bind("mouseup.jsp",j),!1}}),Z&&mb.bind("mousedown.jsp",function(b){if(b.originalTarget===c||b.originalTarget==b.currentTarget){var d,e=a(this),f=e.offset(),g=b.pageX-f.left-db,h=!0,i=function(){var a=e.offset(),c=b.pageX-a.left-ob/2,f=R*P.scrollPagePercent,k=cb*f/(U-R);if(0>g)db-k>c?vb.scrollByX(-f):t(c);else{if(!(g>0))return void j();c>db+k?vb.scrollByX(f):t(c)}d=setTimeout(i,h?P.initialDelay:P.trackClickRepeatFreq),h=!1},j=function(){d&&clearTimeout(d),d=null,a(document).unbind("mouseup.jsp",j)};return i(),a(document).bind("mouseup.jsp",j),!1}})}function p(){mb&&mb.unbind("mousedown.jsp"),fb&&fb.unbind("mousedown.jsp")}function q(){a("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp"),$&&$.removeClass("jspActive"),bb&&bb.removeClass("jspActive")}function r(a,b){Y&&(0>a?a=0:a>_&&(a=_),b===c&&(b=P.animateScroll),b?vb.animate($,"top",a,s):($.css("top",a),s(a)))}function s(a){a===c&&(a=$.position().top),T.scrollTop(0),ab=a;var b=0===ab,e=ab==_,f=a/_,g=-f*(V-S);(wb!=b||yb!=e)&&(wb=b,yb=e,d.trigger("jsp-arrow-change",[wb,yb,xb,zb])),v(b,e),Q.css("top",g),d.trigger("jsp-scroll-y",[-g,b,e]).trigger("scroll")}function t(a,b){Z&&(0>a?a=0:a>cb&&(a=cb),b===c&&(b=P.animateScroll),b?vb.animate(bb,"left",a,u):(bb.css("left",a),u(a)))}function u(a){a===c&&(a=bb.position().left),T.scrollTop(0),db=a;var b=0===db,e=db==cb,f=a/cb,g=-f*(U-R);(xb!=b||zb!=e)&&(xb=b,zb=e,d.trigger("jsp-arrow-change",[wb,yb,xb,zb])),w(b,e),Q.css("left",g),d.trigger("jsp-scroll-x",[-g,b,e]).trigger("scroll")}function v(a,b){P.showArrows&&(jb[a?"addClass":"removeClass"]("jspDisabled"),kb[b?"addClass":"removeClass"]("jspDisabled"))}function w(a,b){P.showArrows&&(pb[a?"addClass":"removeClass"]("jspDisabled"),qb[b?"addClass":"removeClass"]("jspDisabled"))}function x(a,b){var c=a/(V-S);r(c*_,b)}function y(a,b){var c=a/(U-R);t(c*cb,b)}function z(b,c,d){var e,f,g,h,i,j,k,l,m,n=0,o=0;try{e=a(b)}catch(p){return}for(f=e.outerHeight(),g=e.outerWidth(),T.scrollTop(0),T.scrollLeft(0);!e.is(".jspPane");)if(n+=e.position().top,o+=e.position().left,e=e.offsetParent(),/^body|html$/i.test(e[0].nodeName))return;h=B(),j=h+S,h>n||c?l=n-P.horizontalGutter:n+f>j&&(l=n-S+f+P.horizontalGutter),isNaN(l)||x(l,d),i=A(),k=i+R,i>o||c?m=o-P.horizontalGutter:o+g>k&&(m=o-R+g+P.horizontalGutter),isNaN(m)||y(m,d)}function A(){return-Q.position().left}function B(){return-Q.position().top}function C(){var a=V-S;return a>20&&a-B()<10}function D(){var a=U-R;return a>20&&a-A()<10}function E(){T.unbind(Bb).bind(Bb,function(a,b,c,d){var e=db,f=ab,g=a.deltaFactor||P.mouseWheelSpeed;return vb.scrollBy(c*g,-d*g,!1),e==db&&f==ab})}function F(){T.unbind(Bb)}function G(){return!1}function H(){Q.find(":input,a").unbind("focus.jsp").bind("focus.jsp",function(a){z(a.target,!1)})}function I(){Q.find(":input,a").unbind("focus.jsp")}function J(){function b(){var a=db,b=ab;switch(c){case 40:vb.scrollByY(P.keyboardSpeed,!1);break;case 38:vb.scrollByY(-P.keyboardSpeed,!1);break;case 34:case 32:vb.scrollByY(S*P.scrollPagePercent,!1);break;case 33:vb.scrollByY(-S*P.scrollPagePercent,!1);break;case 39:vb.scrollByX(P.keyboardSpeed,!1);break;case 37:vb.scrollByX(-P.keyboardSpeed,!1)}return e=a!=db||b!=ab}var c,e,f=[];Z&&f.push(lb[0]),Y&&f.push(eb[0]),Q.focus(function(){d.focus()}),d.attr("tabindex",0).unbind("keydown.jsp keypress.jsp").bind("keydown.jsp",function(d){if(d.target===this||f.length&&a(d.target).closest(f).length){var g=db,h=ab;switch(d.keyCode){case 40:case 38:case 34:case 32:case 33:case 39:case 37:c=d.keyCode,b();break;case 35:x(V-S),c=null;break;case 36:x(0),c=null}return e=d.keyCode==c&&g!=db||h!=ab,!e}}).bind("keypress.jsp",function(a){return a.keyCode==c&&b(),!e}),P.hideFocus?(d.css("outline","none"),"hideFocus"in T[0]&&d.attr("hideFocus",!0)):(d.css("outline",""),"hideFocus"in T[0]&&d.attr("hideFocus",!1))}function K(){d.attr("tabindex","-1").removeAttr("tabindex").unbind("keydown.jsp keypress.jsp")}function L(){if(location.hash&&location.hash.length>1){var b,c,d=escape(location.hash.substr(1));try{b=a("#"+d+', a[name="'+d+'"]')}catch(e){return}b.length&&Q.find(d)&&(0===T.scrollTop()?c=setInterval(function(){T.scrollTop()>0&&(z(b,!0),a(document).scrollTop(T.position().top),clearInterval(c))},50):(z(b,!0),a(document).scrollTop(T.position().top)))}}function M(){a(document.body).data("jspHijack")||(a(document.body).data("jspHijack",!0),a(document.body).delegate("a[href*=#]","click",function(c){var d,e,f,g,h,i,j=this.href.substr(0,this.href.indexOf("#")),k=location.href;if(-1!==location.href.indexOf("#")&&(k=location.href.substr(0,location.href.indexOf("#"))),j===k){d=escape(this.href.substr(this.href.indexOf("#")+1));try{e=a("#"+d+', a[name="'+d+'"]')}catch(l){return}e.length&&(f=e.closest(".jspScrollable"),g=f.data("jsp"),g.scrollToElement(e,!0),f[0].scrollIntoView&&(h=a(b).scrollTop(),i=e.offset().top,(h>i||i>h+a(b).height())&&f[0].scrollIntoView()),c.preventDefault())}}))}function N(){var a,b,c,d,e,f=!1;T.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("touchstart.jsp",function(g){var h=g.originalEvent.touches[0];a=A(),b=B(),c=h.pageX,d=h.pageY,e=!1,f=!0}).bind("touchmove.jsp",function(g){if(f){var h=g.originalEvent.touches[0],i=db,j=ab;return vb.scrollTo(a+c-h.pageX,b+d-h.pageY),e=e||Math.abs(c-h.pageX)>5||Math.abs(d-h.pageY)>5,i==db&&j==ab}}).bind("touchend.jsp",function(){f=!1}).bind("click.jsp-touchclick",function(){return e?(e=!1,!1):void 0})}function O(){var a=B(),b=A();d.removeClass("jspScrollable").unbind(".jsp"),d.replaceWith(Ab.append(Q.children())),Ab.scrollTop(a),Ab.scrollLeft(b),rb&&clearInterval(rb)}var P,Q,R,S,T,U,V,W,X,Y,Z,$,_,ab,bb,cb,db,eb,fb,gb,hb,ib,jb,kb,lb,mb,nb,ob,pb,qb,rb,sb,tb,ub,vb=this,wb=!0,xb=!0,yb=!1,zb=!1,Ab=d.clone(!1,!1).empty(),Bb=a.fn.mwheelIntent?"mwheelIntent.jsp":"mousewheel.jsp";"border-box"===d.css("box-sizing")?(sb=0,tb=0):(sb=d.css("paddingTop")+" "+d.css("paddingRight")+" "+d.css("paddingBottom")+" "+d.css("paddingLeft"),tb=(parseInt(d.css("paddingLeft"),10)||0)+(parseInt(d.css("paddingRight"),10)||0)),a.extend(vb,{reinitialise:function(b){b=a.extend({},P,b),f(b)},scrollToElement:function(a,b,c){z(a,b,c)},scrollTo:function(a,b,c){y(a,c),x(b,c)},scrollToX:function(a,b){y(a,b)},scrollToY:function(a,b){x(a,b)},scrollToPercentX:function(a,b){y(a*(U-R),b)},scrollToPercentY:function(a,b){x(a*(V-S),b)},scrollBy:function(a,b,c){vb.scrollByX(a,c),vb.scrollByY(b,c)},scrollByX:function(a,b){var c=A()+Math[0>a?"floor":"ceil"](a),d=c/(U-R);t(d*cb,b)},scrollByY:function(a,b){var c=B()+Math[0>a?"floor":"ceil"](a),d=c/(V-S);r(d*_,b)},positionDragX:function(a,b){t(a,b)},positionDragY:function(a,b){r(a,b)},animate:function(a,b,c,d){var e={};e[b]=c,a.animate(e,{duration:P.animateDuration,easing:P.animateEase,queue:!1,step:d})},getContentPositionX:function(){return A()},getContentPositionY:function(){return B()},getContentWidth:function(){return U},getContentHeight:function(){return V},getPercentScrolledX:function(){return A()/(U-R)},getPercentScrolledY:function(){return B()/(V-S)},getIsScrollableH:function(){return Z},getIsScrollableV:function(){return Y},getContentPane:function(){return Q},scrollToBottom:function(a){r(_,a)},hijackInternalLinks:a.noop,destroy:function(){O()}}),f(e)}return d=a.extend({},a.fn.jScrollPane.defaults,d),a.each(["arrowButtonSpeed","trackClickSpeed","keyboardSpeed"],function(){d[this]=d[this]||d.speed}),this.each(function(){var b=a(this),c=b.data("jsp");c?c.reinitialise(d):(a("script",b).filter('[type="text/javascript"],:not([type])').remove(),c=new e(b,d),b.data("jsp",c))})},a.fn.jScrollPane.defaults={showArrows:!1,maintainPosition:!0,stickToBottom:!1,stickToRight:!1,clickOnTrack:!0,autoReinitialise:!1,autoReinitialiseDelay:500,verticalDragMinHeight:0,verticalDragMaxHeight:99999,horizontalDragMinWidth:0,horizontalDragMaxWidth:99999,contentWidth:c,animateScroll:!1,animateDuration:300,animateEase:"linear",hijackInternalLinks:!1,verticalGutter:4,horizontalGutter:4,mouseWheelSpeed:3,arrowButtonSpeed:0,arrowRepeatFreq:50,arrowScrollOnHover:!1,trackClickSpeed:0,trackClickRepeatFreq:70,verticalArrowPositions:"split",horizontalArrowPositions:"split",enableKeyboardNavigation:!0,hideFocus:!1,keyboardSpeed:0,initialDelay:300,speed:30,scrollPagePercent:.8}},this);var sp_webcfg_global; 

if (!"console" in window || typeof console == "undefined") {
    var methods = ["log", "debug", "info", "warn", "error", "assert", "dir", "dirxml", "group", "groupEnd", "time", "timeEnd", "count", "trace", "profile", "profileEnd"];
    window.console = {};
    for (var i = 0; i < methods.length; ++i) {
        window.console[methods[i]] = function () {};
    }
}

/* # HASH-Parameter auslesen: #---------------------------------------------------------------------------------------------------- */
function mmGetHashParams() {
	var loc=(document.location.href+""); 
	var pos=loc.indexOf("#");
	if (pos == -1) 
		return [];
	var hashQs=loc.substring(pos+1, loc.length);
	return mmSplitParams(hashQs);
}
function spGetHashParams() {
	/* wird benoetigt von flash/ce/sendform */
	return mmGetHashParams();
}
function mmSplitParams(params) {
	var result=[];
	var pairs=params.split("&");
	for (var i=0; i < pairs.length; i++) {
		var kv=pairs[i].split("=");
		if (kv.length == 2)
			result[kv[0]]=escape(kv[1]);
	}
	return result;
}


function mmStripHashParams(url) {
	var pos=url.indexOf("#");
	if (pos == -1)
		return url;
	url = url.substring(0,pos);
	return url;
}

/* # Cookies auslesen: #------------------------------------------------------------------------------------------------------------ */
function mmGetCookie (cookieName) {
	function getCookieValue (position) 	{
		var endpos = document.cookie.indexOf(";", position);
		if (endpos == -1)
			endpos = document.cookie.length;
		return unescape(document.cookie.substring(position, endpos));
	}
	var cookieString       = cookieName + "=";
	var cookieStringLength = cookieString.length;
	var allCookieLength    = document.cookie.length;
	var n = 0;
	while (n < allCookieLength) {
		var m = n + cookieStringLength;
		if (document.cookie.substring(n, m) == cookieString)
			return getCookieValue(m);
		n = document.cookie.indexOf(" ", n) + 1;
		if (n == 0) break;
	}
	return null;
}
function getCookieContent( cookieName ) {
	/* legacy: */
	return mmGetCookie(cookieName);
}
function mmSetCookie(name, value, daystoexpire, path, domain, secure) {
	var expires = new Date();
	expires.setTime(expires.getTime() + (daystoexpire * 86400000));
	document.cookie= name + "=" + escape(value) +
		((expires) ? "; expires=" + expires.toGMTString() : "") +
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") +
		((secure) ? "; secure" : "");
} 
function SPONgetCookie (cookieName) {
	return mmGetCookie(cookieName);
}



///* # Social-Bookmark (bookmark-link am artikel): #---------------------------------------------------------------------------------------------------- */
///******* Social Bookmarking functions ******/
///**
//* toggles SocialBookmark-Box on and off
//* @param mmDisplay - ture=on, false=off
//*/
//function mmSocialBookmarkSetDisplay(mmDisplay) {
//mmSocialBookmarkElement = document.getElementById("mmSocialBookmark").style;
//mmSocialBookmarkElement.visibility = (mmDisplay ? "visible" : "hidden");
//mmSocialBookmarkState=mmDisplay;
//}
///**
//* toggle function called form Bookmark Link
//* served mmSocialBookmarkSetDisplay() and starts EventHandler
//*/
//function mmSocialBookmarkToggle() {
//if (!mmSocialBookmarkState) {
//mmSocialBookmarkSetDisplay(true);
//mmStartMouseEvent();
//}
//else 
//mmSocialBookmarkSetDisplay(false);
//}
///**
//* Event handler function tests where the user clicked
//* and acts	according to this (switch SocialBookmark-Box off 
//* stops EventHandler)
//*/
//function mmSocialBookmarkToggleDisplay(e) {
//if (!e) var e = window.event;
//if (e.target) targ = e.target;
//else if (e.srcElement) targ = e.srcElement;
//if (targ.nodeType == 3) // defeat Safari bug
//targ = targ.parentNode;
//if (targ.id == "mmSocialBookmarkLink") {
//mmStopMouseEvent();
//return false;
//}
//var isInDiv = false;
//while(targ != null) {
//if (targ.id == "mmSocialBookmark") {
//isInDiv=true;
//break;
//}
//if (targ.id == "mmSocialBookmarkClose")
//break;
//targ = targ.parentNode;
//}
//if (!isInDiv) {
//mmSocialBookmarkSetDisplay(false);
//mmStopMouseEvent();
//}
//return false;
//}
///**
//* starts EventHandling
//*/
//function mmStartMouseEvent(){
//// DOM Level 2 Event Model;
//if (document.addEventListener) {
//document.addEventListener("mouseup", mmSocialBookmarkToggleDisplay, true);
//}
//// IE 5+ Event Model;
//else if (document.attachEvent) {
//document.attachEvent("onmouseup", mmSocialBookmarkToggleDisplay);
//}
//// IE 4 Event Model;
//else {
//document.onmouseup=mmSocialBookmarkToggleDisplay;
//}
//}
///**
//* stops EventHandling
//* mmOldHandler is a hack for EI4 Event Model
//*/
//// Eventhandler for EI 4 StopEvent;
//var mmOldHandler = document.onmouseup;
//function mmStopMouseEvent() {
//// Unregister the capturing event handlers;
//// DOM Event Model;
//if (document.removeEventListener) {
//document.removeEventListener("mouseup", mmSocialBookmarkToggleDisplay, true);
//}
//// IE 5+ Event Model;
//else if (document.detachEvent) {
//document.detachEvent("onmouseup", mmSocialBookmarkToggleDisplay);
//}
////IE 4 Event Model;
//else {
//document.onmouseup = mmOldHandler;
//}
//}
/* # Redirects fuer mobile Clients #------------------------------------------------------------------------------------------------ */
var mmMobileClients=[
      "midp",
      "240x320",
      "blackberry",
      "netfront",
      "nokia",
      "panasonic",
      "portalmmm",
      "sharp",
      "sie-",
      "sonyericsson",
      "symbian",
      "windows ce",
      "benq",
      "mda",
      "mot-",
      "opera mini",
      "philips",
      "pocket pc",
      "sagem",
      "samsung",
      "sda",
      "sgh-",
      "vodafone",
      "xda"
];
	
function mmIsMobileClient(userAgent) {
      try {
            userAgent=userAgent.toLowerCase();
            for (var i=0; i < mmMobileClients.length; i++)
                  if (userAgent.indexOf(mmMobileClients[i]) != -1)
                        return true;
      }
      catch (e) {
      }
      return false;
}
		
function mmIsThisBrowserMobileClient() {
      return mmIsMobileClient(navigator.userAgent);
}

function mmRedirectIfMobileClient() {
	try {
		if (mmIsThisBrowserMobileClient()) {
			if (document.location.href.indexOf('#nomobile') != -1) {
				document.cookie='nomobile';
				return;
			}	

			if (document.cookie && document.cookie.indexOf('nomobile') != -1) 
				return;
				
			document.location.href="http:/"+"/ml.manager-magazin.de?redirect=1";
		}
	}
	catch (e) { 
	}
}

/* ------------------------------------------------------------------------------------------------ */
/* User Agent detection */
(function($) {

	var spBrowserDetector = function(options) {

		var settings = $.extend({
			'ua' : navigator.userAgent.toLowerCase(),
			'href' : window.location.href
		}, options);
		
		return {
			getUserAgent : function () {
				return settings.ua;
			},
			isFirefox : function() {
				return settings.ua.search(/firefox/i) > -1;
			},
			isInternetExplorer : function() {
				return settings.ua.search(/msie/i) > -1 && !this.isWindowsPhone();
			},
			isSafari : function() {
				return settings.ua.search(/safari/i) > -1 && !this.isIPhone() && !this.isIPad()
					&& !this.isAndroid() && !this.isAndroid4() && !this.isChrome() 
					&& !this.isBlackBerry() && !this.isOpera();
			},
			isChrome : function() {
				return settings.ua.search(/chrome/i) > -1
					&& !this.isAndroid();
			},
			isOpera : function() {
				return settings.ua.search(/opera/i) > -1;
			},			
			isIDevice : function() {
				return (this.isIPad() || this.isIPhone());
			},
			isAndroid : function() {
				return settings.ua.search(/android/i) > -1;
			},
			isAndroid4 : function() {
				return settings.ua.search(/android 4/i) > -1;
			},
			isBlackBerry : function() {
				return settings.ua.search(/blackberry/i) > -1;
			},
			isIPad : function() {
				return settings.ua.search(/ipad/i) > -1;
			},
			isIPhone : function() {
				return settings.ua.search(/(iphone|ipod)/i) > -1;			
			},

			isWindowsPhone : function() {
				return (settings.ua.search(/windows phone/i) > -1 && settings.ua
						.search(/iemobile/i) > -1)
			},
			isMobileDevice : function() {
				return (this.isIDevice() || this.isAndroid() || this.isWindowsPhone());
			},
			/* Sites */
			isWww : function() {
				return settings.href.search(/www.manager-magazin.de/i) > -1 && !this.isMobile() && !this.isApp();
			},
			isMobile : function() {
				return settings.href.search(/m.manager-magazin.de/i) > -1;
			},
			isApp : function() {
				return settings.href.search(/a.manager-magazin.de/i) > -1;
			}
		}
	};

	$.fn.spBrowserDetector = function(method) {
		return new spBrowserDetector();
	};
})(jQuery);

var spUA = $().spBrowserDetector();

/* ------------------------------------------------------------------------------------------------ */
/*  */

/* ------------------------------------------------------------------------------------------------ */
/* SWFObject v2.1 <http://code.google.com/p/swfobject/>
	Copyright (c) 2007-2008 Geoff Stearns, Michael Williams, and Bobby van der Sluis
	This software is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
*/
var swfobject=function(){var b="undefined",Q="object",n="Shockwave Flash",p="ShockwaveFlash.ShockwaveFlash",P="application/x-shockwave-flash",m="SWFObjectExprInst",j=window,K=document,T=navigator,o=[],N=[],i=[],d=[],J,Z=null,M=null,l=null,e=false,A=false;var h=function(){var v=typeof K.getElementById!=b&&typeof K.getElementsByTagName!=b&&typeof K.createElement!=b,AC=[0,0,0],x=null;if(typeof T.plugins!=b&&typeof T.plugins[n]==Q){x=T.plugins[n].description;if(x&&!(typeof T.mimeTypes!=b&&T.mimeTypes[P]&&!T.mimeTypes[P].enabledPlugin)){x=x.replace(/^.*\s+(\S+\s+\S+$)/,"$1");AC[0]=parseInt(x.replace(/^(.*)\..*$/,"$1"),10);AC[1]=parseInt(x.replace(/^.*\.(.*)\s.*$/,"$1"),10);AC[2]=/r/.test(x)?parseInt(x.replace(/^.*r(.*)$/,"$1"),10):0}}else{if(typeof j.ActiveXObject!=b){var y=null,AB=false;try{y=new ActiveXObject(p+".7")}catch(t){try{y=new ActiveXObject(p+".6");AC=[6,0,21];y.AllowScriptAccess="always"}catch(t){if(AC[0]==6){AB=true}}if(!AB){try{y=new ActiveXObject(p)}catch(t){}}}if(!AB&&y){try{x=y.GetVariable("$version");if(x){x=x.split(" ")[1].split(",");AC=[parseInt(x[0],10),parseInt(x[1],10),parseInt(x[2],10)]}}catch(t){}}}}var AD=T.userAgent.toLowerCase(),r=T.platform.toLowerCase(),AA=/webkit/.test(AD)?parseFloat(AD.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,q=false,z=r?/win/.test(r):/win/.test(AD),w=r?/mac/.test(r):/mac/.test(AD);/*@cc_on q=true;@if(@_win32)z=true;@elif(@_mac)w=true;@end@*/return{w3cdom:v,pv:AC,webkit:AA,ie:q,win:z,mac:w}}();var L=function(){if(!h.w3cdom){return }f(H);if(h.ie&&h.win){try{K.write("<script id='__ie_ondomload' defer='true' src='//:'><\/script>");J=C("__ie_ondomload");if(J){I(J,"onreadystatechange",S)}}catch(q){}}if(h.webkit&&typeof K.readyState!=b){Z=setInterval(function(){if(/loaded|complete/.test(K.readyState)){E()}},10)}if(typeof K.addEventListener!=b){K.addEventListener("DOMContentLoaded",E,null)}R(E)}();function S(){if(J.readyState=="complete"){J.parentNode.removeChild(J);E()}}function E(){if(e){return }if(h.ie&&h.win){var v=a("span");try{var u=K.getElementsByTagName("body")[0].appendChild(v);u.parentNode.removeChild(u)}catch(w){return }}e=true;if(Z){clearInterval(Z);Z=null}var q=o.length;for(var r=0;r<q;r++){o[r]()}}function f(q){if(e){q()}else{o[o.length]=q}}function R(r){if(typeof j.addEventListener!=b){j.addEventListener("load",r,false)}else{if(typeof K.addEventListener!=b){K.addEventListener("load",r,false)}else{if(typeof j.attachEvent!=b){I(j,"onload",r)}else{if(typeof j.onload=="function"){var q=j.onload;j.onload=function(){q();r()}}else{j.onload=r}}}}}function H(){var t=N.length;for(var q=0;q<t;q++){var u=N[q].id;if(h.pv[0]>0){var r=C(u);if(r){N[q].width=r.getAttribute("width")?r.getAttribute("width"):"0";N[q].height=r.getAttribute("height")?r.getAttribute("height"):"0";if(c(N[q].swfVersion)){if(h.webkit&&h.webkit<312){Y(r)}W(u,true)}else{if(N[q].expressInstall&&!A&&c("6.0.65")&&(h.win||h.mac)){k(N[q])}else{O(r)}}}}else{W(u,true)}}}function Y(t){var q=t.getElementsByTagName(Q)[0];if(q){var w=a("embed"),y=q.attributes;if(y){var v=y.length;for(var u=0;u<v;u++){if(y[u].nodeName=="DATA"){w.setAttribute("src",y[u].nodeValue)}else{w.setAttribute(y[u].nodeName,y[u].nodeValue)}}}var x=q.childNodes;if(x){var z=x.length;for(var r=0;r<z;r++){if(x[r].nodeType==1&&x[r].nodeName=="PARAM"){w.setAttribute(x[r].getAttribute("name"),x[r].getAttribute("value"))}}}t.parentNode.replaceChild(w,t)}}function k(w){A=true;var u=C(w.id);if(u){if(w.altContentId){var y=C(w.altContentId);if(y){M=y;l=w.altContentId}}else{M=G(u)}if(!(/%$/.test(w.width))&&parseInt(w.width,10)<310){w.width="310"}if(!(/%$/.test(w.height))&&parseInt(w.height,10)<137){w.height="137"}K.title=K.title.slice(0,47)+" - Flash Player Installation";var z=h.ie&&h.win?"ActiveX":"PlugIn",q=K.title,r="MMredirectURL="+j.location+"&MMplayerType="+z+"&MMdoctitle="+q,x=w.id;if(h.ie&&h.win&&u.readyState!=4){var t=a("div");x+="SWFObjectNew";t.setAttribute("id",x);u.parentNode.insertBefore(t,u);u.style.display="none";var v=function(){u.parentNode.removeChild(u)};I(j,"onload",v)}U({data:w.expressInstall,id:m,width:w.width,height:w.height},{flashvars:r},x)}}function O(t){if(h.ie&&h.win&&t.readyState!=4){var r=a("div");t.parentNode.insertBefore(r,t);r.parentNode.replaceChild(G(t),r);t.style.display="none";var q=function(){t.parentNode.removeChild(t)};I(j,"onload",q)}else{t.parentNode.replaceChild(G(t),t)}}function G(v){var u=a("div");if(h.win&&h.ie){u.innerHTML=v.innerHTML}else{var r=v.getElementsByTagName(Q)[0];if(r){var w=r.childNodes;if(w){var q=w.length;for(var t=0;t<q;t++){if(!(w[t].nodeType==1&&w[t].nodeName=="PARAM")&&!(w[t].nodeType==8)){u.appendChild(w[t].cloneNode(true))}}}}}return u}function U(AG,AE,t){var q,v=C(t);if(v){if(typeof AG.id==b){AG.id=t}if(h.ie&&h.win){var AF="";for(var AB in AG){if(AG[AB]!=Object.prototype[AB]){if(AB.toLowerCase()=="data"){AE.movie=AG[AB]}else{if(AB.toLowerCase()=="styleclass"){AF+=' class="'+AG[AB]+'"'}else{if(AB.toLowerCase()!="classid"){AF+=" "+AB+'="'+AG[AB]+'"'}}}}}var AD="";for(var AA in AE){if(AE[AA]!=Object.prototype[AA]){AD+='<param name="'+AA+'" value="'+AE[AA]+'" />'}}v.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'+AF+">"+AD+"</object>";i[i.length]=AG.id;q=C(AG.id)}else{if(h.webkit&&h.webkit<312){var AC=a("embed");AC.setAttribute("type",P);for(var z in AG){if(AG[z]!=Object.prototype[z]){if(z.toLowerCase()=="data"){AC.setAttribute("src",AG[z])}else{if(z.toLowerCase()=="styleclass"){AC.setAttribute("class",AG[z])}else{if(z.toLowerCase()!="classid"){AC.setAttribute(z,AG[z])}}}}}for(var y in AE){if(AE[y]!=Object.prototype[y]){if(y.toLowerCase()!="movie"){AC.setAttribute(y,AE[y])}}}v.parentNode.replaceChild(AC,v);q=AC}else{var u=a(Q);u.setAttribute("type",P);for(var x in AG){if(AG[x]!=Object.prototype[x]){if(x.toLowerCase()=="styleclass"){u.setAttribute("class",AG[x])}else{if(x.toLowerCase()!="classid"){u.setAttribute(x,AG[x])}}}}for(var w in AE){if(AE[w]!=Object.prototype[w]&&w.toLowerCase()!="movie"){F(u,w,AE[w])}}v.parentNode.replaceChild(u,v);q=u}}}return q}function F(t,q,r){var u=a("param");u.setAttribute("name",q);u.setAttribute("value",r);t.appendChild(u)}function X(r){var q=C(r);if(q&&(q.nodeName=="OBJECT"||q.nodeName=="EMBED")){if(h.ie&&h.win){if(q.readyState==4){B(r)}else{j.attachEvent("onload",function(){B(r)})}}else{q.parentNode.removeChild(q)}}}function B(t){var r=C(t);if(r){for(var q in r){if(typeof r[q]=="function"){r[q]=null}}r.parentNode.removeChild(r)}}function C(t){var q=null;try{q=K.getElementById(t)}catch(r){}return q}function a(q){return K.createElement(q)}function I(t,q,r){t.attachEvent(q,r);d[d.length]=[t,q,r]}function c(t){var r=h.pv,q=t.split(".");q[0]=parseInt(q[0],10);q[1]=parseInt(q[1],10)||0;q[2]=parseInt(q[2],10)||0;return(r[0]>q[0]||(r[0]==q[0]&&r[1]>q[1])||(r[0]==q[0]&&r[1]==q[1]&&r[2]>=q[2]))?true:false}function V(v,r){if(h.ie&&h.mac){return }var u=K.getElementsByTagName("head")[0],t=a("style");t.setAttribute("type","text/css");t.setAttribute("media","screen");if(!(h.ie&&h.win)&&typeof K.createTextNode!=b){t.appendChild(K.createTextNode(v+" {"+r+"}"))}u.appendChild(t);if(h.ie&&h.win&&typeof K.styleSheets!=b&&K.styleSheets.length>0){var q=K.styleSheets[K.styleSheets.length-1];if(typeof q.addRule==Q){q.addRule(v,r)}}}function W(t,q){var r=q?"visible":"hidden";if(e&&C(t)){C(t).style.visibility=r}else{V("#"+t,"visibility:"+r)}}function g(s){var r=/[\\\"<>\.;]/;var q=r.exec(s)!=null;return q?encodeURIComponent(s):s}var D=function(){if(h.ie&&h.win){window.attachEvent("onunload",function(){var w=d.length;for(var v=0;v<w;v++){d[v][0].detachEvent(d[v][1],d[v][2])}var t=i.length;for(var u=0;u<t;u++){X(i[u])}for(var r in h){h[r]=null}h=null;for(var q in swfobject){swfobject[q]=null}swfobject=null})}}();return{registerObject:function(u,q,t){if(!h.w3cdom||!u||!q){return }var r={};r.id=u;r.swfVersion=q;r.expressInstall=t?t:false;N[N.length]=r;W(u,false)},getObjectById:function(v){var q=null;if(h.w3cdom){var t=C(v);if(t){var u=t.getElementsByTagName(Q)[0];if(!u||(u&&typeof t.SetVariable!=b)){q=t}else{if(typeof u.SetVariable!=b){q=u}}}}return q},embedSWF:function(x,AE,AB,AD,q,w,r,z,AC){if(!h.w3cdom||!x||!AE||!AB||!AD||!q){return }AB+="";AD+="";if(c(q)){W(AE,false);var AA={};if(AC&&typeof AC===Q){for(var v in AC){if(AC[v]!=Object.prototype[v]){AA[v]=AC[v]}}}AA.data=x;AA.width=AB;AA.height=AD;var y={};if(z&&typeof z===Q){for(var u in z){if(z[u]!=Object.prototype[u]){y[u]=z[u]}}}if(r&&typeof r===Q){for(var t in r){if(r[t]!=Object.prototype[t]){if(typeof y.flashvars!=b){y.flashvars+="&"+t+"="+r[t]}else{y.flashvars=t+"="+r[t]}}}}f(function(){U(AA,y,AE);if(AA.id==AE){W(AE,true)}})}else{if(w&&!A&&c("6.0.65")&&(h.win||h.mac)){A=true;W(AE,false);f(function(){var AF={};AF.id=AF.altContentId=AE;AF.width=AB;AF.height=AD;AF.expressInstall=w;k(AF)})}}},getFlashPlayerVersion:function(){return{major:h.pv[0],minor:h.pv[1],release:h.pv[2]}},hasFlashPlayerVersion:c,createSWF:function(t,r,q){if(h.w3cdom){return U(t,r,q)}else{return undefined}},removeSWF:function(q){if(h.w3cdom){X(q)}},createCSS:function(r,q){if(h.w3cdom){V(r,q)}},addDomLoadEvent:f,addLoadEvent:R,getQueryParamValue:function(v){var u=K.location.search||K.location.hash;if(v==null){return g(u)}if(u){var t=u.substring(1).split("&");for(var r=0;r<t.length;r++){if(t[r].substring(0,t[r].indexOf("="))==v){return g(t[r].substring((t[r].indexOf("=")+1)))}}}return""},expressInstallCallback:function(){if(A&&M){var q=C(m);if(q){q.parentNode.replaceChild(M,q);if(l){W(l,true);if(h.ie&&h.win){M.style.display="block"}}M=null;l=null;A=false}}}}}(); 


/* -------------------- */
/* sp prefix for flash: */
function spCounterFlash(flashid) {
	spCounter('/flash/flash-' + flashid + '-count.html');
}
function spCounter(url) {
	if (typeof(vdz_countframe) != 'undefined') vdz_countframe.location.href = 'http:/'+'/www.manager-magazin.de' + url;
}

function mmFramebuster() {
	if (top!=self) {
		top.location.href=self.location.href;
	}
}

/* -------------------- */
/* iphone: */
function mmClientIsIPhone() {
	return navigator.userAgent.indexOf('iPhone') > -1;
}
function mmClientIsIPad() {
	return navigator.userAgent.indexOf('iPad') > -1;
}
function mmClientIsIPod() {
	return navigator.userAgent.indexOf('iPod') > -1;
}
function mmClientIsIDevice() {
	return mmClientIsIPhone() || mmClientIsIPad() || mmClientIsIPod();
}
function mmHideIPhoneMessage() {
	mmSetCookie('mmIPhoneMessage','seen',1);
	if(document.getElementById('mmIPhoneMessage')) {
	    document.getElementById('mmIPhoneMessage').style.display  = 'none';
	}
	return false;
}
function mmIPhoneMessage() {
	if (mmClientIsIPhone() || mmClientIsIPod()) {
		var cookie = mmGetCookie('mmIPhoneMessage');
		if (cookie != 'seen') {
			document.write('<div id="mmIPhoneMessage"><a onclick="mmHideIPhoneMessage(); return false;" href="#" style="float:right;margin:-14px -14px 0 0;"><img src="/static/sys/v9/buttons/schliessen.jpg" width="20" height="20" alt="schließen" /><\/a><a href="http:/"+"/m.manager-magazin.de/">Hier geht es zur iPhone-optimierten Mobil-Seite<\/a><\/div>');
		}
	}
}
/*  */
function mmCounterContentainer(contentainerid) {
	// mmCounter('/count/contentainer/0,,' + contentainerid + ',00.html');
}
function mmCounter(url) {
	if (typeof(vdz_countframe) != 'undefined') vdz_countframe.location.href = 'http:/'+'/www.manager-magazin.de' + url;
}


/* mHover fix for ie because of pie */
$(document).ready(function(){
	$("#mmHeader ul#mmChannel li .mmNaviLevel2 li").hover( function() {
		$(this).addClass("mmHover");
	}, function(){
		$(this).removeClass("mmHover");
	})
});


function spnetDynamicFrame(read_cfg, get_element, new_id, new_width, new_height, new_url) {
	var config_activated = false;
	if (read_cfg == 'always') {
		config_activated = true;
	}
	if (sp_webcfg_global.social_bm) {
		if( read_cfg == 'social_fbshare' && sp_webcfg_global.social_bm.fb == true) {
			config_activated = true;
		}
		else if( read_cfg == 'social_fb' && sp_webcfg_global.social_bm.fb == true) {
			config_activated = true;
		}
		else if( read_cfg == 'social_tw' && sp_webcfg_global.social_bm.tw == true) {
			config_activated = true;
		}
		else if( read_cfg == 'social_gplus' && sp_webcfg_global.social_bm.gplus == true) {
			config_activated = true;
		}
	}
	if (config_activated) {
		var my_element;
		if( my_element = document.getElementById(get_element) ) {
			var my_frame = document.createElement("iframe");
			my_frame.id                = new_id;
			my_frame.style.width       = new_width;
			my_frame.style.height      = new_height;
			my_frame.src               = new_url;
			my_frame.marginHeight      = "0";
			my_frame.marginWidth       = "0";
			my_frame.scrolling         ="no" ;
			my_frame.style.border      = "none";
			my_frame.frameBorder       ="0"; 
			my_frame.style.overflow    = "hidden";  
			my_frame.allowTransparency ="true";
			my_element.appendChild(my_frame);
		}
	}
}

/* USERCOOKIE: */
function cookieprotect(cbsuccess, cbfail) {
	$.ajax({
		type : "POST",
		url : '/jsapi/usercookie/init.json',
        success: cbsuccess, 
		error: 	 cbfail
	});
}

/* VOTE: */
function submitVote(voteform) {

	if ($(voteform).find("input[type=radio]:checked").length == 0) {
		document.location.href='/votes/vote-' + $(voteform).attr("data-id") + '.html'; 
		return false;
	}; 

	cookieprotect(	function() { $(voteform).unbind("submit")
											.removeAttr("onsubmit")
											.submit(); 
					},
					function() { alert("Ein Fehler ist aufgetreten");}
				);
	return false;
}

function mmAjax(url, data, callback) {
	$.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: data,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		success: function(response){ 
			callback({ajaxSuccess: true, data: response});
		},
		error: function(jqXHR, textStatus, errorThrown){ 
			callback({ajaxSuccess: false, text: textStatus, error: errorThrown});
		}
	});
}

function dynamicDates() {
	var globalTs = $('#unixtime').attr('data-timestamp');
	if ( (typeof globalTs !== 'undefined') && ( globalTs !== '' ) ) {
		$('.headline-date').each( function() {
			var t=$(this),
				ts=t.attr('data-timestamp');
			if ( !ts ) return; // no timestamp - continue to next node
			if ( t.hasClass('launchdate-done') ) return; // avoid duplice calculation
			var diff = globalTs - ts,
				r = ''; // the replacement string

			if ( diff < 120 ) {
				// first minute
				r = '1&nbsp;Minute'; // well, rounded
			}
			else if ( diff < 3600 ) {
				// up to 1 hour
				r = Math.floor( diff / 60 + .5 ) + '&nbsp;Minuten';
			}
			else if ( diff < 86400 ) {
				// up to 1 day
				r = Math.floor( diff / 3600 + .5 ) + '&nbsp;Stunden';
			}
			else
				return; // 'else' leave timestamp alone.

			t.html('vor&nbsp;'+r);

			t.addClass('launchdate-done');
			
		}); /* each */
	}
}

function popup_window(seite,breite,hoehe,scroll,rsize) {
	sbars = (scroll==1)? "yes" : "no";
	rsize = (rsize==1)? "yes" : "no";
	if(seite.substr(0,1) == '/') seite = spnetENV_SERVER + seite;
	var win_name = breite+hoehe;
	var win_attr = "menubar=no,location=no,directories=no,toolbar=no,screenX=0,screenY=0";
	win_attr += ",width=" + breite + ",height=" + hoehe + ",scrollbars=" + sbars + ",resizable=" + rsize;
	sponWin = open(seite,win_name,win_attr);
	if(sponWin != null) sponWin.focus();
}

//Location-Change-Forms
function locationselect(select) {
	var url = $(select).val();
	window.location.href=url;
	return false;
}

// Article Overscroll
function spArticleOverscroll() {
	if(sp_webcfg_global && sp_webcfg_global.articleOverscroll) {
		var ao 		= this;
		ao.activated = false;
		ao.div 	= $("#article-overscroll-area");
		if (ao.div.length == 0) return null;
		// exclude mobile devices (window height problem)
		if (spUA == null || spUA.isMobileDevice()) return null;

		this.init = function() {
			$(window).bind("scroll.spArticleOverscroll", ao.checkposition);
			ao.div.empty();
			ao.adjustposition('init'); 
		}
		
		this.checkposition = function() {
			if (!ao.activated) {
				if ($(window).scrollTop() + $(window).height()*2 > ao.div.offset().top) {
					// erst laden und aktivieren, wenn in die zweite Haelfte gescrollt wurde...
					
					ao.adjustposition('checkposition');

					ao.div.addClass("grid-default center")
					   .prepend(
							   		$("<div>")	.attr("id", "article-overscroll-area-outer")
							   					.load("/fragments/centerpage/page-overscroll.html", ao.activateoverscroll())
							  );

				}
			}
		}

		this.adjustposition = function(where) {
 			if (($(window).scrollTop() + $(window).height()) >= ao.div.offset().top) {
				// wenn schon beim aktivieren die Position zu tief ist, auf die Startkante scrollen
				$(window).scrollTop(ao.div.offset().top - $(window).height());
			}
		};
		
		this.activateoverscroll = function() {
			
			var indicator = 	$("<div>")	.attr("id", "article-overscroll-area-level")
		   									.append($("<div>").attr("id","article-overscroll-area-level-indicator"))
		   									.append($("<div>")
		   												.attr("id","article-overscroll-area-level-text")
		   												.html("zur <br/> Startseite")
		   												.on("click", function(){
		   																	document.location.href = '/#ref=article-overscroll-click';
		   																	$("#article-overscroll-area-level").addClass("active");
		   															 })
		   											);

			ao.div.append(indicator);
			ao.indicatorheight = indicator.outerHeight();
			ao.indicatorInnerHeight = indicator.height();


			
			$(window).unbind('scroll.spArticleOverscroll');
			window.setTimeout(function() {
				$(window).bind("scroll.spArticleOverscroll", ao.checkoverscroll);
				ao.activated = true;
				ao.adjustposition('activate');
			}, 200);
			
		}

		this.checkoverscroll = function() {
			var start 	= ao.div.offset().top-ao.indicatorheight/2;
			var bottom = $(window).scrollTop() + $(window).height();
			var go 		= ao.div.offset().top + ao.div.height();
			
			if(bottom > start){
				var indicate = -195 + ((bottom - start) / ao.div.height()) * ao.indicatorInnerHeight;
				ao.div.find("#article-overscroll-area-level-indicator").css("background-position", "0 " + indicate + "px");
				if(bottom>start+ao.indicatorheight+20) {
					var leveltop = $(window).height()-ao.indicatorheight-10;
					ao.div.addClass("sticky");
					ao.div.find("#article-overscroll-area-level").css("top", leveltop + "px");
				} else {
					ao.div.removeClass("sticky");	
					ao.div.find("#article-overscroll-area-level").css("top", "-"+(ao.indicatorheight/2)+"px");
				}
		    
				if (bottom >= go) {
					$(window).unbind('scroll.spArticleOverscroll');
					ao.div.find("#article-overscroll-area-level-indicator").css("background-position", "0 0");
					$("#article-overscroll-area-level").addClass("active");
					
					document.location.href = '/#ref=article-overscroll';
				}			
			} else {
				ao.div.removeClass("sticky");	
				ao.div.find("#article-overscroll-area-level").css("top", "-"+(ao.indicatorheight/2)+"px");
				ao.div.find("#article-overscroll-area-level-indicator").css("background-position", "0 -200px");
		    }
		};
		
		ao.init();
	}
}

$(document).ready(function(){
	// 2015-04-16, Bug 38258, deactivate, dynamicDates();

	/* markenpartner */
	$('.markenpartner-close').click(function(){
		$('.markenpartner-info').toggle();
		/* $('.markenpartner-infolink').show(); */
		return false;
	});
	    
	$('.markenpartner-infolink').click(function(){
		$('.markenpartner-info').toggle();
		/* $('.markenpartner-infolink').hide(); */
		return false; 
	});
	
	//  
	if("ontouchstart" in document.documentElement) {
		$('div.social-button-more').on('click', function(e) {
			$(this).parent().children('.social-bar-expandx').show();
		});
		$('.social-bar-expandx').on('click', function(e) {
			$('.social-bar-expandx').hide();
		});
	}
	else {
		$('div.social-button-more').mouseenter(function() {
			$(this).parent().children('.social-bar-expandx').fadeIn("fast");
		});
		$('.social-bar-expandx').mouseleave(function() {
			$('.social-bar-expandx').hide();
		});
		$('div.social-button-more').on('click', function() {
			$(this).parent().children('.social-bar-expandx').show();
		});
		$('.social-bar-expandx').on('click', function() {
			$('.social-bar-expandx').hide();
		});
	}
	$('a.social-button-facebook').on('click', function(e) {
		e.preventDefault();
		popup_window( $(this).attr('href'),650,425,0,0 );
	});
	$('a.social-button-xing').on('click', function(e) {
		e.preventDefault();
		popup_window( $(this).attr('href'),650,425,0,0 );
	});
	$('a.social-button-twitter').on('click', function(e) {
		e.preventDefault();
		popup_window( $(this).attr('href'),550,485,0,0 );
	});
	$('a.social-button-gplus').on('click', function(e) {
		e.preventDefault();
		popup_window( $(this).attr('href'),600,400,0,0 );
	});
	$('a.social-button-linkedin').on('click', function(e) {
		e.preventDefault();
		popup_window( $(this).attr('href'),600,400,0,0 );
	});
	$('a.social-button-tumblr').on('click', function(e) {
		e.preventDefault();
		popup_window( $(this).attr('href'),600,400,0,0 );
	});
	$('a.social-button-pinterest').on('click', function(e) {
		e.preventDefault();
		var pinterest_url = $(this).attr('href');
		var og_image_url = $('meta[property=og\\:image]').attr('content');
		if(og_image_url) {
			pinterest_url = pinterest_url.replace(/media=.*&description=/, 'media='+og_image_url+'&description=');
		}
		popup_window(pinterest_url ,750,600,0,0 );
	});

	
	//  
	mmDoOutbrainRecommendations();
	
	// Article-Overscroll aktivieren
	var articleoverscroll = new spArticleOverscroll();
	
}); /* ready */

function onPreRenderCallback(renderSlot, bannerDescriptionAPI, renderControlAPI) {
	var options;
	var htxt = '';
	var wcss = '';
	options = bannerDescriptionAPI.getOptions();
	if (options.wrapper && parseInt(options.wrapper) > 0) {
		$.each( options, function( k, v ) {
			if (k == 'header_text')  { htxt = v; }
			if (k == 'wrapad_class') { wcss = v; }
		});
		pushWrapAdDIVs(renderControlAPI, renderSlot.getAttribute('id'), options.wrapad_class, options.header_text);
	}
}


function pushWrapAdDIVs(renderControlAPI, slotName, customWrapAdCssClass, customAdLabel) {
	var wrapAdClasses;
	wrapAdClasses = ['wrapad'];
	var innerWrapAdClasses;
	innerWrapAdClasses = ['innerwrapad'];
	if (slotName !== undefined) {
		wrapAdClasses.push(slotName);
	}
	if (customWrapAdCssClass !== undefined) {
		wrapAdClasses.push(customWrapAdCssClass);
	}
	if (customAdLabel == undefined) {
		innerWrapAdClasses.push('qc-nolabel');
	}
	renderControlAPI.addInnerWrapper(document.createElement('div')).className = wrapAdClasses.join(' ');
	renderControlAPI.addInnerWrapper(document.createElement('div')).className = innerWrapAdClasses.join(' ');
}


function OAS_RICH(pos) {}


var XADI = function(dsblSwitch) {
	var $this = this;
	this.aobj = null;
	this.dsblSwitch = dsblSwitch;
	this.pvars  = {};
	this.suppress  = {};
	this.campaigns = {};
	this.message = function(messagetype, message) {};
	this.debug = false;
	this.showpos = false;
	this.previewprofile = null;
	this.blocked = false;

	var dsabl_sad = {};
	dsabl_sad.hp = typeof sp_webcfg_global.dsabl_sad_hp != 'undefined' ? sp_webcfg_global.dsabl_sad_hp : false;
	dsabl_sad.all = typeof sp_webcfg_global.dsabl_sad != 'undefined' ? sp_webcfg_global.dsabl_sad : false;
	
	this.disabled = function(pos) {
		if (dsabl_sad.all) {
			return "alle Positionen unterdrueckt (GlobalConfig all)!";
		} else 	if (dsblSwitch && dsblSwitch == 'noads') {
			return "alle Positionen unterdrueckt (setup noads)!";
		} else if (dsblSwitch && typeof dsabl_sad[dsblSwitch] != 'undefined' && dsabl_sad[dsblSwitch]) {    	  
			return "alle Positionen unterdrueckt (GlobalConfig switch '"+dsblSwitch+"')!";	
		} else if (pos && $this.suppress[pos]) {    	  
			return "Position '"+pos+"' unterdrueckt (Flags)!";
		} 
		return null;
	};

	this.setSuppressed = function(mode) {
		if (mode == 1) {
			
			ADI.suppress = {
				"content_ad_1": true, "content_ad_2": true, "content_ad_3": true, "intext_video": true
			};
		} else if (mode == 2) {
			ADI.suppress = {
				"layer_ad": true, "superbanner": true, "aaa_skyscraper_left": true, "skyscraper_right": true, "billboard": true, "header_ad": true,
				"content_ad_1": true, "content_ad_2": true, "content_ad_3": true, "intext_video": true, "subchannel_teaser": true, "subchannel_advertorial": true,
				"promotion_ad_1": true, "promotion_ad_2": true, "promotion_ad_3": true, "promotion_ad_4": true, "promotion_ad_5": true,
				"promotion_ad_vertrieb": true, "footer_ad": true
			};
		}
	};

	this.ad = function(pos) {
		var content = this.disabled(pos);
		if (content === null) {
			if (typeof $this.campaigns[pos] == 'undefined') {
				content = $this.message("error", "Position '"+pos+"' undefiniert!");
				if ($this.showpos) { $('#'+pos).addClass('adition-error'); }
			} else {
				if (typeof adition != 'undefined') {
					if ($this.showpos != '1') {
						adition.srq.push(function(api) { api.renderSlot(pos); });
					}
					if ($this.showpos) { $('#'+pos).addClass('adition-ok'); }
				} else { 
					if ($this.debug) { content = $this.message("error", 'WARNING: adition is ' + typeof adition); }
				}
			}
		} else {
			content = $this.message("info", content);
			if ($this.showpos) { $('#'+pos).addClass('adition-suppressed'); }
		}
		if (content) document.write(content);
		if ($this.showpos) $('#'+pos).addClass('adition-showpos');
	};
	
	this.write = function(pos) {
		document.write('<div class="adition" id="'+pos+'" data-position="'+pos+'"><scr' + 'ipt type="text/javascript">ADI.ad("'+pos+'");</scr' + 'ipt></div>');
		if ($this.debug) {
			 // var m=ADI.message("error",'WARNING: ADI.write deprecated (Migrationsaufruf aus HTML-Asset?)'); 
			 // if(m) document.write(m);
		}
	};

	this.setup = function(aobj) {
		// initiales Laden - nur, wenn Not-Aus nicht aktiv
		$this.aobj = (typeof aobj != 'undefined' ? aobj : null);
		$this.blocked = ($this.aobj === null);

		var hash = new HashParams();
		$this.previewprofile = hash.value("adition.preview");
	};

	this.campaignsToRender = function() {
		// quickfix: Kampagnen alphanumerisch sortiert initialisieren
		var sorted = [],
			keys   = [],
			k, i, len;

		for (k in $this.campaigns) {
			if (k === 'preroll' || k === 'postroll') continue;
			if ($this.campaigns.hasOwnProperty(k)) keys.push(k);
		}
		keys.sort();
		len = keys.length;
		for (i = 0; i < len; i++) {
			k = keys[i];
			sorted.push({name: k, value: $this.campaigns[k]});
		}
		return sorted;
	};
};
function mmMSisLoggedIn()  {
	// mmGetCookie('DNLOGIN');
	return mmGetCookie('boLIstatus') || (mmGetCookie('digasnet.cookie.loginname') && mmGetCookie('digasnet.cookie.passwd'));
}

function mmMSLogin(openerObject, backUrl, feature)  {
	var framelocation = spnetENV_SERVER_SSL;
	var pagename = "login.html"
	framelocation += '/meinmm/';
	if (backUrl == null) {
		if (feature != null) {
			var backUrl = framelocation + feature + '.html';
			if ( mmMSisLoggedIn() )	{
				top.location.href = backUrl;
			} else {
				framelocation += pagename + '?feature=' + feature + '&backUrl=' + framelocation + feature + '.html';
			}
		} else {
			framelocation += pagename;
		}
	} else {
		framelocation += pagename + '?backUrl=' + escape(backUrl);
	}
	document.location.href = framelocation;
	return false;
}



function mmWriteMSLoginLinks() {
	if ( mmMSisLoggedIn() )	{
		var logOutHref = spnetENV_SERVER_SSL + '/meinmm/logout.html?backUrl=' + escape(window.location.href);
		$(".meinmmo-link .inout").attr("href", logOutHref).text('Abmelden').removeAttr('onclick');
		$(".meinmmo-link").removeClass('login').addClass('is-logged-in');
		$(".meinmmo-link").prepend('<a class="meinmm" href="/meinmm">Mein manager magazin</a>');
	} else {
		var logInHref = spnetENV_SERVER_SSL + "/meinmm/index.html";
		$(".meinmmo-link .inout").attr("href", logInHref).attr('onclick', 'mmMSLogin(); return false;').text('Login');
		$(".meinmmo-link").removeClass('is-logged-in').addClass('login');
		$(".meinmmo-link .meinmm").remove();
	}
}

/**
 * funktion, die benutzt wird um die Passwortstärke bei Registrierung und Passwort-Aendern Dialogen anzuzeigen 
 * @param pwd
 * @returns {Boolean}
 */
function showStrength(pwd)
{	 							
	var result = zxcvbn(pwd);
	if (pwd.length > 0)
	{
		// score 0 bis 4
		var strengthText;
		switch (result.score)
		{
			case 0: strengthText = 'Sehr unsicher'; break
			case 1: strengthText = 'unsicher'; break
			case 2: strengthText = 'mittelm&auml;&szlig;ig'; break
			case 3: strengthText = 'sicher'; break
			case 4: strengthText = 'Sehr sicher'; break
		}

		$("#passwordStrengthBar").removeClass("bg0 bg1 bg2 bg3 bg4").addClass(function(index) {return "bg"+result.score;});
		$("#strengthText").html( strengthText );
	} else {
		$("#passwordStrengthBar").attr("class", "strengthBar bg0");
		$("#strengthText").html("");
	}
		 		
	return false;
}



//Standard-Event-Login
function mmEventLoggedIn(){
	// if (typeof console != "undefined" && typeof console.log != "undefined") console.log("Standard-loggedIn");
	mmWriteMSLoginLinks();
	spnetClosePopupLayer();
}

//ready
$(document).ready(function() {
	$(document).bind('mmLoggedIn', mmEventLoggedIn);
});function mmSynchDatumBis(FORM)
{
    if (FORM.suchzeitraum.selectedIndex == 0)
    {
        // Nichts tun;
    }
    else
    {   
    	// - Bis-Datum aus Rollo-Wert errechnen;
        FORM.mmSearchFromDate.value = getDatumFromKey(FORM.suchzeitraum.options[FORM.suchzeitraum.selectedIndex].value);
        FORM.mmSearchToDate.value = getDatumBisKey(FORM.suchzeitraum.options[FORM.suchzeitraum.selectedIndex].value);
        if (FORM.suchzeitraum.options[FORM.suchzeitraum.selectedIndex].value == "all")
        {
            FORM.mmSearchToDate.value = "";
        }
    }
}



function getDatumBisKey(key)
{
    var ret = "";

    switch (key)
            {
        case ""         : ret = "";               break;
        case "all"      : ret = "";               break;
        case "2005"     : ret = "31.12.2004";     break;
        case "2000"     : ret = "31.12.1999";     break;
        case "1995"     : ret = "31.12.1994";     break;
        case "1990"     : ret = "31.12.1989";     break;
        case "1980"     : ret = "31.12.1979";     break;
        case "1970"     : ret = "31.12.1969";     break;
        case "1960"     : ret = "31.12.1959";     break;
        default         : ret = "";               break; // tt.mm.jjjj;
    }

    return ret;
}

function getDatumBisKeyInternational(key)
{
    var ret = "";

    switch (key)
            {
        case ""           : ret = "";             break;
        case "all"      : ret = "";             break;
        case "2005"      : ret = "12/31/2004";     break;
        case "2000"       : ret = "12/31/1999";     break;
        case "1995"     : ret = "12/31/1994";     break;
        case "1990"     : ret = "12/31/1989";     break;
        case "1980"     : ret = "12/31/1979";     break;
        case "1970"     : ret = "12/31/1969";     break;
        case "1960"     : ret = "12/31/1959";     break;
        default           : ret = "";             break; // tt.mm.jjjj;
    }

    return ret;
}

function getDatumFromKey(key)
{
    var ret = "";

    switch (key)
            {
        case ""           : ret = "";             break;
        case "week"   : ret = datum7Tage;     break;
        case "month"   : ret = datum30Tage;     break;
        case "year"      : ret = datum1Jahr;     break;
        case "ab2005"      : ret = "01.01.2005";     break;
        default           : ret = "";         break; // tt.mm.jjjj;
    }

    return ret;
}

function getDatumFromKeyInternational(key)
{
    var ret = "";

    switch (key)
            {
        case ""           : ret = "";             break;
        case "week"   : ret = datum7TageInternational;     break;
        case "month"   : ret = datum30TageInternational;     break;
        case "year"      : ret = datum1JahrInternational;     break;
        case "ab2005"      : ret = "01/01/2005";     break;
        default           : ret = "";         break; // tt.mm.jjjj;
    }

    return ret;
}

// -------------------------------------
// value-Werte fuer DATUM_ROLLO-Optionen;
var datumHeute = "";
var datumGestern = "";
var datum7Tage = "";
var datum7TageInternational = "";
var datum30Tage = "";
var datum30TageInternational = "";
var datum90Tage = "";
var datum1Jahr = "";
var datum1JahrInternational = "";
var datum2Jahre = "";
var datum3Jahre = "";

makeValues_Datum_Rollo();
//-------------------------------------


function makeValues_Datum_Rollo()
{
    // --- option-value-Werte fuer Rollo vb bzw. DATUM_ROLLO;
    var dateNow = new Date();                  // Datum-heute, Typ Date;
    var milliNow = dateNow.getTime();           // Datum-heute in Millisek. seit Computers Geburt;

    var dateX = new Date();                  // Hilfsvar Typ Date;
    var milliX = 0;

    milliX = milliNow - 1000 * 60 * 60 * 24;    // vor 1 Tag: 7Tage*24Std*60min*60sec*1000ms;
    dateX.setTime(milliX);                  // Date-Objekt;
    datumGestern = date2datumAnzeige(dateX); // 'tt.mm.iijj';

    milliX = milliNow - 1000 * 60 * 60 * 24 * 7;    // vor 7 Tagen: 7Tage*24Std*60min*60sec*1000ms;
    dateX.setTime(milliX);                  // Date-Objekt;
    datum7Tage = date2datumAnzeige(dateX); // 'tt.mm.iijj';
    datum7TageInternational = date2datumAnzeigeInternational(dateX); // 'tt.mm.iijj';

    milliX = milliNow - 1000 * 60 * 60 * 24 * 30;   // vor 30 Tagen;
    dateX.setTime(milliX);                  // Date-Objekt;
    datum30Tage = date2datumAnzeige(dateX); // 'tt.mm.iijj';
    datum30TageInternational = date2datumAnzeigeInternational(dateX); // 'tt.mm.iijj';

    milliX = milliNow - 1000 * 60 * 60 * 24 * 90;   // vor 90 Tagen;
    dateX.setTime(milliX);                  // Date-Objekt;
    datum90Tage = date2datumAnzeige(dateX); // 'tt.mm.iijj';

    var yearNow = dateNow.getYear();    // numer. iijj oder jj;
    if (yearNow < 1900)
        yearNow += 1900;                  // num iijj;

    var monthNow = dateNow.getMonth() + 1;
    if (monthNow < 10)
        monthNow = "0" + monthNow;        // mm;

    var dayNow = dateNow.getDate();
    if (dayNow < 10)
        dayNow = "0" + dayNow;            // tt;

    var monthLastYear = "" + monthNow;
    var dayLastYear = "" + dayNow;

    if (monthNow == "02" && dayNow == "29")
    {
        monthLastYear = "03";
        dayLastYear = "01";
    }

    datumHeute = dayNow + "." + monthNow + "." + yearNow;
    datum1Jahr = dayLastYear + "." + monthLastYear + "." + (yearNow - 1);
    datum1JahrInternational = monthLastYear + "/" + dayLastYear + "/" + (yearNow - 1);
    datum2Jahre = dayLastYear + "." + monthLastYear + "." + (yearNow - 2);
    datum3Jahre = dayLastYear + "." + monthLastYear + "." + (yearNow - 3);
}

function date2datumAnzeige(dateObj)

    // Rein: Objekt, das mit dateObj = new Date(); def. wurde;
    // Raus: String 'tt.mm.iijj' fuer FROM/TO_DATE_DISPLAY;
{
    var yearDisplay = dateObj.getYear();
    if (yearDisplay < 1900) yearDisplay += 1900;

    var monthDisplay = dateObj.getMonth() + 1;
    if (monthDisplay < 10) monthDisplay = "0" + monthDisplay;

    var dayDisplay = dateObj.getDate();
    if (dayDisplay < 10) dayDisplay = "0" + dayDisplay;

    var dateDisplay = dayDisplay + "." + monthDisplay + "." + yearDisplay;
    return dateDisplay;
}

function date2datumAnzeigeInternational(dateObj)

    // Rein: Objekt, das mit dateObj = new Date(); def. wurde;
    // Raus: String 'tt.mm.iijj' fuer FROM/TO_DATE_DISPLAY;
{
    var yearDisplay = dateObj.getYear();
    if (yearDisplay < 1900) yearDisplay += 1900;

    var monthDisplay = dateObj.getMonth() + 1;
    if (monthDisplay < 10) monthDisplay = "0" + monthDisplay;

    var dayDisplay = dateObj.getDate();
    if (dayDisplay < 10) dayDisplay = "0" + dayDisplay;

    var dateDisplay = monthDisplay + "/" + dayDisplay + "/" + yearDisplay;
    return dateDisplay;
}

// -------------------------------------------------------------------------------------------------

$(document).ready(function(){

	/* this is searching in the top bar (in the header) */
	$('#search-submit').click(function() {
		var $form = $('#searchform');

		if ( $('#search-type-2').is(':checked') ) {
			$form.attr('action', 'http://boersen.manager-magazin.de/mm/kurse_einzelkurs_suche.htn?Button=Alles&sektion=suche')
				.attr('target', 'Boerse');
		} else {
			$form.attr('action', spnetENV_SERVER + "/suche/index.html")
				.attr('target', '_top');
		}
		
		if( $('#search-term').attr('value') == 'Suche') {
			$('#search-term').attr('value', '');
		}	
		
		$form.submit();
	});

	$('.toolbar-search-submit').click(function() {
		var $form = $('#toolbar-searchform');
		$form.attr('action', spnetENV_SERVER + "/suche/index.html").attr('target', '_top');
		if( $('#toolbar-search-term').attr('value') == 'Suche') {
			$('#toolbar-search-term').attr('value', '');
		}	
		$form.submit();
	});
});

var mmTmpImgSetToLoad;
// Muss ausserhalb des spEnhPaginators stehen, da per timeout aufgerufen;
function mmLoadDeferImgSet(prefix) {
    for (var i=0; i < mmTmpImgSetToLoad.length; i++) {
            var imgElement=document.getElementById(prefix + i);
            if (imgElement != null)
                    imgElement.src = mmTmpImgSetToLoad[i];
    }
}


var mmEnhPaginator = function(paginatorId, imageSets) {
	this.pages=[];
	this.imageSets=imageSets;
	this.completedImageSets=(imageSets != null) ? new Array(imageSets.length) : null;
	this.index=0;
	this.paginatorId=paginatorId;
	this.imageSetToLoad=null;
	this.currentPage=null;
	this.initDone=false;
	this.onChangePage=null;
}

mmEnhPaginator.prototype = {
	checkInit: function() {
		if (!this.initDone) {
			
			var container=document.getElementById(this.paginatorId);
			var pagesTmp=container.getElementsByTagName('ul');
			for (key=0; key < pagesTmp.length; key++)
				if (pagesTmp[key].className == 'paginator-page') {
					this.pages.push(pagesTmp[key]);
				}

			this.currentPage=this.pages[this.index];
			this.initDone=true;
		}
	},
	showNext: function(element) {
		this.checkInit();
		var oldIndex = this.index++;
		if (this.index >= this.pages.length)
			this.index = 0;

		if (this.onChangePage != null)
			this.onChangePage(this.pages[oldIndex]);
		this.switchToNewIndex();
	},
	showPrev: function(element) {
		this.checkInit();
		var oldIndex = this.index--;
		if (this.index < 0)
			this.index = this.pages.length - 1;

		if (this.onChangePage != null)
			this.onChangePage(this.pages[oldIndex]);

		this.switchToNewIndex();
	},
	showNum: function(element) {
		this.checkInit();
		var oldIndex = this.index;
		this.index = arguments[0];
		if (this.index < 0)
			this.index = this.pages.length - 1;
		if (this.onChangePage != null)
			this.onChangePage(this.pages[oldIndex]);
		this.switchToNewIndex();
	},		
	switchToNewIndex: function(newPage) {
		this.currentPage.style.display='none';
		this.currentPage=this.pages[this.index];
		this.currentPage.style.display='block';
		this.checkLoadImages();
	},
	checkLoadImages: function() {
		if (this.imageSets != null) {
			var imageSetIndex=this.index-1;
			if (this.imageSets[imageSetIndex] != null && !this.completedImageSets[imageSetIndex]) {
				mmTmpImgSetToLoad=this.imageSets[imageSetIndex];
				window.setTimeout("mmLoadDeferImgSet('" + this.paginatorId + imageSetIndex + "')", 20);
				this.completedImageSets[imageSetIndex]=true;
			}
		}
	}
};window.jwplayer=function(e){function t(n){if(i[n])return i[n].exports;var r=i[n]={exports:{},id:n,loaded:!1};return e[n].call(r.exports,r,r.exports,t),r.loaded=!0,r.exports}var n=window.webpackJsonpjwplayer;window.webpackJsonpjwplayer=function(i,o){for(var a,s,l=0,u=[];l<i.length;l++)s=i[l],r[s]&&u.push.apply(u,r[s]),r[s]=0;for(a in o)e[a]=o[a];for(n&&n(i,o);u.length;)u.shift().call(null,t)};var i={},r={0:0};return t.e=function(e,n){if(0===r[e])return n.call(null,t);if(void 0!==r[e])r[e].push(n);else{r[e]=[n];var i=document.getElementsByTagName("head")[0],o=document.createElement("script");o.type="text/javascript",o.charset="utf-8",o.async=!0,o.src=t.p+""+({1:"provider.hlsjs",2:"provider.shaka",3:"provider.cast",4:"provider.html5",5:"provider.flash",6:"provider.youtube",7:"polyfills.vttrenderer",8:"polyfills.promise",9:"polyfills.intersection-observer",10:"polyfills.base64",11:"vttparser"}[e]||e)+".js",i.appendChild(o)}},t.m=e,t.c=i,t.p="",t(0)}([function(e,t,n){e.exports=n(168)},function(e,t,n){var i,r;i=[],r=function(){var e={},t=Array.prototype,n=Object.prototype,i=Function.prototype,r=t.slice,o=t.concat,a=n.toString,s=n.hasOwnProperty,l=t.map,u=t.reduce,c=t.forEach,d=t.filter,p=t.every,h=t.some,f=t.indexOf,g=Array.isArray,m=Object.keys,w=i.bind,v=function(e){return e instanceof v?e:this instanceof v?void 0:new v(e)},y=v.each=v.forEach=function(t,n,i){if(null==t)return t;if(c&&t.forEach===c)t.forEach(n,i);else if(t.length===+t.length){for(var r=0,o=t.length;r<o;r++)if(n.call(i,t[r],r,t)===e)return}else for(var a=v.keys(t),r=0,o=a.length;r<o;r++)if(n.call(i,t[a[r]],a[r],t)===e)return;return t};v.map=v.collect=function(e,t,n){var i=[];return null==e?i:l&&e.map===l?e.map(t,n):(y(e,function(e,r,o){i.push(t.call(n,e,r,o))}),i)};var j="Reduce of empty array with no initial value";v.reduce=v.foldl=v.inject=function(e,t,n,i){var r=arguments.length>2;if(null==e&&(e=[]),u&&e.reduce===u)return i&&(t=v.bind(t,i)),r?e.reduce(t,n):e.reduce(t);if(y(e,function(e,o,a){r?n=t.call(i,n,e,o,a):(n=e,r=!0)}),!r)throw new TypeError(j);return n},v.find=v.detect=function(e,t,n){var i;return b(e,function(e,r,o){if(t.call(n,e,r,o))return i=e,!0}),i},v.filter=v.select=function(e,t,n){var i=[];return null==e?i:d&&e.filter===d?e.filter(t,n):(y(e,function(e,r,o){t.call(n,e,r,o)&&i.push(e)}),i)},v.reject=function(e,t,n){return v.filter(e,function(e,i,r){return!t.call(n,e,i,r)},n)},v.compact=function(e){return v.filter(e,v.identity)},v.every=v.all=function(t,n,i){n||(n=v.identity);var r=!0;return null==t?r:p&&t.every===p?t.every(n,i):(y(t,function(t,o,a){if(!(r=r&&n.call(i,t,o,a)))return e}),!!r)};var b=v.some=v.any=function(t,n,i){n||(n=v.identity);var r=!1;return null==t?r:h&&t.some===h?t.some(n,i):(y(t,function(t,o,a){if(r||(r=n.call(i,t,o,a)))return e}),!!r)};v.size=function(e){return null==e?0:e.length===+e.length?e.length:v.keys(e).length},v.last=function(e,t,n){if(null!=e)return null==t||n?e[e.length-1]:r.call(e,Math.max(e.length-t,0))},v.after=function(e,t){return function(){if(--e<1)return t.apply(this,arguments)}},v.before=function(e,t){var n;return function(){return--e>0&&(n=t.apply(this,arguments)),e<=1&&(t=null),n}};var E=function(e){return null==e?v.identity:v.isFunction(e)?e:v.property(e)},x=function(e){return function(t,n,i){var r={};return n=E(n),y(t,function(o,a){var s=n.call(i,o,a,t);e(r,s,o)}),r}};v.groupBy=x(function(e,t,n){v.has(e,t)?e[t].push(n):e[t]=[n]}),v.indexBy=x(function(e,t,n){e[t]=n}),v.sortedIndex=function(e,t,n,i){n=E(n);for(var r=n.call(i,t),o=0,a=e.length;o<a;){var s=o+a>>>1;n.call(i,e[s])<r?o=s+1:a=s}return o};var b=v.some=v.any=function(t,n,i){n||(n=v.identity);var r=!1;return null==t?r:h&&t.some===h?t.some(n,i):(y(t,function(t,o,a){if(r||(r=n.call(i,t,o,a)))return e}),!!r)};v.contains=v.include=function(e,t){return null!=e&&(e.length!==+e.length&&(e=v.values(e)),v.indexOf(e,t)>=0)},v.pluck=function(e,t){return v.map(e,v.property(t))},v.where=function(e,t){return v.filter(e,v.matches(t))},v.findWhere=function(e,t){return v.find(e,v.matches(t))},v.max=function(e,t,n){if(!t&&v.isArray(e)&&e[0]===+e[0]&&e.length<65535)return Math.max.apply(Math,e);var i=-(1/0),r=-(1/0);return y(e,function(e,o,a){var s=t?t.call(n,e,o,a):e;s>r&&(i=e,r=s)}),i},v.difference=function(e){var n=o.apply(t,r.call(arguments,1));return v.filter(e,function(e){return!v.contains(n,e)})},v.without=function(e){return v.difference(e,r.call(arguments,1))},v.indexOf=function(e,t,n){if(null==e)return-1;var i=0,r=e.length;if(n){if("number"!=typeof n)return i=v.sortedIndex(e,t),e[i]===t?i:-1;i=n<0?Math.max(0,r+n):n}if(f&&e.indexOf===f)return e.indexOf(t,n);for(;i<r;i++)if(e[i]===t)return i;return-1};var k=function(){};v.bind=function(e,t){var n,i;if(w&&e.bind===w)return w.apply(e,r.call(arguments,1));if(!v.isFunction(e))throw new TypeError;return n=r.call(arguments,2),i=function(){if(!(this instanceof i))return e.apply(t,n.concat(r.call(arguments)));k.prototype=e.prototype;var o=new k;k.prototype=null;var a=e.apply(o,n.concat(r.call(arguments)));return Object(a)===a?a:o}},v.partial=function(e){var t=r.call(arguments,1);return function(){for(var n=0,i=t.slice(),r=0,o=i.length;r<o;r++)i[r]===v&&(i[r]=arguments[n++]);for(;n<arguments.length;)i.push(arguments[n++]);return e.apply(this,i)}},v.once=v.partial(v.before,2),v.memoize=function(e,t){var n={};return t||(t=v.identity),function(){var i=t.apply(this,arguments);return v.has(n,i)?n[i]:n[i]=e.apply(this,arguments)}},v.delay=function(e,t){var n=r.call(arguments,2);return setTimeout(function(){return e.apply(null,n)},t)},v.defer=function(e){return v.delay.apply(v,[e,1].concat(r.call(arguments,1)))},v.throttle=function(e,t,n){var i,r,o,a=null,s=0;n||(n={});var l=function(){s=n.leading===!1?0:v.now(),a=null,o=e.apply(i,r),i=r=null};return function(){var u=v.now();s||n.leading!==!1||(s=u);var c=t-(u-s);return i=this,r=arguments,c<=0?(clearTimeout(a),a=null,s=u,o=e.apply(i,r),i=r=null):a||n.trailing===!1||(a=setTimeout(l,c)),o}},v.keys=function(e){if(!v.isObject(e))return[];if(m)return m(e);var t=[];for(var n in e)v.has(e,n)&&t.push(n);return t},v.invert=function(e){for(var t={},n=v.keys(e),i=0,r=n.length;i<r;i++)t[e[n[i]]]=n[i];return t},v.defaults=function(e){return y(r.call(arguments,1),function(t){if(t)for(var n in t)void 0===e[n]&&(e[n]=t[n])}),e},v.extend=function(e){return y(r.call(arguments,1),function(t){if(t)for(var n in t)e[n]=t[n]}),e},v.pick=function(e){var n={},i=o.apply(t,r.call(arguments,1));return y(i,function(t){t in e&&(n[t]=e[t])}),n},v.omit=function(e){var n={},i=o.apply(t,r.call(arguments,1));for(var a in e)v.contains(i,a)||(n[a]=e[a]);return n},v.clone=function(e){return v.isObject(e)?v.isArray(e)?e.slice():v.extend({},e):e},v.isArray=g||function(e){return"[object Array]"==a.call(e)},v.isObject=function(e){return e===Object(e)},y(["Arguments","Function","String","Number","Date","RegExp"],function(e){v["is"+e]=function(t){return a.call(t)=="[object "+e+"]"}}),v.isArguments(arguments)||(v.isArguments=function(e){return!(!e||!v.has(e,"callee"))}),v.isFunction=function(e){return"function"==typeof e},v.isFinite=function(e){return isFinite(e)&&!isNaN(parseFloat(e))},v.isNaN=function(e){return v.isNumber(e)&&e!=+e},v.isBoolean=function(e){return e===!0||e===!1||"[object Boolean]"==a.call(e)},v.isNull=function(e){return null===e},v.isUndefined=function(e){return void 0===e},v.has=function(e,t){return s.call(e,t)},v.identity=function(e){return e},v.constant=function(e){return function(){return e}},v.property=function(e){return function(t){return t[e]}},v.propertyOf=function(e){return null==e?function(){}:function(t){return e[t]}},v.matches=function(e){return function(t){if(t===e)return!0;for(var n in e)if(e[n]!==t[n])return!1;return!0}},v.now=Date.now||function(){return(new Date).getTime()},v.result=function(e,t){if(null!=e){var n=e[t];return v.isFunction(n)?n.call(e):n}};var A=0;return v.uniqueId=function(e){var t=++A+"";return e?e+t:t},v}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(8),n(1),n(27),n(32),n(23),n(33),n(14),n(60),n(34),n(63),n(141),n(140)],r=function(e,t,n,i,r,o,a,s,l,u,c,d){var p={};return p.log=function(){window.console&&("object"==typeof console.log?console.log(Array.prototype.slice.call(arguments,0)):console.log.apply(console,arguments))},p.between=function(e,t,n){return Math.max(Math.min(e,n),t)},p.foreach=function(e,t){var n,i;for(n in e)"function"===p.typeOf(e.hasOwnProperty)?e.hasOwnProperty(n)&&(i=e[n],t(n,i)):(i=e[n],t(n,i))},p.indexOf=t.indexOf,p.noop=function(){},p.seconds=e.seconds,p.prefix=e.prefix,p.suffix=e.suffix,t.extend(p,o,a,l,n,s,i,r,u,c,d),p}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){var e={DRAG:"drag",DRAG_START:"dragStart",DRAG_END:"dragEnd",CLICK:"click",DOUBLE_CLICK:"doubleClick",TAP:"tap",DOUBLE_TAP:"doubleTap",OVER:"over",MOVE:"move",OUT:"out"},t={COMPLETE:"complete",ERROR:"error",JWPLAYER_AD_CLICK:"adClick",JWPLAYER_AD_COMPANIONS:"adCompanions",JWPLAYER_AD_COMPLETE:"adComplete",JWPLAYER_AD_ERROR:"adError",JWPLAYER_AD_IMPRESSION:"adImpression",JWPLAYER_AD_META:"adMeta",JWPLAYER_AD_PAUSE:"adPause",JWPLAYER_AD_PLAY:"adPlay",JWPLAYER_AD_SKIPPED:"adSkipped",JWPLAYER_AD_TIME:"adTime",JWPLAYER_CAST_AD_CHANGED:"castAdChanged",JWPLAYER_MEDIA_COMPLETE:"complete",JWPLAYER_READY:"ready",JWPLAYER_MEDIA_SEEK:"seek",JWPLAYER_MEDIA_BEFOREPLAY:"beforePlay",JWPLAYER_MEDIA_BEFORECOMPLETE:"beforeComplete",JWPLAYER_MEDIA_BUFFER_FULL:"bufferFull",JWPLAYER_DISPLAY_CLICK:"displayClick",JWPLAYER_PLAYLIST_COMPLETE:"playlistComplete",JWPLAYER_CAST_SESSION:"cast",JWPLAYER_MEDIA_ERROR:"mediaError",JWPLAYER_MEDIA_FIRST_FRAME:"firstFrame",JWPLAYER_MEDIA_PLAY_ATTEMPT:"playAttempt",JWPLAYER_MEDIA_LOADED:"loaded",JWPLAYER_MEDIA_SEEKED:"seeked",JWPLAYER_SETUP_ERROR:"setupError",JWPLAYER_ERROR:"error",JWPLAYER_PLAYER_STATE:"state",JWPLAYER_CAST_AVAILABLE:"castAvailable",JWPLAYER_MEDIA_BUFFER:"bufferChange",JWPLAYER_MEDIA_TIME:"time",JWPLAYER_MEDIA_TYPE:"mediaType",JWPLAYER_MEDIA_VOLUME:"volume",JWPLAYER_MEDIA_MUTE:"mute",JWPLAYER_MEDIA_META:"meta",JWPLAYER_MEDIA_LEVELS:"levels",JWPLAYER_MEDIA_LEVEL_CHANGED:"levelsChanged",JWPLAYER_CONTROLS:"controls",JWPLAYER_FULLSCREEN:"fullscreen",JWPLAYER_RESIZE:"resize",JWPLAYER_PLAYLIST_ITEM:"playlistItem",JWPLAYER_PLAYLIST_LOADED:"playlist",JWPLAYER_AUDIO_TRACKS:"audioTracks",JWPLAYER_AUDIO_TRACK_CHANGED:"audioTrackChanged",JWPLAYER_LOGO_CLICK:"logoClick",JWPLAYER_CAPTIONS_LIST:"captionsList",JWPLAYER_CAPTIONS_CHANGED:"captionsChanged",JWPLAYER_PROVIDER_CHANGED:"providerChanged",JWPLAYER_PROVIDER_FIRST_FRAME:"providerFirstFrame",JWPLAYER_USER_ACTION:"userAction",JWPLAYER_PROVIDER_CLICK:"providerClick",JWPLAYER_VIEW_TAB_FOCUS:"tabFocus",JWPLAYER_CONTROLBAR_DRAGGING:"scrubbing",JWPLAYER_INSTREAM_CLICK:"instreamClick"};return t.touchEvents=e,t}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){var t=[],n=t.slice,i={on:function(e,t,n){if(!o(this,"on",e,[t,n])||!t)return this;this._events||(this._events={});var i=this._events[e]||(this._events[e]=[]);return i.push({callback:t,context:n}),this},once:function(t,n,i){if(!o(this,"once",t,[n,i])||!n)return this;var r=this,a=e.once(function(){r.off(t,a),n.apply(this,arguments)});return a._callback=n,this.on(t,a,i)},off:function(t,n,i){var r,a,s,l,u,c,d,p;if(!this._events||!o(this,"off",t,[n,i]))return this;if(!t&&!n&&!i)return this._events=void 0,this;for(l=t?[t]:e.keys(this._events),u=0,c=l.length;u<c;u++)if(t=l[u],s=this._events[t]){if(this._events[t]=r=[],n||i)for(d=0,p=s.length;d<p;d++)a=s[d],(n&&n!==a.callback&&n!==a.callback._callback||i&&i!==a.context)&&r.push(a);r.length||delete this._events[t]}return this},trigger:function(e){if(!this._events)return this;var t=n.call(arguments,1);if(!o(this,"trigger",e,t))return this;var i=this._events[e],r=this._events.all;return i&&a(i,t,this),r&&a(r,arguments,this),this},triggerSafe:function(e){if(!this._events)return this;var t=n.call(arguments,1);if(!o(this,"trigger",e,t))return this;var i=this._events[e],r=this._events.all;return i&&s(i,t,this,e),r&&s(r,arguments,this,e),this}},r=/\s+/,o=function(e,t,n,i){if(!n)return!0;if("object"==typeof n){for(var o in n)e[t].apply(e,[o,n[o]].concat(i));return!1}if(r.test(n)){for(var a=n.split(r),s=0,l=a.length;s<l;s++)e[t].apply(e,[a[s]].concat(i));return!1}return!0},a=function(e,t,n){var i,r=-1,o=e.length,a=t[0],s=t[1],l=t[2];switch(t.length){case 0:for(;++r<o;)(i=e[r]).callback.call(i.context||n);return;case 1:for(;++r<o;)(i=e[r]).callback.call(i.context||n,a);return;case 2:for(;++r<o;)(i=e[r]).callback.call(i.context||n,a,s);return;case 3:for(;++r<o;)(i=e[r]).callback.call(i.context||n,a,s,l);return;default:for(;++r<o;)(i=e[r]).callback.apply(i.context||n,t);return}},s=function(e,t,n,i){for(var r,o=-1,a=e.length;++o<a;)try{(r=e[o]).callback.apply(r.context||n,t)}catch(s){console.log('Error in "'+i+'" event handler:',s)}};return i}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){return{BUFFERING:"buffering",IDLE:"idle",COMPLETE:"complete",PAUSED:"paused",PLAYING:"playing",ERROR:"error",LOADING:"loading",STALLED:"stalled"}}.apply(t,i),!(void 0!==r&&(e.exports=r))},,function(e,t,n){var i,r;i=[n(4),n(3),n(1),n(2)],r=function(e,t,n,i){function r(e,t){return/touch/.test(e.type)?(e.originalEvent||e).changedTouches[0]["page"+t]:e["page"+t]}function o(e){var t=e||window.event;return e instanceof MouseEvent&&("which"in t?3===t.which:"button"in t&&2===t.button)}function a(e,t,n){var i;return i=t instanceof MouseEvent||!t.touches&&!t.changedTouches?t:t.touches&&t.touches.length?t.touches[0]:t.changedTouches[0],{type:e,target:t.target,currentTarget:n,pageX:i.pageX,pageY:i.pageY}}function s(e){(e instanceof MouseEvent||e instanceof window.TouchEvent)&&(e.preventManipulation&&e.preventManipulation(),e.preventDefault&&e.preventDefault())}var l=t.touchEvents,u="PointerEvent"in window,c="ontouchstart"in window,d=!(u||c&&i.isMobile()),p=i.isFF()&&i.isOSX(),h=function(e,t){function i(e){"touch"!==e.pointerType&&v(l.OVER,e)}function c(e){"touch"!==e.pointerType&&v(l.MOVE,e)}function h(t){(d||u&&"touch"!==t.pointerType&&!e.contains(document.elementFromPoint(t.x,t.y)))&&v(l.OUT,t)}function f(e,t,n){e.removeEventListener(t,n),e.addEventListener(t,n)}function g(n){y=n.target,x=r(n,"X"),k=r(n,"Y"),o(n)||("pointerdown"===n.type&&n.isPrimary?(t.preventScrolling&&(j=n.pointerId,e.setPointerCapture(j)),f(e,"pointermove",m),f(e,"pointercancel",w),"mouse"===n.pointerType&&"OBJECT"===y.nodeName?f(document,"mouseup",w):f(e,"pointerup",w)):"mousedown"===n.type?(f(document,"mousemove",m),p&&"object"===n.target.nodeName.toLowerCase()?f(e,"click",w):f(document,"mouseup",w)):"touchstart"===n.type&&(f(y,"touchmove",m),f(y,"touchcancel",w),f(y,"touchend",w)),t.preventScrolling&&s(n))}function m(e){var n=6;if(E)v(l.DRAG,e);else{var i=r(e,"X"),o=r(e,"Y"),a=i-x,u=o-k;a*a+u*u>n*n&&(v(l.DRAG_START,e),E=!0,v(l.DRAG,e))}t.preventScrolling&&s(e)}function w(n){var i="pointerup"===n.type||"pointercancel"===n.type;i&&t.preventScrolling&&e.releasePointerCapture(j),e.removeEventListener("pointermove",m),e.removeEventListener("pointercancel",w),e.removeEventListener("pointerup",w),document.removeEventListener("mousemove",m),document.removeEventListener("mouseup",w),y.removeEventListener("touchmove",m),y.removeEventListener("touchcancel",w),y.removeEventListener("touchend",w),E?v(l.DRAG_END,n):t.directSelect&&n.target!==e||n.type.indexOf("cancel")!==-1||("mouseup"===n.type||"click"===n.type||i&&"mouse"===n.pointerType?v(l.CLICK,n):(v(l.TAP,n),"touchend"===n.type&&s(n))),y=null,E=!1}function v(e,i){var r;if(t.enableDoubleTap&&(e===l.CLICK||e===l.TAP))if(n.now()-A<_){var o=e===l.CLICK?l.DOUBLE_CLICK:l.DOUBLE_TAP;r=a(o,i,b),C.trigger(o,r),A=0}else A=n.now();r=a(e,i,b),C.trigger(e,r)}var y,j,b=e,E=!1,x=0,k=0,A=0,_=300;t=t||{},u?(e.addEventListener("pointerdown",g),t.useHover&&(e.addEventListener("pointerover",i),e.addEventListener("pointerout",h)),t.useMove&&e.addEventListener("pointermove",c)):(d&&(e.addEventListener("mousedown",g),t.useHover&&(e.addEventListener("mouseover",i),e.addEventListener("mouseout",h)),t.useMove&&e.addEventListener("mousemove",c)),e.addEventListener("touchstart",g));var C=this;return this.triggerEvent=v,this.destroy=function(){e.removeEventListener("touchstart",g),e.removeEventListener("mousedown",g),y&&(y.removeEventListener("touchmove",m),y.removeEventListener("touchcancel",w),y.removeEventListener("touchend",w),y=null),u&&(t.preventScrolling&&e.releasePointerCapture(j),e.removeEventListener("pointerover",i),e.removeEventListener("pointerdown",g),e.removeEventListener("pointermove",m),e.removeEventListener("pointermove",c),e.removeEventListener("pointercancel",w),e.removeEventListener("pointerout",h),e.removeEventListener("pointerup",w)),e.removeEventListener("click",w),e.removeEventListener("mouseover",i),e.removeEventListener("mousemove",c),e.removeEventListener("mouseout",h),document.removeEventListener("mousemove",m),document.removeEventListener("mouseup",w)},this};return n.extend(h.prototype,e),h}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){function t(e){return!!/[\(,]format=m3u8-/i.test(e)&&"m3u8"}var n=function(e){return e.replace(/^\s+|\s+$/g,"")},i=function(e,t,n){for(e=""+e,n=n||"0";e.length<t;)e=n+e;return e},r=function(e,t){for(var n=0;n<e.attributes.length;n++)if(e.attributes[n].name&&e.attributes[n].name.toLowerCase()===t.toLowerCase())return e.attributes[n].value.toString();return""},o=function(e){if(!e||"rtmp"===e.substr(0,4))return"";var n=t(e);return n?n:(e=e.split("?")[0].split("#")[0],e.lastIndexOf(".")>-1?e.substr(e.lastIndexOf(".")+1,e.length).toLowerCase():void 0)},a=function(e){var t=parseInt(e/3600),n=parseInt(e/60)%60,r=e%60;return i(t,2)+":"+i(n,2)+":"+i(r.toFixed(3),6)},s=function(t,n){if(e.isNumber(t))return t;t=t.replace(",",".");var i=t.split(":"),r=i.length,o=0;if("s"===t.slice(-1))o=parseFloat(t);else if("m"===t.slice(-1))o=60*parseFloat(t);else if("h"===t.slice(-1))o=3600*parseFloat(t);else if(r>1){var a=r-1;4===r&&(n&&(o=parseFloat(i[a])/n),a-=1),o+=parseFloat(i[a]),o+=60*parseFloat(i[a-1]),r>=3&&(o+=3600*parseFloat(i[a-2]))}else o=parseFloat(t);return o},l=function(t,n){return e.map(t,function(e){return n+e})},u=function(t,n){return e.map(t,function(e){return e+n})};return{trim:n,pad:i,xmlAttribute:r,extension:o,hms:a,seconds:s,suffix:u,prefix:l}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){e.exports=n(96)["default"]},,function(e,t){"use strict";function n(e){return c[e]}function i(e){for(var t=1;t<arguments.length;t++)for(var n in arguments[t])Object.prototype.hasOwnProperty.call(arguments[t],n)&&(e[n]=arguments[t][n]);return e}function r(e,t){for(var n=0,i=e.length;n<i;n++)if(e[n]===t)return n;return-1}function o(e){if("string"!=typeof e){if(e&&e.toHTML)return e.toHTML();if(null==e)return"";if(!e)return e+"";e=""+e}return p.test(e)?e.replace(d,n):e}function a(e){return!e&&0!==e||!(!g(e)||0!==e.length)}function s(e){var t=i({},e);return t._parent=e,t}function l(e,t){return e.path=t,e}function u(e,t){return(e?e+".":"")+t}t.__esModule=!0,t.extend=i,t.indexOf=r,t.escapeExpression=o,t.isEmpty=a,t.createFrame=s,t.blockParams=l,t.appendContextPath=u;var c={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;","=":"&#x3D;"},d=/[&<>"'`=]/g,p=/[&<>"'`=]/,h=Object.prototype.toString;t.toString=h;var f=function(e){return"function"==typeof e};f(/x/)&&(t.isFunction=f=function(e){return"function"==typeof e&&"[object Function]"===h.call(e)}),t.isFunction=f;var g=Array.isArray||function(e){return!(!e||"object"!=typeof e)&&"[object Array]"===h.call(e)};t.isArray=g},,function(e,t,n){var i,r;i=[n(2),n(3),n(5),n(1)],r=function(e,t,n,i){var r=e.noop,o=i.constant(!1),a={supports:o,play:r,load:r,stop:r,volume:r,mute:r,seek:r,resize:r,remove:r,destroy:r,setVisibility:r,setFullscreen:o,getFullscreen:r,getContainer:r,setContainer:o,getName:r,getQualityLevels:r,getCurrentQuality:r,setCurrentQuality:r,getAudioTracks:r,getCurrentAudioTrack:r,setCurrentAudioTrack:r,checkComplete:r,setControls:r,attachMedia:r,detachMedia:r,setState:function(e){var i=this.state||n.IDLE;this.state=e,e!==i&&this.trigger(t.JWPLAYER_PLAYER_STATE,{newstate:e})},sendMediaType:function(e){var n=e[0].type,i="oga"===n||"aac"===n||"mp3"===n||"mpeg"===n||"vorbis"===n;this.trigger(t.JWPLAYER_MEDIA_TYPE,{mediaType:i?"audio":"video"})}};return a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){var t={},n={TIT2:"title",TT2:"title",WXXX:"url",TPE1:"artist",TP1:"artist",TALB:"album",TAL:"album"};return t.utf8ArrayToStr=function(e,t){var n,i,r,o,a,s;for(n="",r=e.length,i=t||0;i<r;)if(o=e[i++],0!==o&&3!==o)switch(o>>4){case 0:case 1:case 2:case 3:case 4:case 5:case 6:case 7:n+=String.fromCharCode(o);break;case 12:case 13:a=e[i++],n+=String.fromCharCode((31&o)<<6|63&a);break;case 14:a=e[i++],s=e[i++],n+=String.fromCharCode((15&o)<<12|(63&a)<<6|(63&s)<<0)}return n},t.utf16BigEndianArrayToStr=function(e,t){var n,i,r;for(n="",r=e.length-1,i=t||0;i<r;)254===e[i]&&255===e[i+1]||(n+=String.fromCharCode((e[i]<<8)+e[i+1])),i+=2;return n},t.syncSafeInt=function(e){var n=t.arrayToInt(e);return 127&n|(32512&n)>>1|(8323072&n)>>2|(2130706432&n)>>3},t.arrayToInt=function(e){for(var t="0x",n=0;n<e.length;n++)t+=e[n].toString(16);return parseInt(t)},t.parseID3=function(i){return e.reduce(i,function(i,r){if(!("value"in r)&&"data"in r&&r.data instanceof ArrayBuffer){var o=r,a=new Uint8Array(o.data),s=a.length;r={value:{key:"",data:""}};for(var l=10;l<14&&l<a.length&&0!==a[l];)r.value.key+=String.fromCharCode(a[l]),l++;var u=19,c=a[u];3!==c&&0!==c||(c=a[++u],s--);var d=0;if(1!==c&&2!==c)for(var p=u+1;p<s;p++)if(0===a[p]){d=p-u;break}if(d>0){var h=t.utf8ArrayToStr(a.subarray(u,u+=d),0);if("PRIV"===r.value.key){if("com.apple.streaming.transportStreamTimestamp"===h){var f=1&t.syncSafeInt(a.subarray(u,u+=4)),g=t.syncSafeInt(a.subarray(u,u+=4));f&&(g+=4294967296),r.value.data=g}else r.value.data=t.utf8ArrayToStr(a,u+1);r.value.info=h}else r.value.info=h,r.value.data=t.utf8ArrayToStr(a,u+1)}else{var m=a[u];1===m||2===m?r.value.data=t.utf16BigEndianArrayToStr(a,u+1):r.value.data=t.utf8ArrayToStr(a,u+1)}}if(n.hasOwnProperty(r.value.key)&&(i[n[r.value.key]]=r.value.data),r.value.info){var w=i[r.value.key];e.isObject(w)||(w={},i[r.value.key]=w),w[r.value.info]=r.value.data}else i[r.value.key]=r.value.data;return i},{})},t}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(3),n(4),n(1)],r=function(e,t,n){var i={},r={NEW:0,LOADING:1,ERROR:2,COMPLETE:3},o=function(o,a){function s(t){c=r.ERROR,u.trigger(e.ERROR,t)}function l(t){c=r.COMPLETE,u.trigger(e.COMPLETE,t)}var u=n.extend(this,t),c=r.NEW;this.addEventListener=this.on,this.removeEventListener=this.off,this.makeStyleLink=function(e){var t=document.createElement("link");return t.type="text/css",t.rel="stylesheet",t.href=e,t},this.makeScriptTag=function(e){var t=document.createElement("script");return t.src=e,t},this.makeTag=a?this.makeStyleLink:this.makeScriptTag,this.load=function(){if(c===r.NEW){var t=i[o];if(t&&(c=t.getStatus(),c<2))return t.on(e.ERROR,s),void t.on(e.COMPLETE,l);var n=document.getElementsByTagName("head")[0]||document.documentElement,u=this.makeTag(o),d=!1;u.onload=u.onreadystatechange=function(e){d||this.readyState&&"loaded"!==this.readyState&&"complete"!==this.readyState||(d=!0,l(e),u.onload=u.onreadystatechange=null,n&&u.parentNode&&!a&&n.removeChild(u))},u.onerror=s,n.insertBefore(u,n.firstChild),c=r.LOADING,i[o]=this}},this.getStatus=function(){return c}};return o.loaderstatus=r,o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){var t="free",n="premium",i="enterprise",r="platinum",o="ads",a="unlimited",s="trial",l={setup:[t,n,i,o,a,s,r],dash:[n,i,o,a,s,r],drm:[i,o,a,s],hls:[n,o,i,a,s,r],ads:[o,a,s,r,i],casting:[n,i,o,a,s,r],jwpsrv:[t,n,i,o,s,r]},u=function(t){return function(n){return e.contains(l[n],t)}};return u}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t){"use strict";function n(e,t){var r=t&&t.loc,o=void 0,a=void 0;r&&(o=r.start.line,a=r.start.column,e+=" - "+o+":"+a);for(var s=Error.prototype.constructor.call(this,e),l=0;l<i.length;l++)this[i[l]]=s[i[l]];Error.captureStackTrace&&Error.captureStackTrace(this,n),r&&(this.lineNumber=o,this.column=a)}t.__esModule=!0;var i=["description","fileName","lineNumber","message","name","number","stack"];n.prototype=new Error,t["default"]=n,e.exports=t["default"]},function(e,t,n){var i,r;i=[],r=function(){return{createId:function(e,t){var n,i=e.kind||"cc";return n=e["default"]||e.defaulttrack?"default":e._id||e.file||e.name||e.label||i+t},createLabel:function(e,t){var n=e.label||e.name||e.language;return n||(n="Unknown CC",t+=1,t>1&&(n+=" ["+t+"]")),{label:n,unknownCount:t}}}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2),n(20),n(30),n(127),n(46)],r=function(e,t,i,r,o,a){function s(e,t,n,a){var s,u,c=e.responseXML?e.responseXML.firstChild:null;if(c)for("xml"===i.localName(c)&&(c=c.nextSibling);c.nodeType===c.COMMENT_NODE;)c=c.nextSibling;try{if(c&&"tt"===i.localName(c))s=o(e.responseXML),u=this.convertToVTTCues(s),delete t.xhr,n(u);else{var d=e.responseText;d.indexOf("WEBVTT")>=0?l(d,t,n,a):(s=r(d),u=this.convertToVTTCues(s),delete t.xhr,n(u))}}catch(p){delete t.xhr,a(p)}}function l(e,t,i,r){n.e(11,function(require){var o=n(47),a=new o(window),s=[];a.oncue=function(e){s.push(e)},a.onflush=function(){delete t.xhr,i(s)};try{a.parse(e).flush()}catch(l){delete t.xhr,r(l)}})}var u={};return u.loadFile=function(e,n,i){e.xhr=t.ajax(e.file,function(t){s.call(u,t,e,n,i)},i)},u.cancelXhr=function(t){e.each(t,function(e){var t=e.xhr;t&&(t.onload=null,t.onreadystatechange=null,t.onerror=null,"abort"in t&&t.abort()),delete e.xhr})},u.convertToVTTCues=function(t){var n=e.map(t,function(e){return new a(e.begin,e.end,e.text)});return n},u}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(8)],r=function(e){return{localName:function(e){return e?e.localName?e.localName:e.baseName?e.baseName:"":""},textContent:function(t){return t?t.textContent?e.trim(t.textContent):t.text?e.trim(t.text):"":""},getChildNode:function(e,t){return e.childNodes[t]},numChildren:function(e){return e.childNodes?e.childNodes.length:0}}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(131),n(132),n(52),n(26)],r=function(e,t,n,i){var r={},o={},a=function(n,i){return o[n]=new e(new t(r),i),o[n]},s=function(e,t,o,a){var s=i.getPluginName(e);r[s]||(r[s]=new n(e)),r[s].registerPlugin(e,t,o,a)};return{loadPlugins:a,registerPlugin:s}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){return{repo:"http://ssl.p.jwpcdn.com/player/v/",SkinsIncluded:["seven"],SkinsLoadable:["beelden","bekle","five","glow","roundster","six","stormtrooper","vapor"],dvrSeekLimit:-25}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(8),n(68)],r=function(e,t){function n(e){e=e.split("-");for(var t=1;t<e.length;t++)e[t]=e[t].charAt(0).toUpperCase()+e[t].slice(1);return e.join("")}function i(t,n,i){if(""===n||void 0===n||null===n)return"";var r=i?" !important":"";return"string"==typeof n&&isNaN(n)?/png|gif|jpe?g/i.test(n)&&n.indexOf("url")<0?"url("+n+")":n+r:0===n||"z-index"===t||"opacity"===t?""+n+r:/color/i.test(t)?"#"+e.pad(n.toString(16).replace(/^0x/i,""),6)+r:Math.ceil(n)+"px"+r}var r=function(e,n,i){i=i||"all-players";var r="";if("object"==typeof n){var a=document.createElement("div");o(a,n),r="{"+a.style.cssText+"}"}else"string"==typeof n&&(r=n);t.style([[e,e+r]],i)},o=function(e,t){if(void 0!==e&&null!==e){void 0===e.length&&(e=[e]);var r,o={};for(r in t)o[r]=i(r,t[r]);for(var a=0;a<e.length;a++){var s,l=e[a];if(void 0!==l&&null!==l)for(r in o)s=n(r),l.style[s]!==o[r]&&(l.style[s]=o[r])}}},a=function(e,t){o(e,{transform:t,webkitTransform:t,msTransform:t,mozTransform:t,oTransform:t})},s=function(e,t){var n="rgb";e?(e=String(e).replace("#",""),3===e.length&&(e=e[0]+e[0]+e[1]+e[1]+e[2]+e[2])):e="000000";var i=[parseInt(e.substr(0,2),16),parseInt(e.substr(2,2),16),parseInt(e.substr(4,2),16)];return void 0!==t&&100!==t&&(n+="a",i.push(t/100)),n+"("+i.join(",")+")"};return{css:r,style:o,clearCss:t.clear,transform:a,hexToRgba:s}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){return"7.8.1+commercial_v7-8-1.139.commercial.169552.jwplayer.b14929.freewheel.7e90fa.googima.808f05.vast.87d283.analytics.fdf5f2.plugin-gapro.7e936b.plugin-related.940118.plugin-sharing.8c44f9.vr-plugin.5ca0dc.hls.js."}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(39),n(123),n(1),n(4),n(139),n(3),n(5)],r=function(e,t,n,i,r,o,a,s){var l=function(){function o(e,t){var n=i.extend({},t,{type:e});switch(e){case"flashThrottle":var r="resume"!==t.state;this.set("flashThrottle",r),this.set("flashBlocked",r);break;case"flashBlocked":return void this.set("flashBlocked",!0);case"flashUnblocked":return void this.set("flashBlocked",!1);case"volume":return void this.set(e,t[e]);case"mute":return void(this.get("autostartMuted")||this.set(e,t[e]));case a.JWPLAYER_MEDIA_TYPE:return void(this.mediaModel.get("mediaType")!==t.mediaType&&(this.mediaModel.set("mediaType",t.mediaType),this.mediaController.trigger(e,n)));case a.JWPLAYER_PLAYER_STATE:return void this.mediaModel.set("state",t.newstate);case a.JWPLAYER_MEDIA_BUFFER:this.set("buffer",t.bufferPercent);case a.JWPLAYER_MEDIA_META:var o=t.duration;i.isNumber(o)&&!i.isNaN(o)&&(this.mediaModel.set("duration",o),this.set("duration",o));break;case a.JWPLAYER_MEDIA_BUFFER_FULL:this.mediaModel.get("playAttempt")?this.playVideo():this.mediaModel.on("change:playAttempt",function(){this.playVideo()},this);break;case a.JWPLAYER_MEDIA_TIME:this.mediaModel.set("position",t.position),this.set("position",t.position),i.isNumber(t.duration)&&(this.mediaModel.set("duration",t.duration),this.set("duration",t.duration));break;case a.JWPLAYER_PROVIDER_CHANGED:this.set("provider",d.getName());break;case a.JWPLAYER_MEDIA_LEVELS:this.setQualityLevel(t.currentQuality,t.levels),this.mediaModel.set("levels",t.levels);break;case a.JWPLAYER_MEDIA_LEVEL_CHANGED:this.setQualityLevel(t.currentQuality,t.levels),this.persistQualityLevel(t.currentQuality,t.levels);break;case a.JWPLAYER_AUDIO_TRACKS:this.setCurrentAudioTrack(t.currentTrack,t.tracks),this.mediaModel.set("audioTracks",t.tracks);break;case a.JWPLAYER_AUDIO_TRACK_CHANGED:this.setCurrentAudioTrack(t.currentTrack,t.tracks);break;case"subtitlesTrackChanged":this.persistVideoSubtitleTrack(t.currentTrack,t.tracks);break;case"visualQuality":var s=i.extend({},t);this.mediaModel.set("visualQuality",s);break;case"autoplayFailed":this.set("autostartFailed",!0)}this.mediaController.trigger(e,n)}function l(){return!!e.isIOS()&&!(e.isIOS(6)||e.isIOS(7)||e.isIOS(8)||e.isIOS(9))}var c,d,p=this,h=e.noop;this.mediaController=i.extend({},r),this.mediaModel=new u,n.model(this),this.set("mediaModel",this.mediaModel),this.setup=function(e){return i.extend(this.attributes,e,{item:0,state:s.IDLE,flashBlocked:!1,fullscreen:!1,scrubbing:!1,duration:0,position:0,buffer:0}),this.updateProviders(),this},this.getConfiguration=function(){return i.omit(this.clone(),["mediaModel"])},this.updateProviders=function(){c=new t(this.getConfiguration())},this.setQualityLevel=function(e,t){e>-1&&t.length>1&&"youtube"!==d.getName().name&&this.mediaModel.set("currentLevel",parseInt(e))},this.persistQualityLevel=function(e,t){var n=t[e]||{},i=n.label;this.set("qualityLabel",i)},this.setCurrentAudioTrack=function(e,t){e>-1&&t.length>0&&e<t.length&&this.mediaModel.set("currentAudioTrack",parseInt(e))},this.onMediaContainer=function(){var e=this.get("mediaContainer");h.setContainer(e)},this.changeVideoProvider=function(e){if(this.off("change:mediaContainer",this.onMediaContainer),d&&(d.off(null,null,this),d.getContainer()&&d.remove(),delete d.instreamMode),!e)return d=h=e,void this.set("provider",void 0);h=new e(p.get("id"),p.getConfiguration());
var t=this.get("mediaContainer");t?h.setContainer(t):this.once("change:mediaContainer",this.onMediaContainer),this.set("provider",h.getName()),h.getName().name.indexOf("flash")===-1&&(this.set("flashThrottle",void 0),this.set("flashBlocked",!1)),d=h,d.volume(p.get("volume")),d.mute(this.autoStartOnMobile()||p.get("mute")),d.on("all",o,this),this.get("instreamMode")===!0&&(d.instreamMode=!0),this.set("renderCaptionsNatively",d.renderNatively)},this.destroy=function(){this.off(),d&&(d.off(null,null,this),d.destroy())},this.getVideo=function(){return d},this.setFullscreen=function(e){e=!!e,e!==p.get("fullscreen")&&p.set("fullscreen",e)},this.chooseProvider=function(e){return c.choose(e).provider},this.setItemIndex=function(e){var t=this.get("playlist");e=parseInt(e,10)||0,e=(e+t.length)%t.length,this.set("item",e),this.set("playlistItem",t[e]),this.setActiveItem(t[e])},this.setActiveItem=function(t){this.mediaModel.off(),this.mediaModel=new u,this.set("mediaModel",this.mediaModel),this.set("position",t.starttime||0),this.set("minDvrWindow",t.minDvrWindow),this.set("duration",t.duration&&e.seconds(t.duration)||0),this.setProvider(t)},this.setProvider=function(e){var t=e&&e.sources&&e.sources[0];if(void 0!==t){var n=this.chooseProvider(t);n&&h instanceof n||p.changeVideoProvider(n),h&&(h.init&&h.init(e),this.trigger("itemReady",e))}},this.getProviders=function(){return c},this.resetProvider=function(){h=null},this.setVolume=function(e){e=Math.round(e),this.set("volume",e),d&&d.volume(e);var t=0===e;t!==this.get("mute")&&this.setMute(t)},this.setMute=function(t){if(e.exists(t)||(t=!(this.get("autostartMuted")||this.get("mute"))),this.set("mute",t),d&&d.mute(t),!t){var n=Math.max(10,this.get("volume"));this.set("autostartMuted",!1),this.setVolume(n)}},this.loadVideo=function(t){if(!t){var n=this.get("item");t=this.get("playlist")[n]}this.set("position",t.starttime||0),this.set("duration",t.duration&&e.seconds(t.duration)||0),this.mediaModel.set("playAttempt",!0),this.mediaController.trigger(a.JWPLAYER_MEDIA_PLAY_ATTEMPT,{playReason:this.get("playReason")}),d.load(t)},this.stopVideo=function(){d&&d.stop()},this.playVideo=function(){d.play()},this.persistCaptionsTrack=function(){var e=this.get("captionsTrack");e?this.set("captionLabel",e.name):this.set("captionLabel","Off")},this.setVideoSubtitleTrack=function(e,t){this.set("captionsIndex",e),e&&t&&e<=t.length&&t[e-1].data&&this.set("captionsTrack",t[e-1]),d&&d.setSubtitlesTrack&&d.setSubtitlesTrack(e)},this.persistVideoSubtitleTrack=function(e,t){this.setVideoSubtitleTrack(e,t),this.persistCaptionsTrack()},this.setNextUp=function(e){this.set("nextUp",e)},this.autoStartOnMobile=function(){return this.get("autostart")&&!this.get("sdkplatform")&&(l()&&e.isSafari()||e.isAndroid()&&e.isChrome())&&(!this.get("advertising")||this.get("advertising").autoplayadsmuted)}},u=l.MediaModel=function(){this.set("state",s.IDLE)};return i.extend(l.prototype,o),i.extend(u.prototype,o),l}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(8)],r=function(e){var t={},n=t.pluginPathType={ABSOLUTE:0,RELATIVE:1,CDN:2};return t.getPluginPathType=function(t){if("string"==typeof t){t=t.split("?")[0];var i=t.indexOf("://");if(i>0)return n.ABSOLUTE;var r=t.indexOf("/"),o=e.extension(t);return!(i<0&&r<0)||o&&isNaN(o)?n.RELATIVE:n.CDN}},t.getPluginName=function(e){return e.replace(/^(.*\/)?([^-]*)-?.*\.(swf|js)$/,"$2")},t.getPluginVersion=function(e){return e.replace(/[^-]*-?([^\.]*).*$/,"$1")},t}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){function t(e){return function(){return i(e)}}var n={},i=e.memoize(function(e){var t=navigator.userAgent.toLowerCase();return null!==t.match(e)}),r=n.isInt=function(e){return parseFloat(e)%1===0};n.isFlashSupported=function(){var e=n.flashVersion();return e&&e>=11.2},n.isFF=t(/gecko\//i),n.isIPod=t(/iP(hone|od)/i),n.isIPad=t(/iPad/i),n.isSafari602=t(/Macintosh.*Mac OS X 10_8.*6\.0\.\d* Safari/i),n.isOSX=t(/Mac OS X/i);var o=n.isEdge=function(e){return i(e?new RegExp("\\sedge\\/"+e,"i"):/\sEdge\/\d+/i)},a=n.isIETrident=t(/trident\/.+rv:\s*11/i),s=n.isMSIE=function(e){return e?(e=parseFloat(e).toFixed(1),i(new RegExp("msie\\s*"+e,"i"))):i(/msie/i)};n.isChrome=function(){return i(/\s(?:Chrome|CriOS)\//i)&&!n.isEdge()},n.isIE=function(e){return e?(e=parseFloat(e).toFixed(1),e>=12?o(e):e>=11?a():s(e)):o()||a()||s()},n.isSafari=function(){return i(/safari/i)&&!i(/chrome/i)&&!i(/crios/i)&&!i(/chromium/i)&&!i(/android/i)};var l=n.isIOS=function(e){return i(e?new RegExp("iP(hone|ad|od).+\\s(OS\\s"+e+"|.*\\sVersion/"+e+")","i"):/iP(hone|ad|od)/i)};n.isAndroidNative=function(e){return u(e,!0)};var u=n.isAndroid=function(e,t){return!(t&&i(/chrome\/[123456789]/i)&&!i(/chrome\/18/))&&(e?(r(e)&&!/\./.test(e)&&(e=""+e+"."),i(new RegExp("Android\\s*"+e,"i"))):i(/Android/i))};return n.isMobile=function(){return l()||u()},n.isIframe=function(){return window.frameElement&&"IFRAME"===window.frameElement.nodeName},n.flashVersion=function(){if(n.isAndroid())return 0;var e,t=navigator.plugins;if(t&&(e=t["Shockwave Flash"],e&&e.description))return parseFloat(e.description.replace(/\D+(\d+\.?\d*).*/,"$1"));if("undefined"!=typeof window.ActiveXObject){try{if(e=new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash"))return parseFloat(e.GetVariable("$version").split(" ")[1].replace(/\s*,\s*/,"."))}catch(i){return 0}return e}return 0},n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){var e=window.chrome,t={};return t.NS="urn:x-cast:com.longtailvideo.jwplayer",t.debug=!1,t.availability=null,t.available=function(n){n=n||t.availability;var i="available";return e&&e.cast&&e.cast.ReceiverAvailability&&(i=e.cast.ReceiverAvailability.AVAILABLE),n===i},t.log=function(){if(t.debug){var e=Array.prototype.slice.call(arguments,0);console.log.apply(console,e)}},t.error=function(){var e=Array.prototype.slice.call(arguments,0);console.error.apply(console,e)},t}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(5)],r=function(e){function t(t){return t===e.COMPLETE||t===e.ERROR?e.IDLE:t}return function(e,n,i){if(n=t(n),i=t(i),n!==i){var r=n.replace(/(?:ing|d)$/,""),o={type:r,newstate:n,oldstate:i,reason:e.mediaModel.get("state")};"play"===r?o.playReason=e.get("playReason"):"pause"===r&&(o.pauseReason=e.get("pauseReason")),this.trigger(r,o)}}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(8)],r=function(e,t){function n(e){var t={},n=e.split("\r\n");1===n.length&&(n=e.split("\n"));var r=1;if(n[0].indexOf(" --> ")>0&&(r=0),n.length>r+1&&n[r+1]){var o=n[r],a=o.indexOf(" --> ");a>0&&(t.begin=i(o.substr(0,a)),t.end=i(o.substr(a+5)),t.text=n.slice(r+1).join("\r\n"))}return t}var i=e.seconds;return function(e){var i=[];e=t.trim(e);var r=e.split("\r\n\r\n");1===r.length&&(r=e.split("\n\n"));for(var o=0;o<r.length;o++)if("WEBVTT"!==r[o]){var a=n(r[o]);a.text&&i.push(a)}return i}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(51),n(130)],r=function(e,t,n){var i={sources:[],tracks:[],minDvrWindow:120};return function(r){r=r||{},e.isArray(r.tracks)||delete r.tracks;var o=e.extend({},i,r);e.isObject(o.sources)&&!e.isArray(o.sources)&&(o.sources=[t(o.sources)]),e.isArray(o.sources)&&0!==o.sources.length||(r.levels?o.sources=r.levels:o.sources=[t(r)]);for(var a=0;a<o.sources.length;a++){var s=o.sources[a];if(s){var l=s["default"];l?s["default"]="true"===l.toString():s["default"]=!1,o.sources[a].label||(o.sources[a].label=a.toString()),o.sources[a]=t(o.sources[a])}}return o.sources=e.compact(o.sources),e.isArray(o.tracks)||(o.tracks=[]),e.isArray(o.captions)&&(o.tracks=o.tracks.concat(o.captions),delete o.captions),o.tracks=e.compact(e.map(o.tracks,n)),o}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(8),n(1),n(138)],r=function(e,t,n){var i={};i.createElement=function(e){var t=document.createElement("div");return t.innerHTML=e,t.firstChild},i.styleDimension=function(e){return e+(e.toString().indexOf("%")>0?"":"px")};var r=function(e){return t.isString(e.className)?e.className.split(" "):[]},o=function(t,n){n=e.trim(n),t.className!==n&&(t.className=n)};return i.classList=function(e){return e.classList?e.classList:r(e)},i.hasClass=n.hasClass,i.addClass=function(e,n){var i=r(e),a=t.isArray(n)?n:n.split(" ");t.each(a,function(e){t.contains(i,e)||i.push(e)}),o(e,i.join(" "))},i.removeClass=function(e,n){var i=r(e),a=t.isArray(n)?n:n.split(" ");o(e,t.difference(i,a).join(" "))},i.replaceClass=function(e,t,n){var i=e.className||"";t.test(i)?i=i.replace(t,n):n&&(i+=" "+n),o(e,i)},i.toggleClass=function(e,n,r){var o=i.hasClass(e,n);r=t.isBoolean(r)?r:!o,r!==o&&(r?i.addClass(e,n):i.removeClass(e,n))},i.emptyElement=function(e){for(;e.firstChild;)e.removeChild(e.firstChild)},i.addStyleSheet=function(e){var t=document.createElement("link");t.rel="stylesheet",t.href=e,document.getElementsByTagName("head")[0].appendChild(t)},i.empty=function(e){if(e)for(;e.childElementCount>0;)e.removeChild(e.children[0])},i.bounds=function(e){var t={left:0,right:0,width:0,height:0,top:0,bottom:0};if(!e||!document.body.contains(e))return t;var n=e.getBoundingClientRect(),i=window.pageYOffset,r=window.pageXOffset;return n.width||n.height||n.left||n.top?(t.left=n.left+r,t.right=n.right+r,t.top=n.top+i,t.bottom=n.bottom+i,t.width=n.right-n.left,t.height=n.bottom-n.top,t):t},i}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(34)],r=function(e,t){function n(e){return/^(?:(?:https?|file)\:)?\/\//.test(e)}function i(t){return e.some(t,function(e){return"parsererror"===e.nodeName})}var r={};return r.getAbsolutePath=function(e,i){if(t.exists(i)||(i=document.location.href),t.exists(e)){if(n(e))return e;var r,o=i.substring(0,i.indexOf("://")+3),a=i.substring(o.length,i.indexOf("/",o.length+1));if(0===e.indexOf("/"))r=e.split("/");else{var s=i.split("?")[0];s=s.substring(o.length+a.length+1,s.lastIndexOf("/")),r=s.split("/").concat(e.split("/"))}for(var l=[],u=0;u<r.length;u++)r[u]&&t.exists(r[u])&&"."!==r[u]&&(".."===r[u]?l.pop():l.push(r[u]));return o+a+"/"+l.join("/")}},r.getScriptPath=e.memoize(function(e){for(var t=document.getElementsByTagName("script"),n=0;n<t.length;n++){var i=t[n].src;if(i&&i.indexOf(e)>=0)return i.substr(0,i.indexOf(e))}return""}),r.parseXML=function(e){var t=null;try{"DOMParser"in window?(t=(new window.DOMParser).parseFromString(e,"text/xml"),(i(t.childNodes)||t.childNodes&&i(t.childNodes[0].childNodes))&&(t=null)):(t=new window.ActiveXObject("Microsoft.XMLDOM"),t.async="false",t.loadXML(e))}catch(n){}return t},r.serialize=function(e){if(void 0===e)return null;if("string"==typeof e&&e.length<6){var t=e.toLowerCase();if("true"===t)return!0;if("false"===t)return!1;if(!isNaN(Number(e))&&!isNaN(parseFloat(e)))return Number(e)}return e},r.parseDimension=function(e){return"string"==typeof e?""===e?0:e.lastIndexOf("%")>-1?e:parseInt(e.replace("px",""),10):e},r.timeFormat=function(t,n){if(t<=0&&!n||e.isNaN(parseInt(t)))return"00:00";var i=t<0?"-":"";t=Math.abs(t);var r=Math.floor(t/3600),o=Math.floor((t-3600*r)/60),a=Math.floor(t%60);return i+(r?r+":":"")+(o<10?"0":"")+o+":"+(a<10?"0":"")+a},r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){var t={};return t.exists=function(e){switch(typeof e){case"string":return e.length>0;case"object":return null!==e;case"undefined":return!1}return!0},t.isHTTPS=function(){return 0===window.location.href.indexOf("https")},t.isRtmp=function(e,t){return 0===e.indexOf("rtmp")||"rtmp"===t},t.isYouTube=function(e,t){return"youtube"===t||/^(http|\/\/).*(youtube\.com|youtu\.be)\/.+/.test(e)},t.youTubeID=function(e){var t=/v[=\/]([^?&]*)|youtu\.be\/([^?]*)|^([\w-]*)$/i.exec(e);return t?t.slice(1).join("").replace("?",""):""},t.typeOf=function(t){if(null===t)return"null";var n=typeof t;return"object"===n&&e.isArray(t)?"array":n},t}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(62),n(7),n(94),n(2)],r=function(e,t,n,i){var r=e.extend({constructor:function(e,t){this.className=e+" jw-background-color jw-reset",this.orientation=t,this.dragStartListener=this.dragStart.bind(this),this.dragMoveListener=this.dragMove.bind(this),this.dragEndListener=this.dragEnd.bind(this),this.tapListener=this.tap.bind(this),this.setup()},setup:function(){var e={"default":this["default"],className:this.className,orientation:"jw-slider-"+this.orientation};this.el=i.createElement(n(e)),this.elementRail=this.el.getElementsByClassName("jw-slider-container")[0],this.elementBuffer=this.el.getElementsByClassName("jw-buffer")[0],this.elementProgress=this.el.getElementsByClassName("jw-progress")[0],this.elementThumb=this.el.getElementsByClassName("jw-knob")[0],this.userInteract=new t(this.element(),{preventScrolling:!0}),this.userInteract.on("dragStart",this.dragStartListener),this.userInteract.on("drag",this.dragMoveListener),this.userInteract.on("dragEnd",this.dragEndListener),this.userInteract.on("tap click",this.tapListener)},dragStart:function(){this.trigger("dragStart"),this.railBounds=i.bounds(this.elementRail)},dragEnd:function(e){this.dragMove(e),this.trigger("dragEnd")},dragMove:function(e){var t,n,r=this.railBounds=this.railBounds?this.railBounds:i.bounds(this.elementRail);"horizontal"===this.orientation?(t=e.pageX,n=t<r.left?0:t>r.right?100:100*i.between((t-r.left)/r.width,0,1)):(t=e.pageY,n=t>=r.bottom?0:t<=r.top?100:100*i.between((r.height-(t-r.top))/r.height,0,1));var o=this.limit(n);return this.render(o),this.update(o),!1},tap:function(e){this.railBounds=i.bounds(this.elementRail),this.dragMove(e)},limit:function(e){return e},update:function(e){this.trigger("update",{percentage:e})},render:function(e){e=Math.max(0,Math.min(e,100)),"horizontal"===this.orientation?(this.elementThumb.style.left=e+"%",this.elementProgress.style.width=e+"%"):(this.elementThumb.style.bottom=e+"%",this.elementProgress.style.height=e+"%")},updateBuffer:function(e){this.elementBuffer.style.width=e+"%"},element:function(){return this.el}});return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(62),n(2)],r=function(e,t){var n=e.extend({constructor:function(e,t,n){this.el=document.createElement("div"),this.el.className="jw-icon jw-icon-tooltip "+e+" jw-button-color jw-reset jw-hidden",t&&(this.el.setAttribute("tabindex","0"),this.el.setAttribute("role","button"),this.el.setAttribute("aria-label",t)),n===!0?this.el.setAttribute("aria-hidden","false"):this.el.setAttribute("aria-hidden","true"),this.container=document.createElement("div"),this.container.className="jw-overlay jw-reset",this.openClass="jw-open",this.componentType="tooltip",this.el.appendChild(this.container)},addContent:function(e){this.content&&this.removeContent(),this.content=e,this.container.appendChild(e)},removeContent:function(){this.content&&(this.container.removeChild(this.content),this.content=null)},hasContent:function(){return!!this.content},element:function(){return this.el},openTooltip:function(e){this.trigger("open-"+this.componentType,e,{isOpen:!0}),this.isOpen=!0,t.toggleClass(this.el,this.openClass,this.isOpen)},closeTooltip:function(e){this.trigger("close-"+this.componentType,e,{isOpen:!1}),this.isOpen=!1,t.toggleClass(this.el,this.openClass,this.isOpen)},toggleOpenState:function(e){this.isOpen?this.closeTooltip(e):this.openTooltip(e)}});return n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(16)],r=function(e,t){var n,i=[{configName:"clearkey",keyName:"org.w3.clearkey"},{configName:"widevine",keyName:"com.widevine.alpha"},{configName:"playready",keyName:"com.microsoft.playready"}],r=[],o={},a=function(t){var n=t.get("playlist");return!!t.get("drm")||e.some(n,function(t){return!!t.drm||e.some(t.sources,function(e){return!!e.drm})})},s=function(e){return new Promise(function(t,n){var i;try{i=new window.MSMediaKeys(e)}catch(r){}i?t():n()})},l=function(t){var a=s;return navigator.requestMediaKeySystemAccess&&(a=navigator.requestMediaKeySystemAccess.bind(navigator)),n?n.then(t):(e.forEach(i,function(e){var t=a(e.keyName,[{}]).then(function(){o[e.configName]=!0},function(){o[e.configName]=!1});r.push(t)}),n=Promise.all(r).then(t))},u=function(){return!!navigator.requestMediaKeySystemAccess&&!!MediaKeySystemAccess.prototype.getConfiguration||!!window.MSMediaKeys},c=function(e){return o[e]},d=function(t){n||console.error('DRM only supported with "drm" block in initial setup.',t);var i=e.keys(t);return e.some(i,function(e){return c(e)})};return{containsDrm:a,probe:function(e,n){u()&&t(n)("drm")?l(e):e()},anySupported:d,isSupported:c}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(27),n(16),n(1),n(135),n(37)],r=function(e,t,n,i,r){function o(){return!!window.MediaSource&&!!window.MediaSource.isTypeSupported&&window.MediaSource.isTypeSupported('video/mp4;codecs="avc1.4d400d,mp4a.40.2"')}function a(i,o){if(e.isSafari())return!1;var a=t(o);if(!a("dash"))return!1;if(i.drm&&!r.anySupported(i.drm))return!1;var s=window.MediaSource;if(!window.HTMLVideoElement||!s)return!1;var l=!0;return i.mediaTypes&&(l=n.all(i.mediaTypes,function(e){return s.isTypeSupported(e)})),l&&("dash"===i.type||"mpd"===i.type||(i.file||"").indexOf("mpd-time-csf")>-1)}var s=n.find(i,n.matches({name:"flash"})),l=s.supports;s.supports=function(n,i){if(!e.isFlashSupported()||n.drm)return!1;var r=n&&n.type;if("hls"===r||"m3u8"===r){var o=t(i);return o("hls")}return l.apply(this,arguments)};var u=n.find(i,n.matches({name:"html5"})),c=u.supports;return u.supports=function(e,n){var i=c.apply(this,arguments);if(i&&e.drm&&"hls"===e.type){var r=t(n),o=r("drm");if(o&&e.drm.fairplay){var a=window.WebKitMediaKeys;return a&&a.isTypeSupported&&a.isTypeSupported("com.apple.fps.1_0","video/mp4")}return o}return i},i.push({name:"shaka",supports:a}),i.push({name:"hlsjs",supports:function(n,i){if((e.isChrome()||e.isFF())&&o()&&!e.isMobile()&&!n.drm){var r=n&&n.type,a=n&&n.file;if(a.indexOf(".m3u8")>-1||"hls"===r||"m3u8"===r){var s=t(i);return s("hls")}}}}),i}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(136),n(1),n(38)],r=function(e,t){function i(e,n,i){var r=t.indexOf(e,t.findWhere(e,{name:i})),o=t.indexOf(e,t.findWhere(e,{name:n})),a=e.splice(o,1)[0];e.splice(r,0,a)}var r,o=e.registerProvider,a=e.prototype.reorderProviders;return t.extend(e.loaders,{shaka:function(e){n.e(2,function(require){var t=n(72);o(t),e(t)})},hlsjs:function(e){n.e(1,function(require){var t=n(43);t["default"]&&(t=t["default"]),t.setEdition&&t.setEdition(r),o(t),e(t)})}}),t.extend(e.prototype,{reorderProviders:function(e){var t=a.call(this,e);return"flash"!==e&&i(t,"hlsjs","flash"),t},providerSupports:function(e,t){return r=this.config.edition,e.supports(t,r)}}),e}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,,function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}function r(e,t,n){this.helpers=e||{},this.partials=t||{},this.decorators=n||{},l.registerDefaultHelpers(this),u.registerDefaultDecorators(this)}t.__esModule=!0,t.HandlebarsEnvironment=r;var o=n(11),a=n(17),s=i(a),l=n(99),u=n(97),c=n(107),d=i(c),p="4.0.5";t.VERSION=p;var h=7;t.COMPILER_REVISION=h;var f={1:"<= 1.0.rc.2",2:"== 1.0.0-rc.3",3:"== 1.0.0-rc.4",4:"== 1.x.x",5:"== 2.0.0-alpha.x",6:">= 2.0.0-beta.1",7:">= 4.0.0"};t.REVISION_CHANGES=f;var g="[object Object]";r.prototype={constructor:r,logger:d["default"],log:d["default"].log,registerHelper:function(e,t){if(o.toString.call(e)===g){if(t)throw new s["default"]("Arg not supported with multiple helpers");o.extend(this.helpers,e)}else this.helpers[e]=t},unregisterHelper:function(e){delete this.helpers[e]},registerPartial:function(e,t){if(o.toString.call(e)===g)o.extend(this.partials,e);else{if("undefined"==typeof t)throw new s["default"]('Attempting to register a partial called "'+e+'" as undefined');this.partials[e]=t}},unregisterPartial:function(e){delete this.partials[e]},registerDecorator:function(e,t){if(o.toString.call(e)===g){if(t)throw new s["default"]("Arg not supported with multiple decorators");o.extend(this.decorators,e)}else this.decorators[e]=t},unregisterDecorator:function(e){delete this.decorators[e]}};var m=d["default"].log;t.log=m,t.createFrame=o.createFrame,t.logger=d["default"]},function(e,t,n){var i,r;i=[n(2),n(1)],r=function(e,t){function i(n){t.each(n,function(t,i){n[i]=e.serialize(t)})}function r(e){return e.slice&&"px"===e.slice(-2)&&(e=e.slice(0,-2)),e}function o(t,n){if(n.toString().indexOf("%")===-1)return 0;if("string"!=typeof t||!e.exists(t))return 0;if(/^\d*\.?\d+%$/.test(t))return t;var i=t.indexOf(":");if(i===-1)return 0;var r=parseFloat(t.substr(0,i)),o=parseFloat(t.substr(i+1));return r<=0||o<=0?0:o/r*100+"%"}var a={autostart:!1,controls:!0,displaytitle:!0,displaydescription:!0,mobilecontrols:!1,repeat:!1,castAvailable:!1,skin:"seven",stretching:"uniform",mute:!1,volume:90,width:480,height:270,localization:{play:"Play",playback:"Start playback",pause:"Pause",volume:"Volume",prev:"Previous",next:"Next",cast:"Chromecast",fullscreen:"Fullscreen",playlist:"Playlist",hd:"Quality",cc:"Closed captions",audioTracks:"Audio tracks",replay:"Replay",buffer:"Loading",more:"More",liveBroadcast:"Live broadcast",loadingAd:"Loading ad",rewind:"Rewind 10s",nextUp:"Next Up",related:"Related"},renderCaptionsNatively:!1},s=function(s,l){var u=l&&l.getAllItems(),c=t.extend({},(window.jwplayer||{}).defaults,u,s);i(c),c.localization=t.extend({},a.localization,c.localization);var d=t.extend({},a,c);"."===d.base&&(d.base=e.getScriptPath("jwplayer.js")),d.base=(d.base||e.loadFrom()).replace(/\/?$/,"/"),n.p=d.base,d.width=r(d.width),d.height=r(d.height);var p=e.getScriptPath("jwplayer.js")||d.base;if(d.flashplayer=d.flashplayer||p+"jwplayer.flash.swf",d.flashloader=d.flashloader||p+"jwplayer.loader.swf","http:"===window.location.protocol&&(d.flashplayer=d.flashplayer.replace("https","http"),d.flashloader=d.flashloader.replace("https","http")),d.aspectratio=o(d.aspectratio,d.width),t.isObject(d.skin)&&(d.skinUrl=d.skin.url,d.skinColorInactive=d.skin.inactive,d.skinColorActive=d.skin.active,d.skinColorBackground=d.skin.background,d.skin=t.isString(d.skin.name)?d.skin.name:a.skin),t.isString(d.skin)&&d.skin.indexOf(".xml")>0&&(console.log("JW Player does not support XML skins, please update your config"),d.skin=d.skin.replace(".xml","")),d.aspectratio||delete d.aspectratio,!d.playlist){var h=t.pick(d,["title","description","type","mediaid","image","file","sources","tracks","preload"]);d.playlist=[h]}return d};return s}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i;i=function(){function e(e){if("string"!=typeof e)return!1;var t=o[e.toLowerCase()];return!!t&&e.toLowerCase()}function t(e){if("string"!=typeof e)return!1;var t=a[e.toLowerCase()];return!!t&&e.toLowerCase()}function n(e){for(var t=1;t<arguments.length;t++){var n=arguments[t];for(var i in n)e[i]=n[i]}return e}function i(i,o,a){var s=this,l=/MSIE\s8\.0/.test(navigator.userAgent),u={};l?s=document.createElement("custom"):u.enumerable=!0,s.hasBeenReset=!1;var c="",d=!1,p=i,h=o,f=a,g=null,m="",w=!0,v="auto",y="start",j=50,b="middle",E=50,x="middle";if(Object.defineProperty(s,"id",n({},u,{get:function(){return c},set:function(e){c=""+e}})),Object.defineProperty(s,"pauseOnExit",n({},u,{get:function(){return d},set:function(e){d=!!e}})),Object.defineProperty(s,"startTime",n({},u,{get:function(){return p},set:function(e){if("number"!=typeof e)throw new TypeError("Start time must be set to a number.");p=e,this.hasBeenReset=!0}})),Object.defineProperty(s,"endTime",n({},u,{get:function(){return h},set:function(e){if("number"!=typeof e)throw new TypeError("End time must be set to a number.");h=e,this.hasBeenReset=!0}})),Object.defineProperty(s,"text",n({},u,{get:function(){return f},set:function(e){f=""+e,this.hasBeenReset=!0}})),Object.defineProperty(s,"region",n({},u,{get:function(){return g},set:function(e){g=e,this.hasBeenReset=!0}})),Object.defineProperty(s,"vertical",n({},u,{get:function(){return m},set:function(t){var n=e(t);if(n===!1)throw new SyntaxError("An invalid or illegal string was specified.");m=n,this.hasBeenReset=!0}})),Object.defineProperty(s,"snapToLines",n({},u,{get:function(){return w},set:function(e){w=!!e,this.hasBeenReset=!0}})),Object.defineProperty(s,"line",n({},u,{get:function(){return v},set:function(e){if("number"!=typeof e&&e!==r)throw new SyntaxError("An invalid number or illegal string was specified.");v=e,this.hasBeenReset=!0}})),Object.defineProperty(s,"lineAlign",n({},u,{get:function(){return y},set:function(e){var n=t(e);if(!n)throw new SyntaxError("An invalid or illegal string was specified.");y=n,this.hasBeenReset=!0}})),Object.defineProperty(s,"position",n({},u,{get:function(){return j},set:function(e){if(e<0||e>100)throw new Error("Position must be between 0 and 100.");j=e,this.hasBeenReset=!0}})),Object.defineProperty(s,"positionAlign",n({},u,{get:function(){return b},set:function(e){var n=t(e);if(!n)throw new SyntaxError("An invalid or illegal string was specified.");b=n,this.hasBeenReset=!0}})),Object.defineProperty(s,"size",n({},u,{get:function(){return E},set:function(e){if(e<0||e>100)throw new Error("Size must be between 0 and 100.");E=e,this.hasBeenReset=!0}})),Object.defineProperty(s,"align",n({},u,{get:function(){return x},set:function(e){var n=t(e);if(!n)throw new SyntaxError("An invalid or illegal string was specified.");x=n,this.hasBeenReset=!0}})),s.displayState=void 0,l)return s}if(window.VTTCue)return window.VTTCue;var r="auto",o={"":!0,lr:!0,rl:!0},a={start:!0,middle:!0,end:!0,left:!0,right:!0};return i.prototype.getCueAsHTML=function(){var e=window.WebVTT;return e.convertCueToDOMTree(window,this.text)},i}.call(t,n,t,e),!(void 0!==i&&(e.exports=i))},,function(e,t,n){var i,r;i=[n(8),n(20),n(128),n(129),n(31)],r=function(e,t,n,i,r){function o(t){for(var o={},s=0;s<t.childNodes.length;s++){var l=t.childNodes[s],c=u(l);if(c)switch(c.toLowerCase()){case"enclosure":o.file=e.xmlAttribute(l,"url");break;case"title":o.title=a(l);break;case"guid":o.mediaid=a(l);break;case"pubdate":o.date=a(l);break;case"description":o.description=a(l);break;case"link":o.link=a(l);break;case"category":o.tags?o.tags+=a(l):o.tags=a(l)}}return o=i(t,o),o=n(t,o),new r(o)}var a=t.textContent,s=t.getChildNode,l=t.numChildren,u=t.localName,c={};return c.parse=function(e){for(var t=[],n=0;n<l(e);n++){var i=s(e,n),r=u(i).toLowerCase();if("channel"===r)for(var a=0;a<l(i);a++){var c=s(i,a);"item"===u(c).toLowerCase()&&t.push(o(c))}}return t},c}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(20),n(48),n(2),n(3),n(4),n(1)],r=function(e,t,n,i,r,o){var a=function(){function a(r){var a=n.tryCatch(function(){var n,a=r.responseXML?r.responseXML.childNodes:null,s="";if(a){for(var c=0;c<a.length&&(s=a[c],8===s.nodeType);c++);"xml"===e.localName(s)&&(s=s.nextSibling),"rss"===e.localName(s)&&(n={playlist:t.parse(s)})}if(!n)try{var d=JSON.parse(r.responseText);if(o.isArray(d))n={playlist:d};else{if(!o.isArray(d.playlist))throw null;n=d}}catch(p){return void l("Not a valid RSS/JSON feed")}u.trigger(i.JWPLAYER_PLAYLIST_LOADED,n)});a instanceof n.Error&&l()}function s(e){l("Playlist load error: "+e)}function l(e){u.trigger(i.JWPLAYER_ERROR,{message:e?e:"Error loading file"})}var u=o.extend(this,r);this.load=function(e){n.ajax(e,a,s)},this.destroy=function(){this.off()}};return a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(31),n(51),n(1),n(39)],r=function(e,t,n,i){function r(e,t){for(var n=0;n<e.length;n++){var i=e[n],r=t.choose(i);if(r)return{type:i.type,provider:r.providerToCheck}}return null}function o(e,t){return n.isUndefined(e)?t:e}var a=function(t){return t=n.isArray(t)?t:[t],n.compact(n.map(t,e))};a.filterPlaylist=function(e,t,i,r,a,u,c){var d=[];return n.each(e,function(e){e=n.extend({},e),e.allSources=s(e.sources,i,e.drm||r,e.preload||a,o(e.withCredentials,c)),e.sources=l(e.allSources,t),e.sources.length&&(e.file=e.sources[0].file,(e.preload||a)&&(e.preload=e.preload||a),(e.feedid||u)&&(e.feedid=e.feedid||u),d.push(e))}),d};var s=function(e,i,r,a,s){return n.compact(n.map(e,function(e){if(n.isObject(e)){void 0!==i&&null!==i&&(e.androidhls=i),(e.drm||r)&&(e.drm=e.drm||r),(e.preload||a)&&(e.preload=e.preload||a);var l=o(e.withCredentials,s);return n.isUndefined(l)||(e.withCredentials=l),t(e)}}))},l=function(e,t){t&&t.choose||(t=new i({primary:t?"flash":null}));var o=r(e,t);if(!o)return[];var a=o.provider,s=o.type;return n.filter(e,function(e){return e.type===s&&t.providerSupports(a,e)})};return a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(8),n(1)],r=function(e,t,n){var i={"default":!1},r=function(r){if(r&&r.file){var o=n.extend({},i,r);o.file=t.trim(""+o.file);var a=/^[^\/]+\/(?:x-)?([^\/]+)$/;if(a.test(o.type)&&(o.mimeType=o.type,o.type=o.type.replace(a,"$1")),e.isYouTube(o.file)?o.type="youtube":e.isRtmp(o.file)?o.type="rtmp":o.type||(o.type=t.extension(o.file)),o.type){switch(o.type){case"m3u8":case"vnd.apple.mpegurl":o.type="hls";break;case"dash+xml":o.type="dash";break;case"smil":o.type="rtmp";break;case"m4a":o.type="aac"}return n.each(o,function(e,t){""===e&&delete o[t]}),o}}};return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(26),n(3),n(4),n(15),n(1)],r=function(e,t,n,i,r,o){var a={FLASH:0,JAVASCRIPT:1,HYBRID:2},s=function(s){function l(){switch(t.getPluginPathType(s)){case t.pluginPathType.ABSOLUTE:return s;case t.pluginPathType.RELATIVE:return e.getAbsolutePath(s,window.location.href)}}function u(){o.defer(function(){m=r.loaderstatus.COMPLETE,g.trigger(n.COMPLETE)})}function c(){m=r.loaderstatus.ERROR,g.trigger(n.ERROR,{url:s})}var d,p,h,f,g=o.extend(this,i),m=r.loaderstatus.NEW;this.load=function(){if(m===r.loaderstatus.NEW){if(s.lastIndexOf(".swf")>0)return d=s,m=r.loaderstatus.COMPLETE,void g.trigger(n.COMPLETE);if(t.getPluginPathType(s)===t.pluginPathType.CDN)return m=r.loaderstatus.COMPLETE,void g.trigger(n.COMPLETE);m=r.loaderstatus.LOADING;var e=new r(l());e.on(n.COMPLETE,u),e.on(n.ERROR,c),e.load()}},this.registerPlugin=function(e,t,i,o){f&&(clearTimeout(f),f=void 0),h=t,i&&o?(d=o,p=i):"string"==typeof i?d=i:"function"==typeof i?p=i:i||o||(d=e),m=r.loaderstatus.COMPLETE,g.trigger(n.COMPLETE)},this.getStatus=function(){return m},this.getPluginName=function(){return t.getPluginName(s)},this.getFlashPath=function(){if(d)switch(t.getPluginPathType(d)){case t.pluginPathType.ABSOLUTE:return d;case t.pluginPathType.RELATIVE:return s.lastIndexOf(".swf")>0?e.getAbsolutePath(d,window.location.href):e.getAbsolutePath(d,l())}return null},this.getJS=function(){return p},this.getTarget=function(){return h},this.getPluginmode=function(){return void 0!==typeof d&&void 0!==typeof p?a.HYBRID:void 0!==typeof d?a.FLASH:void 0!==typeof p?a.JAVASCRIPT:void 0},this.getNewInstance=function(e,t,n){return new p(e,t,n)},this.getURL=function(){return s}};return s}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,,,function(e,t,n){var i,r;i=[n(27)],r=function(e){return function(t){if("hls"===t.type){if(t.androidhls===!1&&e.isAndroid())return!1;var n=e.isAndroidNative;if(n(2)||n(3)||n("4.0"))return!1;if(e.isAndroid()&&!e.isFF())return!0}return null}}.apply(t,i),!(void 0!==r&&(e.exports=r))},,function(e,t,n){var i,r;i=[n(1),n(33)],r=function(e,t){function n(e){e.onload=null,e.onprogress=null,e.onreadystatechange=null,e.onerror=null,"abort"in e&&e.abort()}function i(t,i){return function(r){var o=r.currentTarget||i.xhr;if(clearTimeout(i.timeoutId),i.retryWithoutCredentials&&i.xhr.withCredentials){n(o);var a=e.extend({},i,{xhr:null,withCredentials:!1,retryWithoutCredentials:!1});return void d(a)}i.onerror(t,i.url,o)}}function r(e){return function(t){var n=t.currentTarget||e.xhr;if(4===n.readyState){if(clearTimeout(e.timeoutId),n.status>=400){var i;return i=404===n.status?"File not found":""+n.status+"("+n.statusText+")",e.onerror(i,e.url,n)}if(200===n.status)return o(e)(t)}}}function o(e){return function(n){var i=n.currentTarget||e.xhr;if(clearTimeout(e.timeoutId),
e.responseType){if("json"===e.responseType)return a(i,e)}else{var r,o=i.responseXML;if(o)try{r=o.firstChild}catch(l){}if(o&&r)return s(i,o,e);if(u&&i.responseText&&!o&&(o=t.parseXML(i.responseText),o&&o.firstChild))return s(i,o,e);if(e.requireValidXML)return void e.onerror("Invalid XML",e.url,i)}e.oncomplete(i)}}function a(t,n){if(!t.response||e.isString(t.response)&&'"'!==t.responseText.substr(1))try{t=e.extend({},t,{response:JSON.parse(t.responseText)})}catch(i){return void n.onerror("Invalid JSON",n.url,t)}return n.oncomplete(t)}function s(t,n,i){var r=n.documentElement;return i.requireValidXML&&("parsererror"===r.nodeName||r.getElementsByTagName("parsererror").length)?void i.onerror("Invalid XML",i.url,t):(t.responseXML||(t=e.extend({},t,{responseXML:n})),i.oncomplete(t))}var l=function(){},u=!1,c=function(e){var t=document.createElement("a"),n=document.createElement("a");t.href=location.href;try{return n.href=e,n.href=n.href,t.protocol+"//"+t.host!=n.protocol+"//"+n.host}catch(i){}return!0},d=function(t,a,s,d){e.isObject(t)&&(d=t,t=d.url);var p,h=e.extend({xhr:null,url:t,withCredentials:!1,retryWithoutCredentials:!1,timeout:6e4,timeoutId:-1,oncomplete:a||l,onerror:s||l,mimeType:d&&!d.responseType?"text/xml":"",requireValidXML:!1,responseType:d&&d.plainText?"text":""},d);if("XDomainRequest"in window&&c(t))p=h.xhr=new window.XDomainRequest,p.onload=o(h),p.ontimeout=p.onprogress=l,u=!0;else{if(!("XMLHttpRequest"in window))return void h.onerror("",t);p=h.xhr=new window.XMLHttpRequest,p.onreadystatechange=r(h)}var f=i("Error loading file",h);p.onerror=f,"overrideMimeType"in p?h.mimeType&&p.overrideMimeType(h.mimeType):u=!0;try{t=t.replace(/#.*$/,""),p.open("GET",t,!0)}catch(g){return f(g),p}if(h.responseType)try{p.responseType=h.responseType}catch(g){}h.timeout&&(h.timeoutId=setTimeout(function(){n(p),h.onerror("Timeout",t,p)},h.timeout),p.onabort=function(){clearTimeout(h.timeoutId)});try{h.withCredentials&&"withCredentials"in p&&(p.withCredentials=!0),p.send()}catch(g){f(g)}return p};return{ajax:d,crossdomain:c}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(4),n(1)],r=function(e,t,n){function i(e,t,n){var i=document.createElement("param");i.setAttribute("name",t),i.setAttribute("value",n),e.appendChild(i)}function r(r,o,l,u){var c;if(u=u||"opaque",e.isMSIE()){var d=document.createElement("div");o.appendChild(d),d.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%" id="'+l+'" name="'+l+'" tabindex="0"><param name="movie" value="'+r+'"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="wmode" value="'+u+'"><param name="bgcolor" value="'+s+'"><param name="menu" value="false"></object>';for(var p=o.getElementsByTagName("object"),h=p.length;h--;)p[h].id===l&&(c=p[h])}else c=document.createElement("object"),c.setAttribute("type","application/x-shockwave-flash"),c.setAttribute("data",r),c.setAttribute("width","100%"),c.setAttribute("height","100%"),c.setAttribute("bgcolor",s),c.setAttribute("id",l),c.setAttribute("name",l),i(c,"allowfullscreen","true"),i(c,"allowscriptaccess","always"),i(c,"wmode",u),i(c,"menu","false"),o.appendChild(c,o);return c.className="jw-swf jw-reset",c.style.display="block",c.style.position="absolute",c.style.left=0,c.style.right=0,c.style.top=0,c.style.bottom=0,e.isIE()&&"PointerEvent"in window&&(c.style.pointerEvents="none"),n.extend(c,t),c.queueCommands=!0,c.triggerFlash=function(t){var i=this;if("setup"!==t&&i.queueCommands||!i.__externalCall){for(var r=i.__commandQueue,o=r.length;o--;)r[o][0]===t&&r.splice(o,1);return r.push(Array.prototype.slice.call(arguments)),i}var s=Array.prototype.slice.call(arguments,1),l=e.tryCatch(function(){if(s.length){for(var e=s.length;e--;)"object"==typeof s[e]&&n.each(s[e],a);var r=JSON.stringify(s);i.__externalCall(t,r)}else i.__externalCall(t)});return l instanceof e.Error&&(console.error(t,l),"setup"===t)?(l.name="Failed to setup flash",l):i},c.__commandQueue=[],c}function o(e){e&&e.parentNode&&(e.style.display="none",e.parentNode.removeChild(e))}function a(e,t,n){e instanceof window.HTMLElement&&delete n[t]}var s="#000000";return{embed:r,remove:o}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(4),n(1)],r=function(e,t){function n(){}var i=function(e,n){var i,r=this;i=e&&t.has(e,"constructor")?e.constructor:function(){return r.apply(this,arguments)},t.extend(i,r,n);var o=function(){this.constructor=i};return o.prototype=r.prototype,i.prototype=new o,e&&t.extend(i.prototype,e),i.__super__=r.prototype,i};return n.extend=i,t.extend(n.prototype,e),n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(22),n(1),n(34),n(33),n(24)],r=function(e,t,n,i,r){var o={};return o.repo=t.memoize(function(){var t=r.split("+")[0],i=e.repo+t+"/";return n.isHTTPS()?i.replace(/^http:/,"https:"):i}),o.versionCheck=function(e){var t=("0"+e).split(/\W/),n=r.split(/\W/),i=parseFloat(t[0]),o=parseFloat(n[0]);return!(i>o)&&!(i===o&&parseFloat("0"+t[1])>parseFloat(n[1]))},o.loadFrom=function(){return o.repo()},o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){var t=function(){var t={},n={},i={},r={};return{start:function(n){t[n]=e.now(),i[n]=i[n]+1||1},end:function(i){if(t[i]){var r=e.now()-t[i];n[i]=n[i]+r||r}},dump:function(){return{counts:i,sums:n,events:r}},tick:function(t,n){r[t]=n||e.now()},between:function(e,t){return r[t]&&r[e]?r[t]-r[e]:-1}}};return t}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){return document.createElement("video")}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(7)],r=function(e){var t=function(t,n,i){var r=document.createElement("div");return r.className="jw-icon jw-icon-inline jw-button-color jw-reset "+t,r.setAttribute("role","button"),r.setAttribute("tabindex","0"),i&&r.setAttribute("aria-label",i),r.style.display="none",n&&new e(r).on("click tap",function(){n()}),{element:function(){return r},toggle:function(e){e?this.show():this.hide()},show:function(){r.style.display=""},hide:function(){r.style.display="none"}}};return t}.apply(t,i),!(void 0!==r&&(e.exports=r))},,function(e,t,n){var i;i=function(require,e,t){function n(e,t){r(t,o(e))}function i(e){var t=c[e];if(t){for(var n=Object.keys(t),i=0;i<n.length;i+=1)for(var r=t[n[i]],o=0;o<r.parts.length;o+=1)r.parts[o]();delete c[e]}}function r(e,t){for(var n=0;n<t.length;n++){var i=t[n],r=(c[e]||{})[i.id];if(r){for(var o=0;o<r.parts.length;o++)r.parts[o](i.parts[o]);for(;o<i.parts.length;o++)r.parts.push(l(e,i.parts[o]))}else{for(var a=[],o=0;o<i.parts.length;o++)a.push(l(e,i.parts[o]));c[e]=c[e]||{},c[e][i.id]={id:i.id,parts:a}}}}function o(e){for(var t=[],n={},i=0;i<e.length;i++){var r=e[i],o=r[0],a=r[1],s=r[2],l={css:a,media:s};n[o]?n[o].parts.push(l):t.push(n[o]={id:o,parts:[l]})}return t}function a(e){h().appendChild(e)}function s(e){var t=document.createElement("style");return t.type="text/css",t.setAttribute("data-jwplayer-id",e),a(t),t}function l(e,t){var n,i,r,o=d[e];o||(o=d[e]={element:s(e),counter:0});var a=o.counter++;return n=o.element,i=u.bind(null,n,a,!1),r=u.bind(null,n,a,!0),i(t),function(e){if(e){if(e.css===t.css&&e.media===t.media)return;i(t=e)}else r()}}function u(e,t,n,i){var r=n?"":i.css;if(e.styleSheet)e.styleSheet.cssText=f(t,r);else{var o=document.createTextNode(r),a=e.childNodes;a[t]&&e.removeChild(a[t]),a.length?e.insertBefore(o,a[t]):e.appendChild(o)}}var c={},d={},p=function(e){var t;return function(){return"undefined"==typeof t&&(t=e.apply(this,arguments)),t}},h=p(function(){return document.head||document.getElementsByTagName("head")[0]});t.exports={style:n,clear:i};var f=function(){var e=[];return function(t,n){return e[t]=n,e.filter(Boolean).join("\n")}}()}.call(t,n,t,e),!(void 0!==i&&(e.exports=i))},function(e,t,n){var i,r;i=[n(116),n(21),n(1)],r=function(e,t,n){var i=e.selectPlayer,r=function(){var e=i.apply(this,arguments);return e?e:{registerPlugin:function(e,n,i){"jwpsrv"!==e&&t.registerPlugin(e,n,i)}}};return n.extend(e,{selectPlayer:r})}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,function(e,t,n){var i,r;i=[n(2),n(74),n(16)],r=function(e,t,n){var i="invalid",r="RnXcsftYjWRDA^Uy",o=function(o){function a(o){e.exists(o)||(o="");try{o=t.decrypt(o,r);var a=o.split("/");s=a[0],"pro"===s&&(s="premium");var c=n(s);if(a.length>2&&c("setup")){l=a[1];var d=parseInt(a[2]);d>0&&(u=new Date,u.setTime(d))}else s=i}catch(p){s=i}}var s,l,u;this.edition=function(){return u&&u.getTime()<(new Date).getTime()?i:s},this.token=function(){return l},this.expiration=function(){return u},a(o)};return o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){var e=function(e){return window.atob(e)},t=function(e){return unescape(encodeURIComponent(e))},n=function(e){try{return decodeURIComponent(escape(e))}catch(t){return e}},i=function(e){for(var t=new Array(Math.ceil(e.length/4)),n=0;n<t.length;n++)t[n]=e.charCodeAt(4*n)+(e.charCodeAt(4*n+1)<<8)+(e.charCodeAt(4*n+2)<<16)+(e.charCodeAt(4*n+3)<<24);return t},r=function(e){for(var t=new Array(e.length),n=0;n<e.length;n++)t[n]=String.fromCharCode(255&e[n],e[n]>>>8&255,e[n]>>>16&255,e[n]>>>24&255);return t.join("")};return{decrypt:function(o,a){if(o=String(o),a=String(a),0==o.length)return"";for(var s,l,u=i(e(o)),c=i(t(a).slice(0,16)),d=u.length,p=u[d-1],h=u[0],f=2654435769,g=Math.floor(6+52/d),m=g*f;0!=m;){l=m>>>2&3;for(var w=d-1;w>=0;w--)p=u[w>0?w-1:d-1],s=(p>>>5^h<<2)+(h>>>3^p<<4)^(m^h)+(c[3&w^l]^p),h=u[w]-=s;m-=f}var v=r(u);return v=v.replace(/\0+$/,""),n(v)}}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(159),n(3),n(170)],r=function(e,t,n){var i=function(i,r){var o=new e(i,r),a=o.setup;return o.setup=function(){a.call(this),r.on("change:skipButton",this.onSkipButton,this),r.on("change:castActive change:playlistItem",this.showDisplayIconImage,this)},o.showDisplayIconImage=function(e){var t=e.get("castActive"),n=e.get("playlistItem"),i=o.controlsContainer().getElementsByClassName("jw-display-icon-container")[0];t&&n&&n.image?(i.style.backgroundImage='url("'+n.image+'")',i.style.backgroundSize="contain"):(i.style.backgroundImage="",i.style.backgroundSize="")},o.onSkipButton=function(e,t){t?this.addSkipButton():this._skipButton&&(this._skipButton.destroy(),this._skipButton=null)},o.addSkipButton=function(){this._skipButton=new n(this.instreamModel),this._skipButton.on(t.JWPLAYER_AD_SKIPPED,function(){this.api.skipAd()},this),this.controlsContainer().appendChild(this._skipButton.element())},o};return i}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,,,function(e,t,n){t=e.exports=n(82)(),t.push([e.id,".jw-reset{color:inherit;background-color:transparent;padding:0;margin:0;float:none;font-family:Arial,Helvetica,sans-serif;font-size:1em;line-height:1em;list-style:none;text-align:left;text-transform:none;vertical-align:baseline;border:0;direction:ltr;font-variant:inherit;font-stretch:inherit;-webkit-tap-highlight-color:rgba(255,255,255,0)}@font-face{font-family:jw-icons;src:url("+n(84)+") format('woff'),url("+n(83)+') format(\'truetype\');font-weight:400;font-style:normal}.jw-controlbar .jw-menu .jw-option:before,.jw-icon-display,.jw-icon-inline,.jw-icon-tooltip{font-family:jw-icons;-webkit-font-smoothing:antialiased;font-style:normal;font-weight:400;text-transform:none;background-color:transparent;font-variant:normal;-webkit-font-feature-settings:"liga";-ms-font-feature-settings:"liga" 1;-o-font-feature-settings:"liga";font-feature-settings:"liga";-moz-osx-font-smoothing:grayscale}.jw-icon-audio-tracks:before{content:"\\E600"}.jw-icon-buffer:before{content:"\\E601"}.jw-icon-cast:before{content:"\\E603"}.jw-icon-cast.jw-off:before{content:"\\E602"}.jw-icon-cc:before{content:"\\E605"}.jw-icon-cue:before,.jw-icon-menu-bullet:before{content:"\\E606"}.jw-icon-error:before{content:"\\E607"}.jw-icon-fullscreen:before{content:"\\E608"}.jw-icon-fullscreen.jw-off:before{content:"\\E613"}.jw-icon-hd:before{content:"\\E60A"}.jw-rightclick-logo:before{content:"\\E60B"}.jw-icon-next:before{content:"\\E60C"}.jw-icon-pause:before{content:"\\E60D"}.jw-icon-play:before{content:"\\E60E"}.jw-icon-replay:before{content:"\\E610"}.jw-icon-volume:before{content:"\\E612"}.jw-icon-volume.jw-off:before{content:"\\E611"}.jw-icon-more:before{content:"\\E614"}.jw-icon-close:before{content:"\\E615"}.jw-icon-rewind:before{content:"\\E900"}.jwplayer{width:100%;font-size:16px;position:relative;display:block;min-height:0;overflow:hidden;box-sizing:border-box;font-family:Arial,Helvetica,sans-serif;background-color:#000;-webkit-touch-callout:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.jwplayer *{box-sizing:inherit}.jwplayer.jw-flag-aspect-mode{height:auto!important}.jwplayer.jw-flag-aspect-mode .jw-aspect{display:block}.jwplayer .jw-aspect{display:none}.jwplayer.jw-no-focus:focus,.jwplayer .jw-swf{outline:none}.jwplayer.jw-ie:focus{outline:1px dotted #585858}.jwplayer:hover .jw-display-icon-container{background-color:#212121}.jw-controls,.jw-media,.jw-overlays,.jw-preview{position:absolute;width:100%;height:100%;top:0;left:0;bottom:0;right:0}.jw-media{overflow:hidden;cursor:pointer}.jw-overlays{cursor:auto}.jw-media.jw-media-show{visibility:visible;opacity:1}.jw-controls.jw-controls-disabled{display:none}.jw-controls .jw-controls-right{position:absolute;top:0;right:0;left:0;bottom:2.5em}.jw-text{height:1em;font-family:Arial,Helvetica,sans-serif;font-size:.75em;font-style:normal;font-weight:400;color:#fff;text-align:center;font-variant:normal;font-stretch:normal}.jw-plugin{position:absolute;bottom:2.5em}.jw-plugin .jw-banner{max-width:100%;opacity:0;cursor:pointer;position:absolute;margin:auto auto 0;left:0;right:0;bottom:0;display:block}.jw-cast-screen{width:100%;height:100%}.jw-instream{position:absolute;top:0;right:0;bottom:0;left:0;display:none}.jw-icon-playback:before{content:"\\E60E"}.jw-captions,.jw-controls,.jw-overlays,.jw-preview,.jw-title{pointer-events:none}.jw-autostart-mute,.jw-controlbar,.jw-display-icon-container,.jw-display-icon-container .jw-icon,.jw-dock .jw-dock-button,.jw-logo,.jw-media,.jw-nextup-container,.jw-overlays .jw-plugin,.jw-skip{pointer-events:all}.jwplayer video{position:absolute;top:0;right:0;bottom:0;left:0;width:100%;height:100%;margin:auto;background:transparent}.jwplayer video::-webkit-media-controls-start-playback-button{display:none}.jwplayer.jw-stretch-uniform video{-o-object-fit:contain;object-fit:contain}.jwplayer.jw-stretch-none video{-o-object-fit:none;object-fit:none}.jwplayer.jw-stretch-fill video{-o-object-fit:cover;object-fit:cover}.jwplayer.jw-stretch-exactfit video{-o-object-fit:fill;object-fit:fill}.jw-click,.jw-preview{position:absolute;width:100%;height:100%}.jw-preview{display:none;opacity:1;visibility:visible;background:#000 no-repeat 50% 50%}.jw-error .jw-preview,.jw-stretch-uniform .jw-preview,.jwplayer .jw-preview{background-size:contain}.jw-stretch-none .jw-preview{background-size:auto auto}.jw-stretch-fill .jw-preview{background-size:cover}.jw-stretch-exactfit .jw-preview{background-size:100% 100%}.jw-display-icon-container{position:relative;top:50%;display:table;height:3.75em;width:3.75em;margin:-1.875em auto 0;cursor:pointer}.jw-display-icon-container .jw-icon-display{position:relative;display:table-cell;text-align:center;vertical-align:middle!important;background-position:50% 50%;background-repeat:no-repeat;font-size:2.5em}.jw-breakpoint-0 .jw-display-icon-container{height:44px;width:44px;margin:-22px auto 0}.jw-breakpoint-0 .jw-display-icon-container .jw-icon{font-size:22px}.jw-breakpoint-1 .jw-display-icon-container{height:55px;width:55px;margin:-27.5px auto 0}.jw-breakpoint-1 .jw-display-icon-container .jw-icon{font-size:35.2px}.jw-flag-audio-player .jw-display-icon-container,.jw-flag-dragging .jw-display-icon-container{display:none}.jw-icon{font-family:jw-icons;-webkit-font-smoothing:antialiased;font-style:normal;font-weight:400;text-transform:none;background-color:transparent;font-variant:normal;-webkit-font-feature-settings:"liga";-ms-font-feature-settings:"liga" 1;-o-font-feature-settings:"liga";font-feature-settings:"liga";-moz-osx-font-smoothing:grayscale}.jw-controlbar{display:table;position:absolute;bottom:0;height:2.5em;width:100%;padding:0 .5em}.jw-controlbar .jw-hidden{display:none}.jw-background-color{background-color:rgba(33,33,33,.8)}.jw-slider-horizontal{background-color:transparent}.jw-group{display:table-cell}.jw-controlbar-center-group{width:100%;padding:0 .5em}.jw-controlbar-center-group .jw-slider-time,.jw-controlbar-center-group .jw-text-alt{padding:0}.jw-controlbar-center-group .jw-text-alt{display:none}.jw-controlbar-left-group,.jw-controlbar-right-group{white-space:nowrap}.jw-icon-display:hover,.jw-icon-inline:hover,.jw-icon-tooltip:hover,.jw-knob:hover,.jw-option:before:hover{color:#fff}.jw-icon-inline,.jw-icon-tooltip,.jw-slider-horizontal,.jw-text-duration,.jw-text-elapsed{display:inline-block;height:2.5em;position:relative;line-height:2.5em;vertical-align:middle;cursor:pointer;padding:0 .5em}.jw-icon-inline,.jw-icon-tooltip{min-width:1.5625em;text-align:center}.jw-icon-playback{min-width:2.25em}.jw-icon-volume{min-width:1.75em;text-align:left}.jw-time-tip{line-height:1em;pointer-events:none}.jw-icon-cast,.jw-icon-inline.jw-icon-volume,.jw-slider-volume.jw-slider-horizontal{display:none}.jw-button-color{color:hsla(0,0%,100%,.6)}.jw-button-color:hover{color:#fff}.jw-button-color:focus{outline:none;color:#fff}.jw-toggle{color:#fff}.jw-toggle.jw-off{color:hsla(0,0%,100%,.6)}.jw-toggle:focus{outline:none;color:#fff}.jw-dock{clear:right;margin:.75em;display:block;opacity:1}.jw-breakpoint-0 .jw-dock{margin:.225em .1125em}.jw-breakpoint-1 .jw-dock{margin:.45em .225em}.jw-dock:after{content:\'\';clear:both;display:block}.jw-dock-button{cursor:pointer;float:right;height:2.5em;margin:.5em;position:relative;width:2.5em}.jw-breakpoint-0 .jw-dock-button{margin:0 .1125em;height:44px;width:44px}.jw-breakpoint-1 .jw-dock-button{margin:0 .225em;height:44px;width:44px}.jw-dock-button .jw-arrow{display:none;position:absolute;bottom:-.2em;width:.5em;height:.2em;left:50%;margin-left:-.25em}.jw-dock-button .jw-overlay{display:none;position:absolute;top:2.5em;right:0;margin-top:.25em;padding:.5em;white-space:nowrap}.jw-dock-button:hover .jw-arrow,.jw-dock-button:hover .jw-overlay{display:block}.jw-dock-image{width:100%;height:100%;background-position:50% 50%;background-repeat:no-repeat;opacity:.75}.jw-title{display:none;position:absolute;top:0;width:100%;font-size:.875em;height:8em;background:-webkit-linear-gradient(top,#000,#000 18%,transparent);background:linear-gradient(180deg,#000 0,#000 18%,transparent)}.jw-title-primary,.jw-title-secondary{padding:.75em 1.5em;min-height:2.5em;width:100%;color:#fff;white-space:nowrap;text-overflow:ellipsis;overflow-x:hidden}.jw-title-primary{font-weight:700}.jw-title-secondary{margin-top:-.5em}.jw-slider-container{height:1em;width:100%;position:relative;touch-action:none}.jw-buffer,.jw-progress,.jw-rail{position:absolute;cursor:pointer}.jw-progress{background-color:#fff}.jw-rail{background-color:hsla(0,0%,100%,.2)}.jw-buffer{background-color:hsla(0,0%,100%,.3)}.jw-cue,.jw-knob{position:absolute;cursor:pointer}.jw-cue{height:.25em;background-color:rgba(33,33,33,.9);border-radius:25%;width:.5em}.jw-knob{background-color:#fff;width:.5em;height:.5em;border-radius:.25em}.jw-slider-horizontal{height:.25em}.jw-slider-horizontal.jw-slider-volume{width:4em}.jw-slider-horizontal .jw-rail{width:100%}.jw-slider-horizontal .jw-knob{top:-.125em;margin-left:-.25em}.jw-slider-horizontal .jw-buffer,.jw-slider-horizontal .jw-progress,.jw-slider-horizontal .jw-rail{height:.25em}.jw-slider-vertical{padding:1em 1em 0;position:absolute}.jw-slider-vertical .jw-buffer,.jw-slider-vertical .jw-progress,.jw-slider-vertical .jw-rail{bottom:0;height:100%;left:0;right:0;margin:0 auto}.jw-slider-vertical .jw-progress,.jw-slider-vertical .jw-rail,.jw-slider-vertical .jw-slider-container{width:.25em}.jw-slider-vertical .jw-slider-container{height:4em}.jw-slider-vertical .jw-knob{right:0;left:-.125em}.jw-slider-time{width:100%}.jw-tooltip-time{position:absolute}.jw-slider-volume .jw-buffer{display:none}.jw-captions{position:absolute;width:100%;height:inherit;text-align:center;display:none;max-height:81.375%;line-height:1.3em;letter-spacing:normal;word-spacing:normal;text-transform:none;text-indent:0;text-decoration:none;pointer-events:none;overflow:hidden;top:0}.jw-captions.jw-captions-enabled{display:block}.jw-captions-window{display:none;padding:.25em;border-radius:.25em}.jw-captions-text,.jw-captions-window.jw-captions-window-active{display:inline-block}.jw-captions-text{color:#fff;background-color:#000;word-wrap:normal;word-break:normal;white-space:pre-line;font-style:normal;font-weight:400;text-align:center;text-decoration:none;line-height:1.3em;padding:.1em .8em}.jw-text-track-display{font-size:inherit;line-height:1.3em}.jwplayer video::-webkit-media-controls{-webkit-box-pack:start;justify-content:flex-start}.jwplayer video::-webkit-media-text-track-container{max-height:81.375%;line-height:1.3em}.jwplayer .jw-rightclick{display:none;position:absolute;white-space:nowrap}.jwplayer .jw-rightclick.jw-open{display:block}.jwplayer .jw-rightclick ul{list-style:none;font-weight:700;border-radius:.15em;margin:0;border:1px solid #444;padding:0}.jwplayer .jw-rightclick ul li{background-color:#000;border-bottom:1px solid #444;margin:0}.jwplayer .jw-rightclick ul li .jw-rightclick-logo{font-size:2em;color:#ff0147;vertical-align:middle;padding-right:.3em;margin-right:.3em;border-right:1px solid #444}.jwplayer .jw-rightclick ul li a{color:#fff;text-decoration:none;padding:1em;display:block;font-size:.6875em;line-height:1em}.jwplayer .jw-rightclick ul li:last-child{border-bottom:none}.jwplayer .jw-rightclick ul li:hover{background-color:#1a1a1a;cursor:pointer}.jwplayer .jw-rightclick ul .jw-featured{background-color:#252525;vertical-align:middle}.jwplayer .jw-rightclick ul .jw-featured a{color:#777}.jw-logo{position:absolute;margin:.75em;cursor:pointer;pointer-events:all;background-repeat:no-repeat;background-size:contain;top:auto;right:auto;left:auto;bottom:auto}.jw-logo .jw-flag-audio-player{display:none}.jw-logo-top-right{top:0;right:0}.jw-logo-top-left{top:0;left:0}.jw-logo-bottom-left{bottom:0;left:0}.jw-logo-bottom-right{bottom:0;right:0}.jw-icon-tooltip.jw-open .jw-overlay{opacity:1;visibility:visible}.jw-icon-tooltip.jw-hidden{display:none}.jw-overlay:before{position:absolute;top:0;bottom:0;left:-50%;width:100%;background-color:transparent;content:" "}.jw-slider-time .jw-overlay:before{height:1em;top:auto}.jw-menu,.jw-time-tip,.jw-volume-tip{position:relative;left:-50%;margin:0}.jw-volume-tip{width:100%;height:100%;display:block}.jw-time-tip{text-align:center;font-family:inherit;bottom:1.25em;padding:.5em;border-radius:.25em}.jw-time-tip .jw-text{color:#fff;line-height:1em}.jw-controlbar .jw-overlay{margin:0;position:absolute;bottom:2.5em;left:50%;opacity:0;visibility:hidden}.jw-controlbar .jw-overlay .jw-contents{position:relative}.jw-controlbar .jw-option{position:relative;white-space:nowrap;cursor:pointer;list-style:none;height:1.5em;font-family:inherit;line-height:1.5em;color:hsla(0,0%,100%,.6);padding:0 .5em;font-size:.8em}.jw-controlbar .jw-option:before:hover,.jw-controlbar .jw-option:hover{color:#fff}.jw-controlbar .jw-option:before{padding-right:.125em}.jw-controlbar .jw-option.jw-active-option{color:#fff}.jw-skip{cursor:default;position:absolute;float:right;display:inline-block;right:.75em;bottom:3em;padding:.5em}.jw-skip.jw-skippable{cursor:pointer}.jw-skip.jw-hidden{visibility:hidden}.jw-skip .jw-skip-icon{display:none;margin-left:-.75em}.jw-skip .jw-skip-icon:before{content:"\\E60C"}.jw-skip .jw-skip-icon,.jw-skip .jw-text{color:hsla(0,0%,100%,.6);vertical-align:middle;line-height:1.5em;font-size:.7em}.jw-skip.jw-skippable:hover{cursor:pointer}.jw-skip.jw-skippable:hover .jw-skip-icon,.jw-skip.jw-skippable:hover .jw-text{color:#fff}.jw-skip.jw-skippable .jw-skip-icon{display:inline;margin:0}.jwplayer.jw-state-paused.jw-flag-casting .jw-display-icon-container,.jwplayer.jw-state-playing.jw-flag-casting .jw-display-icon-container{display:table}.jwplayer.jw-flag-casting .jw-display-icon-container{border-radius:0;border:1px solid #fff;position:absolute;top:auto;left:.5em;right:.5em;bottom:50%;margin-bottom:-12.5%;height:50%;width:50%;padding:0;background-repeat:no-repeat;background-position:50%}.jwplayer.jw-flag-casting .jw-display-icon-container .jw-icon{font-size:3em}.jwplayer.jw-flag-casting.jw-state-complete .jw-preview{display:none}.jw-cast{position:absolute;width:100%;height:100%;background-repeat:no-repeat;background-size:auto;background-position:50% 50%}.jw-cast-label{position:absolute;left:.5em;right:.5em;bottom:75%;margin-bottom:1.5em;text-align:center}.jw-cast-name{color:#ccc}.jw-nextup-container{-webkit-font-smoothing:antialiased;-moz-font-smoothing:antialiased;background-color:transparent;bottom:3em;cursor:pointer;max-width:300px;min-width:200px;opacity:0;position:absolute;right:.5em;-webkit-transform:translateY(5px);transform:translateY(5px);-webkit-transition:all .15s ease;transition:all .15s ease;visibility:hidden;width:40%}.jw-nextup-container-visible{opacity:1;-webkit-transform:translateY(0);transform:translateY(0);visibility:visible}.jw-nextup{border-radius:0;overflow:hidden;position:relative}.jw-nextup-header{background:rgba(33,33,33,.8);box-sizing:border-box;color:#fff;font-size:12px;font-weight:700;line-height:normal;padding:8px}.jw-nextup-body{background:rgba(0,0,0,.8);color:#fff;overflow:hidden}.jw-nextup-thumbnail{background-position:50%;background-size:cover;display:none;float:left;height:60px;width:45%}.jw-nextup-thumbnail-visible{display:block}.jw-nextup-title{box-sizing:border-box;float:left;font-size:12px;font-weight:700;line-height:1.3;overflow:hidden;padding:5px 6px;position:relative;text-overflow:ellipsis;white-space:nowrap;width:100%}.jw-nextup-thumbnail-visible+.jw-nextup-title{height:60px;white-space:normal;width:55%}.jw-nextup-thumbnail-visible+.jw-nextup-title:after{background:-webkit-linear-gradient(top,transparent,#000);background:linear-gradient(-180deg,transparent,#000);bottom:0;content:\'\';height:30px;left:0;position:absolute;width:100%}.jw-nextup-close{font-family:jw-icons;-webkit-font-smoothing:antialiased;font-style:normal;font-weight:400;text-transform:none;background-color:transparent;font-variant:normal;-webkit-font-feature-settings:"liga";-ms-font-feature-settings:"liga" 1;-o-font-feature-settings:"liga";font-feature-settings:"liga";-moz-osx-font-smoothing:grayscale;border:none;color:hsla(0,0%,100%,.6);font-size:13px;opacity:0;position:absolute;right:5px;top:6px;-webkit-transition:color .15s ease,opacity .15s ease,visibility .15s ease;transition:color .15s ease,opacity .15s ease,visibility .15s ease;visibility:hidden}.jw-nextup-close:before{content:"\\E615"}.jw-nextup-close:active,.jw-nextup-close:hover{color:#fff}.jw-nextup-sticky .jw-nextup-close{opacity:1;visibility:visible}.jw-autostart-mute{min-width:1.75em;text-align:left;position:absolute;bottom:.5em;right:.5em;height:44px;width:44px;text-align:center}.jw-autostart-mute:before{content:"\\E612"}.jw-autostart-mute.jw-off:before{content:"\\E611"}.jw-autostart-mute:before{background-color:rgba(33,33,33,.8);padding:5px 4px 5px 6px}.jw-flag-autostart .jw-controlbar,.jw-flag-autostart .jw-nextup{display:none}.jw-state-idle .jw-preview{display:block}.jw-state-idle .jw-icon-display:before{content:"\\E60E"}.jw-state-idle .jw-captions,.jw-state-idle .jw-controlbar{display:none}.jw-state-idle .jw-title{display:block}.jwplayer.jw-state-playing .jw-display-icon-container{display:none}.jwplayer.jw-state-playing .jw-display-icon-container .jw-icon-display:before,.jwplayer.jw-state-playing .jw-icon-playback:before{content:"\\E60D"}.jwplayer.jw-state-paused .jw-display-icon-container{display:none}.jwplayer.jw-state-paused .jw-display-icon-container .jw-icon-display:before,.jwplayer.jw-state-paused .jw-icon-playback:before{content:"\\E60E"}.jwplayer.jw-state-paused .jw-autostart-mute{display:none}.jwplayer.jw-state-buffering .jw-display-icon-container .jw-icon-display{-webkit-animation:spin 2s linear infinite;animation:spin 2s linear infinite}.jwplayer.jw-state-buffering .jw-display-icon-container .jw-icon-display:before{content:"\\E601"}@-webkit-keyframes spin{to{-webkit-transform:rotate(1turn)}}@keyframes spin{to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}.jwplayer.jw-state-buffering .jw-display-icon-container .jw-text{display:none}.jwplayer.jw-state-buffering .jw-icon-playback:before{content:"\\E60D"}.jwplayer.jw-state-complete .jw-preview{display:block}.jwplayer.jw-state-complete .jw-display-icon-container .jw-icon-display:before{content:"\\E610"}.jwplayer.jw-state-complete .jw-display-icon-container .jw-text{display:none}.jwplayer.jw-state-complete .jw-icon-playback:before{content:"\\E60E"}.jwplayer.jw-state-complete .jw-captions{display:none}.jwplayer.jw-state-error .jw-title,body .jw-error .jw-title{display:block}.jwplayer.jw-state-error .jw-title .jw-title-primary,body .jw-error .jw-title .jw-title-primary{white-space:normal}.jwplayer.jw-state-error .jw-preview,body .jw-error .jw-preview{display:block}.jwplayer.jw-state-error .jw-captions,.jwplayer.jw-state-error .jw-controlbar,body .jw-error .jw-captions,body .jw-error .jw-controlbar{display:none}.jwplayer.jw-state-error:hover .jw-display-icon-container,body .jw-error:hover .jw-display-icon-container{cursor:default;color:#fff;background:#000}.jwplayer.jw-state-error .jw-icon-display,body .jw-error .jw-icon-display{cursor:default;font-family:jw-icons;-webkit-font-smoothing:antialiased;font-style:normal;font-weight:400;text-transform:none;background-color:transparent;font-variant:normal;-webkit-font-feature-settings:"liga";-ms-font-feature-settings:"liga" 1;-o-font-feature-settings:"liga";font-feature-settings:"liga";-moz-osx-font-smoothing:grayscale}.jwplayer.jw-state-error .jw-icon-display:before,body .jw-error .jw-icon-display:before{content:"\\E607"}.jwplayer.jw-state-error .jw-icon-display:hover,body .jw-error .jw-icon-display:hover{color:#fff}body .jw-error{font-size:16px;background-color:#000;color:#fff;width:100%;height:100%;display:table;opacity:1;position:relative}body .jw-error .jw-icon-container{position:absolute;width:100%;height:100%;top:0;left:0;bottom:0;right:0}.jwplayer.jw-flag-cast-available .jw-controlbar{display:table}.jwplayer.jw-flag-cast-available .jw-icon-cast{display:inline-block}.jwplayer.jw-flag-skin-loading .jw-captions,.jwplayer.jw-flag-skin-loading .jw-controls,.jwplayer.jw-flag-skin-loading .jw-title{display:none}.jwplayer.jw-flag-fullscreen{width:100%!important;height:100%!important;top:0;right:0;bottom:0;left:0;z-index:1000;margin:0;position:fixed}.jwplayer.jw-flag-live .jw-controlbar .jw-slider-time,.jwplayer.jw-flag-live .jw-controlbar .jw-text-duration,.jwplayer.jw-flag-live .jw-controlbar .jw-text-elapsed{display:none}.jwplayer.jw-flag-live .jw-controlbar .jw-text-alt{display:inline-block;height:auto;line-height:2.5em;margin:0 -.5em;overflow:hidden;text-overflow:ellipsis;vertical-align:middle;white-space:nowrap}.jwplayer.jw-flag-live.jw-breakpoint-0 .jw-text-alt{max-width:80px}.jwplayer.jw-flag-live.jw-breakpoint-1 .jw-text-alt{max-width:150px}.jwplayer.jw-flag-live.jw-breakpoint-2 .jw-text-alt{max-width:250px}.jwplayer.jw-flag-live.jw-breakpoint-3 .jw-text-alt{max-width:370px}.jwplayer.jw-flag-live.jw-breakpoint-4 .jw-text-alt{max-width:470px}.jwplayer.jw-flag-live.jw-breakpoint-5 .jw-text-alt{max-width:630px}.jwplayer.jw-flag-live.jw-breakpoint-6 .jw-text-alt{max-width:790px}.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-controlbar,.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-dock,.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-logo.jw-hide{display:none}.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-nextup-container,.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-plugin{bottom:.5em}.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-media{cursor:none;-webkit-cursor-visibility:auto-hide}.jwplayer.jw-flag-user-inactive.jw-state-playing .jw-captions{max-height:none}.jwplayer.jw-flag-user-inactive.jw-state-playing video::-webkit-media-text-track-container{max-height:none}.jwplayer.jw-flag-user-inactive.jw-state-buffering .jw-controlbar{display:none}.jwplayer.jw-flag-user-inactive.jw-state-buffering .jw-nextup-container{bottom:.5em}.jwplayer.jw-flag-media-audio .jw-controlbar,.jwplayer.jw-flag-media-audio.jw-flag-user-inactive .jw-controlbar{display:table}.jwplayer.jw-flag-media-audio.jw-flag-user-inactive .jw-nextup-container,.jwplayer.jw-flag-media-audio.jw-flag-user-inactive.jw-state-playing .jw-plugin{bottom:3em}.jwplayer.jw-flag-media-audio.jw-flag-user-inactive.jw-state-playing .jw-captions{max-height:81.375%}.jwplayer.jw-flag-media-audio.jw-flag-user-inactive.jw-state-playing video::-webkit-media-text-track-container{max-height:81.375%}.jwplayer.jw-flag-media-audio.jw-flag-user-inactive.jw-state-playing.jw-flag-touch video::-webkit-media-text-track-container{max-height:70%}.jwplayer.jw-flag-media-audio .jw-autostart-mute{display:none}.jw-flag-media-audio .jw-preview{display:block}.jwplayer.jw-flag-ads .jw-autostart-mute,.jwplayer.jw-flag-ads .jw-captions.jw-captions-enabled,.jwplayer.jw-flag-ads .jw-dock,.jwplayer.jw-flag-ads .jw-logo,.jwplayer.jw-flag-ads .jw-nextup-container,.jwplayer.jw-flag-ads .jw-preview{display:none}.jwplayer.jw-flag-ads video::-webkit-media-text-track-container{display:none}.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-inline,.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-tooltip,.jwplayer.jw-flag-ads .jw-controlbar .jw-slider-horizontal,.jwplayer.jw-flag-ads .jw-controlbar .jw-text{display:none}.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-fullscreen,.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-playback,.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-volume,.jwplayer.jw-flag-ads .jw-controlbar .jw-slider-volume{display:inline-block}.jwplayer.jw-flag-ads .jw-controlbar .jw-text-alt{display:inline-block;height:auto;line-height:2.5em;margin:0 -.5em;overflow:hidden;text-overflow:ellipsis;vertical-align:middle;white-space:nowrap}.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-inline.jw-icon-volume,.jwplayer.jw-flag-ads .jw-controlbar .jw-slider-volume.jw-slider-horizontal{display:inline-block}.jwplayer.jw-flag-ads .jw-controlbar .jw-icon-tooltip.jw-icon-volume{display:none}.jwplayer.jw-flag-ads.jw-breakpoint-0 .jw-text-alt{max-width:80px}.jwplayer.jw-flag-ads.jw-breakpoint-1 .jw-text-alt{max-width:150px}.jwplayer.jw-flag-ads.jw-breakpoint-2 .jw-text-alt{max-width:250px}.jwplayer.jw-flag-ads.jw-breakpoint-3 .jw-text-alt{max-width:370px}.jwplayer.jw-flag-ads.jw-breakpoint-4 .jw-text-alt{max-width:470px}.jwplayer.jw-flag-ads.jw-breakpoint-5 .jw-text-alt{max-width:630px}.jwplayer.jw-flag-ads.jw-breakpoint-6 .jw-text-alt{max-width:790px}.jwplayer.jw-flag-ads-googleima .jw-controlbar{display:table;bottom:0}.jwplayer.jw-flag-ads-googleima.jw-flag-touch .jw-controlbar{font-size:1em}.jwplayer.jw-flag-ads-googleima.jw-flag-touch.jw-state-paused .jw-display-icon-container{display:none}.jwplayer.jw-flag-ads-googleima.jw-skin-seven .jw-controlbar{font-size:.9em}.jwplayer.jw-flag-ads.jw-flag-touch .jw-controlbar{display:table}.jwplayer.jw-flag-ads-vpaid .jw-controlbar,.jwplayer.jw-flag-ads-vpaid .jw-skip,.jwplayer.jw-flag-touch.jw-flag-ads-vpaid .jw-controlbar,.jwplayer.jw-flag-touch.jw-flag-ads-vpaid .jw-skip{display:none}.jwplayer.jw-flag-ads-hide-controls .jw-controls{display:none!important}.jwplayer.jw-flag-overlay-open-related .jw-controls,.jwplayer.jw-flag-overlay-open-related .jw-title,.jwplayer.jw-flag-overlay-open-sharing .jw-controls,.jwplayer.jw-flag-overlay-open-sharing .jw-title,.jwplayer.jw-flag-overlay-open .jw-controls-right .jw-logo,.jwplayer.jw-flag-overlay-open .jw-title{display:none}.jwplayer.jw-flag-rightclick-open{overflow:visible}.jwplayer.jw-flag-rightclick-open .jw-rightclick{z-index:16777215}.jw-flag-controls-disabled .jw-controls{visibility:hidden}.jw-flag-controls-disabled .jw-logo{visibility:visible}.jw-flag-controls-disabled .jw-media{cursor:auto}.jw-flag-controls-disabled.jwplayer .jw-captions{max-height:none}.jw-flag-controls-disabled.jwplayer video::-webkit-media-text-track-container{max-height:none}body .jwplayer.jw-flag-flash-blocked .jw-title{display:block}body .jwplayer.jw-flag-flash-blocked .jw-controls,body .jwplayer.jw-flag-flash-blocked .jw-overlays,body .jwplayer.jw-flag-flash-blocked .jw-preview{display:none}.jw-flag-touch.jw-breakpoint-4 .jw-controlbar,.jw-flag-touch.jw-breakpoint-4 .jw-plugin,.jw-flag-touch.jw-breakpoint-4 .jw-skip,.jw-flag-touch.jw-breakpoint-5 .jw-controlbar,.jw-flag-touch.jw-breakpoint-5 .jw-plugin,.jw-flag-touch.jw-breakpoint-5 .jw-skip,.jw-flag-touch.jw-breakpoint-6 .jw-controlbar,.jw-flag-touch.jw-breakpoint-6 .jw-plugin,.jw-flag-touch.jw-breakpoint-6 .jw-skip,.jw-flag-touch.jw-breakpoint-7 .jw-controlbar,.jw-flag-touch.jw-breakpoint-7 .jw-plugin,.jw-flag-touch.jw-breakpoint-7 .jw-skip{font-size:1.5em}.jw-flag-touch.jw-breakpoint-4 .jw-captions,.jw-flag-touch.jw-breakpoint-5 .jw-captions,.jw-flag-touch.jw-breakpoint-6 .jw-captions,.jw-flag-touch.jw-breakpoint-7 .jw-captions{bottom:4.25em}.jw-flag-touch.jw-breakpoint-4 video::-webkit-media-text-track-container,.jw-flag-touch.jw-breakpoint-5 video::-webkit-media-text-track-container,.jw-flag-touch.jw-breakpoint-6 video::-webkit-media-text-track-container,.jw-flag-touch.jw-breakpoint-7 video::-webkit-media-text-track-container{max-height:70%}.jw-flag-touch.jw-breakpoint-4 .jw-nextup-container,.jw-flag-touch.jw-breakpoint-5 .jw-nextup-container,.jw-flag-touch.jw-breakpoint-6 .jw-nextup-container,.jw-flag-touch.jw-breakpoint-7 .jw-nextup-container{bottom:4.25em}.jw-flag-touch .jw-controlbar .jw-icon-volume{display:inline-block}.jw-flag-touch .jw-display-icon-container{pointer-events:none}.jw-flag-touch.jw-state-paused .jw-display-icon-container{display:table}.jw-flag-touch.jw-state-paused.jw-flag-dragging .jw-display-icon-container{display:none}.jwplayer.jw-flag-audio-player{background-color:transparent}.jwplayer.jw-flag-audio-player .jw-media{visibility:hidden}.jwplayer.jw-flag-audio-player .jw-media object{width:1px;height:1px}.jwplayer.jw-flag-audio-player .jw-display-icon-container,.jwplayer.jw-flag-audio-player .jw-dock,.jwplayer.jw-flag-audio-player .jw-nextup-container,.jwplayer.jw-flag-audio-player .jw-preview,.jwplayer.jw-flag-audio-player .jw-title{display:none}.jwplayer.jw-flag-audio-player .jw-controlbar{vertical-align:middle;display:table;height:100%;left:0;bottom:0;margin:0;width:100%;min-width:100%}.jwplayer.jw-flag-audio-player .jw-controlbar .jw-icon-fullscreen,.jwplayer.jw-flag-audio-player .jw-controlbar .jw-icon-tooltip{display:none}.jwplayer.jw-flag-audio-player .jw-controlbar .jw-icon-inline.jw-icon-volume,.jwplayer.jw-flag-audio-player .jw-controlbar .jw-slider-volume.jw-slider-horizontal{display:inline-block}.jwplayer.jw-flag-audio-player .jw-controlbar .jw-icon-tooltip.jw-icon-volume{display:none}.jwplayer.jw-flag-audio-player.jw-flag-user-inactive .jw-controlbar{display:table}.jwplayer.jw-flag-audio-player .jw-icon-inline{height:auto;line-height:normal}.jwplayer.jw-flag-audio-player .jw-group{vertical-align:middle}.jwplayer.jw-flag-audio-player .jw-controlbar-center-group{padding-bottom:2px}.jw-breakpoint-0 .jw-nextup-container,.jw-breakpoint-0 .jw-text-duration,.jw-breakpoint-0 .jw-text-elapsed,.jw-breakpoint-1 .jw-nextup-container,.jw-breakpoint-1 .jw-text-duration,.jw-breakpoint-1 .jw-text-elapsed{display:none}.jw-skin-seven .jw-color-active,.jw-skin-seven .jw-color-active-hover:hover{color:#fff;stroke:#fff;border-color:#fff}.jw-skin-seven .jw-color-inactive,.jw-skin-seven .jw-color-inactive-hover:hover{color:#cecece;stroke:#cecece;border-color:#cecece}.jw-skin-seven .jw-active-option{background-color:hsla(0,0%,100%,.1)}.jw-skin-seven .jw-display-icon-container{display:table;font-size:20px;border-radius:3.5em}.jw-skin-seven .jw-display-icon-container:hover .jw-icon{color:#fff}.jw-skin-seven .jw-display-icon-container>.jw-icon{color:hsla(0,0%,100%,.9);font-size:1.7em}.jw-skin-seven.jw-state-idle .jw-display-icon-container>.jw-icon{padding-left:4px}.jw-skin-seven .jw-dock-button{border-radius:2.5em}.jw-skin-seven .jw-menu{padding:0}.jw-skin-seven .jw-dock .jw-overlay,.jw-skin-seven .jw-skip{border-radius:.5em}.jw-skin-seven .jw-text{text-rendering:optimizeLegibility;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.jw-skin-seven.jw-breakpoint-0 .jw-display-icon-container .jw-icon{font-size:22px}.jw-skin-seven.jw-breakpoint-1 .jw-display-icon-container .jw-icon{font-size:35.2px}',""]);
},function(e,t){e.exports=function(){var e=[];return e.toString=function(){for(var e=[],t=0;t<this.length;t++){var n=this[t];n[2]?e.push("@media "+n[2]+"{"+n[1]+"}"):e.push(n[1])}return e.join("")},e.i=function(t,n){"string"==typeof t&&(t=[[null,t,""]]);for(var i={},r=0;r<this.length;r++){var o=this[r][0];"number"==typeof o&&(i[o]=!0)}for(r=0;r<t.length;r++){var a=t[r];"number"==typeof a[0]&&i[a[0]]||(n&&!a[2]?a[2]=n:n&&(a[2]="("+a[2]+") and ("+n+")"),e.push(a))}},e}},function(e,t,n){e.exports=n.p+"jw-icons.ttf"},function(e,t,n){e.exports=n.p+"jw-icons.woff"},function(e,t,n){var i=n(9);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){return'<div class="jw-skip jw-background-color jw-hidden jw-reset">\n    <span class="jw-text jw-skiptext jw-reset"></span>\n    <span class="jw-icon-inline jw-skip-icon jw-reset"></span>\n</div>'},useData:!0})},function(e,t,n){var i=n(9);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o;return'<div class="jw-display-icon-container jw-background-color jw-reset">\n    <div class="jw-icon jw-icon-display jw-button-color jw-reset" role="button" tabindex="0" aria-label="'+e.escapeExpression((o=null!=(o=n.ariaLabel||(null!=t?t.ariaLabel:t))?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"ariaLabel",hash:{},data:r}):o))+'"></div>\n</div>\n'},useData:!0})},function(e,t,n){var i=n(9);e.exports=(i["default"]||i).template({1:function(e,t,n,i,r){var o,a,s=null!=t?t:{};return'    <div class="jw-dock-button jw-background-color jw-reset'+(null!=(o=n["if"].call(s,null!=t?t.btnClass:t,{name:"if",hash:{},fn:e.program(2,r,0),inverse:e.noop,data:r}))?o:"")+'" button="'+e.escapeExpression((a=null!=(a=n.id||(null!=t?t.id:t))?a:n.helperMissing,"function"==typeof a?a.call(s,{name:"id",hash:{},data:r}):a))+'">\n        <div class="jw-icon jw-dock-image jw-reset" '+(null!=(o=n["if"].call(s,null!=t?t.img:t,{name:"if",hash:{},fn:e.program(4,r,0),inverse:e.noop,data:r}))?o:"")+'></div>\n        <div class="jw-arrow jw-reset"></div>\n'+(null!=(o=n["if"].call(s,null!=t?t.tooltip:t,{name:"if",hash:{},fn:e.program(6,r,0),inverse:e.noop,data:r}))?o:"")+"    </div>\n"},2:function(e,t,n,i,r){var o;return" "+e.escapeExpression((o=null!=(o=n.btnClass||(null!=t?t.btnClass:t))?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"btnClass",hash:{},data:r}):o))},4:function(e,t,n,i,r){var o;return"style='background-image: url(\""+e.escapeExpression((o=null!=(o=n.img||(null!=t?t.img:t))?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"img",hash:{},data:r}):o))+"\")'"},6:function(e,t,n,i,r){var o;return'        <div class="jw-overlay jw-background-color jw-reset">\n            <span class="jw-text jw-dock-text jw-reset">'+e.escapeExpression((o=null!=(o=n.tooltip||(null!=t?t.tooltip:t))?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"tooltip",hash:{},data:r}):o))+"</span>\n        </div>\n"},compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o;return'<div class="jw-dock jw-reset">\n'+(null!=(o=n.each.call(null!=t?t:{},t,{name:"each",hash:{},fn:e.program(1,r,0),inverse:e.noop,data:r}))?o:"")+"</div>"},useData:!0})},function(e,t,n){var i=n(9);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o,a=null!=t?t:{},s=n.helperMissing,l="function",u=e.escapeExpression;return'<div id="'+u((o=null!=(o=n.id||(null!=t?t.id:t))?o:s,typeof o===l?o.call(a,{name:"id",hash:{},data:r}):o))+'"class="jw-skin-'+u((o=null!=(o=n.skin||(null!=t?t.skin:t))?o:s,typeof o===l?o.call(a,{name:"skin",hash:{},data:r}):o))+' jw-error jw-reset">\n    <div class="jw-title jw-reset">\n        <div class="jw-title-primary jw-reset">'+u((o=null!=(o=n.title||(null!=t?t.title:t))?o:s,typeof o===l?o.call(a,{name:"title",hash:{},data:r}):o))+'</div>\n        <div class="jw-title-secondary jw-reset">'+u((o=null!=(o=n.body||(null!=t?t.body:t))?o:s,typeof o===l?o.call(a,{name:"body",hash:{},data:r}):o))+'</div>\n    </div>\n\n    <div class="jw-icon-container jw-reset">\n        <div class="jw-display-icon-container jw-background-color jw-reset">\n            <div class="jw-icon jw-icon-display jw-reset" aria-hidden="true"></div>\n        </div>\n    </div>\n</div>\n'},useData:!0})},function(e,t,n){var i=n(9);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){return'<div class="jw-logo jw-reset"></div>'},useData:!0})},function(e,t,n){var i=n(9);e.exports=(i["default"]||i).template({1:function(e,t,n,i,r){var o,a=e.escapeExpression;return"        <li class='jw-text jw-option jw-item-"+a((o=null!=(o=n.index||r&&r.index)?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"index",hash:{},data:r}):o))+" jw-reset'>"+a(e.lambda(null!=t?t.label:t,t))+"</li>\n"},compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o;return'<ul class="jw-menu jw-background-color jw-reset">\n'+(null!=(o=n.each.call(null!=t?t:{},t,{name:"each",hash:{},fn:e.program(1,r,0),inverse:e.noop,data:r}))?o:"")+"</ul>"},useData:!0})},function(e,t,n){var i=n(9);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o=e.lambda,a=e.escapeExpression;return'<div class="jw-nextup jw-reset">\n    <div class="jw-nextup-tooltip jw-reset">\n        <div class="jw-nextup-header jw-reset">\n            '+a(o(null!=t?t.nextUpText:t,t))+'\n        </div>\n        <div class="jw-nextup-body jw-background-color jw-reset">\n            <div class="jw-nextup-thumbnail jw-reset"></div>\n            <div class="jw-nextup-title jw-reset">'+a(o(null!=t?t.title:t,t))+'</div>\n        </div>\n    </div>\n    <button class="jw-nextup-close"></button>\n</div>\n'},useData:!0})},function(e,t,n){var i=n(9);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o;return'<div id="'+e.escapeExpression((o=null!=(o=n.id||(null!=t?t.id:t))?o:n.helperMissing,"function"==typeof o?o.call(null!=t?t:{},{name:"id",hash:{},data:r}):o))+'" class="jwplayer jw-reset jw-state-idle" tabindex="0">\n    <div class="jw-aspect jw-reset"></div>\n    <div class="jw-media jw-reset"></div>\n    <div class="jw-preview jw-reset"></div>\n    <div class="jw-title jw-reset">\n        <div class="jw-title-primary jw-reset"></div>\n        <div class="jw-title-secondary jw-reset"></div>\n    </div>\n    <div class="jw-overlays jw-reset"></div>\n    <div class="jw-controls jw-reset"></div>\n</div>'},useData:!0})},function(e,t,n){var i=n(9);e.exports=(i["default"]||i).template({1:function(e,t,n,i,r){var o,a,s=null!=t?t:{},l=n.helperMissing,u="function",c=e.escapeExpression;return'        <li class="jw-reset'+(null!=(o=n["if"].call(s,null!=t?t.featured:t,{name:"if",hash:{},fn:e.program(2,r,0),inverse:e.noop,data:r}))?o:"")+'">\n            <a href="'+c((a=null!=(a=n.link||(null!=t?t.link:t))?a:l,typeof a===u?a.call(s,{name:"link",hash:{},data:r}):a))+'" class="jw-reset" target="_top">\n'+(null!=(o=n["if"].call(s,null!=t?t.showLogo:t,{name:"if",hash:{},fn:e.program(4,r,0),inverse:e.noop,data:r}))?o:"")+"                "+c((a=null!=(a=n.title||(null!=t?t.title:t))?a:l,typeof a===u?a.call(s,{name:"title",hash:{},data:r}):a))+"\n            </a>\n        </li>\n"},2:function(e,t,n,i,r){return" jw-featured"},4:function(e,t,n,i,r){return'                <span class="jw-icon jw-rightclick-logo jw-reset"></span>\n'},compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o;return'<div class="jw-rightclick jw-reset">\n    <ul class="jw-reset">\n'+(null!=(o=n.each.call(null!=t?t:{},null!=t?t.items:t,{name:"each",hash:{},fn:e.program(1,r,0),inverse:e.noop,data:r}))?o:"")+"    </ul>\n</div>"},useData:!0})},function(e,t,n){var i=n(9);e.exports=(i["default"]||i).template({compiler:[7,">= 4.0.0"],main:function(e,t,n,i,r){var o,a=null!=t?t:{},s=n.helperMissing,l="function",u=e.escapeExpression;return'<div class="'+u((o=null!=(o=n.className||(null!=t?t.className:t))?o:s,typeof o===l?o.call(a,{name:"className",hash:{},data:r}):o))+" "+u((o=null!=(o=n.orientation||(null!=t?t.orientation:t))?o:s,typeof o===l?o.call(a,{name:"orientation",hash:{},data:r}):o))+' jw-reset" aria-hidden="true">\n    <div class="jw-slider-container jw-reset">\n        <div class="jw-rail jw-reset"></div>\n        <div class="jw-buffer jw-reset"></div>\n        <div class="jw-progress jw-reset"></div>\n        <div class="jw-knob jw-reset"></div>\n    </div>\n</div>'},useData:!0})},,function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}function r(e){if(e&&e.__esModule)return e;var t={};if(null!=e)for(var n in e)Object.prototype.hasOwnProperty.call(e,n)&&(t[n]=e[n]);return t["default"]=e,t}function o(){var e=new s.HandlebarsEnvironment;return h.extend(e,s),e.SafeString=u["default"],e.Exception=d["default"],e.Utils=h,e.escapeExpression=h.escapeExpression,e.VM=g,e.template=function(t){return g.template(t,e)},e}t.__esModule=!0;var a=n(44),s=r(a),l=n(110),u=i(l),c=n(17),d=i(c),p=n(11),h=r(p),f=n(109),g=r(f),m=n(108),w=i(m),v=o();v.create=o,w["default"](v),v["default"]=v,t["default"]=v,e.exports=t["default"]},function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}function r(e){a["default"](e)}t.__esModule=!0,t.registerDefaultDecorators=r;var o=n(98),a=i(o)},function(e,t,n){"use strict";t.__esModule=!0;var i=n(11);t["default"]=function(e){e.registerDecorator("inline",function(e,t,n,r){var o=e;return t.partials||(t.partials={},o=function(r,o){var a=n.partials;n.partials=i.extend({},a,t.partials);var s=e(r,o);return n.partials=a,s}),t.partials[r.args[0]]=r.fn,o})},e.exports=t["default"]},function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}function r(e){a["default"](e),l["default"](e),c["default"](e),p["default"](e),f["default"](e),m["default"](e),v["default"](e)}t.__esModule=!0,t.registerDefaultHelpers=r;var o=n(100),a=i(o),s=n(101),l=i(s),u=n(102),c=i(u),d=n(103),p=i(d),h=n(104),f=i(h),g=n(105),m=i(g),w=n(106),v=i(w)},function(e,t,n){"use strict";t.__esModule=!0;var i=n(11);t["default"]=function(e){e.registerHelper("blockHelperMissing",function(t,n){var r=n.inverse,o=n.fn;if(t===!0)return o(this);if(t===!1||null==t)return r(this);if(i.isArray(t))return t.length>0?(n.ids&&(n.ids=[n.name]),e.helpers.each(t,n)):r(this);if(n.data&&n.ids){var a=i.createFrame(n.data);a.contextPath=i.appendContextPath(n.data.contextPath,n.name),n={data:a}}return o(t,n)})},e.exports=t["default"]},function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}t.__esModule=!0;var r=n(11),o=n(17),a=i(o);t["default"]=function(e){e.registerHelper("each",function(e,t){function n(t,n,o){u&&(u.key=t,u.index=n,u.first=0===n,u.last=!!o,c&&(u.contextPath=c+t)),l+=i(e[t],{data:u,blockParams:r.blockParams([e[t],t],[c+t,null])})}if(!t)throw new a["default"]("Must pass iterator to #each");var i=t.fn,o=t.inverse,s=0,l="",u=void 0,c=void 0;if(t.data&&t.ids&&(c=r.appendContextPath(t.data.contextPath,t.ids[0])+"."),r.isFunction(e)&&(e=e.call(this)),t.data&&(u=r.createFrame(t.data)),e&&"object"==typeof e)if(r.isArray(e))for(var d=e.length;s<d;s++)s in e&&n(s,s,s===e.length-1);else{var p=void 0;for(var h in e)e.hasOwnProperty(h)&&(void 0!==p&&n(p,s-1),p=h,s++);void 0!==p&&n(p,s-1,!0)}return 0===s&&(l=o(this)),l})},e.exports=t["default"]},function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}t.__esModule=!0;var r=n(17),o=i(r);t["default"]=function(e){e.registerHelper("helperMissing",function(){if(1!==arguments.length)throw new o["default"]('Missing helper: "'+arguments[arguments.length-1].name+'"')})},e.exports=t["default"]},function(e,t,n){"use strict";t.__esModule=!0;var i=n(11);t["default"]=function(e){e.registerHelper("if",function(e,t){return i.isFunction(e)&&(e=e.call(this)),!t.hash.includeZero&&!e||i.isEmpty(e)?t.inverse(this):t.fn(this)}),e.registerHelper("unless",function(t,n){return e.helpers["if"].call(this,t,{fn:n.inverse,inverse:n.fn,hash:n.hash})})},e.exports=t["default"]},function(e,t){"use strict";t.__esModule=!0,t["default"]=function(e){e.registerHelper("log",function(){for(var t=[void 0],n=arguments[arguments.length-1],i=0;i<arguments.length-1;i++)t.push(arguments[i]);var r=1;null!=n.hash.level?r=n.hash.level:n.data&&null!=n.data.level&&(r=n.data.level),t[0]=r,e.log.apply(e,t)})},e.exports=t["default"]},function(e,t){"use strict";t.__esModule=!0,t["default"]=function(e){e.registerHelper("lookup",function(e,t){return e&&e[t]})},e.exports=t["default"]},function(e,t,n){"use strict";t.__esModule=!0;var i=n(11);t["default"]=function(e){e.registerHelper("with",function(e,t){i.isFunction(e)&&(e=e.call(this));var n=t.fn;if(i.isEmpty(e))return t.inverse(this);var r=t.data;return t.data&&t.ids&&(r=i.createFrame(t.data),r.contextPath=i.appendContextPath(t.data.contextPath,t.ids[0])),n(e,{data:r,blockParams:i.blockParams([e],[r&&r.contextPath])})})},e.exports=t["default"]},function(e,t,n){"use strict";t.__esModule=!0;var i=n(11),r={methodMap:["debug","info","warn","error"],level:"info",lookupLevel:function(e){if("string"==typeof e){var t=i.indexOf(r.methodMap,e.toLowerCase());e=t>=0?t:parseInt(e,10)}return e},log:function(e){if(e=r.lookupLevel(e),"undefined"!=typeof console&&r.lookupLevel(r.level)<=e){var t=r.methodMap[e];console[t]||(t="log");for(var n=arguments.length,i=Array(n>1?n-1:0),o=1;o<n;o++)i[o-1]=arguments[o];console[t].apply(console,i)}}};t["default"]=r,e.exports=t["default"]},function(e,t){(function(n){"use strict";t.__esModule=!0,t["default"]=function(e){var t="undefined"!=typeof n?n:window,i=t.Handlebars;e.noConflict=function(){return t.Handlebars===e&&(t.Handlebars=i),e}},e.exports=t["default"]}).call(t,function(){return this}())},function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{"default":e}}function r(e){if(e&&e.__esModule)return e;var t={};if(null!=e)for(var n in e)Object.prototype.hasOwnProperty.call(e,n)&&(t[n]=e[n]);return t["default"]=e,t}function o(e){var t=e&&e[0]||1,n=w.COMPILER_REVISION;if(t!==n){if(t<n){var i=w.REVISION_CHANGES[n],r=w.REVISION_CHANGES[t];throw new m["default"]("Template was precompiled with an older version of Handlebars than the current runtime. Please update your precompiler to a newer version ("+i+") or downgrade your runtime to an older version ("+r+").")}throw new m["default"]("Template was precompiled with a newer version of Handlebars than the current runtime. Please update your runtime to a newer version ("+e[1]+").")}}function a(e,t){function n(n,i,r){r.hash&&(i=f.extend({},i,r.hash),r.ids&&(r.ids[0]=!0)),n=t.VM.resolvePartial.call(this,n,i,r);var o=t.VM.invokePartial.call(this,n,i,r);if(null==o&&t.compile&&(r.partials[r.name]=t.compile(n,e.compilerOptions,t),o=r.partials[r.name](i,r)),null!=o){if(r.indent){for(var a=o.split("\n"),s=0,l=a.length;s<l&&(a[s]||s+1!==l);s++)a[s]=r.indent+a[s];o=a.join("\n")}return o}throw new m["default"]("The partial "+r.name+" could not be compiled when running in runtime-only mode")}function i(t){function n(t){return""+e.main(r,t,r.helpers,r.partials,a,l,s)}var o=arguments.length<=1||void 0===arguments[1]?{}:arguments[1],a=o.data;i._setup(o),!o.partial&&e.useData&&(a=d(t,a));var s=void 0,l=e.useBlockParams?[]:void 0;return e.useDepths&&(s=o.depths?t!==o.depths[0]?[t].concat(o.depths):o.depths:[t]),(n=p(e.main,n,r,o.depths||[],a,l))(t,o)}if(!t)throw new m["default"]("No environment passed to template");if(!e||!e.main)throw new m["default"]("Unknown template object: "+typeof e);e.main.decorator=e.main_d,t.VM.checkRevision(e.compiler);var r={strict:function(e,t){if(!(t in e))throw new m["default"]('"'+t+'" not defined in '+e);return e[t]},lookup:function(e,t){for(var n=e.length,i=0;i<n;i++)if(e[i]&&null!=e[i][t])return e[i][t]},lambda:function(e,t){return"function"==typeof e?e.call(t):e},escapeExpression:f.escapeExpression,invokePartial:n,fn:function(t){var n=e[t];return n.decorator=e[t+"_d"],n},programs:[],program:function(e,t,n,i,r){var o=this.programs[e],a=this.fn(e);return t||r||i||n?o=s(this,e,a,t,n,i,r):o||(o=this.programs[e]=s(this,e,a)),o},data:function(e,t){for(;e&&t--;)e=e._parent;return e},merge:function(e,t){var n=e||t;return e&&t&&e!==t&&(n=f.extend({},t,e)),n},noop:t.VM.noop,compilerInfo:e.compiler};return i.isTop=!0,i._setup=function(n){n.partial?(r.helpers=n.helpers,r.partials=n.partials,r.decorators=n.decorators):(r.helpers=r.merge(n.helpers,t.helpers),e.usePartial&&(r.partials=r.merge(n.partials,t.partials)),(e.usePartial||e.useDecorators)&&(r.decorators=r.merge(n.decorators,t.decorators)))},i._child=function(t,n,i,o){if(e.useBlockParams&&!i)throw new m["default"]("must pass block params");if(e.useDepths&&!o)throw new m["default"]("must pass parent depths");return s(r,t,e[t],n,0,i,o)},i}function s(e,t,n,i,r,o,a){function s(t){var r=arguments.length<=1||void 0===arguments[1]?{}:arguments[1],s=a;return a&&t!==a[0]&&(s=[t].concat(a)),n(e,t,e.helpers,e.partials,r.data||i,o&&[r.blockParams].concat(o),s)}return s=p(n,s,e,a,i,o),s.program=t,s.depth=a?a.length:0,s.blockParams=r||0,s}function l(e,t,n){return e?e.call||n.name||(n.name=e,e=n.partials[e]):e="@partial-block"===n.name?n.data["partial-block"]:n.partials[n.name],e}function u(e,t,n){n.partial=!0,n.ids&&(n.data.contextPath=n.ids[0]||n.data.contextPath);var i=void 0;if(n.fn&&n.fn!==c&&(n.data=w.createFrame(n.data),i=n.data["partial-block"]=n.fn,i.partials&&(n.partials=f.extend({},n.partials,i.partials))),void 0===e&&i&&(e=i),void 0===e)throw new m["default"]("The partial "+n.name+" could not be found");if(e instanceof Function)return e(t,n)}function c(){return""}function d(e,t){return t&&"root"in t||(t=t?w.createFrame(t):{},t.root=e),t}function p(e,t,n,i,r,o){if(e.decorator){var a={};t=e.decorator(t,a,n,i&&i[0],r,o,i),f.extend(t,a)}return t}t.__esModule=!0,t.checkRevision=o,t.template=a,t.wrapProgram=s,t.resolvePartial=l,t.invokePartial=u,t.noop=c;var h=n(11),f=r(h),g=n(17),m=i(g),w=n(44)},function(e,t){"use strict";function n(e){this.string=e}t.__esModule=!0,n.prototype.toString=n.prototype.toHTML=function(){return""+this.string},t["default"]=n,e.exports=t["default"]},,function(e,t,n){var i,r;i=[n(21),n(1)],r=function(e,t){return function(n,i){var r=["seek","skipAd","stop","playlistNext","playlistPrev","playlistItem","resize","addButton","removeButton","registerPlugin","attachMedia","next"];t.each(r,function(e){n[e]=function(){return i[e].apply(i,arguments),n}}),n.registerPlugin=e.registerPlugin}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){return function(t,n){var i=["buffer","controls","position","duration","fullscreen","volume","mute","item","stretching","playlist","captions"];e.each(i,function(e){var i=e.slice(0,1).toUpperCase()+e.slice(1);t["get"+i]=function(){return n._model.get(e)}});var r=["getAudioTracks","getCaptionsList","getWidth","getHeight","getCurrentAudioTrack","setCurrentAudioTrack","getCurrentCaptions","setCurrentCaptions","getCurrentQuality","setCurrentQuality","getQualityLevels","getVisualQuality","getConfig","getState","getSafeRegion","isBeforeComplete","isBeforePlay","getProvider","detachMedia"],o=["setControls","setFullscreen","setVolume","setMute","setCues","setCaptions"];e.each(r,function(e){t[e]=function(){return n[e]?n[e].apply(n,arguments):null}}),e.each(o,function(e){t[e]=function(){return n[e].apply(n,arguments),t}}),t.getPlaylistIndex=t.getItem}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(3),n(5),n(4),n(2),n(64),n(1),n(166),n(112),n(113),n(115),n(24)],r=function(e,t,n,i,r,o,a,s,l,u,c){var d=function(d,p){var h,f=this,g=!1,m={};o.extend(this,n),this.utils=i,this._=o,this.Events=n,this.version=c,this.trigger=function(e,t){t=o.isObject(t)?o.extend({},t):{},t.type=e;var i=window.jwplayer;return i&&i.debug?n.trigger.call(f,e,t):n.triggerSafe.call(f,e,t)},this.dispatchEvent=this.trigger,this.removeEventListener=this.off.bind(this);var w=function(){h=new a(d),s(f,h),l(f,h),h.on(e.JWPLAYER_PLAYLIST_ITEM,function(){m={}}),h.on(e.JWPLAYER_MEDIA_META,function(e){o.extend(m,e.metadata)}),h.on(e.JWPLAYER_READY,function(e){g=!0,v.tick("ready"),e.setupTime=v.between("setup","ready")}),h.on("all",f.trigger)};w(),u(this),this.id=d.id;var v=this._qoe=new r;v.tick("init");var y=function(){g=!1,m={},f.off(),h&&h.off(),h&&h.playerDestroy&&h.playerDestroy()};return this.getPlugin=function(e){return f.plugins&&f.plugins[e]},this.addPlugin=function(e,t){this.plugins=this.plugins||{},this.plugins[e]=t,this.onReady(t.addToPlayer),t.resize&&this.onResize(t.resizeHandler)},this.setup=function(e){return v.tick("setup"),y(),w(),i.foreach(e.events,function(e,t){var n=f[e];"function"==typeof n&&n.call(f,t)}),e.id=f.id,h.setup(e,this),f},this.qoe=function(){var t=h.getItemQoe(),n=v.between("setup","ready"),i=t.between(e.JWPLAYER_MEDIA_PLAY_ATTEMPT,e.JWPLAYER_MEDIA_FIRST_FRAME);return{setupTime:n,firstFrame:i,player:v.dump(),item:t.dump()}},this.getContainer=function(){return h.getContainer?h.getContainer():d},this.getMeta=this.getItemMeta=function(){return m},this.getPlaylistItem=function(e){if(!i.exists(e))return h._model.get("playlistItem");var t=f.getPlaylist();return t?t[e]:null},this.getRenderingMode=function(){return"html5"},this.load=function(e){return h.load(e),f},this.play=function(e,n){if(o.isBoolean(e)||(n=e),n||(n={reason:"external"}),e===!0)return h.play(n),f;if(e===!1)return h.pause(n),f;switch(e=f.getState()){case t.PLAYING:case t.BUFFERING:h.pause(n);break;default:h.play(n)}return f},this.pause=function(e){return o.isBoolean(e)?this.play(!e):this.play()},this.createInstream=function(){return h.createInstream()},this.castToggle=function(){h&&h.castToggle&&h.castToggle()},this.playAd=this.pauseAd=i.noop,this.remove=function(){return p(f),f.trigger("remove"),y(),f},this};return d}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(3)],r=function(e,t){return function(n){var i={onBufferChange:t.JWPLAYER_MEDIA_BUFFER,onBufferFull:t.JWPLAYER_MEDIA_BUFFER_FULL,onError:t.JWPLAYER_ERROR,onSetupError:t.JWPLAYER_SETUP_ERROR,onFullscreen:t.JWPLAYER_FULLSCREEN,onMeta:t.JWPLAYER_MEDIA_META,onMute:t.JWPLAYER_MEDIA_MUTE,onPlaylist:t.JWPLAYER_PLAYLIST_LOADED,onPlaylistItem:t.JWPLAYER_PLAYLIST_ITEM,onPlaylistComplete:t.JWPLAYER_PLAYLIST_COMPLETE,onReady:t.JWPLAYER_READY,onResize:t.JWPLAYER_RESIZE,onComplete:t.JWPLAYER_MEDIA_COMPLETE,onSeek:t.JWPLAYER_MEDIA_SEEK,onTime:t.JWPLAYER_MEDIA_TIME,onVolume:t.JWPLAYER_MEDIA_VOLUME,onBeforePlay:t.JWPLAYER_MEDIA_BEFOREPLAY,onBeforeComplete:t.JWPLAYER_MEDIA_BEFORECOMPLETE,onDisplayClick:t.JWPLAYER_DISPLAY_CLICK,onControls:t.JWPLAYER_CONTROLS,onQualityLevels:t.JWPLAYER_MEDIA_LEVELS,onQualityChange:t.JWPLAYER_MEDIA_LEVEL_CHANGED,onCaptionsList:t.JWPLAYER_CAPTIONS_LIST,onCaptionsChange:t.JWPLAYER_CAPTIONS_CHANGED,onAdError:t.JWPLAYER_AD_ERROR,onAdClick:t.JWPLAYER_AD_CLICK,onAdImpression:t.JWPLAYER_AD_IMPRESSION,onAdTime:t.JWPLAYER_AD_TIME,onAdComplete:t.JWPLAYER_AD_COMPLETE,onAdCompanions:t.JWPLAYER_AD_COMPANIONS,onAdSkipped:t.JWPLAYER_AD_SKIPPED,onAdPlay:t.JWPLAYER_AD_PLAY,onAdPause:t.JWPLAYER_AD_PAUSE,onAdMeta:t.JWPLAYER_AD_META,onCast:t.JWPLAYER_CAST_SESSION,onAudioTrackChange:t.JWPLAYER_AUDIO_TRACK_CHANGED,onAudioTracks:t.JWPLAYER_AUDIO_TRACKS},r={onBuffer:"buffer",onPause:"pause",onPlay:"play",onIdle:"idle"};e.each(r,function(t,i){n[i]=e.partial(n.on,t,e)}),e.each(i,function(t,i){n[i]=e.partial(n.on,t,e)})}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(114),n(1),n(39),n(38),n(21)],r=function(e,t,n,i,r){var o=[],a=0,s=function(t){var n,i;return t?"string"==typeof t?(n=l(t),n||(i=document.getElementById(t))):"number"==typeof t?n=o[t]:t.nodeType&&(i=t,n=l(i.id)):n=o[0],n?n:i?u(new e(i,c)):{registerPlugin:r.registerPlugin}},l=function(e){for(var t=0;t<o.length;t++)if(o[t].id===e)return o[t];return null},u=function(e){return a++,e.uniqueId=a,o.push(e),e},c=function(e){for(var t=o.length;t--;)if(o[t].uniqueId===e.uniqueId){o.splice(t,1);break}},d={selectPlayer:s,registerProvider:n.registerProvider,availableProviders:i,registerPlugin:r.registerPlugin};return s.api=d,d}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(167),n(4),n(1),n(3)],r=function(e,t,n,i){var r=function(t,r,o,a){function s(){p("Setup Timeout Error","Setup took longer than "+m+" seconds to complete.")}function l(){n.each(g,function(e){e.complete!==!0&&e.running!==!0&&null!==t&&c(e.depends)&&(e.running=!0,u(e))})}function u(e){var n=function(t){t=t||{},d(e,t)};e.method(n,r,t,o,a)}function c(e){return n.all(e,function(e){return g[e].complete})}function d(e,t){"error"===t.type?p(t.msg,t.reason):"complete"===t.type?(clearTimeout(h),f.trigger(i.JWPLAYER_READY)):(e.complete=!0,l())}function p(e,t){clearTimeout(h),f.trigger(i.JWPLAYER_SETUP_ERROR,{message:e+": "+t}),f.destroy()}var h,f=this,g=e.getQueue(),m=30;this.start=function(){h=setTimeout(s,1e3*m),l()},this.destroy=function(){clearTimeout(h),this.off(),g.length=0,t=null,r=null,o=null}};return r.prototype=t,r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(19),n(18)],r=function(e,t,n){var i=function(i,r){function o(e){if(e.tracks.length){for(var t=e.tracks||[],n=0;n<t.length;n++){var i=t[n];v[i._id]||p(i)}var r=h();f(),this.setCaptionsList(r)}}function a(e,t){m=t,w=[],v={},y={},j=0}function s(e){a(r,e);var n=e.tracks,i=n&&n.length;if(!r.get("renderCaptionsNatively")&&i){var o,s;for(o=0;o<i;o++)s=n[o],l(s.kind)&&!v[s._id]&&(p(s),t.loadFile(s,u.bind(null,s),c))}var d=h();f(),this.setCaptionsList(d)}function l(e){return"subtitles"===e||"captions"===e}function u(e,t){e.data=t}function c(t){e.log("CAPTIONS("+t+")")}function d(e,t){var n=null;0!==t&&(n=w[t-1]),e.set("captionsTrack",n)}function p(e){if(e.data=e.data||[],e.name=e.label||e.name||e.language,e._id=n.createId(e,w.length),!e.name){var t=n.createLabel(e,j);e.name=t.label,j=t.unknownCount}w.push(e),v[e._id]=e}function h(){for(var e=[{id:"off",label:"Off"}],t=0;t<w.length;t++)e.push({id:w[t]._id,label:w[t].name||"Unknown CC"});return e}function f(){var e=0,t=r.get("captionLabel");if("Off"===t)return void r.set("captionsIndex",0);for(var n=0;n<w.length;n++){var i=w[n];if(t&&t===i.name){e=n+1;break}i["default"]||i.defaulttrack||"default"===i._id?e=n+1:i.autoselect}g(e)}function g(e){w.length?r.setVideoSubtitleTrack(e,w):r.set("captionsIndex",e)}r.on("change:playlistItem",a,this),r.on("change:captionsIndex",d,this),r.on("itemReady",s,this),r.mediaController.on("subtitlesTracks",o,this);var m={},w=[],v={},y={},j=0;this.getCurrentIndex=function(){return r.get("captionsIndex")},this.getCaptionsList=function(){return r.get("captionsList")},this.setCaptionsList=function(e){r.set("captionsList",e)}};return i}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(45),n(120),n(1),n(117),n(118),n(25),n(125),n(50),n(49),n(2),n(75),n(4),n(29),n(5),n(3),n(154)],r=function(e,t,i,r,o,a,s,l,u,c,d,p,h,f,g,m){function w(e){return function(){var t=Array.prototype.slice.call(arguments,0);this._model.getVideo()?this["_"+e].apply(this,t):this.eventsQueue.push([e,t])}}function v(e){return e===f.LOADING||e===f.STALLED?f.BUFFERING:e}var y=function(e){this.originalContainer=this.currentContainer=e,this.eventsQueue=[],i.extend(this,p),this._model=new a};return y.prototype={play:w("play"),pause:w("pause"),seek:w("seek"),stop:w("stop"),load:w("load"),playlistNext:w("next"),playlistPrev:w("prev"),playlistItem:w("item"),setCurrentCaptions:w("setCurrentCaptions"),setCurrentQuality:w("setCurrentQuality"),setFullscreen:w("setFullscreen"),setup:function(a,p){function m(e,t){pe.triggerAfterReady(e,t)}function w(){oe=null,ie.on("all",m,pe),pe.showView(ie.element()),i.defer(y)}function y(){pe.trigger(g.JWPLAYER_READY,{setupTime:0}),pe.triggerAfterReady=pe.trigger;for(var e=0;e<me.length;e++){var t=me[e];pe.trigger(t.type,t.args)}var n=p.getPlugin("related");n&&n.on("nextUp",ue.setNextUp,ue),ne()&&(c.isMobile()&&he().video?j(he().video):pe.play({reason:"autostart"}))}function j(e){"IntersectionObserver"in window&&"IntersectionObserverEntry"in window&&"intersectionRatio"in window.IntersectionObserverEntry.prototype?b(e):n.e(9,function(require){n(54),b(e)})}function b(e){le=new window.IntersectionObserver(x,{threshold:.5}),le.observe(e)}function E(){le.disconnect(),le=void 0}function x(e){if(e&&e.length){var t=he().video,n=e[0],i={reason:"autoplay"};n.target===t&&n.intersectionRatio>=.5?pe.play(i):pe.pause(i)}}function k(e){var t=ue.getProviders(),n=t.required(e,ue.get("primary"));return t.load(n).then(function(){pe.getProvider()||(ue.setProvider(ue.get("playlistItem")),A())})}function A(){for(;pe.eventsQueue.length>0;){var e=pe.eventsQueue.shift(),t=e[0],n=e[1]||[];pe["_"+t].apply(pe,n)}}function _(e){switch(ue.get("state")===f.ERROR&&ue.set("state",f.IDLE),ue.set("preInstreamState","instream-idle"),R(!0),ne()&&ue.once("itemReady",P),pe.trigger("destroyPlugin",{}),typeof e){case"string":C(e);break;case"object":var t=D(e);t&&N(0);break;case"number":N(e)}}function C(e){var t=new u;t.on(g.JWPLAYER_PLAYLIST_LOADED,function(e){_(e.playlist)}),t.on(g.JWPLAYER_ERROR,function(e){e.message="Error loading playlist: "+e.message,this.triggerError(e)},this),t.load(e)}function L(){return pe._instreamAdapter&&pe._instreamAdapter.getState()}function T(){var e=L();return i.isString(e)?e:ue.get("state")}function P(e){var t;if(e&&ue.set("playReason",e.reason),ue.get("state")!==f.ERROR){var n=L();if(i.isString(n))return p.pauseAd(!1);if(ue.get("state")===f.COMPLETE&&(R(!0),N(0)),!ce&&(ce=!0,pe.trigger(g.JWPLAYER_MEDIA_BEFOREPLAY,{playReason:ue.get("playReason")}),ce=!1,se))return se=!1,void(ae=null);if(M()){if(0===ue.get("playlist").length)return!1;t=c.tryCatch(function(){ue.loadVideo()})}else ue.get("state")===f.PAUSED&&(t=c.tryCatch(function(){ue.playVideo()}));return!(t instanceof c.Error)||(pe.triggerError(t),ae=null,!1)}}function R(e){ue.off("itemReady",P);var t=!e;ae=null;var n=c.tryCatch(function(){ue.stopVideo()},pe);return n instanceof c.Error?(pe.triggerError(n),!1):(t&&(de=!0),ce&&(se=!0),!0)}function I(e){ae=null,e&&(ue.set("pauseReason",e.reason),!le||"interaction"!==e.reason&&"external"!==e.reason||E());var t=L();if(i.isString(t))return p.pauseAd(!0);switch(ue.get("state")){case f.ERROR:return!1;case f.PLAYING:case f.BUFFERING:var n=c.tryCatch(function(){he().pause()},this);if(n instanceof c.Error)return pe.triggerError(n),!1;break;default:ce&&(se=!0)}return!0}function M(){var e=ue.get("state");return e===f.IDLE||e===f.COMPLETE||e===f.ERROR}function S(e){ue.get("state")!==f.ERROR&&(ue.get("scrubbing")||ue.get("state")===f.PLAYING||P(!0),he().seek(e))}function O(e,t){R(!0),ue.get("state")===f.ERROR&&ue.set("state",f.IDLE),N(e),P(t)}function D(e){var t=l(e);return t=l.filterPlaylist(t,ue.getProviders(),ue.get("androidhls"),ue.get("drm"),ue.get("preload"),ue.get("feedid"),ue.get("withCredentials")),ue.set("playlist",t),i.isArray(t)&&0!==t.length?(k(t),!0):(pe.triggerError({message:"Error loading playlist: No playable sources found"}),!1)}function N(e){ue.setItemIndex(e)}function Y(e){O(ue.get("item")-1,e||{reason:"external"})}function F(e){O(ue.get("item")+1,e||{reason:"external"})}function W(){if(M()){if(de)return void(de=!1);ae=W;var e=ue.get("item");return e===ue.get("playlist").length-1?void(ue.get("repeat")?F({reason:"repeat"}):(le&&E(),ue.set("state",f.COMPLETE),pe.trigger(g.JWPLAYER_PLAYLIST_COMPLETE,{}))):void F({reason:"playlist"})}}function J(e){he()&&(e=parseInt(e,10)||0,he().setCurrentQuality(e))}function B(){return he()?he().getCurrentQuality():-1}function V(){if(this._model)return this._model.getConfiguration()}function z(){if(this._model.mediaModel)return this._model.mediaModel.get("visualQuality");var e=U();if(e){var t=B(),n=e[t];if(n)return{level:i.extend({index:t},n),mode:"",reason:""}}return null}function U(){return he()?he().getQualityLevels():null}function H(e){he()&&(e=parseInt(e,10)||0,he().setCurrentAudioTrack(e))}function G(){return he()?he().getCurrentAudioTrack():-1}function K(){return he()?he().getAudioTracks():null}function q(e){e=parseInt(e,10)||0,ue.persistVideoSubtitleTrack(e),
pe.trigger(g.JWPLAYER_CAPTIONS_CHANGED,{tracks:X(),track:e})}function Q(){return re.getCurrentIndex()}function X(){return re.getCaptionsList()}function $(){var e=ue.getVideo();if(e){var t=e.detachMedia();if(t instanceof HTMLVideoElement)return t}return null}function Z(){var e=c.tryCatch(function(){ue.getVideo().attachMedia()});return e instanceof c.Error?void c.log("Error calling _attachMedia",e):void("function"==typeof ae&&ae())}function ee(e){i.isBoolean(e)||(e=!ue.get("fullscreen")),ue.set("fullscreen",e),this._instreamAdapter&&this._instreamAdapter._adModel&&this._instreamAdapter._adModel.set("fullscreen",e)}function te(){var e=p.getPlugin("related");e&&e.next()}function ne(){return ue.get("autostart")&&!c.isMobile()||ue.autoStartOnMobile()}var ie,re,oe,ae,se,le,ue=this._model,ce=!1,de=!1,pe=this,he=function(){return ue.getVideo()},fe=new s;fe.track(ue);var ge=new e(a,fe),me=[];ue.setup(ge,fe),ie=this._view=new d(p,ue),oe=new r(p,ue,ie,D),oe.on(g.JWPLAYER_READY,w,this),oe.on(g.JWPLAYER_SETUP_ERROR,this.setupError,this),ue.mediaController.on("all",m,this),ue.mediaController.on(g.JWPLAYER_MEDIA_COMPLETE,function(){i.defer(W)}),ue.mediaController.on(g.JWPLAYER_MEDIA_ERROR,this.triggerError,this),ue.on("change:flashBlocked",function(e,t){if(!t)return void this._model.set("errorEvent",void 0);var n=!!e.get("flashThrottle"),i={message:n?"Click to run Flash":"Flash plugin failed to load"};n||this.trigger(g.JWPLAYER_ERROR,i),this._model.set("errorEvent",i)},this),ue.on("change:state",h,this),ue.on("change:castState",function(e,t){pe.trigger(g.JWPLAYER_CAST_SESSION,t)}),ue.on("change:fullscreen",function(e,t){pe.trigger(g.JWPLAYER_FULLSCREEN,{fullscreen:t}),t&&le&&E()}),ue.on("itemReady",function(){pe.triggerAfterReady(g.JWPLAYER_PLAYLIST_ITEM,{index:ue.get("item"),item:ue.get("playlistItem")})}),ue.on("change:playlist",function(e,t){t.length&&pe.triggerAfterReady(g.JWPLAYER_PLAYLIST_LOADED,{playlist:t})}),ue.on("change:volume",function(e,t){pe.trigger(g.JWPLAYER_MEDIA_VOLUME,{volume:t})}),ue.on("change:mute",function(e,t){pe.trigger(g.JWPLAYER_MEDIA_MUTE,{mute:t})}),ue.on("change:controls",function(e,t){pe.trigger(g.JWPLAYER_CONTROLS,{controls:t})}),ue.on("change:scrubbing",function(e,t){t?I():P()}),ue.on("change:captionsList",function(e,t){try{pe.triggerAfterReady(g.JWPLAYER_CAPTIONS_LIST,{tracks:t,track:Q()})}catch(n){c.log("Error with captionsList event:",n)}}),ue.on("change:mediaModel",function(e){e.mediaModel.on("change:state",function(t,n){var i=v(n);e.set("state",i)})}),re=new o(p,ue),this.triggerAfterReady=function(e,t){me.push({type:e,args:t})},this._play=P,this._pause=I,this._seek=S,this._stop=R,this._load=_,this._next=F,this._prev=Y,this._item=O,this._setCurrentCaptions=q,this._setCurrentQuality=J,this._setFullscreen=ee,this.detachMedia=$,this.attachMedia=Z,this.getCurrentQuality=B,this.getQualityLevels=U,this.setCurrentAudioTrack=H,this.getCurrentAudioTrack=G,this.getAudioTracks=K,this.getCurrentCaptions=Q,this.getCaptionsList=X,this.getVisualQuality=z,this.getConfig=V,this.getState=T,this.setVolume=ue.setVolume.bind(ue),this.setMute=ue.setMute.bind(ue),this.getProvider=function(){return ue.get("provider")},this.getWidth=function(){return ue.get("containerWidth")},this.getHeight=function(){return ue.get("containerHeight")},this.getContainer=function(){return this.currentContainer},this.resize=ie.resize,this.getSafeRegion=ie.getSafeRegion,this.setCues=ie.addCues,this.setCaptions=ie.setCaptions,this.next=te,this.addButton=function(e,t,n,r,o){var a={img:e,tooltip:t,callback:n,id:r,btnClass:o},s=ue.get("dock");s=s?s.slice(0):[],s=i.reject(s,i.matches({id:a.id})),s.push(a),ue.set("dock",s)},this.removeButton=function(e){var t=ue.get("dock")||[];t=i.reject(t,i.matches({id:e})),ue.set("dock",t)},this.checkBeforePlay=function(){return ce},this.getItemQoe=function(){return ue._qoeItem},this.setControls=function(e){i.isBoolean(e)||(e=!ue.get("controls")),ue.set("controls",e);var t=ue.getVideo();t&&t.setControls(e)},this.playerDestroy=function(){this.stop(),this.showView(this.originalContainer),ie&&ie.destroy(),ue&&ue.destroy(),oe&&(oe.destroy(),oe=null)},this.isBeforePlay=this.checkBeforePlay,this.isBeforeComplete=function(){return ue.getVideo().checkComplete()},this.createInstream=function(){return this.instreamDestroy(),this._instreamAdapter=new t(this,ue,ie),this._instreamAdapter},this.skipAd=function(){this._instreamAdapter&&this._instreamAdapter.skipAd()},this.instreamDestroy=function(){pe._instreamAdapter&&pe._instreamAdapter.destroy()},oe.start()},showView:function(e){(document.documentElement.contains(this.currentContainer)||(this.currentContainer=document.getElementById(this._model.get("id")),this.currentContainer))&&(this.currentContainer.parentElement&&this.currentContainer.parentElement.replaceChild(e,this.currentContainer),this.currentContainer=e)},triggerError:function(e){this._model.set("errorEvent",e),this._model.set("state",f.ERROR),this._model.once("change:state",function(){this._model.set("errorEvent",void 0)},this),this.trigger(g.JWPLAYER_ERROR,e)},setupError:function(e){var t=e.message,n=c.createElement(m(this._model.get("id"),this._model.get("skin"),t)),r=this._model.get("width"),o=this._model.get("height");c.style(n,{width:r.toString().indexOf("%")>0?r:r+"px",height:o.toString().indexOf("%")>0?o:o+"px"}),this.showView(n);var a=this;i.defer(function(){a.trigger(g.JWPLAYER_SETUP_ERROR,{message:t})})}},y}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(122),n(121),n(3),n(5),n(2),n(4),n(1)],r=function(e,t,n,i,r,o,a){function s(n){var i="",r=n.get("provider");return r&&(i=r.name),i.indexOf("flash")>=0?t:e}var l={skipoffset:null,tag:null},u=function(e,o,u){function c(){y._adModel.set("state","buffering"),o.set("skipButton",!1),j++;var e,t=h[j];f&&(e=f[j]),E.loadItem(t,e)}function d(e,t){"complete"!==e&&(t=t||{},b.tag&&!t.tag&&(t.tag=b.tag),this.trigger(e,t),"mediaError"!==e&&"error"!==e||h&&j+1<h.length&&c())}function p(e){y._adModel.set("duration",e.duration),y._adModel.set("position",e.position)}var h,f,g,m,w,v=s(o),y=new v(e,o),j=0,b={},E=this,x=a.bind(function(e){e=e||{},e.hasControls=!!o.get("controls"),this.trigger(n.JWPLAYER_INSTREAM_CLICK,e),y&&y._adModel&&(y._adModel.get("state")===i.PAUSED?e.hasControls&&y.instreamPlay():y.instreamPause())},this),k=a.bind(function(){y&&y._adModel&&y._adModel.get("state")===i.PAUSED&&o.get("controls")&&(e.setFullscreen(),e.play())},this);this.type="instream",this.init=function(){g=o.getVideo(),m=o.get("position"),w=o.get("playlist")[o.get("item")],y.on("all",d,this),y.on(n.JWPLAYER_MEDIA_TIME,p,this),y.on(n.JWPLAYER_MEDIA_COMPLETE,A,this),y.init(),g.detachMedia(),o.mediaModel.set("state",i.BUFFERING),e.checkBeforePlay()||0===m&&!g.checkComplete()?(m=0,o.set("preInstreamState","instream-preroll")):g&&g.checkComplete()||o.get("state")===i.COMPLETE?o.set("preInstreamState","instream-postroll"):o.set("preInstreamState","instream-midroll");var t=o.get("state");return t!==i.PLAYING&&t!==i.BUFFERING||g.pause(),u.setupInstream(y._adModel),y._adModel.set("state",i.BUFFERING),u.clickHandler().setAlternateClickHandlers(r.noop,null),this.setText(o.get("localization").loadingAd),this};var A=function(e){var t={};b.tag&&(t.tag=b.tag),h&&j+1<h.length?(this.trigger(n.JWPLAYER_MEDIA_COMPLETE,t),c()):(e.type===n.JWPLAYER_MEDIA_COMPLETE&&(this.trigger(n.JWPLAYER_MEDIA_COMPLETE,t),this.trigger(n.JWPLAYER_PLAYLIST_COMPLETE,{})),this.destroy())};this.loadItem=function(e,i){if(r.isAndroid(2.3))return void this.trigger({type:n.JWPLAYER_ERROR,message:"Error loading instream: Cannot play instream on Android 2.3"});var s=e;a.isArray(e)?(h=e,f=i,e=h[j],f&&(i=f[j])):s=[e];var u=this,c=o.getProviders(),d=v===t?"flash":void 0,p=c.required(s,d);o.set("hideAdsControls",!1),c.load(p).then(function(){if(null!==y){u.trigger(n.JWPLAYER_PLAYLIST_ITEM,{index:j,item:e}),b=a.extend({},l,i),y.load(e),u.addClickHandler();var t=e.skipoffset||b.skipoffset;t&&u.setupSkipButton(t,b)}})},this.setupSkipButton=function(e,t,n){o.set("skipButton",!1),n&&(A=n),y._adModel.set("skipMessage",t.skipMessage),y._adModel.set("skipText",t.skipText),y._adModel.set("skipOffset",e),o.set("skipButton",!0)},this.applyProviderListeners=function(e){y.applyProviderListeners(e),this.addClickHandler()},this.play=function(){y.instreamPlay()},this.pause=function(){y.instreamPause()},this.addClickHandler=function(){u.clickHandler().setAlternateClickHandlers(x,k),y.on(n.JWPLAYER_MEDIA_META,this.metaHandler,this)},this.skipAd=function(e){var t=n.JWPLAYER_AD_SKIPPED;this.trigger(t,e),A.call(this,{type:t})},this.metaHandler=function(e){e.width&&e.height&&u.resizeMedia()},this.destroy=function(){if(this.off(),o.set("skipButton",!1),y){u.clickHandler()&&u.clickHandler().revertAlternateClickHandlers(),o.off(null,null,y),y.instreamDestroy(),u.destroyInstream(),y=null,e.attachMedia();var t=o.get("preInstreamState");switch(t){case"instream-preroll":case"instream-midroll":var n=a.extend({},w);n.starttime=m,o.loadVideo(n),r.isMobile()&&o.mediaModel.get("state")===i.BUFFERING&&g.setState(i.BUFFERING),g.play();break;case"instream-postroll":case"instream-idle":g.stop()}}},this.getState=function(){return!(!y||!y._adModel)&&y._adModel.get("state")},this.setText=function(e){u.setAltText(e?e:"")},this.hide=function(){o.set("hideAdsControls",!0)}};return a.extend(u.prototype,o),u}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(4),n(25),n(29),n(3),n(5),n(2),n(1)],r=function(e,t,n,i,r,o,a){var s=function(e,i){this.model=i,this._adModel=(new t).setup({id:i.get("id"),volume:i.get("volume"),fullscreen:i.get("fullscreen"),mute:i.get("mute")}),this._adModel.on("change:state",n,this);var r=e.getContainer();this.swf=r.querySelector("object")};return s.prototype=a.extend({init:function(){if(o.isChrome()){var e=-1,t=!1;this.swf.on("throttle",function(n){if(clearTimeout(e),"resume"===n.state)t&&(t=!1,this.instreamPlay());else{var i=this;e=setTimeout(function(){i._adModel.get("state")===r.PLAYING&&(t=!0,i.instreamPause())},250)}},this)}this.swf.on("instream:state",this.stateHandler,this).on("instream:time",function(e){this._adModel.set("position",e.position),this._adModel.set("duration",e.duration),this.trigger(i.JWPLAYER_MEDIA_TIME,e)},this).on("instream:complete",function(e){this.trigger(i.JWPLAYER_MEDIA_COMPLETE,e)},this).on("instream:error",function(e){this.trigger(i.JWPLAYER_MEDIA_ERROR,e)},this),this.swf.triggerFlash("instream:init"),this.applyProviderListeners=function(e){this.model.on("change:volume",function(t,n){e.volume(n)},this),this.model.on("change:mute",function(t,n){e.mute(n)},this),e.volume(this.model.get("volume")),e.mute(this.model.get("mute")),e.off(),e.on(i.JWPLAYER_PLAYER_STATE,this.stateHandler,this),e.on(i.JWPLAYER_MEDIA_TIME,function(e){this.trigger(i.JWPLAYER_MEDIA_TIME,e)},this)}},stateHandler:function(e){switch(e.newstate){case r.PLAYING:case r.PAUSED:this._adModel.set("state",e.newstate)}},instreamDestroy:function(){this._adModel&&(this.off(),this.swf.off(null,null,this),this.swf.triggerFlash("instream:destroy"),this.swf=null,this._adModel.off(),this._adModel=null,this.model=null)},load:function(e){this.swf.triggerFlash("instream:load",e)},instreamPlay:function(){this.swf.triggerFlash("instream:play")},instreamPause:function(){this.swf.triggerFlash("instream:pause")}},e),s}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(4),n(29),n(3),n(5),n(25)],r=function(e,t,n,i,r,o){var a=function(a,s){function l(t){var r=t||p.getVideo();if(h!==r){if(h=r,!r)return;r.off(),r.on("all",function(t,n){n=e.extend({},n,{type:t}),this.trigger(t,n)},f),r.on(i.JWPLAYER_MEDIA_BUFFER_FULL,d),r.on(i.JWPLAYER_PLAYER_STATE,u),r.attachMedia(),r.volume(s.get("volume")),r.mute(s.get("mute")||s.get("autostartMuted")),p.on("change:state",n,f)}}function u(e){switch(e.newstate){case r.PLAYING:case r.PAUSED:p.set("state",e.newstate)}}function c(e){s.trigger(e.type,e),f.trigger(i.JWPLAYER_FULLSCREEN,{fullscreen:e.jwstate})}function d(){p.getVideo().play()}var p,h,f=e.extend(this,t);return a.on(i.JWPLAYER_FULLSCREEN,function(e){this.trigger(i.JWPLAYER_FULLSCREEN,e)},f),this.init=function(){p=(new o).setup({id:s.get("id"),volume:s.get("volume"),fullscreen:s.get("fullscreen"),mute:s.get("mute")||s.get("autostartMuted"),instreamMode:!0}),p.on("fullscreenchange",c),this._adModel=p},f.load=function(e){p.set("item",0),p.set("playlistItem",e),p.setActiveItem(e),l(),p.off(i.JWPLAYER_ERROR),p.on(i.JWPLAYER_ERROR,function(e){this.trigger(i.JWPLAYER_ERROR,e)},f),p.loadVideo(e)},f.applyProviderListeners=function(e){l(e),e.off(i.JWPLAYER_ERROR),e.on(i.JWPLAYER_ERROR,function(e){this.trigger(i.JWPLAYER_ERROR,e)},f),s.on("change:volume",function(e,t){h.volume(t)},f),s.on("change:mute",function(e,t){h.mute(t)},f),s.on("change:autostartMuted",function(e,t){t||h.mute(s.get("mute"))},f)},this.instreamDestroy=function(){p&&(p.off(),this.off(),h&&(h.detachMedia(),h.off(),p.getVideo()&&h.destroy()),p=null,a.off(null,null,this),a=null)},f.instreamPlay=function(){p.getVideo()&&p.getVideo().play(!0)},f.instreamPause=function(){p.getVideo()&&p.getVideo().pause(!0)},f};return a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(64),n(3),n(1)],r=function(e,t,n){function i(e){e.mediaController.off(t.JWPLAYER_MEDIA_PLAY_ATTEMPT,e._onPlayAttempt),e.mediaController.off(t.JWPLAYER_PROVIDER_FIRST_FRAME,e._triggerFirstFrame),e.mediaController.off(t.JWPLAYER_MEDIA_TIME,e._onTime)}function r(e){i(e),e._triggerFirstFrame=n.once(function(){var n=e._qoeItem;n.tick(t.JWPLAYER_MEDIA_FIRST_FRAME);var r=n.between(t.JWPLAYER_MEDIA_PLAY_ATTEMPT,t.JWPLAYER_MEDIA_FIRST_FRAME);e.mediaController.trigger(t.JWPLAYER_MEDIA_FIRST_FRAME,{loadTime:r}),i(e)}),e._onTime=a(e._triggerFirstFrame),e._onPlayAttempt=function(){e._qoeItem.tick(t.JWPLAYER_MEDIA_PLAY_ATTEMPT)},e.mediaController.on(t.JWPLAYER_MEDIA_PLAY_ATTEMPT,e._onPlayAttempt),e.mediaController.once(t.JWPLAYER_PROVIDER_FIRST_FRAME,e._triggerFirstFrame),e.mediaController.on(t.JWPLAYER_MEDIA_TIME,e._onTime)}function o(n){function i(n,i,o){n._qoeItem&&o&&n._qoeItem.end(o.get("state")),n._qoeItem=new e,n._qoeItem.tick(t.JWPLAYER_PLAYLIST_ITEM),n._qoeItem.start(i.get("state")),r(n),i.on("change:state",function(e,t,i){n._qoeItem.end(i),n._qoeItem.start(t)})}n.on("change:mediaModel",i)}var a=function(e){var t=0;return function(n){var i=n.position;i>t&&e(),t=i}};return{model:o}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(21),n(49),n(15),n(61),n(22),n(1),n(2),n(3)],r=function(e,t,i,r,o,a,s,l){function u(){var e={LOAD_PROMISE_POLYFILL:{method:c,depends:[]},LOAD_BASE64_POLYFILL:{method:d,depends:[]},LOADED_POLYFILLS:{method:p,depends:["LOAD_PROMISE_POLYFILL","LOAD_BASE64_POLYFILL"]},LOAD_PLUGINS:{method:h,depends:["LOADED_POLYFILLS"]},INIT_PLUGINS:{method:f,depends:["LOAD_PLUGINS","SETUP_VIEW"]},LOAD_SKIN:{method:E,depends:["LOADED_POLYFILLS"]},LOAD_PLAYLIST:{method:m,depends:["LOADED_POLYFILLS"]},CHECK_FLASH:{method:w,depends:["LOADED_POLYFILLS"]},FILTER_PLAYLIST:{method:v,depends:["LOAD_PLAYLIST","CHECK_FLASH"]},SETUP_VIEW:{method:x,depends:["LOAD_SKIN"]},SET_ITEM:{method:k,depends:["INIT_PLUGINS","FILTER_PLAYLIST"]},SEND_READY:{method:A,depends:["SETUP_VIEW","SET_ITEM"]}};return e}function c(e){window.Promise?e():n.e(8,function(require){n(55),e()})}function d(e){window.btoa&&window.atob?e():n.e(10,function(require){n(53),e()})}function p(e){e()}function h(t,n){window.jwplayerPluginJsonp=e.registerPlugin,C=e.loadPlugins(n.get("id"),n.get("plugins")),C.on(l.COMPLETE,t),C.on(l.ERROR,a.partial(g,t)),C.load()}function f(e,t,n){delete window.jwplayerPluginJsonp,C.setupPlugins(n,t),e()}function g(e,t){_(e,"Could not load plugin",t.message)}function m(e,n){var i=n.get("playlist");a.isString(i)?(L=new t,L.on(l.JWPLAYER_PLAYLIST_LOADED,function(t){n.set("playlist",t.playlist),n.set("feedid",t.feedid),e()}),L.on(l.JWPLAYER_ERROR,a.partial(y,e)),L.load(i)):e()}function w(e,t,n){var i="flash"===t.get("primary"),o=s.flashVersion();if(i&&o){var a=n.getContainer(),l=a.parentElement;l||e();var u=document.createElement("div");u.id=t.get("id");var c=""+u.id+"-"+Math.random().toString(16).substr(2),d=t.get("flashloader"),p=t.get("width"),h=t.get("height");s.style(u,{position:"relative",width:p.toString().indexOf("%")>0?p:p+"px",height:h.toString().indexOf("%")>0?h:h+"px"});var f=r.embed(d,u,c,null);l.replaceChild(u,a);var g=function(){clearTimeout(m),f.embedCallback=null,e()};f.embedCallback=g;var m=setTimeout(function(){t.set("primary",void 0),t.updateProviders(),g()},1500)}else e()}function v(e,t,n,i,r){var o=t.get("playlist"),a=r(o);a?e():y(e)}function y(e,t){t&&t.message?_(e,"Error loading playlist",t.message):_(e,"Error loading player","No playable sources found")}function j(e,t){if(a.contains(o.SkinsLoadable,e))return t+"skins/"+e+".css"}function b(e){for(var t=document.styleSheets,n=0,i=t.length;n<i;n++)if(t[n].href===e)return!0;return!1}function E(e,t){var n=t.get("skin"),r=t.get("skinUrl");if(a.contains(o.SkinsIncluded,n))return void e();if(r||(r=j(n,t.get("base"))),a.isString(r)&&!b(r)){t.set("skin-loading",!0);var s=!0,u=new i(r,s);u.addEventListener(l.COMPLETE,function(){t.set("skin-loading",!1)}),u.addEventListener(l.ERROR,function(){t.set("skin","seven"),t.set("skin-loading",!1)}),u.load()}a.defer(function(){e()})}function x(e,t,n,i){i.setup(),e()}function k(e,t){t.once("itemReady",e),t.setItemIndex(t.get("item"))}function A(e){e({type:"complete"})}function _(e,t,n){e({type:"error",msg:t,reason:n})}var C,L;return{getQueue:u,error:_}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2)],r=function(e,t){function n(e){return"jwplayer."+e}function i(){return e.reduce(this.persistItems,function(e,i){var r=l[n(i)];return r&&(e[i]=t.serialize(r)),e},{})}function r(e,t){try{l[n(e)]=t}catch(i){var r=window.jwplayer;r&&r.debug&&console.error(i)}}function o(){e.each(this.persistItems,function(e){l.removeItem(n(e))})}function a(){this.persistItems=["volume","mute","captionLabel","qualityLabel"]}function s(t){e.each(this.persistItems,function(e){t.on("change:"+e,function(t,n){r(e,n)})})}var l={removeItem:t.noop};try{l=window.localStorage}catch(u){}return e.extend(a.prototype,{getAllItems:i,track:s,clear:o}),a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(69),n(2)],r=function(e,t){return n.p=t.loadFrom(),e.selectPlayer}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(8)],r=function(e){function t(e){e||n()}function n(){throw new Error("Invalid DFXP file")}var i=e.seconds;return function(r){t(r);var o=[],a=r.getElementsByTagName("p"),s=30,l=r.getElementsByTagName("tt");if(l&&l[0]){var u=parseFloat(l[0].getAttribute("ttp:frameRate"));isNaN(u)||(s=u)}t(a),a.length||(a=r.getElementsByTagName("tt:p"),a.length||(a=r.getElementsByTagName("tts:p")));for(var c=0;c<a.length;c++){for(var d=a[c],p=d.getElementsByTagName("br"),h=0;h<p.length;h++){var f=p[h];f.parentNode.replaceChild(r.createTextNode("\r\n"),f)}var g=d.innerHTML||d.textContent||d.text||"",m=e.trim(g).replace(/>\s+</g,"><").replace(/(<\/?)tts?:/g,"$1").replace(/<br.*?\/>/g,"\r\n");if(m){var w=d.getAttribute("begin"),v=d.getAttribute("dur"),y=d.getAttribute("end"),j={begin:i(w,s),text:m};y?j.end=i(y,s):v&&(j.end=j.begin+i(v,s)),o.push(j)}}return o.length||n(),o}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(20),n(8),n(2)],r=function(e,t,n){var i="jwplayer",r=function(r,o){for(var a=[],s=[],l=t.xmlAttribute,u="default",c="label",d="file",p="type",h=0;h<r.childNodes.length;h++){var f=r.childNodes[h];if(f.prefix===i){var g=e.localName(f);"source"===g?(delete o.sources,a.push({file:l(f,d),"default":l(f,u),label:l(f,c),type:l(f,p)})):"track"===g?(delete o.tracks,s.push({file:l(f,d),"default":l(f,u),kind:l(f,"kind"),label:l(f,c)})):(o[g]=n.serialize(e.textContent(f)),"file"===g&&o.sources&&delete o.sources)}o[d]||(o[d]=o.link)}if(a.length)for(o.sources=[],h=0;h<a.length;h++)a[h].file.length>0&&(a[h][u]="true"===a[h][u],a[h].label.length||delete a[h].label,o.sources.push(a[h]));if(s.length)for(o.tracks=[],h=0;h<s.length;h++)s[h].file.length>0&&(s[h][u]="true"===s[h][u],s[h].kind=s[h].kind.length?s[h].kind:"captions",s[h].label.length||delete s[h].label,o.tracks.push(s[h]));return o};return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(20),n(8),n(2)],r=function(e,t,n){function i(e){for(var t=[],n=0;n<s(e);n++){var i=e.childNodes[n];"jwplayer"===i.prefix&&"mediatypes"===o(i).toLowerCase()&&t.push(a(i))}return t}var r=t.xmlAttribute,o=e.localName,a=e.textContent,s=e.numChildren,l="media",u=function(e,t){function c(e){var t={zh:"Chinese",nl:"Dutch",en:"English",fr:"French",de:"German",it:"Italian",ja:"Japanese",pt:"Portuguese",ru:"Russian",es:"Spanish"};return t[e]?t[e]:e}var d,p,h="tracks",f=[];for(p=0;p<s(e);p++)if(d=e.childNodes[p],d.prefix===l){if(!o(d))continue;switch(o(d).toLowerCase()){case"content":if(r(d,"duration")&&(t.duration=n.seconds(r(d,"duration"))),r(d,"url")){t.sources||(t.sources=[]);var g={file:r(d,"url"),type:r(d,"type"),width:r(d,"width"),label:r(d,"label")},m=i(d);m.length&&(g.mediaTypes=m),t.sources.push(g)}s(d)>0&&(t=u(d,t));break;case"title":t.title=a(d);break;case"description":t.description=a(d);break;case"guid":t.mediaid=a(d);break;case"thumbnail":t.image||(t.image=r(d,"url"));break;case"player":break;case"group":u(d,t);break;case"subtitle":var w={};w.file=r(d,"url"),w.kind="captions",r(d,"lang").length>0&&(w.label=c(r(d,"lang"))),f.push(w)}}for(t.hasOwnProperty(h)||(t[h]=[]),p=0;p<f.length;p++)t[h].push(f[p]);return t};return u}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){var t={kind:"captions","default":!1},n=function(n){if(n&&n.file)return e.extend({},t,n)};return n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(26),n(2),n(3),n(4),n(1),n(15)],r=function(e,t,n,i,r,o){function a(e,t,n){return function(){var i=e.getContainer().getElementsByClassName("jw-overlays")[0];i&&(i.appendChild(n),n.left=i.style.left,n.top=i.style.top,t.displayArea=i)}}function s(e){function t(){var t=e.displayArea;t&&e.resize(t.clientWidth,t.clientHeight)}return function(){t(),setTimeout(t,400)}}var l=function(l,u){function c(){m||(m=!0,g=o.loaderstatus.COMPLETE,f.trigger(n.COMPLETE))}function d(){if(!v&&(u&&0!==r.keys(u).length||c(),!m)){var i=l.getPlugins();h=r.after(w,c),r.each(u,function(r,a){var s=e.getPluginName(a),l=i[s],u=l.getJS(),c=l.getTarget(),d=l.getStatus();d!==o.loaderstatus.LOADING&&d!==o.loaderstatus.NEW&&(u&&!t.versionCheck(c)&&f.trigger(n.ERROR,{message:"Incompatible player version"}),h())})}}function p(e){if(!v){var i="File not found";e.url&&t.log(i,e.url),this.off(),this.trigger(n.ERROR,{message:i}),d()}}var h,f=r.extend(this,i),g=o.loaderstatus.NEW,m=!1,w=r.size(u),v=!1;this.setupPlugins=function(n,i){var o=[],u=l.getPlugins(),c=i.get("plugins");r.each(c,function(i,l){var d=e.getPluginName(l),p=u[d],h=p.getFlashPath(),f=p.getJS(),g=p.getURL();if(h){var m=r.extend({name:d,swf:h,pluginmode:p.getPluginmode()},i);o.push(m)}var w=t.tryCatch(function(){if(f){var e=c[g];if(!e)return void t.log("JW Plugin already loaded",d,g);var i=document.createElement("div");i.id=n.id+"_"+d,i.className="jw-plugin jw-reset";var o=r.extend({},e),l=p.getNewInstance(n,o,i);l.addToPlayer=a(n,l,i),l.resizeHandler=s(l),n.addPlugin(d,l,i)}});w instanceof t.Error&&t.log("ERROR: Failed to load "+d+".")}),i.set("flashPlugins",o)},this.load=function(){if(t.exists(u)&&"object"!==t.typeOf(u))return void d();g=o.loaderstatus.LOADING,r.each(u,function(e,i){if(t.exists(i)){var r=l.addPlugin(i);r.on(n.COMPLETE,d),r.on(n.ERROR,p)}});var e=l.getPlugins();r.each(e,function(e){e.load()}),d()},this.destroy=function(){v=!0,this.off()},this.getStatus=function(){return g}};return l}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(26),n(52)],r=function(e,t){var n=function(n){this.addPlugin=function(i){var r=e.getPluginName(i);return n[r]||(n[r]=new t(i)),n[r]},this.getPlugins=function(){return n}};return n}.apply(t,i),!(void 0!==r&&(e.exports=r))},,function(e,t,n){var i,r;i=[],r=function(){return{}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(58),n(2),n(1),n(65)],r=function(e,t,n,i){var r=[{name:"youtube",supports:function(e){return t.isYouTube(e.file,e.type)}},{name:"html5",supports:function(n){var r={aac:"audio/mp4",mp4:"video/mp4",f4v:"video/mp4",m4v:"video/mp4",mov:"video/mp4",mp3:"audio/mpeg",mpeg:"audio/mpeg",ogv:"video/ogg",ogg:"video/ogg",oga:"video/ogg",vorbis:"video/ogg",webm:"video/webm",f4a:"video/aac",m3u8:"application/vnd.apple.mpegurl",m3u:"application/vnd.apple.mpegurl",hls:"application/vnd.apple.mpegurl"},o=n.file,a=n.type,s=e(n);if(null!==s)return s;if(t.isRtmp(o,a))return!1;if(!r[a])return!1;if(i.canPlayType){var l=i.canPlayType(r[a]);return!!l}return!1}},{name:"flash",supports:function(e){var i={flv:"video",f4v:"video",mov:"video",m4a:"video",m4v:"video",mp4:"video",aac:"video",f4a:"video",mp3:"sound",mpeg:"sound",smil:"rtmp"},r=n.keys(i);if(!t.isFlashSupported())return!1;var o=e.file,a=e.type;return!!t.isRtmp(o,a)||n.contains(r,a)}}];return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(13),n(38),n(134),n(1),n(137)],r=function(e,t,i,r,o){function a(e){this.config=e||{},this.providers=this.reorderProviders(this.config.primary)}a.loaders={html5:function(e){n.e(4,function(require){var t=n(71);s(t),e(t)})},flash:function(e){n.e(5,function(require){var t=n(57);s(t),e(t)})},youtube:function(e){n.e(6,function(require){var t=n(59);s(t),e(t)})}};var s=a.registerProvider=function(n){var a=n.getName().name;if(!i[a]){if(!r.find(t,r.matches({name:a}))){if(!r.isFunction(n.supports))throw{message:"Tried to register a provider with an invalid object"};t.unshift({name:a,supports:n.supports})}o(n.prototype,e),i[a]=n}};return r.extend(a.prototype,{load:function(e){return Promise.all(r.map(e,function(e){return new Promise(function(t){var n=a.loaders[e.name];n?n(t):t()})}))},reorderProviders:function(e){var n=r.clone(t);if("flash"===e){var i=r.indexOf(n,r.findWhere(n,{name:"flash"})),o=n.splice(i,1)[0],a=r.indexOf(n,r.findWhere(n,{name:"html5"}));n.splice(a,0,o)}return n},providerSupports:function(e,t){return e.supports(t)},required:function(e,t){var n=this,i=this.reorderProviders(t);return e=e.slice(),r.compact(r.map(i,function(t){for(var i=!1,r=e.length;r--;){var o=e[r],a=n.providerSupports(t,o.sources[0]);a&&e.splice(r,1),i=i||a}if(i)return t}))},choose:function(e){e=r.isObject(e)?e:{};for(var t=this.providers.length,n=0;n<t;n++){var o=this.providers[n];if(this.providerSupports(o,e)){var a=t-n-1;return{priority:a,name:o.name,type:e.type,providerToCheck:o,provider:i[o.name]}}}return null}}),a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){return function(t){return e.each(Array.prototype.slice.call(arguments,1),function(e){if(e)for(var n in e)n in t||(t[n]=e[n])}),t}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){return{hasClass:function(e,t){var n=" "+t+" ";return 1===e.nodeType&&(" "+e.className+" ").replace(/[\t\r\n\f]/g," ").indexOf(n)>=0}}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(4)],r=function(e,t){var n=e.extend({get:function(e){return this.attributes=this.attributes||{},this.attributes[e]},set:function(e,t){if(this.attributes=this.attributes||{},this.attributes[e]!==t){var n=this.attributes[e];this.attributes[e]=t,this.trigger("change:"+e,this,t,n)}},clone:function(){return e.clone(this.attributes)}},t);return n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1)],r=function(e){var t={};return t.isDvr=function(e,t){return Math.abs(e)>=Math.max(t,0)},t.streamType=function(n,i){var r=e.isUndefined(i)?120:i,o="VOD";return n===1/0?o="LIVE":n<0&&(o=t.isDvr(n,r)?"DVR":"LIVE"),o},t}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){var e=function(e,n,i){n=n||this,i=i||[];var r=window.jwplayer;if(r&&r.debug)return e.apply(n,i);try{return e.apply(n,i)}catch(o){return new t(e.name,o)}},t=function(e,t){this.name=e,this.message=t.message||t.toString(),this.error=t};return{tryCatch:e,Error:t}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(1)],r=function(e){return function(t,n,i){var r="jw-breakpoint-",o=n,a=i;r+=o>=1280?"7":o>=960?"6":o>=800?"5":o>=640?"4":o>=540?"3":o>=420?"2":o>=320?"1":"0",e.replaceClass(t,/jw-breakpoint-\d+/,r),e.toggleClass(t,"jw-orientation-portrait",a>o)}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(23),n(5),n(1)],r=function(e,t,i,r){var o=t.style,a={back:!0,fontSize:14,fontFamily:"Arial,sans-serif",fontOpacity:100,color:"#FFF",backgroundColor:"#000",backgroundOpacity:100,edgeStyle:null,windowColor:"#FFF",windowOpacity:0,preprocessor:r.identity},s=function(s){function l(e,n,i){if(t.css("#"+e+" .jw-text-track-display",n,e),t.css("#"+e+" .jw-text-track-cue",i,e),t.css("#"+e+" .jw-video::-webkit-media-text-track-display",n,e),t.css("#"+e+" .jw-video::cue",i,e),i.backgroundColor){var r="{background-color: "+i.backgroundColor+" !important;}";t.css("#"+e+" .jw-video::-webkit-media-text-track-display-backdrop",r,e)}}function u(e,n,i){var r=t.hexToRgba("#000000",i);"dropshadow"===e?n.textShadow="0 2px 1px "+r:"raised"===e?n.textShadow="0 0 5px "+r+", 0 1px 5px "+r+", 0 2px 5px "+r:"depressed"===e?n.textShadow="0 -2px 1px "+r:"uniform"===e&&(n.textShadow="-2px 0 1px "+r+",2px 0 1px "+r+",0 -2px 1px "+r+",0 2px 1px "+r+",-1px 1px 1px "+r+",1px 1px 1px "+r+",1px -1px 1px "+r+",1px 1px 1px "+r)}function c(e){f=e,this.selectCues(p,f)}function d(){s.get("renderCaptionsNatively")||n.e(7,function(require){v=n(56)})}var p,h,f,g,m,w,v,y={};g=document.createElement("div"),g.className="jw-captions jw-reset",this.show=function(){g.className="jw-captions jw-captions-enabled jw-reset"},this.hide=function(){g.className="jw-captions jw-reset"},this.populate=function(e){return h=[],p=e,e?void this.selectCues(e,f):(h=[],void this.renderCues())},this.resize=function(){var e=g.clientWidth,t=Math.pow(e/400,.6);if(t){var n=y.fontSize*t;o(g,{fontSize:Math.floor(2*n)/2+"px"})}this.renderCues(!0)},this.renderCues=function(e){e=!!e,v&&v.WebVTT.processCues(window,h,g,e)},this.selectCues=function(e,t){var n,i;e&&e.data&&t&&(i=this.getAlignmentPosition(e,t),i!==!1&&(n=this.getCurrentCues(e.data,i),this.updateCurrentCues(n),this.renderCues(!0)))},this.getCurrentCues=function(e,t){return r.filter(e,function(e){return t>=e.startTime&&(!e.endTime||t<=e.endTime)})},this.updateCurrentCues=function(e){return e.length?r.difference(e,h).length&&(m.className="jw-captions-window jw-reset jw-captions-window-active",h=e):h=[],h},this.getAlignmentPosition=function(e,t){var n=e.source,i=t.metadata;return n?!(!i||!r.isNumber(i[n]))&&i[n]:e.embedded&&t.duration<0?t.position-t.duration:t.position},this.clear=function(){e.empty(g)},this.setContainerHeight=function(e){o(g,{height:e})},this.setup=function(e,n){m=document.createElement("div"),w=document.createElement("span"),m.className="jw-captions-window jw-reset",w.className="jw-captions-text jw-reset",y=r.extend({},a,n);var i=y.fontOpacity,c=y.windowOpacity,d=y.edgeStyle,p=y.backgroundColor,h={},f={color:t.hexToRgba(y.color,i),fontFamily:y.fontFamily,fontStyle:y.fontStyle,fontWeight:y.fontWeight,textDecoration:y.textDecoration};c&&(h.backgroundColor=t.hexToRgba(y.windowColor,c)),u(d,f,i),y.back?f.backgroundColor=t.hexToRgba(p,y.backgroundOpacity):null===d&&u("uniform",f),o(m,h),o(w,f),l(e,h,f),m.appendChild(w),g.appendChild(m),this.populate(s.get("captionsTrack")),s.set("captions",y)},this.element=function(){return g},s.on("change:playlistItem",function(){f=null,h=[]},this),s.on("change:captionsTrack",function(e,t){this.populate(t)},this),s.mediaController.on("seek",function(){h=[]},this),s.mediaController.on("time seek",c,this),s.mediaController.on("subtitlesTrackData",function(){this.selectCues(p,f)},this),s.on("change:state",function(e,t){
switch(t){case i.IDLE:case i.ERROR:case i.COMPLETE:this.hide();break;default:this.show()}},this),s.on("itemReady",d,this)};return s}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(7),n(3),n(4),n(1)],r=function(e,t,n,i){var r=function(r,o,a){function s(e){if(!r.get("flashBlocked"))return c?void c(e):void f.trigger(e.type===t.touchEvents.CLICK?"click":"tap")}function l(){return d?void d():void f.trigger("doubleClick")}var u,c,d,p={enableDoubleTap:!0,useMove:!0};i.extend(this,n),u=o,this.element=function(){return u};var h=new e(u,i.extend(p,a));h.on("click tap",s),h.on("doubleClick doubleTap",l),h.on("move",function(){f.trigger("move")}),h.on("over",function(){f.trigger("over")}),h.on("out",function(){f.trigger("out")}),this.clickHandler=s;var f=this;this.setAlternateClickHandlers=function(e,t){c=e,d=t||null},this.revertAlternateClickHandlers=function(){c=null,d=null}};return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2),n(30)],r=function(e,t,n){function i(e,t){this.time=e,this.text=t,this.el=document.createElement("div"),this.el.className="jw-cue jw-reset"}e.extend(i.prototype,{align:function(e){if("%"===this.time.toString().slice(-1))this.pct=this.time;else{var t=this.time/e*100;this.pct=t+"%"}this.el.style.left=this.pct}});var r={loadChapters:function(e){t.ajax(e,this.chaptersLoaded.bind(this),this.chaptersFailed,{plainText:!0})},chaptersLoaded:function(t){var i=n(t.responseText);e.isArray(i)&&(e.each(i,this.addCue,this),this.drawCues())},chaptersFailed:function(){},addCue:function(e){this.cues.push(new i(e.begin,e.text))},drawCues:function(){var t=this._model.mediaModel.get("duration");if(!t||t<=0)return void this._model.mediaModel.once("change:duration",this.drawCues,this);var n=this;e.each(this.cues,function(e){e.align(t),e.el.addEventListener("mouseover",function(){n.activeCue=e}),e.el.addEventListener("mouseout",function(){n.activeCue=null}),n.elementRail.appendChild(e.el)})},resetChapters:function(){e.each(this.cues,function(e){e.el.parentNode&&e.el.parentNode.removeChild(e.el)},this),this.cues=[]}};return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(36),n(2),n(1),n(7),n(90)],r=function(e,t,n,i,r){var o=e.extend({setup:function(e,o,a){a=a||{},this.iconUI||(this.iconUI=new i(this.el,{useHover:!0,directSelect:!0}),this.toggleValueListener=this.toggleValue.bind(this),this.toggleOpenStateListener=this.toggleOpenState.bind(this),this.openTooltipListener=this.openTooltip.bind(this),this.closeTooltipListener=this.closeTooltip.bind(this),this.selectListener=this.select.bind(this)),this.reset(),e=n.isArray(e)?e:[],t.toggleClass(this.el,"jw-hidden",e.length<2);var s=e.length>2||2===e.length&&a&&a.toggle===!1,l=!s&&2===e.length;if(t.toggleClass(this.el,"jw-toggle",l||!!a.isToggle),t.toggleClass(this.el,"jw-button-color",!l),this.isActive=s||l,s){t.removeClass(this.el,"jw-off"),this.iconUI.on("tap",this.toggleOpenStateListener).on("over",this.openTooltipListener).on("out",this.closeTooltipListener);var u=r(e),c=t.createElement(u);this.addContent(c),this.contentUI=new i(this.content).on("click tap",this.selectListener)}else l&&this.iconUI.on("click tap",this.toggleValueListener);this.selectItem(o)},toggleValue:function(){this.trigger("toggleValue")},select:function(e){if(e.target.parentElement===this.content){var i=t.classList(e.target),r=n.find(i,function(e){return 0===e.indexOf("jw-item")});r&&(this.trigger("select",parseInt(r.split("-")[2])),this.closeTooltipListener())}},selectItem:function(e){if(this.content)for(var n=0;n<this.content.children.length;n++)t.toggleClass(this.content.children[n],"jw-active-option",e===n);t.toggleClass(this.el,"jw-off",0===e)},reset:function(){t.addClass(this.el,"jw-off"),this.iconUI.off(),this.contentUI&&this.contentUI.off().destroy(),this.removeContent()}});return o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(32),n(7),n(1),n(2),n(91)],r=function(e,t,n,i,r){var o=function(e,t,n,i){this._model=e,this._api=t,this._nextButton=n,this._playerElement=i,this.nextUpText=e.get("localization").nextUp,this.state="tooltip"};return n.extend(o.prototype,{setup:function(){this.container=document.createElement("div"),this.container.className="jw-nextup-container jw-reset";var e=i.createElement(r());this.addContent(e),this.closeButton=this.content.querySelector(".jw-nextup-close"),this.tooltip=this.content.querySelector(".jw-nextup-tooltip"),this.showNextUp=!1,this.streamType=void 0,this._model.on("change:mediaModel",this.onMediaModel,this),this._model.on("change:streamType",this.onStreamType,this),this._model.on("change:nextUp",this.onNextUp,this),this._model.on("change:duration",this.onDuration,this),this._model.on("change:position",this.onElapsed,this),this.onMediaModel(this._model,this._model.get("mediaModel")),new t(this.closeButton,{directSelect:!0}).on("click tap",this.hide,this),new t(this.tooltip).on("click tap",this.click,this),new t(this._nextButton.element(),{useHover:!0,directSelect:!0}).on("click tap",this.click,this).on("over",this.show,this).on("out",this.hoverOut,this)},loadThumbnail:function(e){return this.nextUpImage=new Image,this.nextUpImage.onload=function(){this.nextUpImage.onload=null}.bind(this),this.nextUpImage.src=e,{backgroundImage:'url("'+e+'")'}},click:function(){this.state="tooltip",this._api.next(),this.hide()},show:function(){"opened"===this.state||this.hideToolTip||(e.addClass(this.container,"jw-nextup-container-visible"),e.addClass(this._playerElement,"jw-flag-nextup"))},hide:function(){e.removeClass(this.container,"jw-nextup-container-visible"),e.removeClass(this.container,"jw-nextup-sticky"),e.removeClass(this._playerElement,"jw-flag-nextup"),"opened"===this.state&&(this.state="closed")},hoverOut:function(){"opened"!==this.state&&this.hide()},showTilEnd:function(){"opened"!==this.state&&"closed"!==this.state&&(e.addClass(this.container,"jw-nextup-sticky"),this.show(),this.state="opened")},setNextUpItem:function(t){var n=this;setTimeout(function(){if(n.hideToolTip=!(t.title||t.image),!n.hideToolTip){if(n.thumbnail=n.content.querySelector(".jw-nextup-thumbnail"),e.toggleClass(n.thumbnail,"jw-nextup-thumbnail-visible",!!t.image),t.image){var r=n.loadThumbnail(t.image);i.style(n.thumbnail,r)}n.header=n.content.querySelector(".jw-nextup-header"),n.header.innerText=n.nextUpText,n.title=n.content.querySelector(".jw-nextup-title");var o=t.title;n.title.innerText=o?i.createElement(o).textContent:""}},500)},onNextUp:function(e,t){return t?(this.showNextUp=!0,this._nextButton.toggle(!0),void this.setNextUpItem(t)):(this._nextButton.toggle(!1),void(this.showNextUp=!1))},onDuration:function(e,t){if(t){var n=i.seconds(e.get("nextupoffset")||-10);n<0&&(n+=t),this.offset=n}},onMediaModel:function(e,t){t.on("change:state",function(e,t){"complete"===t&&(this.state="tooltip",this.hide())},this)},onElapsed:function(e,t){"VOD"===this.streamType&&this.showNextUp&&t>=this.offset?this.showTilEnd():"opened"!==this.state&&"closed"!==this.state||(this.state="tooltip",this.hide())},onStreamType:function(e,t){this.streamType=t},element:function(){return this.container},addContent:function(e){this.content&&this.removeContent(),this.content=e,this.container.appendChild(e)},removeContent:function(){this.content&&(this.container.removeChild(this.content),this.content=null)}}),o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2),n(30)],r=function(e,t,n){function i(e){this.begin=e.begin,this.end=e.end,this.img=e.text}var r={loadThumbnails:function(e){e&&(this.vttPath=e.split("?")[0].split("/").slice(0,-1).join("/"),this.individualImage=null,t.ajax(e,this.thumbnailsLoaded.bind(this),this.thumbnailsFailed.bind(this),{plainText:!0}))},thumbnailsLoaded:function(t){var r=n(t.responseText);e.isArray(r)&&(e.each(r,function(e){this.thumbnails.push(new i(e))},this),this.drawCues())},thumbnailsFailed:function(){},chooseThumbnail:function(t){var n=e.sortedIndex(this.thumbnails,{end:t},e.property("end"));n>=this.thumbnails.length&&(n=this.thumbnails.length-1);var i=this.thumbnails[n].img;return i.indexOf("://")<0&&(i=this.vttPath?this.vttPath+"/"+i:i),i},loadThumbnail:function(t){var n=this.chooseThumbnail(t),i={display:"block",margin:"0 auto",backgroundPosition:"0 0"},r=n.indexOf("#xywh");if(r>0)try{var o=/(.+)\#xywh=(\d+),(\d+),(\d+),(\d+)/.exec(n);n=o[1],i.backgroundPosition=o[2]*-1+"px "+o[3]*-1+"px",i.width=o[4],i.height=o[5]}catch(a){return}else this.individualImage||(this.individualImage=new Image,this.individualImage.onload=e.bind(function(){this.individualImage.onload=null,this.timeTip.image({width:this.individualImage.width,height:this.individualImage.height})},this),this.individualImage.src=n);return i.backgroundImage='url("'+n+'")',i},showThumbnail:function(e){this.thumbnails.length<1||this.timeTip.image(this.loadThumbnail(e))},resetThumbnails:function(){this.timeTip.image({backgroundImage:"",width:0,height:0}),this.thumbnails=[]}};return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2),n(22),n(35),n(36),n(145),n(148)],r=function(e,t,n,i,r,o,a){var s=r.extend({setup:function(){this.text=document.createElement("span"),this.text.className="jw-text jw-reset",this.img=document.createElement("div"),this.img.className="jw-reset";var e=document.createElement("div");e.className="jw-time-tip jw-background-color jw-reset",e.appendChild(this.img),e.appendChild(this.text),t.removeClass(this.el,"jw-hidden"),this.addContent(e)},image:function(e){t.style(this.img,e)},update:function(e){this.text.innerHTML=e}}),l=i.extend({constructor:function(t,n){this._model=t,this._api=n,this.timeTip=new s("jw-tooltip-time"),this.timeTip.setup(),this.cues=[],this.seekThrottled=e.throttle(this.performSeek,400),this._model.on("change:playlistItem",this.onPlaylistItem,this).on("change:position",this.onPosition,this).on("change:duration",this.onDuration,this).on("change:buffer",this.onBuffer,this),i.call(this,"jw-slider-time","horizontal")},setup:function(){i.prototype.setup.apply(this,arguments),this._model.get("playlistItem")&&this.onPlaylistItem(this._model,this._model.get("playlistItem")),this.elementRail.appendChild(this.timeTip.element()),this.el.addEventListener("mousemove",this.showTimeTooltip.bind(this),!1),this.el.addEventListener("mouseout",this.hideTimeTooltip.bind(this),!1)},limit:function(t){if(this.activeCue&&e.isNumber(this.activeCue.pct))return this.activeCue.pct;var i=this._model.get("duration"),r=this._model.get("streamType");if("DVR"===r){var o=(1-t/100)*i,a=this._model.get("position"),s=Math.min(o,Math.max(n.dvrSeekLimit,a)),l=100*s/i;return 100-l}return t},update:function(e){this.seekTo=e,this.seekThrottled(),i.prototype.update.apply(this,arguments)},dragStart:function(){this._model.set("scrubbing",!0),i.prototype.dragStart.apply(this,arguments)},dragEnd:function(){i.prototype.dragEnd.apply(this,arguments),this._model.set("scrubbing",!1)},onSeeked:function(){this._model.get("scrubbing")&&this.performSeek()},onBuffer:function(e,t){this.updateBuffer(t)},onPosition:function(e,t){this.updateTime(t,e.get("duration"))},onDuration:function(e,t){this.updateTime(e.get("position"),t)},updateTime:function(e,t){var n=0;if(t){var i=this._model.get("streamType");"DVR"===i?n=(t-e)/t*100:"VOD"===i&&(n=e/t*100)}this.render(n)},onPlaylistItem:function(t,n){this.reset(),t.mediaModel.on("seeked",this.onSeeked,this);var i=n.tracks;e.each(i,function(e){e&&e.kind&&"thumbnails"===e.kind.toLowerCase()?this.loadThumbnails(e.file):e&&e.kind&&"chapters"===e.kind.toLowerCase()&&this.loadChapters(e.file)},this)},performSeek:function(){var e,t=this.seekTo,n=this._model.get("duration"),i=this._model.get("streamType");0===n?this._api.play():"DVR"===i?(e=(100-t)/100*n,this._api.seek(e)):(e=t/100*n,this._api.seek(Math.min(e,n-.25)))},showTimeTooltip:function(e){var i=this._model.get("duration");if(0!==i){var r=t.bounds(this.elementRail),o=e.pageX?e.pageX-r.left:e.x;o=t.between(o,0,r.width);var a=o/r.width,s=i*a;i<0&&(s=i-s);var l;if(this.activeCue)l=this.activeCue.text;else{var u=!0;l=t.timeFormat(s,u),i<0&&s>n.dvrSeekLimit&&(l="Live")}this.timeTip.update(l),this.showThumbnail(s),t.addClass(this.timeTip.el,"jw-open"),this.timeTip.el.style.left=100*a+"%"}},hideTimeTooltip:function(){t.removeClass(this.timeTip.el,"jw-open")},reset:function(){this.resetChapters(),this.resetThumbnails()}});return e.extend(l.prototype,o,a),l}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(36),n(35),n(7),n(2)],r=function(e,t,n,i){var r=e.extend({constructor:function(r,o,a){this._model=r,e.call(this,o,a,!0),this.volumeSlider=new t("jw-slider-volume jw-volume-tip","vertical"),this.addContent(this.volumeSlider.element()),this.volumeSlider.on("update",function(e){this.trigger("update",e)},this),i.removeClass(this.el,"jw-hidden"),new n(this.el,{useHover:!0,directSelect:!0}).on("click",this.toggleValue,this).on("tap",this.toggleOpenState,this).on("over",this.openTooltip,this).on("out",this.closeTooltip,this),this._model.on("change:volume",this.onVolume,this)},toggleValue:function(){this.trigger("toggleValue")}});return r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(1),n(4),n(22),n(7),n(35),n(149),n(146),n(150),n(66)],r=function(e,t,n,i,r,o,a,s,l,u){function c(e,t){var n=document.createElement("span");return n.className="jw-text jw-reset "+e,t&&n.setAttribute("role",t),n}function d(e,t){var n=new s(e,t);return n}function p(e,n){var i=document.createElement("div");return i.className="jw-group jw-controlbar-"+e+"-group jw-reset",t.each(n,function(e){e.element&&(e=e.element()),i.appendChild(e)}),i}function h(t,n){this._api=t,this._model=n,this._isMobile=e.isMobile(),this._localization=this._model.get("localization"),this.setup()}return t.extend(h.prototype,n,{setup:function(){this.build(),this.initialize()},build:function(){var n,i,r,s=new a(this._model,this._api),h=this._localization.play,f=this._localization.next,g=this._localization.volume,m=this._localization.rewind;this._isMobile||(n=new o("jw-slider-volume","horizontal"),i=new l(this._model,"jw-icon-volume",g)),this._model.get("sdkplatform")||e.isIOS(8)||e.isIOS(9)||(r=u("jw-icon-volume",this._api.setMute,g)),this.elements={alt:c("jw-text-alt","status"),play:u("jw-icon-playback",this._api.play.bind(this,{reason:"interaction"}),h),rewind:u("jw-icon-rewind",this.rewind.bind(this),m),next:u("jw-icon-next",null,f),elapsed:c("jw-text-elapsed","timer"),time:s,duration:c("jw-text-duration","timer"),hd:d("jw-icon-hd",this._localization.hd),cc:d("jw-icon-cc",this._localization.cc),audiotracks:d("jw-icon-audio-tracks",this._localization.audioTracks),mute:r,volume:n,volumetooltip:i,cast:u("jw-icon-cast jw-off",this._api.castToggle,this._localization.cast),fullscreen:u("jw-icon-fullscreen",this._api.setFullscreen,this._localization.fullscreen)},this.layout={left:[this.elements.play,this.elements.rewind,this.elements.elapsed],center:[this.elements.time,this.elements.alt],right:[this.elements.duration,this.elements.next,this.elements.hd,this.elements.cc,this.elements.audiotracks,this.elements.mute,this.elements.cast,this.elements.volume,this.elements.volumetooltip,this.elements.fullscreen]},this.menus=t.compact([this.elements.hd,this.elements.cc,this.elements.audiotracks,this.elements.volumetooltip]),this.layout.left=t.compact(this.layout.left),this.layout.center=t.compact(this.layout.center),this.layout.right=t.compact(this.layout.right),this.el=document.createElement("div"),this.el.className="jw-controlbar jw-background-color jw-reset",this.elements.left=p("left",this.layout.left),this.elements.center=p("center",this.layout.center),this.elements.right=p("right",this.layout.right),this.el.appendChild(this.elements.left),this.el.appendChild(this.elements.center),this.el.appendChild(this.elements.right)},initialize:function(){this.elements.play.show(),this.elements.fullscreen.show(),this.elements.mute&&this.elements.mute.show(),this.onVolume(this._model,this._model.get("volume")),this.onPlaylistItem(),this.onMediaModel(this._model,this._model.get("mediaModel")),this.onCastAvailable(this._model,this._model.get("castAvailable")),this.onCastActive(this._model,this._model.get("castActive")),this.onCaptionsList(this._model,this._model.get("captionsList")),this._model.on("change:volume",this.onVolume,this),this._model.on("change:mute",this.onMute,this),this._model.on("change:playlistItem",this.onPlaylistItem,this),this._model.on("change:mediaModel",this.onMediaModel,this),this._model.on("change:castAvailable",this.onCastAvailable,this),this._model.on("change:castActive",this.onCastActive,this),this._model.on("change:duration",this.onDuration,this),this._model.on("change:position",this.onElapsed,this),this._model.on("change:fullscreen",this.onFullscreen,this),this._model.on("change:captionsList",this.onCaptionsList,this),this._model.on("change:captionsIndex",this.onCaptionsIndex,this),this._model.on("change:streamType",this.onStreamTypeChange,this),this.elements.volume&&this.elements.volume.on("update",function(e){var t=e.percentage;this._api.setVolume(t)},this),this.elements.volumetooltip&&(this.elements.volumetooltip.on("update",function(e){var t=e.percentage;this._api.setVolume(t)},this),this.elements.volumetooltip.on("toggleValue",function(){this._api.setMute()},this)),this.elements.hd.on("select",function(e){this._model.getVideo().setCurrentQuality(e)},this),this.elements.hd.on("toggleValue",function(){this._model.getVideo().setCurrentQuality(0===this._model.getVideo().getCurrentQuality()?1:0)},this),this.elements.cc.on("select",function(e){this._api.setCurrentCaptions(e)},this),this.elements.cc.on("toggleValue",function(){var e=this._model.get("captionsIndex");this._api.setCurrentCaptions(e?0:1)},this),this.elements.audiotracks.on("select",function(e){this._model.getVideo().setCurrentAudioTrack(e)},this),new r(this.elements.duration).on("click tap",function(){if("DVR"===this._model.get("streamType")){var e=this._model.get("position");this._api.seek(Math.max(i.dvrSeekLimit,e))}},this),new r(this.el).on("click tap drag",function(){this.trigger("userAction")},this),t.each(this.menus,function(e){e.on("open-tooltip",this.closeMenus,this)},this)},onCaptionsList:function(e,t){var n=e.get("captionsIndex");this.elements.cc.setup(t,n,{isToggle:!0})},onCaptionsIndex:function(e,t){this.elements.cc.selectItem(t)},onPlaylistItem:function(){this.elements.time.updateBuffer(0),this.elements.time.render(0),this.elements.duration.innerHTML="00:00",this.elements.elapsed.innerHTML="00:00",this.elements.audiotracks.setup()},onMediaModel:function(e,n){n.on("change:levels",function(e,t){this.elements.hd.setup(t,e.get("currentLevel"))},this),n.on("change:currentLevel",function(e,t){this.elements.hd.selectItem(t)},this),n.on("change:audioTracks",function(e,n){var i=t.map(n,function(e){return{label:e.name}});this.elements.audiotracks.setup(i,e.get("currentAudioTrack"),{toggle:!1})},this),n.on("change:currentAudioTrack",function(e,t){this.elements.audiotracks.selectItem(t)},this)},onVolume:function(e,t){this.renderVolume(e.get("mute"),t)},onMute:function(e,t){this.renderVolume(t,e.get("volume"))},renderVolume:function(t,n){this.elements.mute&&e.toggleClass(this.elements.mute.element(),"jw-off",t),this.elements.volume&&this.elements.volume.render(t?0:n),this.elements.volumetooltip&&(this.elements.volumetooltip.volumeSlider.render(t?0:n),e.toggleClass(this.elements.volumetooltip.element(),"jw-off",t))},onCastAvailable:function(e,t){this.elements.cast.toggle(t)},onCastActive:function(t,n){e.toggleClass(this.elements.cast.element(),"jw-off",!n)},onElapsed:function(t,n){var i,r=t.get("duration");i="DVR"===t.get("streamType")?"-"+e.timeFormat(-r):e.timeFormat(n),this.elements.elapsed.innerHTML=i},onDuration:function(t,n){var i;i="DVR"===t.get("streamType")?"Live":e.timeFormat(n),this.elements.duration.innerHTML=i},onFullscreen:function(t,n){e.toggleClass(this.elements.fullscreen.element(),"jw-off",n)},element:function(){return this.el},setAltText:function(e){this.elements.alt.innerHTML=e},addCues:function(e){this.elements.time&&(t.each(e,function(e){this.elements.time.addCue(e)},this),this.elements.time.drawCues())},closeMenus:function(e){t.each(this.menus,function(t){e&&e.target===t.el||t.closeTooltip(e)})},hideComponents:function(){this.closeMenus()},rewind:function(){var e=this._model.get("position"),t=this._model.get("duration"),n=e-10,i=0;"DVR"===this._model.get("streamType")&&(i=t),this._api.seek(Math.max(n,i))},onStreamTypeChange:function(e){var t=e.get("streamType");this.elements.rewind.toggle("LIVE"!==t),"DVR"===t&&(this.elements.duration.innerHTML="Live")}}),h}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(4),n(7),n(86),n(1)],r=function(e,t,n,i,r){var o=function(o){r.extend(this,t),this.model=o,this.el=e.createElement(i({ariaLabel:this.model.get("localization").playback}));var a=this;this.iconUI=new n(this.el).on("click tap",function(e){a.trigger(e.type)}),this.model.on("change:state",function(e,t){var n=a.el.getElementsByClassName("jw-icon-display");if(n.length){var i=a.model.get("localization"),r=i.playback;switch(t){case"buffering":r=i.buffer;break;case"playing":r=i.pause;break;case"complete":r=i.replay;break;case"error":r=""}""===r?n[0].removeAttribute("aria-label"):n[0].setAttribute("aria-label",r)}})};return r.extend(o.prototype,{element:function(){return this.el}}),o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(87),n(2),n(1),n(7)],r=function(e,t,n,i){var r=function(e){this.model=e,this.setup(),this.model.on("change:dock",this.render,this)};return n.extend(r.prototype,{setup:function(){var n=this.model.get("dock"),r=this.click.bind(this),o=e(n);this.el=t.createElement(o),new i(this.el).on("click tap",r)},getDockButton:function(e){return t.hasClass(e.target,"jw-dock-button")?e.target:t.hasClass(e.target,"jw-dock-text")?e.target.parentElement.parentElement:e.target.parentElement},click:function(e){var t=this.getDockButton(e),i=t.getAttribute("button"),r=this.model.get("dock"),o=n.findWhere(r,{id:i});o&&o.callback&&o.callback(e)},render:function(){var n=this.model.get("dock"),i=e(n),r=t.createElement(i);this.el.innerHTML=r.innerHTML},element:function(){return this.el}}),r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(88)],r=function(e){function t(t,n,i,r){return e({id:t,skin:n,title:i,body:r})}return t}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(7),n(2),n(3),n(1),n(4),n(89)],r=function(e,t,n,i,r,o){var a=t.style,s={linktarget:"_blank",margin:8,hide:!1,position:"top-right"},l=function(l){var u,c,d=new Image,p=i.extend({},l.get("logo"));return i.extend(this,r),this.setup=function(r){if(c=i.extend({},s,p),c.hide="true"===c.hide.toString(),u=t.createElement(o()),c.file){c.hide&&t.addClass(u,"jw-hide"),t.addClass(u,"jw-logo-"+(c.position||s.position)),"top-right"===c.position&&(l.on("change:dock",this.topRight,this),l.on("change:controls",this.topRight,this),this.topRight(l)),l.set("logo",c),d.onload=function(){var e={backgroundImage:'url("'+this.src+'")',width:this.width,height:this.height};if(c.margin!==s.margin){var t=/(\w+)-(\w+)/.exec(c.position);3===t.length&&(e["margin-"+t[1]]=c.margin,e["margin-"+t[2]]=c.margin)}a(u,e),l.set("logoWidth",e.width)},d.src=c.file;var h=new e(u);h.on("click tap",function(e){t.exists(e)&&e.stopPropagation&&e.stopPropagation(),this.trigger(n.JWPLAYER_LOGO_CLICK,{link:c.link,linktarget:c.linktarget})},this),r.appendChild(u)}},this.topRight=function(e){var t=e.get("controls"),n=e.get("dock"),i=t&&(n&&n.length||e.get("sharing")||e.get("related"));a(u,{top:i?"3.5em":0})},this.element=function(){return u},this.position=function(){return c.position},this.destroy=function(){d.onload=null},this};return l}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2)],r=function(e,t){function n(e,t){t.off("change:mediaType",null,this),t.on("change:mediaType",function(t,n){"audio"===n&&this.setImage(e.get("playlistItem").image)},this)}function i(e,n){var i=e.get("autostart")&&!t.isMobile()||e.get("item")>0;return i?(this.setImage(null),e.off("change:state",null,this),void e.on("change:state",function(e,t){"complete"!==t&&"idle"!==t&&"error"!==t||(this.setImage(n.image),this.resize(null,null,e.get("stretching")))},this)):void this.setImage(n.image)}var r=function(e){this.model=e,e.on("change:playlistItem",i,this),e.on("change:mediaModel",n,this)};return e.extend(r.prototype,{setup:function(e){this.el=e;var t=this.model.get("playlistItem");t&&this.setImage(t.image)},setImage:function(n){var i=this.image;i&&(i.onload=null,this.image=null),this.model.off("change:state",null,this);var r="";e.isString(n)&&(r='url("'+n+'")',i=this.image=new Image,i.src=n),t.style(this.el,{backgroundImage:r})},resize:function(e,n,i){if("uniform"===i){if(e&&(this.playerAspectRatio=e/n),!this.playerAspectRatio)return;var r=this.image,o=null;if(r){if(0===r.width){var a=this;return void(r.onload=function(){a.resize(e,n,i)})}var s=r.width/r.height;Math.abs(this.playerAspectRatio-s)<.09&&(o="cover")}t.style(this.el,{backgroundSize:o})}},element:function(){return this.el}}),r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(93),n(1),n(7),n(24)],r=function(e,t,n,i,r){var o=function(){};return n.extend(o.prototype,{buildArray:function(){var t=r.split("+"),n=t[0],i={items:[{title:"Powered by JW Player "+n,featured:!0,showLogo:!0,link:"https://jwplayer.com/learn-more"}]},o=n.indexOf("-")>0,a=t[1];if(o&&a){var s=a.split(".");i.items.push({title:"build: ("+s[0]+"."+s[1]+")",link:"#"})}var l=this.model.get("provider");if(l&&l.name.indexOf("flash")>=0){var u="Flash Version "+e.flashVersion();i.items.push({title:u,link:"http://www.adobe.com/software/flash/about/"})}return i},buildMenu:function(){var n=this.buildArray();return e.createElement(t(n))},updateHtml:function(){this.el.innerHTML=this.buildMenu().innerHTML},rightClick:function(e){return this.lazySetup(),!this.mouseOverContext&&(this.hideMenu(),this.showMenu(e),!1)},getOffset:function(e){for(var t=e.target,n=e.offsetX||e.layerX,i=e.offsetY||e.layerY;t!==this.playerElement;)n+=t.offsetLeft,i+=t.offsetTop,t=t.parentNode;return{x:n,y:i}},showMenu:function(t){var n=this.getOffset(t);return this.el.style.left=n.x+"px",this.el.style.top=n.y+"px",e.addClass(this.playerElement,"jw-flag-rightclick-open"),e.addClass(this.el,"jw-open"),clearTimeout(this._menuTimeout),this._menuTimeout=setTimeout(this.hideMenu.bind(this),3e3),!1},hideMenu:function(){return this.elementUI.off("out",this.hideMenu,this),this.mouseOverContext?void this.elementUI.on("out",this.hideMenu,this):(e.removeClass(this.playerElement,"jw-flag-rightclick-open"),void e.removeClass(this.el,"jw-open"))},lazySetup:function(){this.el||(this.el=this.buildMenu(),this.layer.appendChild(this.el),this.hideMenuHandler=this.hideMenu.bind(this),this.addOffListener(this.playerElement),this.addOffListener(document),this.model.on("change:provider",this.updateHtml,this),this.elementUI=new i(this.el,{useHover:!0}).on("over",function(){this.mouseOverContext=!0},this).on("out",function(){this.mouseOverContext=!1},this))},setup:function(e,t,n){this.playerElement=t,this.model=e,this.mouseOverContext=!1,this.layer=n,t.oncontextmenu=this.rightClick.bind(this)},addOffListener:function(e){e.addEventListener("mousedown",this.hideMenuHandler),e.addEventListener("touchstart",this.hideMenuHandler),e.addEventListener("pointerdown",this.hideMenuHandler)},removeOffListener:function(e){e.removeEventListener("mousedown",this.hideMenuHandler),e.removeEventListener("touchstart",this.hideMenuHandler),e.removeEventListener("pointerdown",this.hideMenuHandler)},destroy:function(){clearTimeout(this._menuTimeout),this.el&&(this.hideMenu(),this.elementUI.off(),this.removeOffListener(this.playerElement),this.removeOffListener(document),this.hideMenuHandler=null,this.el=null),this.playerElement&&(this.playerElement.oncontextmenu=null,this.playerElement=null),this.model&&(this.model.off("change:provider",this.updateHtml),this.model=null)}}),o}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(1),n(2)],r=function(e,t){var n=function(e){this.model=e,this.model.on("change:playlistItem",this.playlistItem,this)};return e.extend(n.prototype,{hide:function(){this.el.style.display="none"},show:function(){this.el.style.display=""},setup:function(e){this.el=e;var t=this.el.getElementsByTagName("div");this.title=t[0],this.description=t[1],this.model.get("playlistItem")&&this.playlistItem(this.model,this.model.get("playlistItem")),this.model.on("change:logoWidth",this.update,this),this.model.on("change:dock",this.update,this)},update:function(e){var n={paddingLeft:0,paddingRight:0},i=e.get("controls"),r=e.get("dock"),o=e.get("logo");if(o){var a=1*(""+o.margin).replace("px",""),s=e.get("logoWidth")+(isNaN(a)?0:a);"top-left"===o.position?n.paddingLeft=s:"top-right"===o.position&&(n.paddingRight=s)}if(i&&r&&r.length){var l=56*r.length;n.paddingRight=Math.max(n.paddingRight,l)}t.style(this.el,n)},playlistItem:function(e,t){if(e.get("displaytitle")||e.get("displaydescription")){var n="",i="";t.title&&e.get("displaytitle")&&(n=t.title),t.description&&e.get("displaydescription")&&(i=t.description),this.updateText(n,i)}else this.hide()},updateText:function(e,t){this.title.innerHTML=e,this.description.innerHTML=t,this.title.firstChild||this.description.firstChild?this.show():this.hide()},element:function(){return this.el}}),n}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(3),n(4),n(22),n(5),n(143),n(144),n(152),n(153),n(155),n(151),n(156),n(171),n(158),n(147),n(1),n(92),n(142),n(66)],r=function(e,t,i,r,o,a,s,l,u,c,d,p,h,f,g,m,w,v,y){var j=e.style,b=e.bounds,E=e.isMobile(),x=["fullscreenchange","webkitfullscreenchange","mozfullscreenchange","MSFullscreenChange"],k=function(k,A){function _(t){var n=0,i=A.get("duration"),o=A.get("position");"DVR"===A.get("streamType")&&(n=i,i=Math.max(o,r.dvrSeekLimit));var a=e.between(o+t,n,i);k.seek(a)}function C(t){var n=e.between(A.get("volume")+t,0,100);k.setVolume(n)}function L(e){return!e.ctrlKey&&!e.metaKey&&!!A.get("controls")}function T(e){if(!L(e))return!0;switch(Ee||oe(),e.keyCode){case 27:k.setFullscreen(!1);break;case 13:case 32:k.play({reason:"interaction"});break;case 37:Ee||_(-5);break;case 39:Ee||_(5);break;case 38:C(10);break;case 40:C(-10);break;case 67:var t=k.getCaptionsList(),n=t.length;if(n){var i=(k.getCurrentCaptions()+1)%n;k.setCurrentCaptions(i)}break;case 77:k.setMute();break;case 70:k.setFullscreen();break;default:if(e.keyCode>=48&&e.keyCode<=59){var r=e.keyCode-48,o=r/10*A.get("duration");k.seek(o)}}return/13|32|37|38|39|40/.test(e.keyCode)?(e.preventDefault(),!1):void 0}function P(){qe=!1,e.removeClass(we,"jw-no-focus")}function R(e){e.target&&e.target.blur&&e.target.blur()}function I(){qe=!0,e.addClass(we,"jw-no-focus")}function M(){qe||P(),Ee||oe()}function S(){var e=b(we),n=Math.round(e.width),i=Math.round(e.height);He(ze),n&&i&&(n!==je||i!==be)&&(je=n,be=i,clearTimeout(Ve),Ve=setTimeout(Z,50),A.set("containerWidth",n),A.set("containerHeight",i),v(we,n,i),Qe.trigger(t.JWPLAYER_RESIZE,{width:n,height:i}))}function O(){document.body.contains(we)&&(He(ze),ze=Ue(S))}function D(t,n,i){var r={color:t,borderColor:t,stroke:t},o={color:n,borderColor:n,stroke:n};e.css("#"+i+" .jw-color-active",r,i),e.css("#"+i+" .jw-color-active-hover:hover",r,i),e.css("#"+i+" .jw-color-inactive",o,i),e.css("#"+i+" .jw-color-inactive-hover:hover",o,i)}function N(t,n){n=n||!1,e.toggleClass(we,"jw-flag-casting",n)}function Y(t,n){e.toggleClass(we,"jw-flag-cast-available",n),e.toggleClass(ve,"jw-flag-cast-available",n)}function F(t,n){e.replaceClass(we,/jw-stretch-\S+/,"jw-stretch-"+n)}function W(e){e&&!E&&(e.element().addEventListener("mousemove",V,!1),e.element().addEventListener("mouseout",z,!1))}function J(){A.get("state")!==o.IDLE&&A.get("state")!==o.COMPLETE&&A.get("state")!==o.PAUSED||!A.get("controls")||k.play({reason:"interaction"}),Be?re():oe();
}function B(e){e.link?(k.pause(!0),k.setFullscreen(!1),window.open(e.link,e.linktarget)):A.get("controls")&&k.play({reason:"interaction"})}function V(){clearTimeout(Fe)}function z(){oe()}function U(e){Qe.trigger(e.type,e)}function H(t,n){n?(Se&&Se.destroy(),e.addClass(we,"jw-flag-flash-blocked")):(Se&&Se.setup(A,we,we),e.removeClass(we,"jw-flag-flash-blocked"))}function G(){A.get("controls")&&k.setFullscreen()}function K(){var n=we.getElementsByClassName("jw-overlays")[0];n.addEventListener("mousemove",oe),Ae=new s(A,ye,{useHover:!0}),Ae.on("click",function(){U({type:t.JWPLAYER_DISPLAY_CLICK}),A.get("controls")&&k.play({reason:"interaction"})}),Ae.on("tap",function(){U({type:t.JWPLAYER_DISPLAY_CLICK}),J()}),Ae.on("doubleClick",G),Ae.on("move",oe),Ae.on("over",oe);var i=new l(A);i.on("click",function(){U({type:t.JWPLAYER_DISPLAY_CLICK}),k.play({reason:"interaction"})}),i.on("tap",function(){U({type:t.JWPLAYER_DISPLAY_CLICK}),J()}),e.isChrome()&&!e.isMobile()&&i.el.addEventListener("mousedown",function(){var e=A.getVideo(),t=e&&0===e.getName().name.indexOf("flash");if(t){var n=function(){document.removeEventListener("mouseup",n),i.el.style.pointerEvents="auto"};this.style.pointerEvents="none",document.addEventListener("mouseup",n)}}),ve.appendChild(i.element()),Ce=new u(A),Le=new c(A),Le.on(t.JWPLAYER_LOGO_CLICK,B);var r=document.createElement("div");r.className="jw-controls-right jw-reset",Le.setup(r),r.appendChild(Ce.element()),ve.appendChild(r),Ie=new a(A),Ie.setup(we.id,A.get("captions")),ve.parentNode.insertBefore(Ie.element(),Te.element());var o=A.get("height");E&&("string"==typeof o||o>=1.5*Je)?e.addClass(we,"jw-flag-touch"):(Se=new h,Se.setup(A,we,we)),xe=new d(k,A),xe.on(t.JWPLAYER_USER_ACTION,oe),A.on("change:scrubbing",ge),A.autoStartOnMobile()&&(Re=y("jw-autostart-mute jw-off",ee,A.get("localization").volume),Re.show(),ve.appendChild(Re.element()),xe.renderVolume(!0,A.get("volume")),e.addClass(we,"jw-flag-autostart"),A.set("autostartMuted",!0),A.on("change:autostartFailed",ee),A.on("change:autostartMuted",ee),A.on("change:mute",ee)),Pe=new g(A,k,xe.elements.next,we),Pe.setup(),ve.appendChild(Pe.element()),ve.appendChild(xe.element()),we.addEventListener("focus",M),we.addEventListener("blur",P),we.addEventListener("keydown",T),we.onmousedown=I,we.onmouseup=R}function q(t,n,i){var r,o=we.className;i=!!i,i&&(o=o.replace(/\s*aspectMode/,""),we.className!==o&&(we.className=o),j(we,{display:"block"},i)),e.exists(t)&&e.exists(n)&&(A.set("width",t),A.set("height",n)),r={width:t},e.hasClass(we,"jw-flag-aspect-mode")||(r.height=n),A.get("aspectratio")&&me(),j(we,r,!0),Q(n),Z(t,n)}function Q(t){if(Me=X(t),xe&&!Me){var n=Ee?Ee:A;he(n,n.get("state"))}e.toggleClass(we,"jw-flag-audio-player",Me)}function X(e){if(A.get("aspectratio"))return!1;if(m.isString(e)&&e.indexOf("%")>-1)return!1;var t=m.isNumber(e)?e:A.get("containerHeight");return $(t)}function $(e){return e&&e<=Je*(E?1.75:1)}function Z(t,n){if(!t||isNaN(Number(t))){if(!ye)return;t=ye.clientWidth}if(!n||isNaN(Number(n))){if(!ye)return;n=ye.clientHeight}ke&&ke.resize(t,n,A.get("stretching")),e.isMSIE(9)&&document.all&&!window.atob&&(t=n="100%");var i=A.getVideo();if(i){var r=i.resize(t,n,A.get("stretching"));r&&(clearTimeout(Ve),Ve=setTimeout(Z,250)),A.get("aspectratio")&&me(),Ie.resize()}}function ee(){var t=!A.get("autostartFailed"),n=A.get("mute");t&&(n=!1),A.off("change:autostartFailed",ee),A.off("change:mute",ee),A.off("change:autostartMuted",ee),A.set("autostartFailed",void 0),A.set("autostartMuted",void 0),k.setMute(n),xe.renderVolume(n,A.get("volume")),Re.hide(),e.removeClass(we,"jw-flag-autostart")}function te(){if(Ke){var e=document.fullscreenElement||document.webkitCurrentFullScreenElement||document.mozFullScreenElement||document.msFullscreenElement;return!(!e||e.id!==A.get("id"))}return Ee?Ee.getVideo().getFullScreen():A.getVideo().getFullScreen()}function ne(e){var t=A.get("fullscreen"),n=void 0!==e.jwstate?e.jwstate:te();t!==n&&A.set("fullscreen",n),clearTimeout(Ve),Ve=setTimeout(Z,200)}function ie(t,n){n?(e.addClass(t,"jw-flag-fullscreen"),j(document.body,{"overflow-y":"hidden"}),oe()):(e.removeClass(t,"jw-flag-fullscreen"),j(document.body,{"overflow-y":""})),Z()}function re(){Be=!1,clearTimeout(Fe),xe.hideComponents(),e.addClass(we,"jw-flag-user-inactive"),Ie.renderCues(!0)}function oe(){Be||(e.removeClass(we,"jw-flag-user-inactive"),Ie.renderCues(!0)),Be=!0,clearTimeout(Fe),Fe=setTimeout(re,We)}function ae(){k.setFullscreen(!1)}function se(){_e&&_e.setState(A.get("state")),le(A,A.mediaModel.get("mediaType")),A.mediaModel.on("change:mediaType",le,this)}function le(t,n){var i="audio"===n,r=A.getVideo(),o=r&&0===r.getName().name.indexOf("flash");e.toggleClass(we,"jw-flag-media-audio",i),i&&!o?we.insertBefore(ke.el,ye):we.insertBefore(ke.el,Ie.element())}function ue(t,n){var i=t.get("minDvrWindow"),r=e.streamType(n,i),o="LIVE"===r;t.set("streamType",r),e.toggleClass(we,"jw-flag-live",o),Qe.setAltText(o?t.get("localization").liveBroadcast:"")}function ce(e,t){return t?void(t.name?Te.updateText(t.name,t.message):Te.updateText(t.message,"")):void Te.playlistItem(e,e.get("playlistItem"))}function de(){var e=A.getVideo();return!!e&&e.isCaster}function pe(){e.replaceClass(we,/jw-state-\S+/,"jw-state-"+Oe)}function he(e,t){Oe=t,clearTimeout(Ge),t===o.PLAYING?fe(e,t):Ge=setTimeout(function(){fe(e,t)},33)}function fe(t,n){if(e.toggleClass(we,"jw-flag-dragging",t.get("scrubbing")),pe(),de())return void e.addClass(ye,"jw-media-show");switch(n){case o.PLAYING:Z();break;case o.PAUSED:oe()}}function ge(e){he(e,e.get("state"))}function me(){var e=we.getElementsByClassName("jw-aspect")[0];Ie.setContainerHeight(e.offsetHeight)}var we,ve,ye,je,be,Ee,xe,ke,Ae,_e,Ce,Le,Te,Pe,Re,Ie,Me,Se,Oe,De,Ne,Ye,Fe=-1,We=E?4e3:2e3,Je=40,Be=!1,Ve=-1,ze=-1,Ue=window.requestAnimationFrame||function(e){return window.setTimeout(e,17)},He=window.cancelAnimationFrame||window.clearTimeout,Ge=-1,Ke=!1,qe=!1,Qe=m.extend(this,i);window.webpackJsonpjwplayer&&n(161),this.model=A,this.api=k,we=e.createElement(w({id:A.get("id")})),e.isIE()&&e.addClass(we,"jw-ie");var Xe=A.get("width"),$e=A.get("height");j(we,{width:Xe.toString().indexOf("%")>0?Xe:Xe+"px",height:$e.toString().indexOf("%")>0?$e:$e+"px"}),Ne=we.requestFullscreen||we.webkitRequestFullscreen||we.webkitRequestFullScreen||we.mozRequestFullScreen||we.msRequestFullscreen,Ye=document.exitFullscreen||document.webkitExitFullscreen||document.webkitCancelFullScreen||document.mozCancelFullScreen||document.msExitFullscreen,Ke=Ne&&Ye,this.onChangeSkin=function(t,n){e.replaceClass(we,/jw-skin-\S+/,n?"jw-skin-"+n:"")},this.handleColorOverrides=function(){function t(t,i,r){if(r){t=e.prefix(t,"#"+n+" ");var o={};o[i]=r,e.css(t.join(", "),o,n)}}var n=A.get("id"),i=A.get("skinColorActive"),r=A.get("skinColorInactive"),o=A.get("skinColorBackground");t([".jw-toggle",".jw-button-color:hover"],"color",i),t([".jw-active-option",".jw-progress"],"background",i),t([".jw-text",".jw-option",".jw-button-color",".jw-toggle.jw-off",".jw-tooltip-title",".jw-skip .jw-skip-icon"],"color",r),t([".jw-cue",".jw-knob"],"background",r),t([".jw-background-color",".jw-tooltip-title"],"background",o),D(i,r,n)},this.setup=function(){this.handleColorOverrides(),A.get("skin-loading")===!0&&(e.addClass(we,"jw-flag-skin-loading"),A.once("change:skin-loading",function(){e.removeClass(we,"jw-flag-skin-loading")})),this.onChangeSkin(A,A.get("skin"),""),A.on("change:skin",this.onChangeSkin,this),ye=we.getElementsByClassName("jw-media")[0],ve=we.getElementsByClassName("jw-controls")[0];var n=we.getElementsByClassName("jw-preview")[0];ke=new p(A),ke.setup(n);var i=we.getElementsByClassName("jw-title")[0];Te=new f(A),Te.setup(i),K(),oe(),A.set("mediaContainer",ye),A.mediaController.on("fullscreenchange",ne);for(var r=x.length;r--;)document.addEventListener(x[r],ne,!1);window.removeEventListener("resize",O),window.addEventListener("resize",O,!1),E&&(window.removeEventListener("orientationchange",O),window.addEventListener("orientationchange",O,!1)),A.on("change:errorEvent",ce),A.on("change:controls",Ze),Ze(A,A.get("controls")),A.on("change:state",he),A.on("change:duration",ue,this),A.on("change:flashBlocked",H),H(A,A.get("flashBlocked")),k.onPlaylistComplete(ae),k.onPlaylistItem(se),A.on("change:castAvailable",Y),Y(A,A.get("castAvailable")),A.on("change:castActive",N),N(A,A.get("castActive")),A.on("change:hideAdsControls",function(t,n){e.toggleClass(we,"jw-flag-ads-hide-controls",n)}),A.get("stretching")&&F(A,A.get("stretching")),A.on("change:stretching",F),he(A,o.IDLE),A.on("change:fullscreen",et),W(xe),W(Le);var a=A.get("aspectratio");if(a){e.addClass(we,"jw-flag-aspect-mode");var s=we.getElementsByClassName("jw-aspect")[0];j(s,{paddingTop:a})}k.on(t.JWPLAYER_READY,function(){S(),q(A.get("width"),A.get("height"))})};var Ze=function(t,n){if(n){var i=Ee?Ee.get("state"):A.get("state");he(t,i)}e.toggleClass(we,"jw-flag-controls-disabled",!n)},et=function(t,n){var i=A.getVideo();n&&A.get("autostartMuted")&&ee(),Ke?(n?Ne.apply(we):Ye.apply(document),ie(we,n)):e.isIE()?ie(we,n):(Ee&&Ee.getVideo()&&Ee.getVideo().setFullscreen(n),i.setFullscreen(n)),i&&0===i.getName().name.indexOf("flash")&&i.setFullscreen(n)};this.resize=function(e,t){var n=!0;q(e,t,n),S()},this.resizeMedia=Z,this.reset=function(){document.contains(we)&&we.parentNode.replaceChild(De,we),e.emptyElement(we)},this.setupInstream=function(t){this.instreamModel=Ee=t,Ee.on("change:controls",Ze,this),Ee.on("change:state",he,this),e.addClass(we,"jw-flag-ads"),oe()},this.setAltText=function(e){xe.setAltText(e)},this.destroyInstream=function(){if(Ee&&(Ee.off(null,null,this),Ee=null),this.setAltText(""),e.removeClass(we,["jw-flag-ads","jw-flag-ads-hide-controls"]),A.set("hideAdsControls",!1),A.getVideo){var t=A.getVideo();t.setContainer(ye)}ue(A,A.get("duration")),Ae.revertAlternateClickHandlers()},this.addCues=function(e){xe&&xe.addCues(e)},this.clickHandler=function(){return Ae},this.controlsContainer=function(){return ve},this.getContainer=this.element=function(){return we},this.getSafeRegion=function(t){var n={x:0,y:0,width:A.get("containerWidth")||0,height:A.get("containerHeight")||0},i=A.get("dock");return i&&i.length&&A.get("controls")&&(n.y=Ce.element().clientHeight,n.height-=n.y),t=t||!e.exists(t),t&&A.get("controls")&&(n.height-=xe.element().clientHeight),n},this.setCaptions=function(e){Ie.clear(),Ie.setup(A.get("id"),e),Ie.resize()},this.destroy=function(){clearTimeout(Ge),clearTimeout(Ve),clearTimeout(Fe),window.removeEventListener("resize",O),window.removeEventListener("orientationchange",O);for(var t=x.length;t--;)document.removeEventListener(x[t],ne,!1);A.mediaController&&A.mediaController.off("fullscreenchange",ne),we.removeEventListener("keydown",T,!1),Se&&Se.destroy(),_e&&(A.off("change:state",_e.statusDelegate),_e.destroy(),_e=null),Ee&&this.destroyInstream(),Le&&Le.destroy(),e.clearCss(A.get("id"))}};return k}.apply(t,i),!(void 0!==r&&(e.exports=r))},,function(e,t,n){var i=n(81);"string"==typeof i&&(i=[["all-players",i,""]]),n(68).style(i,"all-players"),i.locals&&(e.exports=i.locals)},function(e,t,n){var i,r;i=[n(69),n(1),n(24),n(2),n(8),n(7),n(73),n(15),n(74),n(65),n(3),n(5),n(50),n(31),n(28),n(48),n(21)],r=function(e,t,n,i,r,o,a,s,l,u,c,d,p,h,f,g,m){var w={};return w.api=e,w._=t,w.version=n,w.utils=t.extend(i,r,{canCast:f.available,key:a,extend:t.extend,scriptloader:s,rssparser:g,tea:l,UI:o}),w.utils.css.style=w.utils.style,w.vid=u,w.events=t.extend({},c,{state:d}),w.playlist=t.extend({},p,{item:h}),w.plugins=m,w.cast=f,w}.apply(t,i),!(void 0!==r&&(e.exports=r))},,,,function(e,t,n){var i,r;i=[n(119),n(16),n(169)],r=function(e,t,i){var r=e.prototype.setup;return e.prototype.setup=function(e,o){e.analytics&&(e.sdkplatform=e.sdkplatform||e.analytics.sdkplatform),r.apply(this,arguments);var a=this._model.get("edition"),s=t(a),l=this._model.get("cast"),u=this;s("casting")&&l&&l.appid&&n.e(3,function(require){var e=n(70);u._castController=new e(u,u._model),u.castToggle=u._castController.castToggle.bind(u._castController)});var c=i.setup();this.once("ready",c.onReady,this),o.getAdBlock=c.checkAdBlock},e}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(73),n(16),n(1),n(2),n(63),n(124),n(37)],r=function(e,t,i,r,o,a,s){function l(e,t,n){if(t){var i=t.client;delete t.client,/\.(js|swf)$/.test(i||"")||(i=o.repo()+n),e[i]=t}}function u(e,n){var r=i.clone(n.get("plugins"))||{},a=n.get("edition"),s=t(a),u=/^(vast|googima|freewheel)$/,c=/\.(js|swf)$/,d=o.repo(),p=i.clone(n.get("advertising"));if(s("ads")&&p&&(c.test(p.client)?r[p.client]=p:u.test(p.client)&&(r[d+p.client+".js"]=p),delete p.client),s("jwpsrv")){var h=n.get("analytics");i.isObject(h)||(h={}),l(r,h,"jwpsrv.js")}l(r,n.get("ga"),"gapro.js"),l(r,n.get("sharing"),"sharing.js");var f=n.get("related"),g=i.isObject(f),m=n.get("visualplaylist")!==!1||g;g||(f={disableRelated:!0}),f.showDockButton=m,l(r,f,"related.js");var w=n.get("mobileSdk");if(!w){var v=n.get("playlist");i.some(v,function(e){if(e.stereomode&&e.stereomode.length>0)return l(r,n.get("vr")||{},"vr.js"),!0})}n.set("plugins",r),e()}function c(t,i){var s=i.get("key")||window.jwplayer&&window.jwplayer.key,l=new e(s),u=l.edition();if(i.set("key",s),i.set("edition",u),"unlimited"===u){var c=r.getScriptPath("jwplayer.js");if(!c)return void a.error(t,"Error setting up player","Could not locate jwplayer.js script tag");n.p=c,r.repo=o.repo=o.loadFrom=function(){return c}}i.updateProviders(),"invalid"===u?a.error(t,"Error setting up player",(void 0===s?"Missing":"Invalid")+" license key"):t()}function d(e,t){s.containsDrm(t)?s.probe(e,t.get("edition")):e()}function p(){var e=a.getQueue();return e.CHECK_KEY={method:c,depends:["LOADED_POLYFILLS"]},e.PROBE_DRM_SUPPORT={method:d,depends:["CHECK_KEY"]},e.FILTER_PLUGINS={method:u,depends:["CHECK_KEY"]},e.FILTER_PLAYLIST.depends.push("PROBE_DRM_SUPPORT"),e.LOAD_PLUGINS.depends.push("FILTER_PLUGINS"),e.SETUP_VIEW.depends.push("CHECK_KEY"),e}return{getQueue:p}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(126),n(162),n(1)],r=function(e,t,n){var i=window,r=n.extend(e,t);return"function"==typeof i.define&&i.define.amd&&i.define([],function(){return r}),i.jwplayer?i.jwplayer:r}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[],r=function(){function e(){var e=document.createElement("div");return e.className=n,e.innerHTML="&nbsp;",e.style.width="1px",e.style.height="1px",e.style.position="absolute",e.style.background="transparent",e}function t(){function t(){var e=this,t=e._view.element();t.appendChild(a),r=!0,i()&&e.trigger("adBlock")}function i(){return!!r&&(!!o||(""!==a.innerHTML&&a.className===n&&null!==a.offsetParent&&0!==a.clientHeight||(o=!0),o))}var r=!1,o=!1,a=e();return{onReady:t,checkAdBlock:i}}var n="afs_ads";return{setup:t}}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(2),n(3),n(7),n(4),n(1),n(85)],r=function(e,t,n,i,r,o){var a=function(e){this.model=e,this.setup()};return r.extend(a.prototype,i,{setup:function(){this.destroy(),this.skipMessage=this.model.get("skipText"),this.skipMessageCountdown=this.model.get("skipMessage"),this.setWaitTime(this.model.get("skipOffset"));var t=o();this.el=e.createElement(t),this.skiptext=this.el.getElementsByClassName("jw-skiptext")[0],this.skipAdOnce=r.once(this.skipAd.bind(this)),new n(this.el).on("click tap",r.bind(function(){this.skippable&&this.skipAdOnce()},this)),this.model.on("change:duration",this.onChangeDuration,this),this.model.on("change:position",this.onChangePosition,this),this.onChangeDuration(this.model,this.model.get("duration")),this.onChangePosition(this.model,this.model.get("position"))},updateMessage:function(e){this.skiptext.innerHTML=e},updateCountdown:function(e){this.updateMessage(this.skipMessageCountdown.replace(/xx/gi,Math.ceil(this.waitTime-e)))},onChangeDuration:function(t,n){if(n){if(this.waitPercentage){if(!n)return;this.itemDuration=n,this.setWaitTime(this.waitPercentage),delete this.waitPercentage}e.removeClass(this.el,"jw-hidden")}},onChangePosition:function(t,n){this.waitTime-n>0?this.updateCountdown(n):(this.updateMessage(this.skipMessage),this.skippable=!0,e.addClass(this.el,"jw-skippable"))},element:function(){return this.el},setWaitTime:function(t){if(r.isString(t)&&"%"===t.slice(-1)){var n=parseFloat(t);return void(this.itemDuration&&!isNaN(n)?this.waitTime=this.itemDuration*n/100:this.waitPercentage=t)}r.isNumber(t)?this.waitTime=t:"string"===e.typeOf(t)?this.waitTime=e.seconds(t):isNaN(Number(t))?this.waitTime=0:this.waitTime=Number(t)},skipAd:function(){this.trigger(t.JWPLAYER_AD_SKIPPED)},destroy:function(){this.el&&(this.el.removeEventListener("click",this.skipAdOnce),this.el.parentElement&&this.el.parentElement.removeChild(this.el)),delete this.skippable,delete this.itemDuration,delete this.waitPercentage}}),a}.apply(t,i),!(void 0!==r&&(e.exports=r))},function(e,t,n){var i,r;i=[n(157),n(1)],r=function(e,t){var n=function(){};return t.extend(n.prototype,e.prototype,{buildArray:function(){var t=e.prototype.buildArray.apply(this,arguments);if(this.model.get("abouttext")){t.items[0].showLogo=!1,t.items.push(t.items.shift());var n={title:this.model.get("abouttext"),link:this.model.get("aboutlink")||t.items[0].link};t.items.unshift(n)}return t}}),n}.apply(t,i),!(void 0!==r&&(e.exports=r))}]);// if we're in an Iframe, get SPON interface from parent window
// this is fatal if video is embedded by a third party site (x-domain, spon embed videos), so try-catch Bug 38750
var spInterface;
if (window.parent.location != window.location) {
	try {
		spInterface = window.parent.$().spInterface;
	}
	catch(e) {
		console.log("Error while trying to load spInterface from parent: " + e.message);
	}
}	

// if spUA is not defined, we're likely in an Iframe, so let's try it via SPON interface
if (!window.spUA && spInterface)
	window.spUA = spInterface('getBrowserDetector');


function isValidPluginVersion() {
	var fpversion = swfobject.getFlashPlayerVersion();
	var configuredVersion = sp_webcfg_global.flash.pluginversion;
	if((fpversion.major > configuredVersion.major) || ((fpversion.major == configuredVersion.major) && (fpversion.minor >= configuredVersion.minor))) {
		/* ab Version 11 alles ok, wenn Version 10 mindestens minor release 1 */
		return true;
	} else {
		return false;
	}
}

/**
 * Main videoplayer function to set up the player with all variables & behaviour like event handling, extra UI elements etc
 * @param  {Object}  config  The configuration object, can hold the following parameters (for defaults, see at start of function):
 *                   {
 *                       wrapperElemId    ID of HTML element wrapping parts (player placeholder, error field, start- & endscreen)
 *                       videoId          ID of video which should be played
 *                       videoFilename
 *                       {
 *                           hq           name of video file to be used for high quality
 *                           sq           name of video file to be used for standard quality
 *                       }
 *                       poster           image to be shown by player at start, when autoplay is off
 *                       prerollGateway   VAST URL for preroll ad
 *                       postrollGateway  VAST URL for postroll ad
 *                       overlayGateway   VAST URL for overlay ad (in-stream)
 *                       overlayStartAt   how many seconds into content video until overlay is shown
 *                       allowAds         whether to show ads
 *                       credit           tracking info
 *                       displaycat       tracking info
 *                       videocat         tracking info
 *                       server           videoserver URL to be used
 *                       trackingsize     parameter to distinguish different player styles (i.e. centerpage vs iframe vs embed etc)
 *                       autoplay         whether to play video on page load (not possible on mobile devices due to hardware restrictions)
 *                       callbacks
 *                       {
 *                           onFirstFrame       fires when video begins to play for the first time
 *                           onUserInteraction  fires when user klicks on player (for play/pause, mute/volume, fullscreen, seek etc.)
 *                           onAdStart          fires at the beginning of each pre/postroll ad
 *                           onAdEnd            fires at the end of each pre/postroll ad
 *                           onVideoEnd         fires at the end of the content video (but before postroll, if there is one)
 *                           onFinalEnd         fires at the end of everything including postroll
 *                           onAdCompanions     fires when VAST URL response contains ad companion data
 *                       }
 *                       embedWidth       width of player
 *                       embedHeight      height of player
 *                       muted            mute audio on initial playback
 *                       dockDisabled     don't show extra dock (info button, share buttons)
 *                   }
 * @return {void}
 */
function mmStartVideo(config){
	// set config param defaults
	var defaults = {
		overlayGateway:		'',
		overlayStartAt:		7,
		allowAds:			false,
		autoplay:			true,
		callbacks: 			{},
		embedWidth:			640,
		embedHeight:		360,
		muted:				false,
		dockDisabled:		false
	};
	config = $.extend({}, defaults, config);

	// make sure we have a wrapper ID
	if (!config.wrapperElemId || config.wrapperElemId === '') return false;

	// special hash param switches to affect ads & statistics
	var hashParams = (typeof mmGetHashParams === 'function' ? mmGetHashParams() : []);
	// extra ref parameter for statistics
	var refParam = hashParams.ref !== null ? hashParams.ref : '';

	// deactivating ads, also possible through ADI
	if (hashParams["adition.keinevideowerbung"] == "1" || (typeof ADI == 'object' && ADI.disabled())) {
		config.prerollGateway	= "";
		config.postrollGateway	= "";
		config.overlayGateway	= "";
	} else {
		if (config.videoadserver === 'smartclip') {
			if (config.prerollGateway)  config.prerollGateway  += '&itype=Desktop&rnd='+(Math.random().toString(36)+'00000000000000000').slice(2, 18);
			if (config.postrollGateway) config.postrollGateway += '&itype=Desktop&rnd='+(Math.random().toString(36)+'00000000000000000').slice(2, 18);
			console.log("ADS RANDOMIZED", config.prerollGateway, config.postrollGateway)
		
		} else if (config.videoadserver === 'adition') {
			// setting extra parameter "videobelegung"
			if (hashParams["adition.videobelegung"] && hashParams["adition.videobelegung"] !== '') {
				if (config.prerollGateway)	config.prerollGateway	+= '&prf[videobelegung]=' + hashVideoBelegung;
				if (config.postrollGateway)	config.postrollGateway	+= '&prf[videobelegung]=' + hashVideoBelegung;
				if (config.overlayGateway)	config.overlayGateway	+= '&prf[videobelegung]=' + hashVideoBelegung;
			}
		}
	}
	
	var wrapperElem = $('#' + config.wrapperElemId);
	if (!wrapperElem.length) return false;   // wrapper element not found
	var html5PlayerWrapperId = wrapperElem.find('.videoplayer-placeholder').attr('id');
	if (!html5PlayerWrapperId) {
		// assign an ID to player wrapper element, because JW player needs an ID for setup
		var playerIdCount = 1;
		// make sure the ID doesn't exist yet
		while ($('#mmVideoplayer-' + playerIdCount).length)
			playerIdCount++;
		html5PlayerWrapperId = 'mmVideoplayer-' + playerIdCount;
		wrapperElem.find('.videoplayer-placeholder').attr('id', html5PlayerWrapperId);
	}

	if ((spUA.isIDevice() || spUA.isAndroid()) && parseInt(config.videoId) < 58671) {
		// this video is limited to desktop
		wrapperElem.find('.video-player-error')
			.html('Dieses Video steht für iOS- & Android-Geräte nicht zur Verfügung.')
			.show();
	} else if (!document.createElement('video').canPlayType && !isValidPluginVersion()) {
		// no HTML5 video possible & Flash version not sufficient for fallback play
		var fpversion = swfobject.getFlashPlayerVersion();
		wrapperElem.find('.video-player-error')
			.html('<b>Der ben&ouml;tigte Flash Player wurde nicht gefunden.</b><br /><br />' + 
				'M&ouml;gliche Ursachen:<br />- JavaScript ist deaktivert.<br />' + 
				'- Der Player ist nicht vorhanden. <a href="http://get.adobe.com/de/flashplayer/" target="_blank">Jetzt herunterladen und installieren</a><br />' + 
				'<br /><b>Hinweis:</b> Sie werden zum Installieren des Flash Players auf die Website von<br />Adobe weitergeleitet.' +
				'Dieses Video ben&ouml;tigt mindestens Version ' + sp_webcfg_global.flash.pluginversion.major + '.' + sp_webcfg_global.flash.pluginversion.minor + ' des Flash-Plugins. Gefundene Version: ' + fpversion.major + '.' + fpversion.minor + '.' + fpversion.release)
			.show();
	} else {
		// make sure a filename is provided (note: JW Player can only play videofiles directly, no video ID HTML redirect possible)
		if (config.videoFilename === null || config.videoFilename === '') return;

		// detect if we're inside an iframe
		var isInIframe = window.parent != window.self;
		// get video data
		var videoData = wrapperElem.data();

		var infoWindow = '';
		{
			infoWindow = $('<div class="jw-infowindow">')
			infoWindow.append(
					$('<a class="jw-brand-logo" href="/"'+(isInIframe ? ' target="_blank"' : '')+'>').append(
						'<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjEwMPRyoQAABwxJREFUaN7tm3tQVFUcxxeWl0SSiMvu3Xt3eQZiuiC5iILT2Ixvekmk+UKzUCvLhHAYB1QMKpXxgUj5BmywxkItSMHGGktIGkVCChTysYDKCKY85PXrd87y3rvCkn/ssntmvrO795zfPedzf7/zO/exVyDoU1hW6shx0oWoA6iLqCpUtQGpApWD+gRZfFmWMRPwFYZhLLBBODYsR7WjYAjoASqZZSXDe8FyHGuFFYeGEGhf5aCn7TpgJWbo2fVDFLSnEqVSqRkCMx74454RANehvBFYGm0EsFQYyXEE+ISxAKPOE+BSIwKuFXSsXcYC3GgCHuJqMnnY5GGTh00eNgGbQtoQgFkXV5A+O1pDrLNLRxsGnAKmgcOc5TByxmJgvMcBx0q694HfmTE+MHLmEhiJbZyUU/v0wYLUw0uzD9ymtmdA6ukNjtMXgEPw2yAKnA2cjKX9dvbPyTm6ndQ7TntTbcsygwBGI/uwTSA4XIlSdSu1GhxCPgSJQgnWCT+BIO02bsM2qVVgdvAm2C+NUx8sZ2ewW50CZodUtE7dphpsNmXhoDxpGwIn/KK09/6xvcXuImr/zIJoMN//D7Wj9tiX1dZfQTJ+Mj2YoqA5YLHzIm6vVo8T25nvqwCHV1YBJ5XoOIcp8EY6AAGCdAl3POL1CLDclod1t3rX0XoVHukF8HR4oqYtEdo8tWYvDkiMwF4gTPlLo43Frks46HfVEH3tD94Cyy3nMHJ8eW2JCLTEN+AJAeNv682n1d7g6YzI6tOzYP5lmdZ64Z4SCqsNWJh0GSy2/6HVnnjUZv0x7WPAMZLo1BLajboBPwGZYZhKxim1Av9vYRTZfrS/dz4ZtIfJgA/eAJvob0CYXKy1Q6vPzoJVfC6//YEbIPYL1A6M9paJ+WC98XvtB23vNRgWfVRLJCFwZKo2YB09jIOxW7WDzkHRlJfUCaVvSHYkHQ5lub1AZ2ASAWSOyhgn9fThOSD2mNBkUidweHkFT2h3AjNPJmmRLN2ZZQXpdzQGRBKajBHRNtabT+kMbJFUSA8YAbJbvYcnMVbSZZAsS/SgH1Lp4uHBAH+gBsY1jx/4PMgkIjog680/DgqYc5YjsBiBk3mAq8BxxiIT8ABDutHkYROwCdiggRuNLWnpr4c5PPEY/l4y2B6+BS5HK8Hn2yrgMirBFq++HKcvpGMcEiEt3FUIMldXWLxoIWQVFMPdpjboLO2o6sY2OHbqDISGhsCooGDDD2nP9Kvwy2/nobW1FR5Xmpub4UR+EcgyKg3UwwdugjKzGqobHg/at1z9twW8vq40PA+TeVrxoIUXqqGhAWpqauDhw4e89YX3mmFUuspwPCxE7x691qDVi7t3J4GbmyvExMRobbOz+AEI9huIh/0wlFvbNSEuXPgdcnJOw9atW0AqlUBU1Md07pLtGRkZUFBQQH+Tcr+5jUbJ4D28NA6B7qpv1HXqSA2MCF3TfXmYUdu7HkWugbsuDxPOaNSb4TW02C9IfRNv3zW8pr4NnxdphuqVK8Ugk7EgEjlCQIA/sDimdeuiIDY2FiQSJ7p/BpeuhIT4Lpv38+5jH3dgWNRXOl48UCBvkPgEoCb2UEDXXUdOxtGbZb3rJwIz9vmuzpjnxmvUE9EbBMReoQTRWCUUXC7WAM7OzqZenTIlEMLCllDgiIi1CK8Ed3dXyM3NpQfDz298t83P50Cs8AfGW6G/Tx7kcg4qKsq1As+bFwqJidvo97Vr14C//wS0kcHcua/SbT4+iu7kVXhJ/588uLjI4fr16zoBOzvLYNmyMAgPfwciIyO7bIqKLtNpoNfPlkiolpRcoQMmJxulpX9DWVkZZGVlUcj589/QAPbwcIP8/HyYMMEPZs2a1QWcl5dH96fXwCQBZWZm0gHX1tZSMHd3Nzh58gT9PmbMaJg69QXaNjIyAoKCJtNpEB//CYjFIpg0aWL30rRzB7XRew+vXLmCDriurg4UirHUiyqViiYsEvJkvnp5ecKGDbHo+R9oG2KnUIzDg/UdtW1paYGQkNcM4+kh8Vh5uWbiamtrg0ePHmlsb29vh/r6evrZWciazDBiw3g+TLwVFraYF24ghcAHB882rAfiDCOhZ1Q9vTaQQkKZnIH1M3f194F4XNxG6rGBFDLnSeYeIKx+/uWBzMOZM6fTc+impiZeUHLVdPx4JmbswIHMWw0P/6l/f/NlqMg5NMngKSl7IC0tDZKSdsHy5W/h6aRvf+utNtUT4CP6/t8M4kWyXuvoTT6pCPBKI/pTSwZ5BYDFL7eNALaVZaVzBXK5E/FyjBEAZyOwTefLWba44dQQhi3BJMf2fSPNHitSyVo1hEAbUOnINkoul/O9nSYRYoMXUfs63lAjb3Y1dqiJ51OXbf39bhzgZ3+291GF5D0lzE9KhDXvyfgfXHcDl3eRBvcAAAAASUVORK5CYII="/>'
					));
			infoWindow.append('<div class="jw-info-title">'+videoData.videoTitle+'</div>');
			var text = $('<div class="jw-info-text" />');
			if (videoData.videoTeaser && videoData.videoTeaser !== '')
				text.append('<p>'+videoData.videoTeaser+'</p>');
			if (videoData.videoArticleUrl && videoData.videoArticleUrl !== '') {
				if($('#video-embed-player').length) {
					text.append('<a href="http://www.manager-magazin.de'+videoData.videoArticleUrl+'" target="_blank">Zum Artikel</a>');	
				} else {
					text.append('<a href="'+videoData.videoArticleUrl+'" target="_top">Zum Artikel</a>');
				}
			}
			text.append('<span class="jw-info-date">'+videoData.videoDate+'</span>');
			infoWindow.append(text);
		}
				
		// set generic player options
		jwplayer.key = spnetENV_JWPLAYER_KEY;
		var jwplayerOptions = {
			flashplayer:	'/static/flash/flashvideo/jwplayer7.8.1.flash.swf',
			primary:		'flash',
			title:			infoWindow.html(),
			mediaid:		config.videoId,
			image:			config.poster,
			width:			config.embedWidth,
			height:			config.embedHeight,
			autostart:		config.autoplay,
			mute:			config.muted,
			ga:				{},    // enable Google Analytics, uses the GA JS already on the page
			skin: {
				name:			'mmo'
			},
			sharing: (config.dockDisabled)?null:{
				link: decodeURIComponent(videoData.videoSocialUrl),
    			heading: 'Video Teilen',
			    sites: ['facebook','twitter','googleplus']
			  }
		};

		// read cookie for HQ/SQ
		var qualityCookieName = 'sponVideoplayerQuality';
		var playerQuality = 'hq';
		if ($.cookie(qualityCookieName))
			playerQuality = $.cookie(qualityCookieName);
		else
			$.cookie(qualityCookieName, playerQuality, { expires: 365, path: '/' });

		// set video sources
		if (typeof config.videoFilename == 'object') {
			jwplayerOptions.sources = [];
			// check all quality levels that we're expecting
			$.each(['sq', 'hq'], function(i, quality) {
				if (config.videoFilename[quality] !== '') {
					var sourceInfo = {file: config.server + config.videoFilename[quality]};
					// set this quality as default if quality read from cookie matches
					if (playerQuality == quality)
						sourceInfo['default'] = true;
					jwplayerOptions.sources.push(sourceInfo);
				}
			});
		} else {
			// legacy: videoFilename is a single string
			jwplayerOptions.file = config.server + config.videoFilename;
		}

		if (config.allowAds && (config.prerollGateway !== '' || config.postrollGateway !== '' || config.overlayGateway !== '')) {
			// set player options for VAST ads
			jwplayerOptions.advertising = {
				client:		'vast',
				admessage:	spnetENV_JWPLAYER_ADMESSAGE,
				schedule:	{}
			};
			if (config.prerollGateway !== '')
				jwplayerOptions.advertising.schedule.preroll	= { offset: 'pre',  tag: config.prerollGateway };
			if (config.postrollGateway !== '')
				jwplayerOptions.advertising.schedule.postroll	= { offset: 'post', tag: config.postrollGateway };
			if (config.overlayGateway !== '')
				jwplayerOptions.advertising.schedule.overlay	= { offset: config.overlayStartAt, type: 'nonlinear', tag: config.overlayGateway };
		}

		// instantiate player
		var jwplayerInstance = jwplayer(html5PlayerWrapperId).setup(jwplayerOptions);

		if (!config.dockDisabled) {
			jwplayerInstance.addButton(
				'/static/sys/pixel.gif',
				'Info', 
				function() {
					$('.jw-title').toggle();
				},
				'info'
			);
		}
		
		jwplayerInstance.on('ready', function() {
			var playerElem = $('#' + this.id);

			// store player instance from JW Player setup for future reference (i.e. removing player)
			playerElem.data('playerInstance', jwplayerInstance);

			// detect if we're on a touch device
			var touchabilityClass = 'jw-'+('ontouchstart' in document.createElement('div') ? '' : 'un')+'touchable';
			playerElem.addClass(touchabilityClass);


			// get video data
			var videoData = playerElem.parents('.video-elements').data();

			// move duration display over to left group
			playerElem.find('.jw-text-duration').detach().appendTo(playerElem.find('.jw-controlbar-left-group'));

			// hover handler for icons, cause JW Player has a bug when keeping controls open (won't happen on hover for dock icons)
			playerElem.on('mouseenter', '.jw-icon', function() {
				playerElem.addClass('jw-flag-icon-hover');
			});
			playerElem.on('mouseleave', '.jw-icon', function() {
				playerElem.removeClass('jw-flag-icon-hover');
			});

			// callback for user interaction on whole player (= click on any button)
			playerElem.on('click', function() {
				if (typeof config.callbacks.onUserInteraction === 'function')
					config.callbacks.onUserInteraction();
			});

			if (!config.dockDisabled) {
				// add left dock with info button & addtional text
				playerElem.find('.jw-dock').append('<div class="jw-dock-left jw-reset"></div>');
				playerElem.find('.jw-dock-left').append('<div class="jw-icon jw-icon-info jw-button-color"></div>');

				var infoWindow = '<div class="jw-infowindow">'+
									'<a class="jw-brand-logo" href="/"'+(isInIframe ? ' target="_blank"' : '')+'><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjEwMPRyoQAABwxJREFUaN7tm3tQVFUcxxeWl0SSiMvu3Xt3eQZiuiC5iILT2Ixvekmk+UKzUCvLhHAYB1QMKpXxgUj5BmywxkItSMHGGktIGkVCChTysYDKCKY85PXrd87y3rvCkn/ssntmvrO795zfPedzf7/zO/exVyDoU1hW6shx0oWoA6iLqCpUtQGpApWD+gRZfFmWMRPwFYZhLLBBODYsR7WjYAjoASqZZSXDe8FyHGuFFYeGEGhf5aCn7TpgJWbo2fVDFLSnEqVSqRkCMx74454RANehvBFYGm0EsFQYyXEE+ISxAKPOE+BSIwKuFXSsXcYC3GgCHuJqMnnY5GGTh00eNgGbQtoQgFkXV5A+O1pDrLNLRxsGnAKmgcOc5TByxmJgvMcBx0q694HfmTE+MHLmEhiJbZyUU/v0wYLUw0uzD9ymtmdA6ukNjtMXgEPw2yAKnA2cjKX9dvbPyTm6ndQ7TntTbcsygwBGI/uwTSA4XIlSdSu1GhxCPgSJQgnWCT+BIO02bsM2qVVgdvAm2C+NUx8sZ2ewW50CZodUtE7dphpsNmXhoDxpGwIn/KK09/6xvcXuImr/zIJoMN//D7Wj9tiX1dZfQTJ+Mj2YoqA5YLHzIm6vVo8T25nvqwCHV1YBJ5XoOIcp8EY6AAGCdAl3POL1CLDclod1t3rX0XoVHukF8HR4oqYtEdo8tWYvDkiMwF4gTPlLo43Frks46HfVEH3tD94Cyy3nMHJ8eW2JCLTEN+AJAeNv682n1d7g6YzI6tOzYP5lmdZ64Z4SCqsNWJh0GSy2/6HVnnjUZv0x7WPAMZLo1BLajboBPwGZYZhKxim1Av9vYRTZfrS/dz4ZtIfJgA/eAJvob0CYXKy1Q6vPzoJVfC6//YEbIPYL1A6M9paJ+WC98XvtB23vNRgWfVRLJCFwZKo2YB09jIOxW7WDzkHRlJfUCaVvSHYkHQ5lub1AZ2ASAWSOyhgn9fThOSD2mNBkUidweHkFT2h3AjNPJmmRLN2ZZQXpdzQGRBKajBHRNtabT+kMbJFUSA8YAbJbvYcnMVbSZZAsS/SgH1Lp4uHBAH+gBsY1jx/4PMgkIjog680/DgqYc5YjsBiBk3mAq8BxxiIT8ABDutHkYROwCdiggRuNLWnpr4c5PPEY/l4y2B6+BS5HK8Hn2yrgMirBFq++HKcvpGMcEiEt3FUIMldXWLxoIWQVFMPdpjboLO2o6sY2OHbqDISGhsCooGDDD2nP9Kvwy2/nobW1FR5Xmpub4UR+EcgyKg3UwwdugjKzGqobHg/at1z9twW8vq40PA+TeVrxoIUXqqGhAWpqauDhw4e89YX3mmFUuspwPCxE7x691qDVi7t3J4GbmyvExMRobbOz+AEI9huIh/0wlFvbNSEuXPgdcnJOw9atW0AqlUBU1Md07pLtGRkZUFBQQH+Tcr+5jUbJ4D28NA6B7qpv1HXqSA2MCF3TfXmYUdu7HkWugbsuDxPOaNSb4TW02C9IfRNv3zW8pr4NnxdphuqVK8Ugk7EgEjlCQIA/sDimdeuiIDY2FiQSJ7p/BpeuhIT4Lpv38+5jH3dgWNRXOl48UCBvkPgEoCb2UEDXXUdOxtGbZb3rJwIz9vmuzpjnxmvUE9EbBMReoQTRWCUUXC7WAM7OzqZenTIlEMLCllDgiIi1CK8Ed3dXyM3NpQfDz298t83P50Cs8AfGW6G/Tx7kcg4qKsq1As+bFwqJidvo97Vr14C//wS0kcHcua/SbT4+iu7kVXhJ/588uLjI4fr16zoBOzvLYNmyMAgPfwciIyO7bIqKLtNpoNfPlkiolpRcoQMmJxulpX9DWVkZZGVlUcj589/QAPbwcIP8/HyYMMEPZs2a1QWcl5dH96fXwCQBZWZm0gHX1tZSMHd3Nzh58gT9PmbMaJg69QXaNjIyAoKCJtNpEB//CYjFIpg0aWL30rRzB7XRew+vXLmCDriurg4UirHUiyqViiYsEvJkvnp5ecKGDbHo+R9oG2KnUIzDg/UdtW1paYGQkNcM4+kh8Vh5uWbiamtrg0ePHmlsb29vh/r6evrZWciazDBiw3g+TLwVFraYF24ghcAHB882rAfiDCOhZ1Q9vTaQQkKZnIH1M3f194F4XNxG6rGBFDLnSeYeIKx+/uWBzMOZM6fTc+impiZeUHLVdPx4JmbswIHMWw0P/6l/f/NlqMg5NMngKSl7IC0tDZKSdsHy5W/h6aRvf+utNtUT4CP6/t8M4kWyXuvoTT6pCPBKI/pTSwZ5BYDFL7eNALaVZaVzBXK5E/FyjBEAZyOwTefLWba44dQQhi3BJMf2fSPNHitSyVo1hEAbUOnINkoul/O9nSYRYoMXUfs63lAjb3Y1dqiJ51OXbf39bhzgZ3+291GF5D0lzE9KhDXvyfgfXHcDl3eRBvcAAAAASUVORK5CYII="/></a>'+
									'<div class="jw-info-title">'+videoData.videoTitle+'</div>'+
									'<div class="jw-info-text">';

				if (videoData.videoTeaser && videoData.videoTeaser !== '')
					infoWindow +=		'<p>'+videoData.videoTeaser+'</p>';

				if (videoData.videoArticleUrl && videoData.videoArticleUrl !== '') {
					if($('#video-embed-player').length) {
						infoWindow += '<a href="http://www.manager-magazin.de'+videoData.videoArticleUrl+'" target="_blank">Zum Artikel</a>';
					}
					else {
						infoWindow += '<a href="'+videoData.videoArticleUrl+'" target="_top">Zum Artikel</a>';
					}
				}
					

				infoWindow +=			'<span class="jw-info-date">'+videoData.videoDate+'</span>'+
									'</div>'+
								'</div>';

				playerElem.find('.jw-dock-left').append(infoWindow);

				// click handler for info window
				playerElem.on('click touchstart', '.jw-icon-info', function() {
					$(this).add(playerElem.find('.jw-infowindow')).toggleClass('jw-open');
					playerElem.toggleClass('jw-flag-infowindow-open');
				});

				// copy share buttons to endscreen
				playerElem.parent().find('.video-end-screen .sharebuttons-placeholder').replaceWith(
					$('<div class="jw-skin-mmo ' + touchabilityClass + '"><div class="jw-dock"></div></div>')
				);
				playerElem.parent().find('.video-end-screen .jw-dock').append(playerElem.find('.jw-dock-right').clone());
				playerElem.parent().find('.video-end-screen .jw-icon-share, .video-end-screen .jw-shares').addClass('jw-open');

			}

			// flags to make sure that tracking is happening only once
			playerElem.data({
				startTracked:	false,
				endTracked:		false
			});
		});

		// define function for Netmind tracking (depends on whether we're in an Iframe or on a normal page)
		var doNmTracking = function(trackingParams) {
			// modify default tracking params by the ones passed to this function
			trackingParams = $.extend({
				'sp.atyp':				33,
				'sp.videocredit':		config.credit,
				'sp.df':				config.trackingsize,
				'sp.videodisplaycatid':	config.displaycat,
				'sp.videocatid':		config.videocat,
				'sp.aid':				config.videoId,
				'sp.channel':			835
			}, trackingParams);

			if (!window.spNmAjax && spInterface)
				spInterface('reCountPage', {newParamsOnly: true, params: trackingParams});
			else
				spNmAjax(trackingParams);
		};

		var doTrackEnd = function() {
			doNmTracking({
				'sp.site':	1002,
				'sp.event':	'end'
			});
			if (typeof config.callbacks.onVideoEnd === 'function')
				config.callbacks.onVideoEnd();
		};

		// callback for very first playback of video
		jwplayerInstance.on('firstFrame', function() {
			// make sure the player is really muted if desired
			this.setMute(config.muted);

			if (typeof config.callbacks.onFirstFrame === 'function')
				config.callbacks.onFirstFrame();
		});

		// callback for start of video
		jwplayerInstance.on('play', function() {
			var playerElem = $('#' + this.id);

			// move HD button over to dock, each time a new video plays - because JW Player puts it back into the controlbar on each new video
			playerElem.find('.jw-icon-hd').detach().prependTo(playerElem.find('.jw-dock-right'));

			// only track once! onPlay fires every time the player state switches to PLAYING (= also when unpausing the video)
			// onPlay does NOT fire on preroll - howevery this is desired, cause we want to track the actual content video
			if (!playerElem.data('startTracked')) {
				playerElem.data('startTracked', true);

				doNmTracking({
					'ref':		refParam,
					'sp.site':	2
				});

				// also track usage of SQ quality
				if (playerQuality == 'sq') {
					doNmTracking({
						'sp.site':			1002,
						'sp.videoquality':	'sq'
					});
				}
			}
		});

		// callback for end of video (fires AFTER end of postroll, if there is one!)
		jwplayerInstance.on('complete', function() {
			var playerElem = $('#' + this.id);

			// if video end hasn't been tracked before (= if there is no postroll), track it now
			if (!playerElem.data('endTracked'))
				doTrackEnd();

			if (typeof config.callbacks.onFinalEnd === 'function')
				config.callbacks.onFinalEnd();
		});

		jwplayerInstance.on('adImpression', function(e) {
			var playerElem = $('#' + this.id);
			
			// always hide the dock & shares
			playerElem.find('.jw-icon-share, .jw-shares').removeClass('jw-open-forced');
			playerElem.find('.jw-dock, .jw-controlbar').removeAttr('style');

			// if we're at postroll start: track main content video end, because onComplete fires after postroll
			if (e.adposition == 'post') {
				// if endscreen is present, hide share buttons cause they don't really make sense after a postroll
				wrapperElem.find('.video-end-screen').addClass('jw-after-postroll');
				// make sure we don't track the video-end again on completion of postroll (which is when the JW Player "onComplete" fires)
				playerElem.data('endTracked', true);
				doTrackEnd();
			}

			if (typeof config.callbacks.onAdStart === 'function')
				config.callbacks.onAdStart();
		});

		jwplayerInstance.on('adComplete', function() {
			if (typeof config.callbacks.onAdEnd === 'function')
				config.callbacks.onAdEnd();
		});

		// callback for very first playback of video
		jwplayerInstance.on('adCompanions', function(e) {
			if (typeof config.callbacks.onAdCompanions === 'function')
				config.callbacks.onAdCompanions(e);
		});

		jwplayerInstance.on('time', function(player) {
			var playerElem = $('#' + this.id);

			// force display of controls, dock & shares when in last part of video
			if (player.position > player.duration - 3) {
				playerElem.find('.jw-icon-share, .jw-shares').addClass('jw-open-forced');
				playerElem.find('.jw-dock, .jw-controlbar').show();
			} else {
				playerElem.find('.jw-icon-share, .jw-shares').removeClass('jw-open-forced');
				playerElem.find('.jw-dock, .jw-controlbar').removeAttr('style');
			}
		});

		jwplayerInstance.on('levelsChanged', function(e) {
			// track a change in quality when HQ/SQ is clicked
			var playerQuality = e.currentQuality == 1 ? 'hq' : 'sq';
			$.cookie(qualityCookieName, playerQuality, { expires: 365, path: '/' });
			// when switching to SQ we have to track explicitly, because the JW "onPlay" 1st tracking point was already fired
			if (playerQuality == 'sq') {
				doNmTracking({
					'sp.site': 1002,
					'sp.videoquality': 'sq'
				});
			}
		});

		// callback for errors, to display user-friendly info
		jwplayerInstance.on('error setupError', function(e) {
			$('#' + this.id).hide();
			wrapperElem.find('.video-player-error')
				.html('Leider steht für dieses Video keine auf Ihrem Gerät abspielbare Datei zur Verfügung.')
				.show();
		});

		// click handler for video end screen "replay" button (handler on parent, watching for end screen selector,
		// cause in case of video component player, where end screen is loaded asynchronously)
		wrapperElem.on('click touchstart', '.video-end-screen .watch-video-again', function(e) {
			e.preventDefault();
			mmToggleEndScreen(config.wrapperElemId);
			mmRestartVideo(config.wrapperElemId);
		});
	}

}

function mmResetJwPlayerVars(wrapperElemId) {
	// reset tracking flags
	$('#' + wrapperElemId + ' .jwplayer').data({
		startTracked:	false,
		endTracked:		false
	});
	// also make sure that endscreen dock items are visible again
	$('#' + wrapperElemId + ' .video-end-screen').removeClass('jw-after-postroll');
}

function mmRestartVideo(wrapperElemId) {
	// make sure IVW reload is also possible when we're inside an Iframe
	var doIvwReload = function() {
		if (!window.spIvwReload && spInterface)
			spInterface('reCountPage', {countNM: false, countIVW: true});
		else
			spIvwReload();
	};

	// check if there's a JW Player instance
	if (typeof jwplayer === 'function') {
		// restart the player inside provided wrapper element
		mmResetJwPlayerVars(wrapperElemId);
		$('#' + wrapperElemId + ' .jwplayer').data('playerInstance').play();
		doIvwReload();
	} else {
		// no JW Player & no flash player = other (maybe legacy) video methods - to be sure we do a full page reload in this instance!
		document.location.reload();
	}

	return false;
}

// EndScreen anzeigen
function mmToggleEndScreen(wrapperElemId) {
	// support legacy function call without specific ID
	if (!wrapperElemId || wrapperElemId === '')
		endscreenElem = $('#mmVideoEndScreen');
	else
		endscreenElem = $('#' + wrapperElemId + ' .video-end-screen');

	endscreenElem.fadeToggle(300).delay(300);
}

function mmIpadRemoveOldVideolinks(){
	if (mmClientIsIPad()){
		var css = document.styleSheets[0];
		var rule = ".no-ipad-video {display:none;}";
		css.insertRule(rule, css.cssRules.length);		
	}
}


function videoInlineStart(pane) {
	var teasercontent 	= $(pane).find(".panoplayer-teaser");
	var pic     		= $(pane).find(".js-panoplayer-teaserpic"); 
	var credit  		= $(pane).find(".video-credit"); 
	var newpane = $('<iframe>')
					.attr("src","/video/video-" + $(pane).attr("data-assetid") + "-inline.html")
					.attr("frameborder","0") 
					.attr("scrolling","no")
					.attr("allowfullscreen", "")
					.attr("width", pic.width())
					.attr("height", pic.height() + credit.height());
	$(pane).append(newpane);
	teasercontent.hide();
}

function videoInlineHide(videoid) {
	var pane = 		$("#js-panoplayer-inline-"+videoid);
	pane.find(".panoplayer-teaser").show();
	pane.find("iframe").remove();
}

/* HOME-PLAYER: ----------------------------------------------------------- */

var VideoConfig = VideoConfig || {};
var VideoHomePlayer = (function(Player) {

	// private :  -----------------------------
	var userInteractionHappened = false;
	var activeVideoId = 0;
	// check if an element is currently visible/on screen
	// use distance > 0 if this function should already return true if the element gets close...
	function isOnScreen(element, distanceFromScreen) {
	    var curPos = element.offset();
	    var curTop = curPos.top;
	    var curHeight = element.height();
	    var curBottom = curTop + curHeight;
	    var screenTop = $(document).scrollTop();
	    var screenBottom = $(document).scrollTop() + window.innerHeight;
	    var onScreen = false;
	    onScreen = ( curBottom > (screenTop - distanceFromScreen) && curTop < (screenBottom + distanceFromScreen));
	    // --- console.log( "video-home: ONSCREEN=" + onScreen 
	    // --- 		       + " distanceFromScreen=" + distanceFromScreen 
	    // --- 		       + " curTop="             + curTop 
	    // --- 		       + " screenTop="          + screenTop 
	    // --- 		       + "curBotton="           + curBottom 
	    // --- 		       + "screenBottom="        + screenBottom 
	    // --- 		       + "innerHeight="         + window.innerHeight);
	    return onScreen;
	}

	// public api: --------------------------------------------------------------------
	Player.setInitialVideoid = function(id) {
		// console.log("VideoHomePlayer: setting initial video-id to " +id);
		activeVideoId = id;
	};

	/* ----------------------------------------------------------------------------- */
	// Reminder wird vom Videoplayer aufgerufen
	Player.setReminder = function(event) { 
		var reminder_div = '#video-home-player-reminder';
		$(reminder_div).empty().hide();

		if (event.companions) {
			$.each(event.companions, function(i, companion) {
				// MMO home videoplayer only supports Iframe companions at this point
				if (companion.type == 'iframe') {
					$(reminder_div).show();
					// $(reminder_div).addClass('mmCommercial');
					// $(reminder_div).append('<h5>ANZEIGE</h5>');
					/* reminder_big: 876x50  reminder_small: 304x17 */
					// neu: hoehe 75px
					$('<iframe />', {
						src: companion.resource,
						width: '304',
						height: '75',
						scrolling: 'no',
						frameborder: 0
					}).appendTo(reminder_div);
				}
			});
		}
	};

	/* ----------------------------------------------------------------------------- */
	// 
	Player.onVideoPlayerRunning = function() {
		// player has started playing -> check if the player is still visible
		// may have already been scrolled away...
		Player.checkIfThereIsAPlayerToStart();
	};

	/* ----------------------------------------------------------------------------- */
	//
	Player.onVideoPlayerInteraction = function () {
	    if (userInteractionHappened) return;
	    // console.log("onVideoPlayerInteraction happened");
	    userInteractionHappened = true;
	    // unbind scroll event listener -> we are not interested anymore after user interaction with the player
	    $(window).unbind("scroll");
	};

	/* ----------------------------------------------------------------------------- */
	// this is called in the window's scroll event to check if the player needs to play or pause
	Player.checkIfThereIsAPlayerToStart = function() {
		// don't do auto play & pause anymore if user already interacted with the player
		if (userInteractionHappened) return;

		var homeContainer = $('.video-home-container');
		if (!homeContainer.find('.jwplayer').length) {
			// 1. player not yet created:
			// if the video area is on screen with its half height -> start playing
			// height is currently set to 171, so we check with
			if (isOnScreen(homeContainer, -85))
				Player.replacePlaceholderWithVideoplayer(null,true);
		} else {
			// 2. player is present:
			// look if the videoplayer is partly visible - play if visible, pause if not
			if (isOnScreen(homeContainer, -85))
				Player.sendCmdPlay();
			else
				Player.sendCmdPause();
		}
	};

	/* ----------------------------------------------------------------------------- */
	// send a command to player
	Player.sendCmdPause = function () {
		var jwPlayerInstance = $('.video-home-container .jwplayer').data('playerInstance');
		if (!jwPlayerInstance) return;
		jwPlayerInstance.pause(true);   // "force pause" -> without "true", player would toggle between play & pause
	};

	/* ----------------------------------------------------------------------------- */
	// send a command to player
	Player.sendCmdPlay = function () {
		var jwPlayerInstance = $('.video-home-container .jwplayer').data('playerInstance');
		if (!jwPlayerInstance) return;
		jwPlayerInstance.play(true);   // "force play" -> without "true", player would toggle between play & pause
	};
	
	/* ----------------------------------------------------------------------------- */
	// re-initialize the player with a new video
	// this is called from out HTML-Testsite
	Player.playVideoId = function (videoid) {
		if( ! videoid ) {
			videoid = activeVideoId;
		}
		userInteractionHappened = true;
		// make sure endscreen isn't visible (needed when user clicks a new video from the list, after current video has finished)
		$('.video-home-container .video-end-screen').hide();
		// --- console.log("video-home: USER_INTERACTION: PLAY videoid " + videoid);
		Player.replacePlaceholderWithVideoplayer(videoid,true);
	};
	
	/* ----------------------------------------------------------------------------- */
	// replace the placeholder (DIV) or previous videoplayer with a new one.
	Player.replacePlaceholderWithVideoplayer = function (videoid,autoplay) {
		// initialize videoid with default if not given
		if(! videoid || videoid === null) {
			videoid = activeVideoId;
		}
		activeVideoId = videoid;
		// initialize autoplay with default if not given
		if( typeof autoplay === 'undefined' ) {
			autoplay = true;
		}

		// console.log("video-home: REPLACE started for videoid=" + videoid);
		// singularly tag the now playing video in the
		var videoList = $('#video-home-player-list');
		var nowPlaying = videoList.children('.js-video-hp-starter-'+videoid);
		var notPlaying = videoList.children('.js-video-hp-starter').not(nowPlaying);
		// which social box to show:
		var allSocial = $('#video-home-social .article-info');
		var curSocial = $('#video-home-social-'+videoid);

		nowPlaying.slideUp();
		notPlaying.slideDown();
		// just if additional styling desired ( i.e. to indicate loading )
		notPlaying.removeClass('video-home-now-playing');
		nowPlaying.addClass('video-home-now-playing');
		// transfer titles
		$('#video-home-headline').html( nowPlaying.find('.video-home-text').clone() );
		// unhide the relevant social box only
		curSocial.show();
		allSocial.not(curSocial).hide();

		// clear pane and reset reminder:
		$("#video-home-player-credit").empty();
		$('#video-home-player-reminder').empty().hide();
		var noIVWTracking       = true;
		var startMuted          = true;
		var my_config           = VideoConfig[videoid];
		var my_credit           = my_config['credit'];
		var my_allowads         = my_config['allowads'];
		var category_id         = my_config['catid'] ? my_config['catid'] : 0;
		var display_category_id = my_config['display_catid'] ? my_config['display_catid'] : 0;
		var my_cdnhost          = my_config['cdnhost'];
		var videoadserver  		= my_config['videoadserver'];
		var prerollGateway  	= my_config['prerollGateway'];
		var postrollGateway 	= my_config['postrollGateway'];
		// ACHTUNG: 
		// HP-Player schmale Spalte, Werbung: Sonderbelegung, nicht aus dem Video oder sonstwo, sondern HIER fest gesetzt: 
		// var prerollGateway  = 'http://ad8.adfarm1.adition.com/banner?sid=3020497&wpt=X';
		// var postrollGateway = 'http://ad8.adfarm1.adition.com/banner?sid=3020498&wpt=X';
		// Stand 12/2016: (Umstellung auf Smartclip) QC hat da noch nichts parat, meldet sich ggf. spaeter

		$("#video-home-player-credit").html(my_credit);

		if (userInteractionHappened) {
			startMuted = false;
			noIVWTracking = false;
		}

		// if necessary, remove previous player to make room for new one
		if ($('.video-home-container .jwplayer').length) {
			$('.video-home-container .jwplayer').data('playerInstance').remove();
			$('.video-home-container .videoplayer-placeholder').removeAttr('id');
			$('#mmVideoElements').off('click touchstart');   // needed because this event handler isn't managed by JW Player directly
		}

		mmStartVideo({ 
			wrapperElemId: 'mmVideoElements', 
			videoId: videoid, 
			videoFilename: {hq: my_config['hqfilename'], sq: my_config['sqfilename']}, 
			poster: my_config['poster'],
			videoadserver: videoadserver,
			prerollGateway: prerollGateway, 
			postrollGateway: postrollGateway, 
			allowAds: my_allowads, 
			credit: my_credit, 
			displaycat: display_category_id, 
			videocat: category_id, 
			server: my_cdnhost, 
			trackingsize: 'klein', 
			autoplay: autoplay, 
			callbacks: {onFirstFrame: function() {VideoHomePlayer.onVideoPlayerRunning();}, onUserInteraction: function() {VideoHomePlayer.onVideoPlayerInteraction();}, onFinalEnd: function() {mmToggleEndScreen('mmVideoElements');}, onAdCompanions: function(e) {VideoHomePlayer.setReminder(e);}}, 
			embedWidth: 310, 
			embedHeight: 174, 
			muted: startMuted, 
			dockDisabled: true
		});

		// overwriting global iam_data is ok here, because the page itself (homepage) is already loaded at this point
		// also we need iam_data to be global, so that videoplayer-restart ("watch video again") can re-track IVW values
		window.iam_data = {
			st: 'managerm',
			cp: 'mmo-www-835-6415',
			sv: 'ke'
		};
		// ...but we will only actually track, if we're not on the first video!
		if (!noIVWTracking)
			iom.c(iam_data, 1);
	};

	/* ----------------------------------------------------------------------------- */
	// replace the placeholder (DIV) or previous SWF Videoplayer with a new one.
	Player.toggleVideoList = function () {
		$("#video-home-player-bottom").slideToggle("slow", function() {
			var vhp=$(this).closest(".video-home-player");
			// toggle superiar div (for matters like open/close arrows, etc)
			vhp.toggleClass('video-home-player-open', $(this).is( ":visible" ));
		});
	};
	return Player;
}(VideoHomePlayer || {})); // function
function mmVgWortCount(token) {
	document.write('<div style="display:none;"><img src="http:/'+'/manager.met.vgwort.de/na/'+token+'" width="1" height="1" alt="" align="right" /></div>');
}
var mm_hp_art_next_counter = 0;
var mm_hx_nextlink = 'index';

function mm_hp_art_load() {
	$(".loadicon").show();
	$(".load-more").hide();
	var mm_hx_nextlink_ajax = '/news/hx' + mm_hx_nextlink + '.html';
	//	console.log("get: mm_hx_nextlink_ajax=" + mm_hx_nextlink_ajax)
	$.get(mm_hx_nextlink_ajax, function(data) {
	    $("#hp-teaser-list-bottom").append(data);
		// console.log("loading finished, counter new: " + mm_hp_art_next_counter);
		// console.log("homepage_main: iterating mm_hp_art_list");
		for (i = 0, len = mm_hp_art_list.length; i < len; i++) {
			var classname = 'hxa' + mm_hp_art_list[i];
			$('.'+classname).css("display", "none");
		// var $items = $('.'+classname);
		// if($items.length) {
		// mm_hp_art_next_counter = mm_hp_art_next_counter - $items.length;
		// console.log($items.length + " duplicates found, setting counter back to " + mm_hp_art_next_counter);
		//}
		}
	    if(mm_hp_art_next_counter > 75) {
			$('.js-art-loader-button').html('<a class="load-more" href="/news/' + mm_hx_nextlink + '.html">weitere Artikel im Archiv</a>');
		}
	    // console.log("mm_hp_art_load done: mm_hx_nextlink=" + mm_hx_nextlink);
	    // console.log("mm_hp_art_load counter="+mm_hp_art_next_counter);
		spNmReCount();
	    Mmonline.UI.shareSocial();
	    Mmonline.UI.changeAuthorOverlayDOM();
	    $(".loadicon").hide();
		$(".load-more").show();
		// tell sticky main-nav new page-size
		$(document.body).trigger("sticky_kit:recalc");

		dynamicDates();
	});
}

$(document).ready(function(){
	$('.js-art-loader-button .load-more').click( function() {
		mm_hp_art_load();
		return false;
	});

}); /* ready */
function spnetOpenPopupLayer(classname, openerObject, headline, framelocation)  {
	var popup = document.getElementById('js-popup-layer');
	var html = '<a class="close-button" href="javascript:parent.spnetClosePopupLayer();"><img src="/static/te/buttons/schliessen.jpg" \/><\/a><h1>' + headline + '</h1>';
	html += '<iframe id="popup-layer-iframe" src="' + framelocation + '" scrolling="no" frameborder="0"></iframe>';
	html = '<div class="popup-layer-skin">' + html + '</div>';
	popup.innerHTML = html;
	popup.className = classname;
	if (openerObject != null) {
		var oPopup   = $("#js-popup-layer").offset().top;
		var hPopup	 = $("#js-popup-layer").height();
		var oWindow  = $(document).scrollTop();
		var oElement = $(openerObject).offset().top;
		var hElement = $(openerObject).height();

		var o = oElement - hPopup + hElement + 50;	// Login-Fenster ueber das Ausrichtungsobjekt legen, 50px sind der Rand des Popups
		if (o < oWindow) o = oWindow + 10;		// runter, wenn ausserhalb des Sichtbereichs
		
		$("#js-popup-layer").css({"top" : o + "px"});
	} 
}

function spnetClosePopupLayer()  {
	var popup = document.getElementById('js-popup-layer');
	popup.innerHTML = '';
	popup.className = '';
}function spnetOpenSendForm(language, which)  {
	var headline;
	var framelocation;
	if (which == 'sms') {
		headline = 'Artikel verschicken als SMS';
		framelocation = 'http:/'+'/ml.manager-magazin.de/sendArticle.do?id=' + parent.spMetadataAssetId;
	} else {
		framelocation = spnetENV_SERVER + '/artikelversand/index-' + language + '.html';
		if (language == 'en') {
			headline = 'Send this article';
		} else {
			headline = 'Diesen Artikel verschicken';
		}
	}
	spnetOpenPopupLayer('send-form-popup-layer', null, headline, framelocation);
	var offset = 0;
	if( typeof( window.pageYOffset ) == 'number' ) {
		offset = window.pageYOffset;
	} else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
		offset = document.body.scrollTop;
	} else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
		offset = document.documentElement.scrollTop;
	}
	if (offset > 500) scrollTo(0,0);
}

function spnetSendFormSetAction(form, language)  {
	if (form.action == '') {
		var action = spnetENV_SERVER + '/artikelversand/';
		action += 'online/a-';
		var smsversand = document.getElementById('spnetSendFormSMS');
		if (smsversand != null) {
			smsversand.style.display = 'block';
		}
		action += parent.spMetadataAssetId + '-' + language + '.html';
		form.action = action;
	}
}

function spnetAutoOpenSendForm(language, which)  {
	try {
		var params=spGetHashParams();
		var sendarticle = params["spnet.sendarticle"];
		if (sendarticle == null) return;
	} catch (e) {}
	spnetOpenSendForm(language, which);
}
function spDoGa() {
	/* Universal Analytics: */
	if( sp_webcfg_global.google_analytics != undefined && sp_webcfg_global.google_analytics == true) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-40157549-1', 'auto' , {
        	anonymizeIp: true
        });
		ga('set', 'dimension1', window.location.hash);
		ga('send', 'pageview');
	}
}
/* Plista IFrame Artikel Empfehlungen */
/* obsolet nach Outbrain */
function mmDoPlista(id, url, channelname, date) {
}

// Outbrain Recommendations
function mmDoOutbrainRecommendations() {
	if(sp_webcfg_global.outbrain_recommendations != undefined && sp_webcfg_global.outbrain_recommendations == true) {
		$('#spRecommendations').each(function(i,e){
			var obtype  = $(e).attr("data-obtype");
			var wpre = 'AR';
			var wnum;
			if (obtype == 'nobox') {
				wpre = 'TR';
				wnum = 1;
			} else if (obtype == 'noncompliant') {
				wnum = 4;
			} else if (obtype == 'noarticle') {
				wnum = 5;
			} else {
				if (!$(e).attr("data-assetcount") || $(e).attr("data-assetcount")=="") {
					console.log("NO RECOMMENDATIONS, assetcount not defined");
				} else {
					var count = parseInt($(e).attr("data-assetcount"));
					if 		(count === 0) wnum = '1'
					else if (count >=  4) wnum = '4'
					else if (count === 3) wnum = '3'
					else wnum = '2';
				}
			}
			if (typeof wnum != 'undefined') {
				if ($(e).attr("data-language") == 'en') wnum = parseInt(wnum) + 10;
				var src = 'https://widgets.outbrain.com/hub/index.html';
				var permalink = window.location.protocol + '/'+'/'+ window.location.host + $(e).attr("data-permalink");

				var height = (wpre === 'TR' ? '0'  : '620');
				src += '?permalink=' 	+ encodeURIComponent(permalink);
				src += '&widgetId='  	+ encodeURIComponent(wpre + "_" + wnum);
				src += '&fbk=' 			+ '0';
				src += '&ob-template=' 	+ encodeURIComponent('DE_ManagerMagazin'); 
				var obframe = $('<iframe>')
								.attr("src",src)
								.attr("frameborder", 	"0")
								.attr("scrolling",		"no")
								.attr("transparent", "transparent")
								.attr("allowTransparency", "true")
								.attr("width", "860")
								.attr("height", height);
				
				$(e).html(obframe);
				if (wpre === 'TR') $(e).hide();
				console.log("FILL RECOMMENDATIONS", "permalink="+permalink, "assetcount="+$(e).attr("data-assetcount"), "widgetId="+wpre + "_" + wnum, src);
			};
		});
	} else {
		// do nothing
		console.log("RECOMMENDATIONS OFF by global configuration"); 
	};
};
/* allgemeine Tools */
function HashParams() {
	var loc=(document.location+"");
	this.url = loc;
	this.hashQueryString = null;
	this.params = {};
	
	var pos=loc.indexOf("#");

	if (pos > -1) {
		this.hashQueryString = loc.substring(pos+1, loc.length);
		this.url 		 	 = loc.substring(0,pos);
		
		var pairs=this.hashQueryString.split("&");
		for (var i=0; i < pairs.length; i++) {
			var kv=pairs[i].split("=");
			if (kv.length == 2) 
				this.params[kv[0]]=decodeURIComponent(kv[1]);
		}
	}

	this.exist = function(which) {
		if (typeof which == 'undefined') {
			return (Object.keys(this.params).length > 0);
		} else {
			return (typeof this.params[which] != 'undefined');
		}
	}
	
	this.value = function(which) {
		return this.params[which];
	}
}/* allgemeiner Forms-Processor */
function FormsProcessor(id) {
	var that = this;

	that.id = id;
	that.processor = $("#"+id);
	that.form = $("#"+id+" form.forms-processor-form");
	that.box = $("#"+id+" .forms-messagebox")
	that.config = {};

	that.okmessage = '<strong>Ihre Nachricht wurde versandt</strong>';
	that.errormessage = '<strong>Ihre Nachricht konnte nicht versandt werden!</strong> Ein unbekannter Fehler ist aufgetreten';
	
	that.init = function(jsonconfig) {
		if (jsonconfig) {
			that.config = jsonconfig;
			if (jsonconfig.errormessage)
				that.errormessage = jsonconfig.errormessage;
			if (jsonconfig.okmessage)
				that.okmessage = jsonconfig.okmessage;
		}
		
		$(that.form).bind("submit", function(e) { that.send(); return false; });
		$(that.form).find("select.update").bind("change", function(e) { that.update(this) });
		$(that.form).find("textarea").val(function(index, value){ return $.trim(value); });
		
		// Pre-Select?
		var hashparams = new HashParams();
		if (hashparams.exist()) {
			$(that.form).find("select").each(function() {
				var elem = $(this);
				var name = elem.attr("name");
				if (elem.is(":visible")) {
					if (hashparams.exist("fp-"+name)) {
						elem.find("option[data-value='" + hashparams.value("fp-"+name) + "']").attr('selected',true);
						elem.trigger("change");
					}
				}
			});
		}
	}

	that.update = function(element) {

		$(that.box).empty()
		   .removeClass("messagebox")
		   .removeClass("ok")
		   .removeClass("error");
			
		var active_formpart = $(element).parent();
		var todo = $(active_formpart).find('select.update option:selected');
		
		// Felder trimmen
		$(that.form).find("textarea, input").val(function(index, value){ return $.trim(value); });
			
		// formparts nach dem aktuellen ausblenden und resetten
		var fparts = $(active_formpart).find('~ .formpart');
		$(fparts).hide();
		$(fparts).find(".error").removeClass("error");
		$(fparts).find('select.update').prop('selectedIndex',0);
		$(fparts).find('select.update option.info').show();
						
		// Display-Eigenschaften
		var allparts = $(that.form).find('.formpart');
		$(allparts).find(".formelement").removeClass("show")
									  	.removeClass("hide")
	  									.removeClass("req")
	  									.removeClass("noreq");
														
		var info = that.config;	

		$(allparts).filter(":visible").each(function(i, part) {
			var partkey = $(part).find('select.update option:selected').attr("data-value");
			$(part).find(".infobox").remove();
			if (partkey == null) {
				return true; // continue;
			}
			info = info[partkey];
			if (info == null) {
				return false; // break
			}
			if (info["display"]) {
				$.each(info["display"], function(key, value) {
					$('.formelement[data-id='+key+']').addClass(value ? 'show' : 'hide');
			    });
			}
			if (info["require"]) {
			    $.each(info["require"], function(key, value) {
			    	$('.formelement[data-id='+key+']').addClass(value ? 'req' : 'noreq');
			    });
			}
	
			if (info["infobox"]) {
				$(part).append($('<div>').addClass("infobox").html(info["infobox"]));
			}
	
		});
		
		// bitte waehlen etc. ausblenden
		if (!$(todo).hasClass("info")) {
			$(active_formpart).find('select.update option.info').hide();
		}
		
		// Folge-Formpart einblenden
		if ($(todo).attr("data-formpart")) {
			$(that.form).find('.formpart[data-id='+$(todo).attr("data-formpart")+']').show();
		}
		if ($(todo).attr("data-action") == "send") {
			$(that.form).find('.formpart[data-id=send]').show();
		}
		
		// wenn letztes element aus dem Sichtbereich geraten ist: scrollen
		var screenY	= $(document).scrollTop();
		var screenH = $(window).height();
		var screenE = $(element).offset().top;
		if (screenE < screenY+100) {
			$('html, body').animate({
			    scrollTop: screenE-100
			});
		}
		return false;
	};

	
	that.send = function() {
	
		that.disable();

		$(that.box).empty()
		   .removeClass("messagebox")
		   .removeClass("ok")
		   .removeClass("error");
		
		var postdata = {};
		var errordata = {};
		var isError = false;
		
		// check required
		var allparts = $(that.form).find('.formpart').filter(":visible");
		
		// trimmen
		$(allparts).find("textarea, input").val(function(index, value){ return $.trim(value); });
		// error reset
		$(allparts).find(".error").removeClass("error");
		

		$(allparts).each(function(i, part) {
			var fields = $(part).find(".required, .req").not(".noreq").find("[name]").filter(":visible");
			$(fields).each( function(i, field) {
				if ($(field).val() == '') {
					$(field).addClass("error")
					 		.focus(function() {$(this).removeClass("error")});
					isError = true;
				}
			});
		});
		if (isError) {
			$(that.box)	.addClass("messagebox error")
						.html(that.errormessage);
			that.enable();
		} else {

			$(allparts).each(function(i, part) {
				var fields = $(part).find("[name]").filter("input, textarea").filter(":visible");
				$(fields).each( function(i, field) {
					if ($(field).attr("type") == 'checkbox') {
						postdata[$(field).attr("name")] = $(field).is(':checked') ? 'ja' : 'nein';
					} else {
						if (postdata[$(field).attr("name")]) 
							postdata[$(field).attr("name")] += "\n\n" + $(field).val();
						else 
							postdata[$(field).attr("name")] = $(field).val();
					}
				});
			});

			// Konfiguration auslesen
			postdata["forms-config-xmlid"]  = $(that.processor).attr("data-forms-config-xmlid");
			postdata["forms-config-formid"] = that.id;
			
			var formsConfigPath = "";
			$(allparts).find('select.config').each(function(i, elem) {
				if (formsConfigPath != "") formsConfigPath +="&";
				formsConfigPath += $(elem).attr("name") + "=" + $(elem).find("option:selected").attr("data-value"); 
			});
			postdata["forms-config-path"] = formsConfigPath; 

			var formsConfigLabel = "";
			$(allparts).find("[name][data-label]").each(function(i, elem) {
				if (formsConfigLabel != "") formsConfigLabel +="&";
				formsConfigLabel += $(elem).attr("name") + "=" + $(elem).attr("data-label"); 
			});
			postdata["forms-config-label"] = formsConfigLabel;
			
			// reCaptcha
			$(that.form).find('#g-recaptcha-response').each(function(i, field) {
				postdata["g-recaptcha-response"] = $(field).val();				
			}); 
			
			$.ajax({
				type : "POST",
				url : $(that.processor).attr("data-forms-action"),
				contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
				dataType : "json",
				data: postdata
			})
			
			.done(function( data ) {
							if (data.errormessage) {
								$(that.box).addClass("messagebox error")
										   .html(data.errormessage);
								if (typeof grecaptcha != 'undefined') grecaptcha.reset();
							} else if (data.rawmail) {
								$(that.box).addClass("messagebox ok")
										   .html("Auf diesem System werden keine Mails verschickt..<br />Hier die Mail samt Headern:<br /><br />")
										   .append($('<pre>').text(data.rawmail));
							} else {
								$(that.box).html(that.okmessage)
										   .addClass("messagebox ok");
								$(that.form).remove();
							};
							that.enable(); 

			})
			
			.fail(function(data) {
							var msg;
							var resp = data.responseText;
							if (resp.status.errormessageinternal) {
								msg = resp.status.errormessageinternal;
							} else {
								msg = 'Ein Fehler ist aufgetreten.';
							}
							$(that.box)	.html('<strong>'+msg+'</strong>')
										.addClass("messagebox error");

							that.enable();

			});
		}
		return false;
	};
	
	that.disable = function() {

		$(that.processor).addClass("wait")
						 .find("input[type=submit]").addClass("disabled");
	}
	that.enable = function() {

		$(that.processor).removeClass("wait")
		 				 .find("input[type=submit]").removeClass("disabled");
		
		if ($(that.box).hasClass("messagebox")) {

			// Mitte des Screens
			var screenY	= $(document).scrollTop();
			var screenH = $(window).height();
			var boxY    = $(that.processor).offset().top;
			var boxH    = $(that.processor).height();

			var bestPos = boxY - (screenH-boxH)/2;
			if (bestPos < 0) bestPos = 0;
			if (bestPos > boxY) bestPos = boxY -100;

			if (screenY > boxY || screenY+screenH < boxY+boxH+100) {
				$('html, body').animate({
				    scrollTop: bestPos
				});
			}
		}

	}

};

