var sp_webcfg_global = {
		google_analytics:        true,
		google_analytics_inpage: true,
		meetricsMX:              's413.mxcdn.net/bb-serve/mtrcs_132650.js',
        outbrain_recommendations:true,
		linkpulse:		         false,
		flash: { 
			player: 'coreFlashPlayer-019.swf', pluginversion: { major: 10, minor: 1 }
		},
		social_bm: { 
			fb: true, 
			tw: true, 
			gplus: true 
		},
		articleOverscroll: true
};
