Intro
-----------
   
Präsentieren ist Alltag in den meisten Berufszweigen, Präsentieren von Produkten, Ideen oder sich selber als Person.  Laptop, Beamer und PowerPoint sind das Handwerkszeug für Vorträge in fast allen 
Bereichen. 

Bis vor einigen Jahren wurden in Konferenzräumen Reden und Vorträge gehalten, Meetings hießen noch Besprechungen und in Hörsälen wurde referiert und diskutiert. Neue Ideen und Erkenntnisse wurden vorgestellt und präsentiert wurden Produkte. Mittlerweile wurde alles zu einer Präsentation, 
vorwiegend hergestellt mit dem Programm PowerPoint: die Unternehmensphilosophie, das neue Computersystem, jedwede Information, die Mitarbeiterbesprechung, die Jubiläumsfeier, die Messeprodukte und so gut wie jede Fortbildung. Doch die Klagen gelangweilter Zuhörer über endlose Aufzählungs-Paraden, textüberladene Folien, Zahlenschlachten und unübersichtliche Diagramme nehmen stetig zu, enttäuschte Redner zweifeln an ihrem Fachwissen oder der Konzentrationsfähigkeit 
ihres Publikums. 

Trotzdem ist die PowerPoint-Welle anscheinend nicht zu stoppen: Schätzungsweise 
werden weltweit pro Tag 30 Millionen PowerPoint-Präsentationen hergestellt. Mit PowerPoint wurden die Begriffe „Vortrag“ und „Referat“ in vielen Bereichen durch „Präsentation“ ersetzt und „Präsentation“ ist mittlerweile fast gleichgesetzt mit PowerPoint-Folienpräsentation. 
   

Warum
~~~~~~

Wie konnte es zu einem solchen Siegeszug eines eigentlich äußerst banalen Programmes kommen?  Ursprünglich von einem Programmierer zum Eigenbedarf für die Produktpräsentation geschrieben, wurde dieses Programm von der Firma Microsoft aufgekauft, ausgebaut und seit Ende der 80er Jahre erfolgreich vermarktet. Bei den Mitarbeitern vieler Firmen wurden offene Türen eingerannt, suggeriert diese Programm doch, dass die mühsame Erarbeitung eines Vortrags jetzt, assistiert von PowerPoint, einfacher, strukturierter, multimedial und unter Vernachlässigung rhetorischer Bemühungen technisch up-to-date, zudem noch erfolgversprechend, ablaufen kann. Hier liegt wohl das Geheimnis des Erfolgs: die Anfertigung einer solchen Präsentation verlangt weniger Eigeninitiative und kreative  Bemühungen, Gliederungstexte können problemlos aus Word übernommen werden, zur Garnierung werden einige Bilder eingefügt und fertig ist die Präsentation. Zwar murrt das Publikum, aber die meisten Zuhörer sind auch PowerPointfolien-Produzenten, sie stellen also das Medium nicht prinzipiell in Frage, nur die Form. Und der kleine Rest der Zuhörerschaft lehnt Beamerpräsentationen sowieso in jeder Form ab. 


Aber so wenig, wie die Beherrschung eines Textverarbeitungsprogramms einen Schriftsteller entstehen lässt, übt die Fähigkeit, Folien zu erstellen, die für einen erfolgreichen Vortrag vor einem Publikum elementaren Voraussetzungen wie: Rhetorik, Gestik, Eingehen auf die Zuhörer, Koordination von Rede und Technik etc. 


Das Problem
~~~~~~~~~~~~

Aber was, wenn diese Fähigkeiten nicht vernachlässigt sondern gepflegt würden? Wenn vor jedem Vortrag überlegt würde: was ist die für das Thema angemessene Präsentationsmethode?  Wenn, falls die Wahl auf PowerPoint fällt, die Grundstruktur des PP-Layouts, die aus Text-Aufzählungen besteht, durchbrochen würde zugunsten einer den Vortrag unterstützenden Visualisierung? 
 
Die Anwendung von PowerPoint reformieren: dazu muss zunächst analysiert werden, was in weiten Teilen in die falsche Richtung läuft.  PowerPoint bedeutet, frei übersetzt, „starkes Argument“. Was wiederum die Herkunft aus der Produkt- und Verkaufsbranche zeigt. Die Grundstruktur von PP sind Aufzählungen und vergleichende Auflistungen. Komplexe Zusammenhänge werden so auf lineare Abläufe reduziert. Durch den in Aufzählungspunkten gebannten Text wird eine Spontanität des Vortrags stark eingeschränkt. Erscheint Text an einer exponierten Stelle wie den meist großen Projektionsflächen des Beamers, unterliegen die Zuhörer fast immer sofort einem Lesezwang und das gesprochene Wort konkurriert mit dem  geschriebenen. Sind die Wörter identisch, wundert oder ärgert sich der Leser/Zuhörer. Sind sie es nicht, geht es ihm meist ebenso.



Und nun
~~~~~~~~

Was also ist mit PowerPoint anzufangen? Ohne weitere (Gedanken-)Investition sicherlich nicht viel. 
Aber: wenn die Vorstellung fallen gelassen wird, dass PP ein Textmedium ist und die unzähligen visuellen Darstellungsmöglichkeiten, die die digitale Darstellungsweise bietet, in Betracht gezogen wird, dann bieten sich ganz andere Möglichkeiten. Es geht dann nicht mehr darum, den Vortragstext möglichst bunt und virtuos auf die Folien zu bannen, sondern die Hauptpunkte des Anliegens dem Publikum zu veranschaulichen. Was eben nicht durch eine Wörter-Verdoppelung: gesprochenes + geschriebenes Wort geschehen kann, sondern durch Bilder. Ein Bild sagt mehr als tausend Worte…, Fotos, Formen Strukturen, Piktogramm, Ablaufpläne, einfache Diagramme, passend ausgewählt und in Form gebracht bereichern eine Präsentation. Falls sie das nicht tun, sollte man vielleicht auf jede Form von Medienunterstützung verzichten und bei der reinen Rede bleiben.  




Notwendige Präsentationstechniken
----------------------------------

heute wird im Allgemeinen vorausgesetzt, dass man sowohl fachliche Themen als auch sich selbst, seine Interessen und Forderungen angemessen darstellen kann. Dazu sind grundlegende Kenntnisse von Präsentationstechniken vonnöten.


.. admonition:: Aufgabe

	Welche Fähigkeiten/Tätigkeiten sind für eine gute Präsentation notwendig ?
	
	Finden Sie mindestens 10 unterschiedliche Aspekte 


.. admonition:: Lösung


	- Thema festlegen, bzw. Themenfindung oder –eingrenzung 
	- Ziel der Präsentation festlegen (überprüfbares Ziel) 
	- Anlass und Vorgaben (Raum, Zeit, Inhalt, Ablauf) berücksichtigen 
	- Zielgruppe beschreiben und Präsentation darauf ausrichten 
	- Brainstorming, Gliederung, Strukturierung 
	- Stoffsammlung / Materialsammlung, Recherche mit Quellen(-beurteilung) 
	- Zitieren und Bildrechte kennen 
	- Argumentationsaufbau entspr. Informations- oder Überzeugungspräsentation 
	- Kenntnis verschiedener Präsentationsmethoden 
	- Auswahl einer angemessenen Präsentationsmethode 
	- Visualisierung der wichtigsten Aussagen mithilfe von Schaubildern, Grafiken, Fotos, Diagrammen 
	- Inszenierung des Vortrags (Rhetorik, Koordination etc.) 
	- Besonderheiten bei der Planung einer Präsentation mit PowerPoint 
	  
	  - Beherrschen des Programms 
	  - Beachten der Regeln für eine gute Präsentation 
	  - Gegebenenfalls Kenntnisse Bildbearbeitung 
	  - Koordinierung Vortrag – Technik 

