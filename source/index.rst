.. Präsentation documentation master file, created by
   sphinx-quickstart on Wed Nov 05 19:26:07 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Präsentationen
==============


.. toctree::
   :maxdepth: 2

   
   intro.rst
   medien.rst
   farbe.rst
   schrift.rst
   layout.rst
   
..   kommunikation.rst
..  recht.rst
..  bild.rst
..   layout.rst
..   checkliste.rst
..   Aufgabe.rst


.. .. only:: html

..    Indices and tables
..    ==================

..    * :ref:`genindex`
..    * :ref:`modindex`
..    * :ref:`search`

