Aufgabe
=======

Erstellen Sie zu ihrem Thema eine Präsentation unter folgenden Gesichtspunkten.

- Die Präsentation dient einem Programmierkollegen zu einem ersten Kennenlernen des Sachverhaltes.

- Informieren Sie sich über das jeweilige Thema  mit Hilfe von mindestens drei 
  verschiendenen Quellen (z.B. Wikipedia, Tutorial, Online-Bücher, Bücher, etc).
  Dokumentieren Sie diese Quellen innerhalb Ihrer Präsentation.

- Grenzen Sie in Absprache mit dem Lehrer das Themengebiet so ab, dass Sie dieses in ca. 10-15 Minuten darstellen können.

  
- Erstellen Sie eine 

  - beamergestützte Präsentation
  - Dokumentation, in der Sie die Motive/Absichten der Präsentation beschreiben.
    Gehen Sie dabei auf Aspekte wie Farben, Fonts, Layout, Navigation ein.
  
  
**Themen**

- ADO.NET als Modell zum Datenbankzugriff im .NET-Framework
- LINQ als ORM-Framework
- Hibernate als ORM-Framework (Java/.NET)
- Doctrine als ORM-Framework (php)
- Indizierung in Datenbanken (Pro/Kontra/Probleme)
- Transaktionssicherheit in Datenbanken
- Visualisierung der verschiedenen JOINS innerhalb von SQL-Abfragen
- InnoSetup (Installer)
- NSIS (Installer)
- Sliding Window Mechanismus des TCP-Protocols
- Dijkstra-Algorithmus (Shortest Path): Erklärung und beispielhafte Anwendung
- Balanced Tree (B-Baum): Definition, Verwendung, Demo
- Unit-Testing (JUnit/NUnit/...). Definition, Verwendung/Demo
- BASE / CAP versus ACID: NoSQl als Alternative zu relationalen DB's
- Triggers und Views in Datenbanken
- StoredProcedures/Functions in Datenbanken
- OrientDb:  Abfragesprache im Vergleich zu SQL
- Neo4J   :  Abfragesprache im Vergleich zu SQL
- Ports und Sockets aus Programmierersicht
- Methoden der Kollisionserkennung in Spielen










