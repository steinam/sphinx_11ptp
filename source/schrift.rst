Schrift
===========


Sie kennen dies: Präsentationen mit langen Aufzählungslisten 
mit Text auf jeder Folie. Spätestens nach der dritten Folie ist das 
Publikum müde!

Auf Text werden Sie in Ihren Präsentationen kaum verzichten können. Beachten Sie jedoch, dass Sie es sind, der mit Worten kommuniziert und nicht Ihr Präsentationsmedium. Die Präsentation dient Ihnen und Ihrem Publikum zur visuelle Unterstützung der Sprache. Dabei sind Bilder, Grafiken, Farben und Animationen wesentlich eindrucksvoller als Text. 

Leider  werden  immer  noch  häufg  Präsentationen  gezeigt, die  viel  zu  viel Text  enthalten.  Der  Zuhörer  muss  sich  dann zwangsläufig entscheiden, ob er zuhören oder den Text lesen soll. Beides geht nicht, es sei denn, der Text wird abgelesen. Dies macht die Sache aber nicht besser!


.. only:: html

	.. sidebar:: Lösung
	
		.. image:: material/schrift_lesen_loesung.jpg
			:width: 100px

.. admonition:: Aufgabe

	Wie könnte man folgende Präsentation verbessern ?
	
	.. image:: material/schrift_lesen_vorgabe.jpg
	

.. only:: latex
	
	.. image:: material/schrift_lesen_loesung.jpg
			:width: 100px
	
**Begründung:**

Auf  der  linken  Folie  steht  alles,  was  gesagt  werden  könnte. 
Wozu ist der Präsentator überhaupt da? Um vorzulesen? Das Publikum käme sich blöd vor. Im linken Beispiel bleibt alles offen. Das Publikum muss zuhören, um Informationen zu erhalten. Die wenigen Schlagworte, ergänzt durch das große Foto, dienen zur Veranschaulichung und unterstützen den Vortragenden, ohne ihn zu ersetzen.



	
Schriften bewerten und auswählen
---------------------------------

Lesen Sie die Schriftbeispiele: Was stimmt hier nicht?
  
.. image:: material/schriftbeispiel.jpg
	
	
Schriftcharakter und inhaltliche Aussage des Textes passen nicht zusammen: 
Eine Werbeagentur würde keine altmodisch wirkende (Fraktur-)
Schrift wählen, für den Straßenbau eignet sich keine feine und 
schmale Schrift, ein Zeitungsartikel in einer Schreibschrift wäre 
nicht  lesbar  usw.  Eine  gut  gewählte  Schrift  stellt  einen  optischen Bezug zum Inhalt her. Durch Tausch der Schriften erhalten wir eine deutlich bessere Lösung: 

.. image:: material/schriftbeispiel_loesung.jpg


Schriftgröße und Zeilenabstand
-------------------------------

Eine Schrift sollte nicht nur beim Erstellen auf dem Monitor lesbar sein, sondern Sie muss beim Ablaufen der Präsentation auf der Präsentationsfläche zu lesen sein. Die dazu notwendige Schriftgröße kann in Abhängigkeit von Raumtiefe und Breite der Präsentationsfläche berechnet werden.

.. image:: material/schriftgroessenberechnung.jpg

Dies ergibt folgende Kombinationsmöglichkeiten.

.. image:: material/schriftgroessenberechnung_matrix.jpg


**Zeilenabstand**

.. sidebar:: ZAB

	.. image:: material/zab.jpg
	
	
Der Zeilenabstand (ZAB) ist der Abstand zwischen zwei Schrift-
grundlinien. Er beträgt standardmäßig 120 % der Schriftgröße, 
also  bei  einer  24-pt-Schrift  28,8 pt.  In  Präsentationssoftware 
wird dieser Abstand meistens als „Einfach“ bezeichnet. 
Bei  einer  schmalen  und  hohen  Schrift  kann  es  sinnvoll  sind, 
den Zeilenabstand etwas zu vergrößern, z.B. auf 150 % (Ein-
stellung: 1,5). Wird der Zeilenabstand jedoch zu groß gewählt, 
dann fällt der Text optisch auseinander:

.. image:: material/zab_gelungen_j_n.jpg

Der Text im linken Beispiel ergibt keine Einheit und muss Zeile 
für  Zeile  gelesen  werden.  Den Text  rechts  nehmen  wir  nach 
dem sogenannten **Gesetz der Nähe** als einheitlichen Textblock 
wahr. Er bildet einen Gegenpol zum Scheinwerfer, der den Text 
zu beleuchten scheint.


**Zeilenlänge**

Beim Lesen eines Textes folgen wir der (unsichtbaren) Grundli-
nie der Schrift. Je länger eine Zeile wird, umso leichter kann das 
Auge in der Zeile verrutschen. Aus diesem Grund dürfen Zeilen 
nicht zu lang sein. Für Bildschirmpräsentationen gilt, dass eine 
Zeile  maximal  fünfzig  Zeichen  (Buchstaben  plus  Leerzeichen 
plus Satzzeichen) enthalten sollte.

.. image:: material/zeilenlaenge.jpg

Weiterhin sollte darauf geachtet werden, dass bei der Verwendung verschiedener Schriften diese aus verschiedenen Schriftarten genommen werden.

.. image:: material/schriftart_kombination.jpg




**Systemschriften**

Sämtliche  Schriften  eines  Computers  beﬁnden  sich  in  einem 
speziellen Ordner des Betriebssystems. Bei der Verwendung von Schriften in einem Präsentationsprogramm wird auf diesen Ordner zugegriffen.
Problematisch  wird  es,  wenn  Sie  Ihre  Präsentation  mittels 
USB-Stick auf den Rechner im Präsentationsraum übertragen und die gewählte Schrift auf diesem Rechner **nicht** installiert ist.  
Die Software ersetzt Ihre Schrift kurzerhand durch eine vorhandene Schrift und Ihr schönes Layout ist dahin. 
Treffen  Sie  eine  der  folgenden  Maßnahmen,  damit  Ihre Schriften dargestellt werden:
- Nehmen Sie, wenn möglich, immer Ihr eigenes Laptop mit.
- Erstellen Sie aus Ihrer Präsentation ein PDF. Dieses Format bindet Schriften immer mit ein.
- Verwenden  Sie  eine  Systemschrift,  also  eine  Schrift,  die 
zusammen  mit  dem  Betriebssystem  installiert  wurde,  z.B. 
Verdana, Arial, Georgia, Impact, Lucida, Times (New Roman), 
Palatino.
- Bei PowerPoint gibt es die Option *Schriftarten in Datei einbetten*. 
