Medien
=======

Zur Planung einer Präsentsation gehört zwangsläufig die Auswahl eines oder mehrerer Präsentationsmedien. Häufig fällt die Entscheidung dann schnell auf eine Beamer-gestützte Präsentation. Dies muss aber keineswegs die beste bzw. die einzige Lösung sein.

.. admonition:: Aufgabe

	Welche Gründe kann es für eine Präsentation ohne Beamer geben ?
	
.. admonition:: Lösung

	- Mein Publikum besteht aus etwa 10/50/100 Personen
	- Professionelle Gestaltung ist mir wichtig
	- Ich lege Wert auf farbige Grafiken und Bilder
	- Das Handling soll möglichst einfach sein
	- Die Präsentation muss flexibel (transportabel) sein
	- Meine technischen Kenntnissse sind gering
	- Ich will das Publikum stark einbeziehen
	- Ich bereite mich am liebsten am Computer vor
	- Mein Publikums soll ein Handout erhalten
	- Ich möchte mit Animationen arbeiten
	- Ich möchte eher moderieren als präsentieren
	- Die Information soll die ganze Zeit sichtbar sein
	- Ich habe wenig Zeit für die Vorbereitung
	- Die Präsentation muss mehrfach wiederholt werden
	- Mir liegen die Texte/Bilder in elektronischer Form vor
	- Ich will Gegenstände zeigen
	- Ich möchte Sound und Video einsetzen
	- Ich will die Präsentation optimal vorbereiten können
	

	
	
.. sidebar:: Checkliste
	
	.. image:: material/checkliste_medien.jpg
		:width: 200px
	
	 

.. admonition:: **Aufgabe**

	Erstellen Sie innerhalb ihrer Gruppe eine Präsentation, die eines der unten dargestellten Szenarien abbildet.
	Überlegen Sie sich eine durchgängige Struktur und 
	Verwenden Sie für alle Folien einen sog. Folienmaster. Die Gruppe muss in der Lage sein, seine Ergebnisse zum Ende der nächsten Woche zu präsentieren.


	**Szenario 1**:

	Die Auszubildenden des 1. Lehrjahres kommen nach der Berufsschulwoche zurück in die Firma. Manchen von Ihnen (3) ist der Einsatz von Bedingungen und Schleifen innerhalb der Powershell nicht klar geworden. Ihr Chef gibt Ihnen den Auftrag, die Defizite abzustellen und reserviert Ihnen dafür den Besprechungsraum für zwei Stunden.
	Der Raum besitzt Beamer und Whiteboard sowie Wifi. 



	**Szenario 2**:

	Ihre Firma wird im Frühjahr mit einem Stand auf der Cebit vertreten sein. Sie sollen eine Präsentation erstellen, die die Kernkompetenzen ihrer Firma darstellt. Sie ist im Bereich Web-Development (Drupal, Wordpress) und Netzwerklösungen (Firewall, USVs, Telefonanlagen) tätig. Sie beschäftigen 150 Mitarbeiter deutschlandweit und haben Niederlassungen in den Städten Berlin, Hamburg, Köln, Stuttgart und München). Der Ausbildungsleiter möchte eine selbstablaufende Präsentation, die am Stand unbeaufsichtigt laufen soll.



	**Szenario 3**:

	Ein Kunde (Architekturbüro mit 30 Mitarbeitern)  möchte von Ihnen eine Netzwerklösung (Firewall, USV, Router, Switch, WiFi, FileServer, EmailServer-Lösung). Die bestehenden Clients (Windows) und die Netzwerkkabel können erhalten bleiben.  
	Erstellen Sie eine Präsentation, die dem Kunden die wesentlichen Elemente der neuen Lösung vermittelt. 

	Für die Präsentation beim Kunden ist eine Stunde vorgesehen. Beamer, Whiteboard und Flipchart ist vorhanden.

	.. image:: architekturbuero.jpg	
	
