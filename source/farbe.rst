Farbe
======

.. sidebar:: Was empfinden Sie bei diesem Bild

      :download:`Farbe <material/rot_empfindung.jpg>`

	  http://www.bilder-plus.de/farb-lexikon.php


      
Farbwirkung
-----------
      
Farben  lösen  immer  Empﬁndungen  und  Gefühle  in  uns  aus. 
Diese Wirkung der Farben ist aber nicht angeboren, sondern sie wird durch unsere Erfahrungen und unser kulturelles Umfeld bestimmt.

Welche Farbe hat die Liebe? Welche Farbe hat die Gefahr? Welche Farbe hat der Sommer?

Viele Menschen werden auf alle drei Fragen gleich antworten: ROT.  

Wie kann es sein, dass eine Farbe mit so unterschiedlichen Dingen assoziiert wird? Es gibt eben nicht die Farbe zu der bestimmten Emotion. Wir verbinden mit jeder Farbe viele unterschiedliche  Erfahrungen  und  damit  auch  Empﬁndungen. 
Welche Wirkung  eine  Farbe  in  einer  konkreten  Situation  hat, wird immer durch den Kontext, durch die sie umgebenden Farben bestimmt.

.. image:: material/farbe_und_kontext.jpg


Farben machen das Design Ihrer Präsentation visuell interessanter. Sie gliedern und geben den Inhalten unterschiedliche Bedeutung. Die Farben sind in Ihrer Präsentation also nicht Selbstzweck, sondern  gestalterisches  Mittel,  Ihre  Botschaft  dem  Publikum besser verständlich zu machen. Aber wie viele und welche Farben setzen Sie ein?

	

Farbkombinationen
------------------

Um Farben zu beschreiben und zu unterteilen können Farbkreise sehr hilfreich sein. Es gibt weder Anfang noch Ende, das heißt, dass jede Farbe des Farbkreise rechts und links eine benachbarte Farbe hat.


.. only:: html

	.. sidebar:: Link
	
		http://www.bilder-plus.de/farbkreis.php




RGB
****

Ein Farbkreis unterteilt sich in Primär-, Sekundär-, Tertiär-, Quartärfarben, dies kann beliebig fortgeführt werden. Die Primärfarben im RGB -Farbkreis sind die Grundfarben: Rot (RGB 255 0 0 ), Grün (RGB 0 255 0) und Blau(RGB 0 0 255). 

.. image:: material/farbkreis_rgb_primaer.jpg
	:width: 100px

.. image:: material/farbkreis_rgb_sekundaer.jpg
	:width: 100px

.. image:: material/farbkreis_rgb_tertiaer.jpg
	:width: 100px


Aus den Primärfarben mischen sich dann alle weiteren Farben, je nachdem in welchen Mischverhältniss man diese mit einanderer mischt. Bei einem 50 zu 50 Verhältnis ergeben sich die Sekundärfarben. In unseren Beispiel sind die Gelb(RGB 255 255 0), Pink (RGB 255 0 255) und Türkis(RGB 0 255 255) Die Tertiärfarben sind die Mischung zwischen einer Primär- und einer Sekundärfarbe, oder man mischt die Primärfarben im Verhältniss 1/3 zu 2/3. Diese wären RGB-Farbkreis Grün-Gelb(RGB 127 255 0), Hellblau(RGB 0 127 255), Violett(RGB 127 0 255), Magenta(RGB 255 0 127), Orange(CMYK 255 127 0) und Grün-Gelb(RGB 0 255 127).

Wie in jedem Farbkreis stehen sich auch im RGB-Farbraum die Komplementärfarben gegenüber. Komplementärfarben sind die Farben mit dem größten Kontrast zu einander. In unserem Beispiel sind dies Rot - Türkis, Grün - Pink und Blau - Gelb. Es stehen sich je eine Primärfarbe und eine Sekundärfarbe gegenüber. 

.. image:: material/farbkreis_rgb_komplementaer.jpg
	:width: 100px

CMYK
****

.. image:: material/farbkreis_cmyk_primaer.jpg
	:width: 100px


Wir **drucken** unsere Bilder im CMYK-Farbmodell, dessen Grundlage die **subtraktive** Farbmischung ist. CMYK ist eine Abkürzung, die für die drei Farben Cyan, Magenta, Yellow(Gelb) und dem Schwarzanteil Key steht. Auf Grund dieser 4 verwendeten Farben nennt man dieses Druckverfahren auch 4-Farbdruck. Auf dem nächsten Bild sieht man die Aufteilung des Hauptmotives in die 4 einzelnen Farbkanäle.


.. image:: material/cmyk_kanaele.jpg

Die CMYK-Farbwerte ergeben sich aus den entsprechenden Werten der Einzelfarben von 0% bis 100 %. Ein Wert von 0% bedeutet es wird keine Farbe dieses Kanals gedruckt, 100 % bedeutet eine Volldeckung dieser Farbe. Die Zwischenwerte entstehen durch Rasterung. 


So ergibt sich eine sehr große Palette zur Darstellung von verschiedenen Farben. Das nächste Bild zeigt die Farbentstehung durch das Hinzufügen immer weiterer Farbkanäle, in der Reihenfolge: Cyan | Cyan & Magenta | Cyan, Magenta & Gelb| Cyan, Magenta, Gelb & Schwarz.

.. image:: material/cymk_mischung.jpg


Obwohl man die Sekundär-Farben durch das Hinzufügen einer weiteren Farben bekommt, wird der CMYK-Druck als subtraktive Farbmischung bezeichnet, weil sich die neue Farbe auf weniger Anteile im Lichtspektrum reduziert. Man nimmt also von der Strahlungsenergie etwas weg. Ein weißer Gegenstand reflektiert die meisten Lichtstrahlen des Spektrums, während ein tiefschwarzes Objekt fast gar kein Licht reflektiert. So fügen wir zwar Farbe hinzu, reduzieren oder subtrahieren aber die Helligkeit.



Warme/kalte Farben
******************

.. image:: material/farbkreis.jpg

.. image:: material/warm_kalt_Farben.jpg





Gleichabständig
*******************

Harmonische  und  zugleich  spannende  Farbkombinationen  erzielen  Sie  durch  die Wahl  gleichabständiger  Farben  aus  dem Farbkreis. Sie können aus einem zwölfteiligen Farbkreis harmonische Drei- oder Vierklänge auswählen. Für Kombinationen mit mehr Farben müssen Sie den Farbkreis weiter unterteilen.


.. image:: material/farbdreiklang.jpg

.. image:: material/farbvierklang.jpg






Nebeneinanderliegend
********************

Im Farbkreis nebeneinanderliegende Farben ergeben ein Ton-in-Ton-Farbschema.  Achten  Sie  darauf,  dass  die  Farben  vom Betrachter visuell klar unterscheidbar sind. Nur so erfüllen die Farben den Zweck der Gliederung und Hervorhebung einzelner Designbereiche. Wärmere Farben, Gelb, Orange und Rot, wirken freundlich und vermitteln Nähe. Kältere Farben aus dem blauen Teil des 
Farbkreises  wirken  sachlich  und  distanziert.  Setzen  Sie  die dunkleren  Farben  Ihres  Farbschemas  zur  Hervorhebung  ein. Die helleren unterstützen den Inhalt.


.. image:: material/farbe_nebeneinander.jpg



Sättigung/Helligkeit
********************

Die  Aufmerksamkeit  des  Betrachters  gewinnen  Sie  mit  gesättigten  Farben.  Diese  haben  einen  starken  Signalcharakter, überlagern  damit  aber  häuﬁg  den  eigentlichen  Inhalt.  Setzen Sie deshalb im sachlichen inhaltsbezogenen Design Ihrer Präsentation gesättigte Farben nur sehr sparsam als Akzent oder 
Auszeichnung ein. 


.. image:: material/farbe_saettigung.jpg



Farbkontrast
-------------

Farben wirken nie für sich, sondern immer in Beziehung zu ihrer 
Umgebung. Diese Beziehung der Farben nennt man Farbkontrast. Die drei bedeutendsten Kontraste sind der Komplementärkontrast, der Simultankontrast und der Warm-Kalt-Kontrast. Gerade  für  die  Lesbarkeit  von  Schrift  sind  Farbkontraste  von großer Bedeutung. 



**Komplementärkontrast**

Komplementärfarben  sind  Farbenpaare,  die  in  der  Mischung 
Unbunt ergeben. Sie liegen sich im Farbkreis gegenüber. Zusätzlich zum Komplementärkontrast treten meist noch der Hell-Dunkel-Kontrast  und  ein  Flimmerkontrast  auf.  Das  Farbﬂimmern  können  Sie  verhindern,  wenn  Sie  die  Farben  aufhellen oder abdunkeln.



.. image:: material/farbe_komplementaerkontrast.jpg





**Simultankontrast**

Der Simultankontrast ist der Farbkontrast, der die Farbwirkung einer Farbe in ihrem Umfeld beschreibt. Er ist somit praktisch immer  und  überall  wirksam.  Die  Farben  verändern  durch  die Umgebungsfarbe Ihre subjektive Farbwahrnehmung.

.. image:: material/farbe_simultankontrast.jpg

**Warm-Kalt-Kontrast**

Die Wirkung des Warm-Kalt-Kontrastes geht über das rein visuelle Farbempﬁnden hinaus. Ob eine Farbe als warm oder als kalt empfunden wird, ist u.a. Gegenstand der Farbpsychologie. Die Farben von Gelb bis Rot gelten als warme Farben, die Farben von Grün bis Blau als kalte Farben.


.. image:: material/farbe_warm_kalt_kontrast.jpg



**Farbpalette/Farbschema**

Verwenden Sie Farben sparsam. Der Betrachter kann nur maximal fünf Farben auf einmal erfassen. Verwenden Sie besser drei oder vier Farben. Diese genügen vollkommen, um in Ihrer Präsentation die farblichen Akzente zu setzen. 
Der Einsatz der Farben und damit die Hervorhebung einzelner Bereiche erfolgt nach der Wertigkeit. Wählen Sie für wichtige Teile des Designs, z.B. Überschriften, eine auffallende Farbe. 

Für weniger wichtige Bereiche oder große Flächen nehmen Sie eine hellere meist weniger gesättigte Farbe oder ein neutrales helleres Grau. Große weiße Flächen mit Schrift oder Graﬁk sind in der Projektion für den Leser sehr anstrengend.
Wechseln  Sie  in  Ihrer  Präsentation  nicht  die  Farbe  für  ein Element.  Der  Hintergrund  bleibt  grau,  die  Überschrift  bleibt z.B. rot. Sie sollten die interessanten Inhalte Ihrer Präsentation nicht durch ein sich änderndes Farbenspiel überlagern. Bleiben Sie bei dem gewählten Farbschema. Sie verbessern damit die Orientierung ihres Publikums.





**Farben bewerten und auswählen**

Die Wahl der Farben ist von verschiedenen Faktoren abhängig. Im  Mittelpunkt  steht  natürlich  der  Inhalt  und  die  Zielsetzung Ihrer Präsentation. Daneben sind es aber auch technische Einﬂussgrößen, die die Farbwahl beeinﬂussen. Die folgenden Fragen und die Tabelle zur Erstellung eines Polaritätsproﬁls helfen 
Ihnen bei der Farbwahl:


.. image:: material/kriterien_farbwahl.jpg


Farbschema
-----------

Das Farbklima ist ein wichtiger Teil des Corporate Designs einer 
Firma, einer Organisation oder eines Landes. Die Farbidentität 
und der Wunsch nach Wiedererkennung führen zu einem klar 
deﬁnierten Farbcode, in dem die Zuordnung der einzelnen Farben geregelt ist. Die Deﬁnition der Farben ist im Allgemeinen 
zusammen mit den anderen visuellen Gestaltungsregeln wie 
z.B. der Schrift und dem Layout im Styleguide festgelegt.

**Farbschema der Bundesregierung**

"Das  Farbspektrum  ist  für  die Themenvielfalt  der  Bundesre-
gierung  optimiert  und  wird  einer  breiten  Zielgruppe  gerecht. 
Leuchtende Farben, die den Schwerpunkt des Farbspektrums 
bilden, erzeugen eine positive Stimmung, während wenige ge-
deckte Farben Seriosität und Glaubwürdigkeit vermitteln."

.. image:: material/farbschema_regierung.jpg


Drei Beispiele für die freie themenbezogene Farbauswahl

.. image:: material/farbschema_beispiel.jpg
.. image:: material/farbschema_beispiel_erlaeuterung.jpg


Farbige Schrift
----------------

Wie alle Elemente auf Ihren Folien oder anderen Präsentationsmedien muss auch die Auswahl und Verwendung von Farben für Ihre Texte immer dem Erreichen Ihres Kommunikationsziels dienen.

**Farbe und Kontrast**

Die Lesbarkeit ist der zentrale Punkt bei der Verwendung von Schrift. Sie wird wesentlich durch den optischen Kontrast von Schriftfarbe  und  Hintergrundfrabe  beeinﬂusst.  Die  beste Wirkung  erzielen  Sie  bei  ausreichender  Helligkeitsdifferenz  zwischen  Schriftfarbe  und  Hintergrundfarbe.  Sehr  starke  Helligkeitsunterschiede  oder  Farbkontraste  sind  für  das  Auge  des  Betrachters  sehr  anstrengend. Verwenden  Sie  deshalb  keine reinen Farben, sondern wählen Sie für das Auge angenehme Helligkeits- und Farbunterschiede. Leider gibt hierfür keine absoluten Farbwerte. Testen Sie verschiedene Kombinationen.


.. image:: material/farbe_kontrast.jpg

Die drei linken oberen Felder zeigen jeweils einen Komplementärkontrast  zwischen  Schrift-  und  Hintergrundfarbe.  Bei  den oberen beiden Felder ist die Helligkeit etwa gleich groß. Dadurch entsteht eine Flimmerwirkung, die die Lesbarkeit stark vermindert. In den unteren linken beiden Beispielen ist der Helligkeitsunterschied so groß, dass es Überstrahlungen gibt, die die Lesbarkeit ebenfalls negativ beeinﬂussen. Bei  den  rechten  Beispielen  ist  die  Helligkeit  und  Farbsättigung reduziert. Als Ergebnis zeigen alle vier Felder eine gute Lesbarkeit der Schrift. 



Farbe und Aussage
-----------------


Die Schrift- und Hintergrundfarben sind Teil des Farbklimas Ihrer Präsentation. Neben der Stimmigkeit des Themas der Präsentation und den Farben müssen Sie bei der Zuordnung der Schriftfarben die Wertigkeit der verschiedenen Textebenen be-
achten. Dabei gilt die allgemeine Regel, je wichtiger ein Text, desto optisch auffälliger ist seine Schriftfarbe.

.. image:: material/farbe_aussage.jpg

In der rechten Folie haben die Überschrift und die technische Zeichnung dieselbe Farbe. Dadurch ist die Zuordnung für den Betrachter eindeutig. Auch die Wertigkeit und die Zusammengehörigkeit  der  drei  Unterpunkte  ist  durch  ihre  eigene  Farbe klar erkennbar.



Übungsaufgabe
--------------


.. only:: html

	.. sidebar:: Download
	
		:download:`Farbharmonie <material/UebungFarbharmonie.pdf>`
		
		:download:`Farbkontrast <material/UebungFarbkontrast.pdf>`
		
		:download:`Farbschema <material/UebungFarbschema.pdf>`
		

.. only:: html

	.. admonition:: Datei
	
		:download:`Farbharmonie <material/UebungFarbharmonie.pdf>`
		
		:download:`Farbkontrast <material/UebungFarbkontrast.pdf>`
		
		:download:`Farbschema <material/UebungFarbschema.pdf>`
		
		
		
Die  Zielgruppe  der  Präsentation  sind Teilnehmer  eines  Seminars zur politischen Bildung, Frauen und Männer mittleren Alters mit höherer Schulbildung. 
Das  Folienbeispiel  ist Teil  einer  Präsentation  mit  dem Titel „Deutschland in Zahlen“. Das Zahlenmaterial stammt aus dem Statistischen  Jahrbuch  2012  des  Statistischen  Bundesamtes. 
Die  Folie  zeigt  die  nach  den  Einwohnerzahlen  zehn  größten Städte Deutschlands. Der Fokus liegt dabei auf Stuttgart. Die einzige Variable in den Folienbeispielen ist die Farbe. Das Layout und die Schrift bleiben unverändert.



.. admonition:: Aufgabe

	Beurteilen Sie jeweils die beiden Folien hinsichtlich der Kriterien 
	
	- Farbharmonie

          Die Auswahl  der  Farben  sind Teil  der  Botschaft. Wichtig  sind 
          dabei eine klare farbliche Gliederung und Farben, die in Ihrer Anmutung den Inhalt der Präsentation stützen.
	  
	  .. image:: material/farbe_uebung_farbharmonie_farbkontrast.jpg
	
	
	- Farbkontrast
	
	  Der Blick des Betrachters wird durch den farblichen Kontrast auf die wesentlichen Sachverhalte gelenkt.
	
	  .. image:: material/farbe_uebung_farbkontrast.jpg
	
	
	- Farbschema
	
	  Die Auswahl und Abstufung der Farben der Folienelemente soll in der Wahrnehmung des Betrachters der Wertigkeit entsprechen.   
	
	
	  .. image:: material/farbe_uebung_farbschema.jpg
	
	
        
        

