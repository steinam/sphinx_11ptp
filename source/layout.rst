Layout
=========


Seitenverhältnis
----------------


Gedruckte  Produkte  besitzen  oft  ein  Hochformat,  das  heißt,
dass die Seitenbreite im Vergleich zur Seitenhöhe geringer ist. Im  Unterschied  hierzu  wird  für  Präsentationen  fast  immer ein **Querformat** verwendet, bei dem also die Breite größer ist als die Höhe. Der Grund ist die physiologische Eigenschaft unserer Augen, horizontal einen deutlich größeren Bereich scharf
sehen zu können als vertikal. Querformatige Darstellungen sind für  unsere Augen  deshalb  besser  geeignet.  Sie  kennen  dies aus dem Kino, wo ein extrem breites Format verwendet wird.

Das **Seitenverhältnis** gibt an, wie sich die Breite und Höhe zueinander verhalten. Üblicherweise werden hierfür ganze Zahlen verwendet. Leider gibt es derzeit drei unterschiedliche Seitenverhältnisse – eine Vereinheitlichung ist nicht absehbar.

.. image:: material/seitenverhaeltnis.jpg

.. image:: material/seitenverh_ppt.jpg



Im  Idealfall  kennen  Sie  den  Beamer,  mit  dem  Sie  präsentieren  werden.  Besitzt  dieser  beispielsweise  eine  Auﬂösung von 1280 x 800 Pixel, dann ergibt sich durch Division 1280 : 80 = 16 und 800 : 80 = 10, so dass es sich also um einen 16 :10-Beamer  handelt.  Dieses Verhältnis  stellen  Sie  in  der
Präsentationssoftware ein. Ist  nicht  bekannt,  mit  welchem  Beamer  Sie  präsentieren werden, dann könnten Sie als Kompromiss das mittlere Verhältnis wählen, also 16 :10. Kommt für Ihre Präsentation ein Beamer  mit  demselben Verhältnis  zum  Einsatz,  dann  wird die Präsentation formatfüllend dargestellt (siehe Graﬁk oben Mitte). Wird diese Präsentation auf einem 4 : 3-Beamer gezeigt, dann ist dies nicht schlimm, denn es werden oben und unten schwarze Balken gezeigt, wie Sie dies von Spielﬁlmen im Fernsehen kennen. Im Fall eines 16 : 9-Beamers beﬁnden sich die schwarzen Balken links und rechts.

.. image:: material/seitenverh_vergleich.jpg


Layout
------

.. only:: html

	.. sidebar:: Beispiel Gestaltungsprinzip

		  :download:`Übung <material/Uebung_Gestaltungsprinzipien.pdf>`



Gute Gestaltung ist niemals willkürlich oder zufällig – sie befolgt Regeln und Prinzipien. Ein sehr wichtiges Prinzip ist, dass unser Gehirn nach Struktur und Ordnung sucht. Auch wenn Sie vielleicht beim Anblick Ihres Schreibtischs das Gegenteil denken: Im Chaos fühlen wir uns nicht wohl und ﬁnden uns nicht (gut) zurecht.

In einer guten Präsentation werden Sie die zugrundeliegende Struktur und Ordnung erkennen. Dem Publikum wird hierdurch das Aufnehmen der Informationen erheblich leichter gemacht. Dabei  bedeutet  Struktur  und  Ordnung  nicht,  dass  alle  Folien identisch aussehen sollen – dies wäre langweilig.

.. image:: material/layout_schlecht.jpg


Das Layout dieser Präsentation lässt keinerlei Struktur und Ordnung  erkennen.  Beim  Durchblättern  der  Folien "springt"  der Text auf und ab oder nach links und rechts. Hierdurch entsteht eine Unruhe, die den Betrachter bewusst oder zumindest unbewusst stört.


.. image:: material/raster.jpg

Als  Hilfmittel  zur  Gestaltung  von  Präsentationen  verwenden wir ein Raster, das als Gestaltungsraster bezeichnet wird.
Ein Raster sorgt dafür, dass gleichartige Elemente, z.B. Überschriften oder ein Logo, sich auch immer  exakt  an  derselben  Stelle beﬁnden.  Ein  Raster  sorgt  dafür, dass Folien einheitliche Seitenränder erhalten. Ein Raster lässt aber
andererseits  genügend  Freiraum zur  individuellen  Gestaltung  der Folien.

.. image:: material/raster_gelungen.jpg


Folienmaster
~~~~~~~~~~~~

Das Erstellen eines guten Layouts ist ein langwieriger Prozess und muss für jede Seite eigentlich wiederholt werden. Um dies zu vermeiden erstellt man innerhalb seiner Präsentationssoftware sog. Masterfolien. Layout, Fonts und verwendete Fraben stehen dann für alle Folien zur Verfügung. 



Gute Layouts
~~~~~~~~~~~~

Jede Präsentationssoftware stellt zahllose Vorlagen bereit. Sie sollten jedoch versuchen Ihre eigenen Layouts zu entwickeln bzw bestehende Layouts in Ihrem Sinne umzuwandeln. Denn so wie Sie als  Präsentator  einmalig  und  unverwechselbar  sind,  soll  dies auch für die visuelle Unterstützung Ihrer Präsentation gelten. Machen Sie Ihre Präsentation zu etwas Besonderem!


**Drittel-Regel**

.. image:: material/drittel_regel.jpg

Ein einfaches Prinzip, das auch in der  Fotograﬁe  häuﬁg  angewandt
wird,  ist  die  Drittel-Regel:  Wenn Sie Ihr Layout in horizontaler und
vertikaler  Richtung  dritteln,  so erhalten  Sie  ein  Raster  mit  neun
Feldern. Diese Felder lassen sich nun zur Platzierung von Texten und
Bildern nutzen.



.. image:: material/beispiel_drittel.jpg

Durch die Anwendung der Drittel-Regel erreichen Sie eine asymmetrische Gestaltung Ihrer Folien. Diese wirkt spannungsvoll und dynamisch.

**Goldener Schnitt**

Es gibt eine weitere asymmetrische Aufteilung, die sowohl in der Malerei
(siehe Graﬁk von Leonarda da Vinci) als  auch  in  der  Architektur  immer
wieder  angewandt  wurde:  der  Goldene Schnitt. Dieser teilt eine Seite
ungefähr  im  Verhältnis  3 : 5  (exakt: 1 : 1,618) auf und wird als besonders
ästhetisch und harmonisch empfunden.  Beurteilen  Sie  selbst,  ob  Sie
diese Empﬁndung teilen. (Quelle: http://www.saxoprint.de/blog/der-goldene-schnitt/)


.. image:: material/goldener_Schnitt.jpg


**Symmetrie**

Symmetrie  wird  in  der  Gestaltung oft als statisch und langweilig empfunden. Es kann aber durchaus auch reizvoll  sein,  eine  Folie  exakt  symmetrisch zu gestalten. Dies wird beispielsweise  dadurch  erreicht,  dass ein symmetrisches Motiv exakt mittig platziert wird. Im Beispiel unten wird der Eindruck durch die spiegelbildliche Schrift noch verstärkt

.. image:: material/layout_symmetrie.jpg

**Weißraum**

Haben  Sie  den  Mut  zu  leeren  Flächen! Viele  Präsentationen sind zu voll und überfordern den Betrachter mit einem Zuviel an Information. Bedenken Sie, dass Ihr Publikum den Inhalt Ihrer Folien nicht kennt und innerhalb weniger Sekunden erfassen muss. Dies ist nur möglich, wenn Sie sich beim Layout Flächen
vorsehen, die frei bleiben. Der Fachbegriff hierfür lautet Weißraum. Wenn Sie einmal gezielt darauf achten, werden Sie feststellen, dass Sie auch mit leeren Flächen eine Wirkung erzielen können.

.. image:: material/layout_weissraum.jpg

.. note::

	Links wurde fast die gesamte Folienﬂäche genutzt. Auf einer Projektionswand  mit  mehreren  Quadratmeter  Fläche  können die Informationen nicht auf einen Blick erfasst werden.
	Rechts  wurden  die  Informationen  kompakt  zusammengefasst. Sowohl die Graﬁk als auch die Schrift werden von einem Rand umgeben. Die graue Fläche hinter der Grafik sorgt dafür, dass diese als Einheit wahrgenommen wird. Dies liegt daran, dass  unser  Gehirn  geschlossene  Formen  besser  wahrnimmt als offene, man spricht vom **Gesetz der Geschlossenheit**. Die umgebenden weißen Flächen sorgen schließlich dafür, dass die Grafik optisch im Vordergrund steht.


**Balance**

Stellen Sie sich Ihre Folie als Brett vor, das auf einer Nagelspitze liegt. Es gibt nur einen einzigen Punkt, in dem das Brett exakt waagrecht liegt – in Balance ist. Wenn Sie nun etwas auf eine Hälfte des Bretts legen, dann gerät es aus dem Gleichgewicht und Sie müssen dafür sorgen, dass Sie es durch ein Gegengewicht auf der anderen Hälfte wieder in Balance bringen.

Auch  beim  Graﬁkdesign  schaffen  Sie  Ungleichgewichte,  z. B. durch Anwendung der Drittel-Regel oder des Goldenen Schnitts. Das Gleichgewicht wird durch den Weißraum bzw. Text im dritten Drittel der Folie wieder hergestellt.

.. image:: material/layout_balance.jpg

.. note::

	Das angeschnittene Foto in der linken Abbildung wirkt dominant und „erdrückt“ den Text, der die eigentliche Botschaft der Folie enthält. Rechts stehen Foto und Text in einem harmonischeren Verhältnis.  Der  große Weißraum  um  den Text  bringt diesen noch besser zur Geltung.


**Lese(r)führung**

Sie kennen dies: Wir stehen vor einem Automaten und verstehen dessen Bedienung nicht. Der Grund könnte sein, dass die Bedienoberﬂäche nicht sehr benutzerfreundlich gestaltet wurde. Benutzerfreundlichkeit wird im Fachbegriff als **Usability** bezeichnet.
Mit Präsentationen verhält es sich ganz ähnlich: Wenn es uns nicht gelingt, unsere Zuschauer mitzunehmen und zu führen, dann werden wir mit unserer Präsentation nicht das gewünschte Ergebnis erzielen. Durch die Gestaltung unserer Folien können wir bewusst darauf Einﬂuss nehmen, wie sie betrachtet
und wahrgenommen werden.


.. admonition:: Checkliste

	- Wichtiges oben links – Unwichtiges unten rechts

	  Dies entspricht unserer Lesegewohnheit von links oben nach rechts unten. Verschwenden Sie nicht den wertvollen Platz im linken oberen Eck mit einer ständig wiederkehrenden Information wie z.B. dem Firmenlogo oder dem Thema – dies würde den Betrachter nach wenigen Folien langweilen.

	- Wichtiges groß – Unwichtiges klein

	  Je größer Sie einen Text oder ein Bild gestalten, umso eher wird es als Erstes wahrgenommen. Unwichtige Informationen wie Quellenangaben oder der eigene Name sollten hingegen dezent und unauffällig platziert werden.

	- Gleichartiges gleich gestalten

	  Wiederkehrende Elemente einer Präsentation, z. B. Überschriften, Logo, Datum, Thema, müssen sich immer an derselben Stelle beﬁnden, damit sich der Betrachter daran orientieren kann. Ein Raster hilft dabei, diese Forderung zu erfüllen. Darüber hinaus können Sie in der Präsentationssoftware "Folienmaster" erstellen, die als Vorlage für alle Folien dienen.

	- Bilder vor Text

	  Bilder nehmen wir schneller wahr als Text. Beim Durchblättern einer Zeitschrift schauen wir immer zuerst auf die Bilder, danach auf den Text. Nutzen Sie dies bei Ihrer Präsentation, indem Sie möglichst viele Bilder und Graﬁken verwenden. Wenn Text als Infor-mation wichtig ist, dann verteilen Sie diesen besser nach der Präsentation als Handout.

	- Farbführung

	  Durch die gezielte Platzierung von Farben beeinﬂussen Sie, wohin der Betrachter zuerst schauen wird.


Anschauungsbeispiele
~~~~~~~~~~~~~~~~~~~~


**Layout mit Fuß- oder Kopfbereich**

Wiederkehrende  Informationen (Logo, Seitenzahl, etc. )  dürfen  nicht  zu  auf-
dringlich  platziert  werden,  da  sie  für  den  Betrachter  wenig
„spannend“ sind. Platzieren Sie deshalb diese Informationen
in einem separaten Fuß- oder eventuell auch Kopfbereich. Dieser kann z. B. farbig hinterlegt sein oder durch eine Linie vom
eigentlichen Inhalt abgetrennt werden.

.. image:: material/layout_kopf_fuss.jpg


**Layout mit einheitlichen Rändern**

Überlegen Sie sich gut, ob Sie die oben erwähnten Zusatzinformationen tatsächlich auf jeder Folie benötigen. Ihre Zuschauer sind ja nicht dumm und wissen, wo sie sind und welches Datum wir haben. Das Beispiel zeigt ein Layout ohne einen Fuß- oder Kopfbereich. 
Wichtig ist jedoch auch hier, dass Sie Ihre Texte und Bilder nicht 
willkürlich platzieren, sondern dass sie von einheitlichen Rändern umgehen sind.


.. image:: material/layout_ohne_Rand.jpg

Wie Sie sehen bilden die vier Ränder einen Rahmen um das Bild, vergleichbar mit einem Bilderrahmen an der Wand. Wenn Sie statt Weiß eine dunkle Hintergrundfarbe wählen, dann kommen die Farben noch stärker zur Geltung.



**Layout mit randabfallenden Bildern**


Bilder dürfen, im Unterschied zu Text, bis an den Folienrand platziert werden – man spricht von randabfallenden Bildern. Randabfallende Bilder  können eine sehr schöne Wirkung erzielen.

.. image:: material/layout_randabfallend.jpg


Das  Beispiel  zeigt  ein  Layout,  das  die  Folie  nach  der  Drittel-Regel aufteilt. Bei größerer Textmenge ist eine umgekehrte Lösung denkbar, bei der für das Bild ein Drittel und für den Text zwei Drittel des Platzes vorgesehen werden.


**Layout mit formatfüllenden Bildern**


Die Wirkung eines Bildes wird nochmals gesteigert, wenn es 
die gesamte Folienﬂäche bedeckt. Sie kennen dies aus ganzseitigen Bildanzeigen in Zeitschriften.

.. image:: material/layout_formatfuellend.jpg


Wenn zusätzlich Text benötigt wird, dann muss unbedingt darauf geachtet werden, dass der Kontrast zwischen Schrift und Hintergrund ausreichend groß und der Hintergrund nicht zu unruhig ist. Verzichten Sie andernfalls lieber auf Text.

**Kombination von Layouts**

Bei längeren Präsentationen kann es sinnvoll sein, mehrere Layouts zu kombinieren, um dem Zuschauer Abwechslung zu bieten. So könnte beispielsweise ein Grundlayout wie links oben dargestellt  durch  einzelne  Folien  mit  formatfüllenden  Bildern ergänzt  werden.  Durch  die Verwendung  eines  Rasters,  eine einheitliche Typograﬁe  und  Farbgestaltung  stellen  Sie  sicher, dass der Zuschauer den berühmten roten Faden nicht verliert, 
sondern  Ihre  abwechslungsreiche  Präsentation  mit  Interesse 
verfolgen wird.