import java.util.Arrays;

import processing.core.PApplet;
import processing.core.PShape;

public class EinwohnerWuerzburg extends PApplet {

	PShape wuerzburg;
	PShape[] stadtbezirke;

	String title = "Einwohner in Würzburg";
	String source = "Quelle: Einwohnermelderegister Stadt Würzburg";

	int[] einwohner = { 18539, // Altstadt
			6207, // Dürrbachau
			17880, // Frauenland
			8545, // Grombühl
			10471, // Heidingsfeld
			9851, // Heuchelhof
			10851, // Lengfeld
			4948, // Lindleinsmühle
			4237, // Rottenbauer
			13670, // Sanderau
			4674, // Steinbachtal
			6861, // Versbach
			11804 // Zellerau
	};

	int col = 30;

	@Override
	public void settings() {
		size(500, 600);
	}

	@Override
	public void setup() {
		wuerzburg = loadShape("/home/biornus/Dokumente/Programme/BigDataAnalysis/EinwohnerWuerzburg/Lage_Würzburger_Bezirke_bearbeitet.svg");
		stadtbezirke = wuerzburg.getChildren();

		wuerzburg.disableStyle();
		noLoop();
		colorMode(HSB, 360, 100, 100);
		textAlign(CENTER, CENTER);
	}

	@Override
	public void draw() {
		background(0, 0, 100);
		stroke(col, 20, 99);

		pushMatrix();
		translate(0, 30);
		for (int i = 0; i < stadtbezirke.length; i++) {
			fill(col, map(einwohner[i], getMinValue(), getMaxValue(), 30, 100), 100);
			stadtbezirke[i].scale((float) 0.5);
			shape(stadtbezirke[i], 50, 0);
			println(stadtbezirke[i].getName());
			fill(0, 0, 00);
			float[] coords = new float[2];
			coords = getCoords(stadtbezirke[i].getChild(0));
			textSize(9);
			switch (stadtbezirke[i].getName()) {
			case "Grombühl":
				coords[0] = coords[0] - 50;
				break;
			case "Heidingsfeld":
				coords[0] = coords[0] + 50;
				break;

			default:
				break;
			}
			text(einwohner[i], coords[0] * 0.5F + 50, coords[1] * 0.5F);
			// Namen anzeigen
			// if (stadtbezirke[i].getChild(0).contains(mouseX, mouseY)) {
			// fill(0, 0, 40);
			// textSize(14);
			// text(stadtbezirke[i].getName(), coords[0] * 0, 5F, coords[1] * 0.5F + 10);
			// }
		}
		popMatrix();

		legende(10, 50, getMinValue(), getMaxValue());

		text(source, width / 2, height * 19 / 20);
		textSize(20);
		text(title, width / 2, height * 1 / 20);

	}

	int getMaxValue() {
		int[] values = new int[einwohner.length];
		arrayCopy(einwohner, values);
		Arrays.sort(values);
		return values[values.length - 1];
	}

	int getMinValue() {
		int[] values = new int[einwohner.length];
		arrayCopy(einwohner, values);
		Arrays.sort(values);
		return values[0];
	}

	void legende(int x, int y, int valMin, int valMax) {
		pushMatrix();
		translate(x + 10, y + 10);
		fill(0, 0, 0);
		text("Legende:", 30, 0);
		translate(0, 25);

		for (int i = 0; i < 12; i++) {
			fill(col, map(4000 + 1500 * i, getMinValue(), getMaxValue(), 30, 100), 100);
			rect(0, i * 20 - 3, 20 - 13, 10);
			fill(0, 0, 0);
			text(4000 + 1500 * i, 30, i * 20);
		}
		popMatrix();
	}

	float[] getCoords(PShape thisShape) {
		thisShape.scale(6.5F);
		float xMax = 0;
		float yMax = 0;
		float xMin = 100000;
		float yMin = 100000;
		float[] coords = new float[2];

		for (int i = 0; i < thisShape.getVertexCount(); i++) {
			if (thisShape.getVertexX(i) > xMax) {
				xMax = thisShape.getVertexX(i);
			}
			if (thisShape.getVertexY(i) > yMax) {
				yMax = thisShape.getVertexY(i);
			}
			if (thisShape.getVertexX(i) < xMin) {
				xMin = thisShape.getVertexX(i);
			}
			if (thisShape.getVertexY(i) < yMin) {
				yMin = thisShape.getVertexY(i);
			}
		}
		coords[0] = (xMin + xMax) / 2;
		coords[1] = (yMin + yMax) / 2;
		return coords;
	}

	public static void main(String[] args) {
		PApplet.main("EinwohnerWuerzburg");
	}

}
