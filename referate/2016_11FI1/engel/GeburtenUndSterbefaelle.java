import java.util.Arrays;

import processing.core.PApplet;
import processing.core.PShape;

public class GeburtenUndSterbefaelle extends PApplet {

	PShape wuerzburg;
	PShape[] stadtbezirke;

	String title = "Geburtenüberschuss/-defizit";
	String source = "Quelle: Einwohnermelderegister Stadt Würzburg";

	double[] einwohner = { -65, // Altstadt
			35, // Dürrbachtal
			4, // Frauenland
			26, // Grombühl
			-56, // Heidingsfeld
			15, // Heuchelhof
			19, // Lengfeld
			-56, // Lindleinsmühle
			13, // Rottenbauer
			-134, // Sanderau
			10, // Steinbachtal
			8, // Versbach
			13 // Zellerau
	};

	int feldFarbe = 269;

	int schriftFarbeH = 0;
	int schriftFarbeS = 0;
	int schriftFarbeV = 0;

	@Override
	public void settings() {
		size(500, 600);
	}

	@Override
	public void setup() {
		wuerzburg = loadShape("/home/biornus/Dokumente/Programme/BigDataAnalysis/EinwohnerWuerzburg/Lage_Würzburger_Bezirke_bearbeitet.svg");
		stadtbezirke = wuerzburg.getChildren();

		wuerzburg.disableStyle();
		noLoop();
		colorMode(HSB, 360, 100, 100);
		textAlign(CENTER, CENTER);
	}

	@Override
	public void draw() {
		background(0, 0, 100);
		stroke(feldFarbe, 20, 99);

		pushMatrix();
		translate(0, 30);
		for (int i = 0; i < stadtbezirke.length; i++) {
			fill(feldFarbe, map((float) einwohner[i], (float) getMinValue(), (float) getMaxValue(), 30, 100), 100);
			stadtbezirke[i].scale((float) 0.5);
			shape(stadtbezirke[i], 50, 0);
			println(stadtbezirke[i].getName());
			fill(schriftFarbeH, schriftFarbeS, schriftFarbeV);
			float[] coords = new float[2];
			coords = getCoords(stadtbezirke[i].getChild(0));
			textSize(9);
			switch (stadtbezirke[i].getName()) {
			case "Grombühl":
				coords[0] = coords[0] - 50;
				break;
			case "Heidingsfeld":
				coords[0] = coords[0] + 50;
				break;

			default:
				break;
			}
			text(Double.toString(einwohner[i]), coords[0] * 0.5F + 50, coords[1] * 0.5F);
			// Namen anzeigen
			// if (stadtbezirke[i].getChild(0).contains(mouseX, mouseY)) {
			// fill(0, 0, 40);
			// textSize(14);
			// text(stadtbezirke[i].getName(), coords[0] * 0, 5F, coords[1] * 0.5F + 10);
			// }
		}
		popMatrix();

		legende(10, 50, getMinValue(), getMaxValue());

		text(source, width / 2, height * 19 / 20);
		textSize(20);
		text(title, width / 2, height * 1 / 20);

	}

	double getMaxValue() {
		double[] values = new double[einwohner.length];
		arrayCopy(einwohner, values);
		Arrays.sort(values);
		return values[values.length - 1];
	}

	double getMinValue() {
		double[] values = new double[einwohner.length];
		arrayCopy(einwohner, values);
		Arrays.sort(values);
		return values[0];
	}

	void legende(int x, int y, double d, double e) {
		pushMatrix();
		translate(x + 10, y + 10);
		fill(0, 0, 0);
		text("Legende:", 30, 0);
		translate(0, 25);

		// for (int i = 0; i < 12; i++) {
		// fill(col, map((float) 4000 + 1500 * i, (float) getMinValue(), (float) getMaxValue(), 30, 100), 100);
		// rect(0, i * 20 - 3, 20 - 13, 10);
		// fill(0, 0, 0);
		// text(Double.toString(getMaxValue() - getMinValue() * i), 30, i * 20);
		// }
		// Min und Max Werte für die Beschriftung finden
		int minVal = floor((float) d);
		int maxVal = ceil((float) e);
		float step = (float) (maxVal - minVal) / 5;

		// Farbfelder und Beschriftungen zeichnen
		for (int i = 0; i < 6; i++) {
			fill(feldFarbe, map(minVal + step * i, (float) getMinValue(), (float) getMaxValue(), 30, 100), 100);
			rect(0, i * 20 - 3, 20 - 13, 10);
			fill(schriftFarbeH, schriftFarbeS, schriftFarbeV);
			text((float) round((minVal + step * i) * 100) / 100, 30, i * 20);
		}
		popMatrix();

	}

	float[] getCoords(PShape thisShape) {
		thisShape.scale(6.5F);
		float xMax = 0;
		float yMax = 0;
		float xMin = 100000;
		float yMin = 100000;
		float[] coords = new float[2];

		for (int i = 0; i < thisShape.getVertexCount(); i++) {
			if (thisShape.getVertexX(i) > xMax) {
				xMax = thisShape.getVertexX(i);
			}
			if (thisShape.getVertexY(i) > yMax) {
				yMax = thisShape.getVertexY(i);
			}
			if (thisShape.getVertexX(i) < xMin) {
				xMin = thisShape.getVertexX(i);
			}
			if (thisShape.getVertexY(i) < yMin) {
				yMin = thisShape.getVertexY(i);
			}
		}
		coords[0] = (xMin + xMax) / 2;
		coords[1] = (yMin + yMax) / 2;
		return coords;
	}

	public static void main(String[] args) {
		PApplet.main("GeburtenUndSterbefaelle");
	}

}
