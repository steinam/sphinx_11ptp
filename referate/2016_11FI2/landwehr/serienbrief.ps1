﻿cls

$DatenFirmen = new-object system.collections.arraylist											#Neue Arrayliste
$filepathword = "H:\deutsch\test\final\geschaeftsbrief.doc"										#Dateipfad der Word Datei
$filepathexcel = "H:\deutsch\test\final\Lieferfirmen_Februar_2016.xlsx"							#Dateipfad der Exel Datei
$Platzhalter = new-object system.collections.arraylist											#Neue Arrayliste für die Suchen/Einfügen Funktion
[string]$CurDate = Get-Date -format d															#Aktuelle Datum
$Platzhalter = ("<Name>","<PLZ>","<STRASSE>","<Ort>","<Ansprechpartner>","<Anrede>")			
$DatenLieferungen = @()																			#Anlegen des Primären Datenarrays


$objExcel = New-Object -ComObject Excel.Application												#Neues Objekt Excel
$objExcel.Visible = $true																		#Öffnet Excel "sichtbar"
$WorkBook = $objExcel.Workbooks.Open($filepathexcel)											#Öffnen der Datei
$Sheetlieferfirmen = $WorkBook.sheets.item("Lieferfirmen")										#Anelgen der Kartei Lieferfirmen
$Sheetlieferungen = $WorkBook.sheets.item("Lieferungen")										#Anlegen der Kartei Lieferungen

for($i = 2; $i -le 11; [void]$i++){																#Für jedes Element der Kartei Lieferfirmen wird ein Eintrag ins Array mit allen Daten der Zellen geschrieben
[void]$DatenFirmen.Add(($Sheetlieferfirmen.Cells.Item($i, 2).text ,$Sheetlieferfirmen.Cells.Item($i, 3).text,$Sheetlieferfirmen.Cells.Item($i, 4).text ,$Sheetlieferfirmen.Cells.Item($i, 5).text,$Sheetlieferfirmen.Cells.Item($i, 6).text,$Sheetlieferfirmen.Cells.Item($i, 7).text))
}


for ($z = 0; $z -le 10; [void]$z++){															#Dem Array wird für die Anzahl der Firmen ein "Puffer" hinzugefügt
	$DatenLieferungen += ,@($z+1)																#(verhindern eines Nullpointer)
}
	
for($n =2; $n -le 48; [void]$n++)
	{
	$tempNR = $Sheetlieferungen.Cells.Item($n, 1).text											#Jedes Element der Kartei Lieferungen wird seinem Platz im Array hinzugefügt
	[int]$tempFNR = $Sheetlieferungen.Cells.Item($n, 2).text									#Die Daten werden Temporär abgespeichert
	$tempLNR = $Sheetlieferungen.Cells.Item($n, 3).text
	$tempDa = $Sheetlieferungen.Cells.Item($n, 4).text
	$tempPos = $Sheetlieferungen.Cells.Item($n, 5).text
	$Datenlieferungen[$tempFNR-1] += ,($tempFNR,$tempNR,$tempLNR,$tempPos)						#Hier werden sie im Array eingefügt, die Stelle ist dabei die Lieferantennummer-1
}


for([int]$anzahlFirmen = 0; $anzahlFirmen -lt $Datenlieferungen.length -1; $anzahlFirmen++){	#Für die anzahl der Firmen wird die Schleife ausgeführt
	
	$Word = New-Object -ComObject Word.Application												#Anlegen des Objekts für Word
	$Word.Visible = $True																		#Öffnet Word sichtbar
	$Document = $Word.Documents.open($filepathword)												#Öffnen der Datei
	$Selection = $Word.Selection																#Variable die direkt auf die geöffnete Seite zugreift
	
	$wdFindContinue = 1 																		#Diverse Variablen für die Suchfunktion werden festgelegt
	$MatchCase = $False 																		
	$MatchWholeWord = $False 
	$MatchWildcards = $False 
	$MatchSoundsLike = $False 
	$MatchAllWordForms = $False 
	$Forward = $True 
	$Wrap = $wdFindContinue 
	$Format = $False 
	$wdReplaceNone = 2 
	

	for($i = 0; $i -le 5; $i++){																#Es gibt 5 zu ersetzende worte, also wird dies 5 mal ausgeführt.
		[string]$ReplaceWith = [string]$DatenFirmen[$anzahlFirmen][$i]							#Das zu ersetzende wird festgelegtt
		[string]$FindText = [string]$Platzhalter[$i]											#Der zu ersetzende Text wird festgelegt
		$a = $Selection.Find.Execute($FindText,$MatchCase,$MatchWholeWord,$MatchWildcards,$MatchSoundsLike,$MatchAllWordForms,$Forward,$Wrap,$Format,$ReplaceWith,$wdReplaceNone) 
		
	}
	$a = $Selection.Find.Execute("<aktuellesDatum>",$MatchCase,$MatchWholeWord, 				#trägt das aktuelle Datum ein
	   		$MatchWildcards,$MatchSoundsLike,$MatchAllWordForms,$Forward, 				 
	   		$Wrap,$Format,$CurDate,$wdReplaceNone) 										

	[Void]$word.Selection.GoTo([Microsoft.Office.Interop.Word.WdGoToItem]::wdGoToPage,[Microsoft.Office.Interop.Word.WdGoToDirection]::wdGoToAbsolute,1)
$Document.Bookmarks.Item("\page").range.select()												#
$word.Selection.Collapse([Microsoft.Office.Interop.Word.WdCollapseDirection]::wdCollapseEnd)	#
$word.Selection.InsertNewPage()																	#Hier wird eine neue Seite angefügt
[Void]$word.Selection.GoTo([Microsoft.Office.Interop.Word.WdGoToItem]::wdGoToPage,				#und die Seite wird ausgewählt
                 [Microsoft.Office.Interop.Word.WdGoToDirection]::wdGoToAbsolute, 2)			#
[int]$zeilen = $Datenlieferungen[$anzahlFirmen].length											#Anzahl der Zeilen der Tabelle wird mit anzahl der Positionen der Firma gleich gesetzt

[int]$spalten = 4				 																#Anzahl der Spalten der Tabelle
$Table = $Selection.Tables.add($Selection.Range,$zeilen,$spalten,[Microsoft.Office.Interop.Word.WdDefaultTableBehavior]::wdWord9TableBehavior, [Microsoft.Office.Interop.Word.WdAutoFitBehavior]::wdAutoFitContent)

#$Table.AutoFormat(0)																			#je nach "Zahl" wird das Design der Tabelle geändert
$Table.cell(1,1).range.text = "Lfd Nr"
$Table.cell(1,1).range.Bold = 1																	#Eintragen in die Tabelle
$Table.cell(1,2).range.Bold = 1																	#Schrift wird Fett gedruckt
$Table.cell(1,2).range.text = "Lieferschein Nr"
$Table.cell(1,3).range.Bold = 1
$Table.cell(1,3).range.text = "Datum"
$Table.cell(1,4).range.Bold = 1
$Table.cell(1,4).range.text = "Positionen"

[string]$LfdNR = "0"																			#
[string]$LieNR = "0"																			#Einfügen der Variablen
[string]$DatumL = "0"																			#
[string]$Posi = "0"																				#
[int]$indexTabelle = 2																			#

for($n = 1; $n -lt [int]$Datenlieferungen[$anzahlFirmen].length ; $n++){						#Schleife ausführen = Anzahl der Positionen

	$LfdNR = $Datenlieferungen[$anzahlFirmen][$n][0]											#
	$LieNR = $Datenlieferungen[$anzahlFirmen][$n][1]											#Eintragen der Werte für LfdNr und Ko aus dem richtigen Platz aus 
	$DatumL = $Datenlieferungen[$anzahlFirmen][$n][2]											#dem Array
	$Posi = $Datenlieferungen[$anzahlFirmen][$n][3]												#


	$Table.cell($indexTabelle,1).range.Bold = 0													#
	$Table.cell($indexTabelle,1).range.text = $LfdNr											#
	$Table.cell($indexTabelle,2).range.Bold = 0													#
	$Table.cell($indexTabelle,2).range.text = $LieNR											#Eintragen in die Richtige Tabellen Zeile
	$Table.cell($indexTabelle,3).range.Bold = 0													#Fall Fett Druck gewünscht, die 0 in 1 umwandeln
	$Table.cell($indexTabelle,3).range.text = $DatumL											#
	$Table.cell($indexTabelle,4).range.Bold = 0													#
	$Table.cell($indexTabelle,4).range.text = $Posi												#
	$indexTabelle++																				# Tabellen index wird hochgezählt
}



$Speichern = "H:\deutsch\test\final\save\geschaeftsbrief"+[string]$DatenFirmen[$anzahlFirmen][0]+".doc" #Speicherpfad wird definiert mit Dateiname 
$Document.SaveAs([ref]$Speichern,[ref]$SaveFormat::wdFormatDocument)							#Speichern als Word Dokument

$word.Quit()																					#Word wird beendet um das Original zum bearbeiten erneut öffnen zu können

}



