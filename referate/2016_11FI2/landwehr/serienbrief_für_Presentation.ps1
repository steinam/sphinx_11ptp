﻿cls
										
$filepathword = "H:\deutsch\test\final\geschaeftsbrief.doc"										
$filepathexcel = "H:\deutsch\test\final\Lieferfirmen_Februar_2016.xlsx"

$DatenFirmen = new-object system.collections.arraylist	
$Platzhalter = new-object system.collections.arraylist											
[string]$CurDate = Get-Date -format d															
$Platzhalter = ("<Name>","<PLZ>","<STRASSE>","<Ort>","<Ansprechpartner>","<Anrede>")			
$DatenLieferungen = @()																			

$objExcel = New-Object -ComObject Excel.Application												
$objExcel.Visible = $true																		
$WorkBook = $objExcel.Workbooks.Open($filepathexcel)											
$Sheetlieferfirmen = $WorkBook.sheets.item("Lieferfirmen")										
$Sheetlieferungen = $WorkBook.sheets.item("Lieferungen")										

#_________________________________________________________________________________________



for($i = 2; $i -le 11; [void]$i++){																
[void]$DatenFirmen.Add(($Sheetlieferfirmen.Cells.Item($i, 2).text ,$Sheetlieferfirmen.Cells.Item($i, 3).text,$Sheetlieferfirmen.Cells.Item($i, 4).text ,$Sheetlieferfirmen.Cells.Item($i, 5).text,$Sheetlieferfirmen.Cells.Item($i, 6).text,$Sheetlieferfirmen.Cells.Item($i, 7).text))
}

#_________________________________________________________________________________________



for ($z = 0; $z -le 10; [void]$z++){															
	$DatenLieferungen += ,@($z+1)																
}
	
#_________________________________________________________________________________________	
	
	
for($n =2; $n -le 48; [void]$n++)
	{
	$tempNR = $Sheetlieferungen.Cells.Item($n, 1).text											
	[int]$tempFNR = $Sheetlieferungen.Cells.Item($n, 2).text									
	$tempLNR = $Sheetlieferungen.Cells.Item($n, 3).text
	$tempDa = $Sheetlieferungen.Cells.Item($n, 4).text
	$tempPos = $Sheetlieferungen.Cells.Item($n, 5).text
	$Datenlieferungen[$tempFNR-1] += ,($tempFNR,$tempNR,$tempLNR,$tempPos)						
}


#_________________________________________________________________________________________



for([int]$anzahlFirmen = 0; $anzahlFirmen -lt $Datenlieferungen.length -1; $anzahlFirmen++){	
	
	$Word = New-Object -ComObject Word.Application												
	$Word.Visible = $True																		
	$Document = $Word.Documents.open($filepathword)												
	$Selection = $Word.Selection																
	
	$wdFindContinue = 1 																		
	$MatchCase = $False 																		
	$MatchWholeWord = $False 
	$MatchWildcards = $False 
	$MatchSoundsLike = $False 
	$MatchAllWordForms = $False 
	$Forward = $True 
	$Wrap = $wdFindContinue 
	$Format = $False 
	$wdReplaceNone = 2 
#_________________________________________________________________________________________	

	for($i = 0; $i -le 5; $i++){																
		[string]$ReplaceWith = [string]$DatenFirmen[$anzahlFirmen][$i]							
		[string]$FindText = [string]$Platzhalter[$i]											
		$a = $Selection.Find.Execute($FindText,$MatchCase,$MatchWholeWord,$MatchWildcards,$MatchSoundsLike,$MatchAllWordForms,$Forward,$Wrap,$Format,$ReplaceWith,$wdReplaceNone) 
		
	}
	$a = $Selection.Find.Execute("<aktuellesDatum>",$MatchCase,$MatchWholeWord, 				
	   		$MatchWildcards,$MatchSoundsLike,$MatchAllWordForms,$Forward, 				 
	   		$Wrap,$Format,$CurDate,$wdReplaceNone) 										
#_________________________________________________________________________________________



	[Void]$word.Selection.GoTo([Microsoft.Office.Interop.Word.WdGoToItem]::wdGoToPage,[Microsoft.Office.Interop.Word.WdGoToDirection]::wdGoToAbsolute,1)
$Document.Bookmarks.Item("\page").range.select()												
$word.Selection.Collapse([Microsoft.Office.Interop.Word.WdCollapseDirection]::wdCollapseEnd)	
$word.Selection.InsertNewPage()																	
[Void]$word.Selection.GoTo([Microsoft.Office.Interop.Word.WdGoToItem]::wdGoToPage,				
                 [Microsoft.Office.Interop.Word.WdGoToDirection]::wdGoToAbsolute, 2)			
[int]$zeilen = $Datenlieferungen[$anzahlFirmen].length											

[int]$spalten = 4				 																
$Table = $Selection.Tables.add($Selection.Range,$zeilen,$spalten,[Microsoft.Office.Interop.Word.WdDefaultTableBehavior]::wdWord9TableBehavior, [Microsoft.Office.Interop.Word.WdAutoFitBehavior]::wdAutoFitContent)


#_________________________________________________________________________________________


#$Table.AutoFormat(0)																		
$Table.cell(1,1).range.text = "Lfd Nr"
$Table.cell(1,1).range.Bold = 1																
$Table.cell(1,2).range.Bold = 1																	
$Table.cell(1,2).range.text = "Lieferschein Nr"
$Table.cell(1,3).range.Bold = 1
$Table.cell(1,3).range.text = "Datum"
$Table.cell(1,4).range.Bold = 1
$Table.cell(1,4).range.text = "Positionen"

[string]$LfdNR = "0"																			
[string]$LieNR = "0"																			
[string]$DatumL = "0"																			
[string]$Posi = "0"																				
[int]$indexTabelle = 2																			

for($n = 1; $n -lt [int]$Datenlieferungen[$anzahlFirmen].length ; $n++){						

	$LfdNR = $Datenlieferungen[$anzahlFirmen][$n][0]											
	$LieNR = $Datenlieferungen[$anzahlFirmen][$n][1]											 
	$DatumL = $Datenlieferungen[$anzahlFirmen][$n][2]											
	$Posi = $Datenlieferungen[$anzahlFirmen][$n][3]												


	$Table.cell($indexTabelle,1).range.Bold = 0													
	$Table.cell($indexTabelle,1).range.text = $LfdNr											
	$Table.cell($indexTabelle,2).range.Bold = 0													
	$Table.cell($indexTabelle,2).range.text = $LieNR											
	$Table.cell($indexTabelle,3).range.Bold = 0													
	$Table.cell($indexTabelle,3).range.text = $DatumL											
	$Table.cell($indexTabelle,4).range.Bold = 0													
	$Table.cell($indexTabelle,4).range.text = $Posi												
	$indexTabelle++																				
}



$Speichern = "H:\deutsch\test\final\save\geschaeftsbrief"+[string]$DatenFirmen[$anzahlFirmen][0]+".doc"  
$Document.SaveAs([ref]$Speichern,[ref]$SaveFormat::wdFormatDocument)							

$word.Quit()																					

}



